import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './app'
import { configureStore } from './app/store/configureStore'
import { PersistGate } from 'redux-persist/integration/react'

export const store = configureStore().store

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={configureStore().persistor}>
      <App />
      {/* <Appp/> */}
    </PersistGate>
  </Provider>,
  document.getElementById('root')
);



