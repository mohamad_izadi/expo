import appLocaleData from 'react-intl/locale-data/ar';
import arMessages from '../locales/ar';

const ArLang = {
    messages: {
        ...arMessages
    },
    locale: 'ar',
    data: appLocaleData
};
export default ArLang;