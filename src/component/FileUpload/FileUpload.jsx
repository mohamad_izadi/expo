import React from 'react'
import Link from '@material-ui/icons/Link'
/*************change lang ***************/
import MessageLang , {messageLang} from '../../lang/';
/*************change lang ***************/


const FileUpload =({formatfile,multiple,showfilename,Upload_Func})=>{
    return(
        <label className='fileupload'>
            <input
                style={{display: "none"}}
                type='file'
                accept={formatfile}
                multiple={multiple}
                onChange={Upload_Func} />
                <span className='fileupload_text'>
                    <Link/> <MessageLang id="Oppotunity.item.btn-sendrequest-upload" />
                </span>
                <span>{showfilename==="type" ? <MessageLang id="Oppotunity.item.btn-sendrequest-upload_alertformat" /> : showfilename}</span>
        </label>
)
}
export default FileUpload;

