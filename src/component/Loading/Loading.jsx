import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useLoading, ThreeDots } from '@agney/react-loading';
import { Box, Grid, Typography } from '@material-ui/core';

/*******messageLang */
import MessageLang from '../../lang/'
/*******messageLang */


// https://reactjsexample.com/simple-and-accessible-loading-indicators-with-react/
const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        height: '350px'
    },
    loading: {
        textAlign: 'center'
    },
    text: {
        paddingTop: '10px'
    }
}));

export default function Loading() {
    const { containerProps, indicatorEl } = useLoading({
        loading: true,
        indicator: <ThreeDots width="50" />,
        loaderProps: {
            style: { margin: '0 auto', color: '#475974' }
        },
    });

    const classes = useStyles();
    return (
        <Box>
            <Grid
                className={classes.root}
                container
                direction="row"
                justify="center"
                alignItems="center"
                item lg={12} xs={12}>
                <section {...containerProps} className={classes.loading}>
                    {indicatorEl}
                    {/* <img src={'/static/images/moving_blocks.gif'} alt='' className='loading_image' /> */}
                    <br />
                    <br />
                    <Typography className={classes.text}>
                        <MessageLang id="General.loading.component" />
                    </Typography>
                </section>
            </Grid>
        </Box>
    );
}
