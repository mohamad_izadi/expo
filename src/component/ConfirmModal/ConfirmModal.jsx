import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

export default function ConfirmModal({title,desc,agree,disagree,close,accept}) {
  const [open, setOpen] = React.useState(true);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    close();
  };
  return (
      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
        className='confirmmodal'
      >
        <DialogTitle id="responsive-dialog-title" className='confirmmodal__title'>{title}</DialogTitle>
        {desc ?
            <DialogContent>
                <DialogContentText className='confirmmodal__desc'>
                {desc}
                </DialogContentText>
            </DialogContent>
        :null}
        <DialogActions>
          <Button autoFocus 
            onClick={handleClose} 
            color="primary"
            className='confirmmodal__disagree'>
            {disagree}
          </Button>
          <Button 
            onClick={accept} 
            color="primary" autoFocus
            className='confirmmodal__agree'>
            {agree}
          </Button>
        </DialogActions>
      </Dialog>
  );
}
