import React, { useState, useEffect } from 'react'
import { useTheme } from '@material-ui/core/styles';
import { AppBar, Tabs, Tab, Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <>
                    {children}
                </>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const TabNavs = (props) => {
    const { components, labels, icons, handleChangeIndex, classNames, defualtValue} = props
    const theme = useTheme()
    const [value, setValue] = useState(defualtValue || 0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classNames}>
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="secondary"
                    textColor="primary"
                >
                    {icons ? labels.map((label, index) =>
                        <Tab key={index} label={label} icon={icons[index]} {...a11yProps({ index })} />
                    ) : labels.map((label, index) =>
                        <Tab key={index} label={label} {...a11yProps({ index })} />
                    )}
                </Tabs>
            </AppBar>
            <SwipeableViews
                // axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex(value)}
            >
                {components.map((component, index) =>
                    <TabPanel key={index} value={value} index={index} dir={theme.direction} >
                        {component}
                    </TabPanel>
                )}
            </SwipeableViews>
        </div>
    )
}

export default TabNavs
