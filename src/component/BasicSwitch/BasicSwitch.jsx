import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';

const BasicSwitch = ({ error, label, onBlur, onChange, value, helperText, className, name, type }) => {
    const [checked, setChecked] = React.useState(false);
    return (

        <div className="switch">
            <label className="switch__label">{label}</label>
            <Switch disabled checked inputProps={{ 'aria-label': 'primary checkbox' }} />
        </div>

    )
}

export default BasicSwitch
