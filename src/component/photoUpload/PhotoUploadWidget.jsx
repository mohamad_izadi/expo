import React, {useState, useEffect} from 'react';
import PhotoWidgetDropzone from './PhotoWidgetDropzone';
import PhotoWidgetCropper from './PhotoWidgetCropper';
import { Container, Grid, Button, Box, CircularProgress } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import DeleteIcon from '@material-ui/icons/Delete';
import ConfirmModal from '../ConfirmModal/ConfirmModal'
/*************change lang ***************/
import MessageLang,{messageLang} from '../../lang/';
/*************change lang ***************/

const PhotoUploadWidget = ({ close ,loading, uploadPhoto }) => {
  const [files, setFiles] = useState([]);
  const [image, setImage] = useState(null);
  const [confirm, setconfirm] = useState(false);

  useEffect(() => {
    return () => {
      files.forEach(file => URL.revokeObjectURL(file.preview));
    };
  }, [files]);

  const openconfirm_Func =()=>{
    setconfirm(true)
  }

  const closeconfirm =()=>{
    setconfirm(false)
  }
  const uploadimg =()=>{
    uploadPhoto(image)
    close()
  }

  return (
    <Container className={'avatar'}>
      <Grid container>
        <Grid item lg={12} xs={12}>
          {files.length === 0 ?
            <PhotoWidgetDropzone setFiles={setFiles} clear={close} /> :
            <>
              <PhotoWidgetCropper
                setImage={setImage}
                imagePreview={files[0].preview}
              />

              <Box my={2}
                textAlign='center'>
                <Button
                  variant="contained"
                  className={'bluebtn'}
                  // onClick={() => uploadPhoto(image)}
                  onClick={() => openconfirm_Func()}
                  startIcon={<CloudUploadIcon />}
                >
                  {loading && <CircularProgress color='secondary' size={26} />}
                  {!loading && <MessageLang id="Profile.profileuploader.save"/>}
                </Button>
              </Box> 
              
            </>
          }
        </Grid>
      </Grid>
      {confirm ?
          <ConfirmModal title={messageLang("Profile.profileuploader.confirm")}
          agree={messageLang("Profile.profileuploader.agree")}
          disagree={messageLang("Profile.profileuploader.disagree")} 
          close={closeconfirm} 
          accept={()=>uploadimg()}
          />
      :null}
    </Container>
  );
};

export default PhotoUploadWidget
