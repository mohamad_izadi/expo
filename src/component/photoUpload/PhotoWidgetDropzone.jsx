import React, { useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { Typography, Box, Button } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const dropzoneStyles = {
  border: 'dashed 3px',
  borderColor: '#eee',
  borderRadius: '5px',
  paddingTop: '0',
  textAlign: 'center',
  height: 'auto'
};

const dropzoneActive = {
  borderColor: 'green'
};

const PhotoWidgetDropzone = ({ setFiles , clear }) => {
  const onDrop = useCallback(acceptedFiles => {
    setFiles(
      acceptedFiles.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file)
        })
      )
    );
  }, [setFiles]);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <div
      {...getRootProps()}
      style={
        isDragActive ? { ...dropzoneStyles, ...dropzoneActive } : dropzoneStyles
      }
    >
      <input {...getInputProps()} />
      <CloudUploadIcon className={'upload-icon'} color="action" />
      <Typography className={'description'}>
        <MessageLang id="Profile.profileuploader.title" />
      </Typography>
      <Box my={2}
        flexDirection="column">
        <Button
          variant="contained"
          className={'bluebtn'} 
          startIcon={<CloudUploadIcon />}
        >
          <MessageLang id="Profile.profileuploader.upload" />
        </Button>
        {/* <Button
          variant="contained"
          className={'delete-button'}
          startIcon={<DeleteIcon />}
          onClick={clear}
        >
          <MessageLang id="Profile.profileuploader.delete" />
        </Button> */}
      </Box>
    </div>
  );
};

export default PhotoWidgetDropzone;
