import React, { useRef } from 'react';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';
import { Container, Box, Button } from '@material-ui/core';


const PhotoWidgetCropper = ({ setImage, imagePreview }) => {
  const cropper = useRef(null);

  const cropImage = () => {
    if (
      cropper.current &&
      typeof cropper.current.getCroppedCanvas() === 'undefined'
    ) {
      return;
    }
    cropper &&
      cropper.current &&
      cropper.current.getCroppedCanvas().toBlob((blob) => {
        setImage(blob);
      }, 'image/jpeg');
  };

  return (
    <Container>
      <Cropper
        ref={cropper}
        src={imagePreview}
        style={{ height: '200px', width: '100%', margin: '0 auto' }}
        // Cropper.js options
        aspectRatio={1 / 1}
        preview='.img-preview'
        guides={false}
        viewMode={1}
        dragMode='move'
        scalable={true}
        cropBoxMovable={true}
        cropBoxResizable={true}
        crop={cropImage}
      />

    </Container>
  );
};

export default PhotoWidgetCropper;
