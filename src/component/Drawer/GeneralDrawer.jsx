
import React , {useState,useEffect} from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import {
  Drawer,
  Box,
  IconButton
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';


const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

export default function GeneralDrawer(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

    useEffect(()=>{
      setState({ ...state, [props.from]: props.isOpen });
    },[])

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });

  };

  const CloseHandler=()=>{
    toggleDrawer(props.from, false)
    props.onClose()
  }

  return (
      <Drawer anchor={props.from} open={state[props.from]} className={props.style ? props.style : ''}>
          
          <div
            className={clsx(classes.list, {[classes.fullList]: props.from === 'top' || props.from === 'bottom',})}
            role="presentation"
          >
            <Box
                alignItems="flex-start"
                display="flex"
                flexDirection="column"
                className="Drawer_Header"
                onClick={CloseHandler}
            >
                <div 
                className="modal__header__title"
                style={props.subTitle ? null : {transform: 'translate(0, 50%)'}}
                >{props.title}</div>
                {
                    props.subTitle && <div className="modal__header__sub-title">{props.subTitle}</div>
                }
                
                <IconButton aria-label="close" onClick={CloseHandler} className="modal__header__close-btn">
                    <CloseIcon/>
                </IconButton>
            </Box>
            <div className="Drawer_Body">
              {props.children}
            </div>
          </div>
      </Drawer>
      
  );
}
