import React ,{useRef} from 'react'
import GetApp from '@material-ui/icons/GetApp';
/*************change lang ***************/
import MessageLang from '../lang/';
import { ViewDayOutlined } from '@material-ui/icons';
/*************change lang ***************/

const Downloadbtn = ({type ,src}) => {
  const dl = useRef(null);

  const downloadFnc =()=>{
    fetch(src).then(function(t) {
      return t.blob().then((b)=>{
        var a = document.createElement("a");
        a.href=URL.createObjectURL(b);
        a.setAttribute("download",type)
        a.click();
      })
    })

  }
    return (
      <div className={type === 'Video' ? 'dlbtnbox':'null'}>
        <div className={type === 'Video' ? 'downloadbtn bluebtn' : 'downloadbtn bluebtn pdfbtn' }
          onClick={()=>downloadFnc()}>
          <GetApp/>
          <MessageLang id="stand.getfile" />
        </div>
      </div>
    )
}

export default Downloadbtn;
