import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import { Search, FilterList } from '@material-ui/icons';
import { Hidden, IconButton } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';

import { messageLang } from '../../lang/'


const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    search: {
        position: 'relative',
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },

        marginLeft: 0,
        width: '100%',
        height: 72,
        padding: '14px 21.1px 13px 21px',
        borderBottom: 'solid 1px #eaeaea',
        backgroundColor: '#f8f8f8'
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 2,
        top: 0
    },
    FilterIcon: {
        color: '#9F9F9F',
        right: '23px',
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 2,
        top: 0
    },
    inputRoot: {
        color: 'inherit',
        width: '100%'
    },

    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
}));

const SearchBox = ({ className, HandlerFilter, onChange, ...rest }) => {
    const classes = useStyles();

    return (
        <div className={classes.search} data-tut="reactour__iso_c" style={{ backgroundColor: '#f8f8f8' }}>
            <div className={classes.searchIcon}>
                <Search />
            </div>
            <div className="search-container Search_Style">
                <TextField
                    placeholder={messageLang("form.label.placeholder.search")}
                    classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                    }}
                    className="inputInput"
                    // inputProps={{ 'aria-label': 'search' }}
                    onChange={onChange}
                    InputProps={{
                        endAdornment: (
                            window.innerWidth > 960 ?
                                <InputAdornment position="end">
                                </InputAdornment>
                                :
                                <InputAdornment position="end"
                                    onClick={() => { HandlerFilter() }} >
                                    <FilterList style={{ color: '#9F9F9F' }} />
                                </InputAdornment>

                        ),
                    }}
                />
            </div>
        </div>
    )
}

export default SearchBox
