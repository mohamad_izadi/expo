import React from 'react'
import { TablePagination, } from '@material-ui/core'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import * as locales from '@material-ui/core/locale';

const TableFooter = ({ lang, total, limit, page, ChangePage, ChangeRowsPerPage,className }) => {
    return (
        <ThemeProvider theme={lang === 'fa' ? (outerTheme) => createMuiTheme(outerTheme, locales['faIR']) : null} >
            <TablePagination
                className={`table_footer ${className}`}
                component="div"
                rowsPerPageOptions={[5, 10, 25]}
                colSpan={3}
                count={total}
                rowsPerPage={limit}
                page={page}
                onChangePage={ChangePage}
                // onChangeRowsPerPage={ChangeRowsPerPage}
            />
        </ThemeProvider>
    )
}
export default TableFooter;

/*---------------*/
{/* <FooterPagination 
                lang={lang}
                total={total} 
                limit={companyFilters.limit} 
                page={companyFilters.page}
                ChangePage={handleChangePage}
                /> */}