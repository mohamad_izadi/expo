import React , {useState} from 'react'
import { Avatar, BottomNavigation, CircularProgress } from '@material-ui/core'
import { useSelector } from 'react-redux';
import SendIcon from '@material-ui/icons/Send';

const Sendbox_input = ({Value,Msg,Placeholder,Send,Sendbykey,onchange}) => {
    const { loading } = useSelector(state => state.async)
    const [msg, setMsg] = useState('')
    const { currentUser: user } = useSelector((state) => state.auth);

    return (
        <BottomNavigation value={Value} className={'comments-footer'}>
            <div className="comment-send-box">
                <div className="send-box-emoji">
                    <Avatar src={user && user.avatar==="" || user && user.avatar===null ? '/static/images/user.png' : user.avatar} width={15} height={15} />
                </div>
                <input value={msg}
                    onChange={e=>setMsg(e.target.value)}
                    onKeyDown={e=>Sendbykey(e,msg)}
                    className={'comment-send-input'} placeholder={Placeholder} />

                <div className={'comment-send-icon'} onClick={()=>Send(msg)}>
                    {loading ? <CircularProgress color='secondary' className={'send-loading'} size={26} /> : <SendIcon />}
                </div>
            </div>
        </BottomNavigation>
    )
}
export default Sendbox_input

/*--------------------------------------------*/
{/* <BottomNavigation
        value={value}
        className={'comments-footer'}
    >
        <div className="comment-send-box">
            <div className="send-box-emoji">
                <Avatar src="/static/images/user.png" width={15} height={15} />
            </div>

            <input value={msg}
                onChange={(event) => {setMsg(event.target.value)}}
                onKeyDown={handleKeyDown}
                className={'comment-send-input'} placeholder={messageLang('form.label.placeholder.enterText')} />

            <div className={'comment-send-icon'}
                onClick={() => {createComment_func()}}>
                {loading ? <CircularProgress color='secondary' className={'send-loading'} size={26} /> : <SendIcon />}
            </div>
        </div>
    </BottomNavigation> */}