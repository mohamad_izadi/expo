import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Dialog ,AppBar ,Toolbar ,Slide ,Grid} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import Header_Menu from '../../views/Landing/Header_Menu'
import { tr } from 'date-fns/locale';

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="bottom" ref={ref} {...props} />;
});

 const FullPageModall =({close , children ,style , back}) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
    close()
  };

  useEffect(()=>{
    setOpen(false)
    setOpen(true)
  },[])

  return (
    <div>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition} className={back=='light' ? 'modal lightmode' : 'modal bluemode'}>
          {/* <Grid>
              <Header_Menu style={style} background={true} />
          </Grid> */}
          <Toolbar className='modal__bar'>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
          </Toolbar>
          <Grid className='modal__content'>
              {children}
          </Grid>
      </Dialog>
    </div>
  );
}

export default FullPageModall;