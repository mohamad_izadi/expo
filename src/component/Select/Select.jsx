import React from "react";
import PropTypes from "prop-types";

class Select extends React.Component {
    makeOption = option => (
        <option key={option.id} value={option.id}>
            {option.name}
        </option>
    );

    handleChange = event => {
        this.props.onChange(event.target.id, event.target.value);
        event.preventDefault();
    };

    handleBlur = event => {
        this.props.onBlur(this.props.id, true);
        event.preventDefault();
    };

    render() {
        const { id, name, options, onBlur, label } = this.props;
        return (
            <div>
                <select
                    id={id}
                    label={label}
                    name={name}
                    component="select"
                    className={'select-input'}
                    onChange={this.handleChange}
                    onBlur={onBlur && this.handleBlur}
                    value={this.props.value}
                >
                    {options.map(this.makeOption)}
                </select>
            </div>
        );
    }
}

Select.propTypes = {
    options: PropTypes.arrayOf(String)
};

Select.defaultValue = {
    options: []
};

export default Select;
