import React, { useState } from 'react'
import {
    Avatar,
    Box,
    CardHeader,
    Button,
} from '@material-ui/core';
import agent from '../../app/api/agent'
import Rating from '@material-ui/lab/Rating';
import { addComment } from '../../app/store/actions/productActions'
import { useDispatch } from 'react-redux';

/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const Voting =({msg ,close , product, from , back})=>{
    const [point,setpoint]=useState(0)
    const dispatch = useDispatch()

    const updateRating = (newRating) => {
        setpoint(newRating);
    }
    
    const AddPointing=()=>{
        if(from==='product'){
           dispatch(addComment(product.id, msg ,point))
           back();
        }
        close()
    }

    return(
        <Box className='voting'>
            <Box className='voting__top'>
                <CardHeader
                    className='voting__top__card'
                    avatar={
                        <Avatar alt="" src={product.media.length>0 ? product.media : '/static/images/default.png'} width={30} height={30}/>
                    }
                    title={product.name}
                    subheader={product.category}
                />
                {/* <Rating
                    name="simple-controlled"
                    value={point}
                    className={'comment-rate'}
                    onChange={(event, newValue) => {
                        updateRating(newValue);
                    }}
                /> */}
               <Rating
                    value={point}
                    name="hover-feedback"
                    className={'comment-rate'}
                    onChange={(event, newValue) => updateRating(newValue)}
                />
            </Box>
            <div className="voting__desc">
                {msg}
            </div>
            <Box className='voting__btns'>
                <Button variant="contained" className='bluebtn' onClick={() => AddPointing()}>
                    <MessageLang id="vote.btn-submit" />
                </Button>
                <Button variant="outlined" onClick={() => close()}>
                    <MessageLang id="vote.btn-cancel" />
                </Button>
            </Box>
        </Box>
    )
}
export default Voting