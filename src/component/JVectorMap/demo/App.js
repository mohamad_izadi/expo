import React from 'react';
import { VectorMap } from '../lib';

const handleClick = (event, code) => {
  console.log(`You have click ${code}`)
}

var mapData = {
  AU: 760,
  BR: 550,
  CA: 120,
  DE: 1300,
  FR: 540,
  GB: 690,
  GE: 200,
  IN: 200,
  RO: 600,
  RU: 300,
  US: 2920
};

// const regionStyle = {
//   initial: {
//     fill: "#e4e4e4",
//     "fill-opacity": 0.9,
//     stroke: "none",
//     "stroke-width": 0,
//     "stroke-opacity": 0
//   }
// };

const series = {
  regions: [
    {
      values: mapData,
      scale: ["#AAAAAA", "#444444"],
      normalizeFunction: "polynomial"
    }
  ]
}

const containerStyle = {
  width: "1000px",
  height: "420px"
};

class App extends React.Component {

  changeBg() {
    this.refs.map.setBackgroundColor('red');
  }

  render() {
    return (
      <div>
        <VectorMap map={'expo'}
          backgroundColor="transparent"
          zoomOnScroll={true}
          containerStyle={{
            width: "1000px",
            height: "450px"
          }}
          containerClassName="map"
          regionStyle={{
            initial: {
              // fill: "rgb(2, 178, 255)",
              // "fill-opacity": 0.5,
              stroke: "white",
              "stroke-width": 1.5,
              "stroke-opacity": 1.5
            },
            hover: {
              "fill-opacity": 0.8,
              cursor: "pointer"
            },
            selected: {
              fill: "#2938bc" //color for the clicked country
            },
            selectedHover: {}
          }}
          regionsSelectable={true}
          series={series}
          // ref="map"
          onRegionClick={handleClick}
        />
      </div>
    );
  }
}

export default App;
