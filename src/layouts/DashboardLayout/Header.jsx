
/*------------------------------------------------------------------*/

import React, { useState } from 'react';
import clsx from 'clsx';
import { fade, makeStyles, createStyles, withStyles } from '@material-ui/core/styles';
import {
    Avatar,
    Box,
    Drawer
} from '@material-ui/core'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import MenuList from "@material-ui/core/MenuList";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { KeyboardArrowDown, TramRounded } from '@material-ui/icons';
import { useSelector } from 'react-redux';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { useHistory } from "react-router-dom";
import Popper from '@material-ui/core/Popper';
import { useDispatch } from 'react-redux'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { signoutUser } from '../../app/store/actions/authActions'
import { loadNotificationCounter } from '../../app/store/actions/persistActions';
import agent from '../../app/api/agent'
import ConfirmModal from '../../component/ConfirmModal/ConfirmModal'
/*************change lang ***************/
import MessageLang, { messageLang } from '../../lang/';
/*************change lang ***************/
import Notification from './Notification'


const drawerWidth = 240;

export const useStyles = makeStyles((theme) =>
    createStyles({
        grow: {
            flexGrow: 1,
        },
        appBar: {
            zIndex: theme.zIndex.drawer + 1,
            transition: theme.transitions.create(['width', 'margin'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
            backgroundColor: '#fff',
            borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
            boxShadow: 'none',
            color: '#111',
            [theme.breakpoints.up('md')]: {
                '& .MuiToolbar-regular': {
                    minHeight: 56
                }
            },
        },
        appBarLocation: {
            zIndex: theme.zIndex.drawer + 1,
            transition: theme.transitions.create(['width', 'margin'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
            backgroundColor: '#2d323e',
            borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
            boxShadow: 'none',
            color: '#111',
            [theme.breakpoints.up('md')]: {
                '& .MuiToolbar-regular': {
                    Height: 56
                }
            },
        },
        appBarShift: {
            marginLeft: drawerWidth,
            width: `calc(100% - ${drawerWidth}px)`,
            height: 56,
            transition: theme.transitions.create(['width', 'margin'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            }),
        },

        title: {
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'block',
            },
            fontSize: 18,
            fontWeight: 900,
            color: '#2d323e'
        },
        profile: {
            cursor: 'pointer'
        },
        profileMenu: {
            borderRadius: '0 0 4px 4px'
        },
        popper: {
            zIndex: '9999',
            top: '6px!important'
        },
        profileList: {
            width: '175px',
            textAlign: 'center'
        },
        Img: {
            width: 40,
            height: 40,
            borderRadius: '50%',
            marginRight: 8,
            backgroundColor: '#fff',
            [theme.breakpoints.up('sm')]: {
                marginRight: 16,
            },
        },
        search: {
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginRight: theme.spacing(2),
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(3),
                width: 'auto',
            },
        },
        searchIcon: {
            padding: theme.spacing(0, 2),
            height: '100%',
            position: 'absolute',
            pointerEvents: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        inputRoot: {
            color: 'inherit',
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('md')]: {
                width: '20ch',
            },
        },
        headerstyle: {
            display: 'flex',
            width: '100%',
            justifyContent: 'space-between',
            alignItems: 'center',
            margin: '0px 12px'
        },
        Title: {
            display: 'flex',
            alignItems: 'flex-end',
            // fontSize: '16px',
            fontWeight: 'bold',
            // color:'#2D323E'
        },
        Icon: {
            marginLeft: '5px',
        },
        /*------------------*/
        hide: {
            display: 'none',
        },
        name: {
            paddingLeft: '10px',
            fontSize:'14px',
            fontWeight:300,
        },
        drawer: {
            width: drawerWidth + 60,
            flexShrink: 0,
            whiteSpace: 'nowrap',
        },
        drawerOpen: {
            // width: drawerWidth,
            [theme.breakpoints.up('xs')]: {
                width: '100%',
            },
            [theme.breakpoints.up('sm')]: {
                width: drawerWidth + 60,
            },
            zIndex: 9999,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            }),
            // backgroundColor: theme.palette.primary.main,
            // color: '#fff'
        },
        drawerClose: {
            [theme.breakpoints.up('xs')]: {
                width: 1,
            },
            [theme.breakpoints.up('sm')]: {
                width: 1,
            },
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
            overflowX: 'hidden',
            width: theme.spacing(0) + 1,
            [theme.breakpoints.up('sm')]: {
                width: theme.spacing(7) + 1,
            },
            // backgroundColor: theme.palette.primary.main,
            // color: '#fff'
        },
    }),
);

const StyledListItemIcon = withStyles({
    root: {
        color: '#rgba(0, 0, 0, 0.87)',
        minWidth: '40px'
    },
})(ListItemIcon);

const Header = ({ open, onOpenChange, title, location, headerIcon, notification, route, short_name }) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);

    const [placement, setPlacement] = React.useState();
    const { currentUser: user } = useSelector(state => state.auth)

    const isMenuOpen = Boolean(anchorEl);
    const desk = window.innerWidth > 600 ? true : false

    const [open_MiniMenu, setOpen_MiniMenu] = React.useState(false);
    const anchorRef = React.useRef(null);
    const anchorRef_Mobile = React.useRef(null);

    const [logoutconfirm, setlogoutconfirm] = useState(false)

    const dispatch = useDispatch()
    const history = useHistory()

    const logout_func = () => {
        setlogoutconfirm(true)
    }
    const close = () => {
        setlogoutconfirm(false)
    }

    const PoperProfile = (
        <MenuList
            role="menu"
            className={`${classes.profileList} minimenu_header`}>
            <MenuItem
                onClick={() => history.push(`/${route}/profile/${user.id}`)}
                className={classes.dropdownItem}
            >
                <StyledListItemIcon>
                    <AccountCircle />
                </StyledListItemIcon>
                <ListItemText primary={<MessageLang id="DashboardLayout.header.profile" />} />
            </MenuItem>
            <MenuItem
                // onClick={() => dispatch(signoutUser())}
                onClick={() => logout_func()}
                className={classes.dropdownItem}
            >
                <StyledListItemIcon>
                    <ArrowBackIcon />
                </StyledListItemIcon>
                <ListItemText primary={<MessageLang id="DashboardLayout.header.logout" />} />
            </MenuItem>
        </MenuList>
    )


    const handleToggle = () => {
        setOpen_MiniMenu((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }
        if (anchorRef_Mobile.current && anchorRef_Mobile.current.contains(event.target)) {
            return;
        }
        setOpen_MiniMenu(false);
    };

    const MiniMenu = () => {
        return (
            <Popper
                className={`$classes.popper MiniMenu`}
                open={open_MiniMenu} anchorEl={anchorRef_Mobile.current}
                transition disablePortal>
                {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        id="profile-menu-list-grow"
                        style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                    >
                        <Paper className={classes.profileMenu}>
                            <ClickAwayListener onClickAway={handleClose}>
                                {PoperProfile}
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        )
    }
    const [isOpenNoti, setIsOpenNoti] = useState(false);

    const handelSelectNotification = () => {
        dispatch(loadNotificationCounter)
        agent.Notification.update()
        setIsOpenNoti(val => !val)
    }
    return (
        <div>
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={() => onOpenChange()}
                        edge="start"
                        className={`${clsx({ [classes.hide]: open, })} HeaderIcons`}
                    >
                        {/* <img src="/static/images/logo/logoMini.svg" alt="logo" width="30" height="30" /> */}
                        {short_name === 'wevex' ?
                            <img alt="Wevex" src="/static/images/logo/wevexMini.png" width="30" height="30" /> :
                            <img src="/static/images/logo/logoMini.svg" alt="logo" width="30" height="30" />}
                    </IconButton>
                    <Divider orientation="vertical" flexItem className={"Divider_showstate"} />
                    <div className={classes.grow} />
                    <div className={classes.headerstyle}>
                        <span className={`${classes.Title}`}>
                            {headerIcon}
                            <span className={`${classes.Icon} header_title`}>
                                {title}
                            </span>
                        </span>

                        {user && desk &&
                            <Box
                                data-tut="reactour__iso_b"
                                alignItems="center"
                                display="flex"
                                flexDirection="row"
                                aria-haspopup="true"
                                className={classes.profile}
                                //
                                ref={anchorRef}
                                aria-controls={open_MiniMenu ? 'menu-list-grow' : undefined}
                                // aria-haspopup="true"
                                onClick={handleToggle}
                            >
                                {user.avatar == "" || user.avatar == null ?
                                    <AccountCircle color='action' />
                                    :
                                    <Avatar
                                        className={classes.avatar}
                                        src={user.avatar}
                                    />
                                }

                                <Typography
                                    className={classes.name}
                                    color="textPrimary"
                                >
                                    {user.first_name + ' ' + user.last_name}
                                </Typography>
                                <IconButton>
                                    <KeyboardArrowDown />
                                </IconButton>
                            </Box>
                        }
                        {user && !desk &&
                            <>
                                {user.avatar === "" || user.avatar === null ?
                                    <AccountCircle color='action'
                                        ref={anchorRef_Mobile}
                                        aria-controls={open_MiniMenu ? 'menu-list-grow' : undefined}
                                        aria-haspopup="true"
                                        onClick={handleToggle} />
                                    :
                                    <Avatar
                                        className={classes.avatar}
                                        src={user.avatar}
                                        // onClick={handleMobileMenuOpen}
                                        ref={anchorRef_Mobile}
                                        aria-controls={open_MiniMenu ? 'menu-list-grow' : undefined}
                                        aria-haspopup="true"
                                        onClick={handleToggle}
                                    />
                                }
                            </>
                        }
                    </div>
                    <Divider orientation="vertical" flexItem />
                    <IconButton
                        edge="end"
                        aria-label="account of current user"
                        // aria-controls={menuId}
                        aria-haspopup="true"
                        // style={{ marginLeft: '-22px' }}
                        onClick={handelSelectNotification}
                    >
                        <Badge badgeContent={notification.counter.not_seen_signs} max={20} color="secondary">
                            <NotificationsIcon />
                        </Badge>
                    </IconButton>

                    <Drawer
                        anchor="right"
                        open={isOpenNoti}
                        onClose={() => setIsOpenNoti(false)}
                        className={`${clsx(classes.drawer, classes.root, {
                            [classes.drawerOpen]: isOpenNoti,
                            [classes.drawerClose]: !isOpenNoti,
                        })} dashboard-drawer`}
                        classes={{
                            paper: clsx({
                                [classes.drawerOpen]: isOpenNoti,
                                [classes.drawerClose]: !isOpenNoti,
                            }),
                        }}
                    >
                        <Notification data={notification.list} close={() => setIsOpenNoti(false)} />
                    </Drawer>
                </Toolbar>
            </AppBar>
            {logoutconfirm ?
                <ConfirmModal title={messageLang("DashboardLayout.header.logout.confirm")}
                    agree={messageLang("DashboardLayout.header.logout.agree")}
                    disagree={messageLang("DashboardLayout.header.logout.disagree")}
                    close={close}
                    accept={() => dispatch(signoutUser())}
                />
                : null}
            {open_MiniMenu ? <MiniMenu /> : null}

        </div >
    );
}

export default Header
