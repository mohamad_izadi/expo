import React, { useEffect, useState } from 'react'
import clsx from 'clsx';
import {
    Avatar,
    Box
} from '@material-ui/core'
import { Redirect } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import MoreIcon from '@material-ui/icons/MoreVert';
import NavigateNext from '@material-ui/icons/NavigateNext'
import NavigateBefore from '@material-ui/icons/NavigateBefore'
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button'
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { useStyles } from './Header'
import { useSelector } from 'react-redux'
import { useHistory } from "react-router-dom"
import agent from '../../app/api/agent'
import MessageLang from '../../lang/';

const HeaderExpo = ({ open, onOpenChange, title, short_name, headerIcon }) => {
    const classes = useStyles();
    const { details } = useSelector((state) => state.pavilion);
    let history = useHistory();
    const mobileMenuId = 'primary-search-account-menu-mobile';

    const goNext = () => {
        history.push(`/${short_name}/exhibition/${details.records.next}`)
        history.go(0)
    }

    const goPrev = () => {
        history.push(`/${short_name}/exhibition/${details.records.previous}`)
        history.go(0)
    }


    return (
        <AppBar
            position="fixed"
            className={clsx(classes.appBarLocation, {
                [classes.appBarShift]: open,
            })}
        >
            <Toolbar style={{ minHeight: '56px' }}>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={() => onOpenChange()}
                    edge="start"
                    className={clsx(classes.menuButton, {
                        [classes.hide]: open,
                    })}
                >
                    <MenuIcon />
                </IconButton>
                {details && details.media && details.media.logo && <img alt={''} className={classes.Img} src={details.media.logo} />}
                <div className='expoheader-icon'>
                    {headerIcon}
                </div>

                <Typography className='expo_header_title' variant="h6" noWrap>
                    {details && details.name}
                    {title}
                </Typography>
                <div className='expoheader-nav-buttons'>
                    {
                        details && details.records && details.records.next ?
                            <Button variant="outlined" className="next-btn"
                                onClick={() => goNext()}
                                startIcon={<NavigateNext />}>
                                <MessageLang id="Expo.header.next" />
                            </Button> : <></>
                    }
                    {
                        details && details.records && details.records.previous ?
                            <Button variant="outlined" className="prev-btn"
                                onClick={() => goPrev()}
                                endIcon={<NavigateBefore />}>
                                <MessageLang id="Expo.header.previous" />
                            </Button> : <></>
                    }
                </div>
                <div className={classes.grow} />

                <div className={classes.sectionDesktop}>

                    <Divider orientation="vertical" flexItem />
                    <IconButton
                        edge="end"
                        aria-label="account of current user"
                        aria-controls='primary-search-account-menu'
                        aria-haspopup="true"
                        onClick={() => {
                            agent.Expo.goBack();
                            history.goBack()
                        }}
                        style={{ color: '#fff' }}
                    >
                        <Badge color="#fff">
                            <HighlightOffIcon />
                        </Badge>
                    </IconButton>
                </div>
                {/* <div className={classes.sectionMobile}>
                    <IconButton
                        aria-label="show more"
                        aria-controls={mobileMenuId}
                        aria-haspopup="true"
                        // onClick={handleMobileMenuOpen}
                        color="inherit"
                    >
                        <MoreIcon />
                    </IconButton>
                </div> */}
            </Toolbar>
        </AppBar >
    )
}

export default HeaderExpo
