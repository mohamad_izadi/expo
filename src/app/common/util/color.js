var color1 = ''
var color2 = ''
var color3 = ''

export function getColor(number) {
    switch (number) {
        case 1:
            {
                color1 = '#f8faff';
                color2 = '#edf0f6';
                color3 = '#dbe1ee';
                break;
            }
        case 2:
            {
                color1 = '#081723';
                color2 = '#373f45';
                color3 = '#081723';
                break;
            }
        case 3:
            {
                color1 = '#a45403';
                color2 = '#824201';
                color3 = '#613101';
                break;
            }
        case 4:
            {
                color1 = '#582c00';
                color2 = '#422101';
                color3 = '#301801';
                break;
            }
        case 5:
            {
                color1 = '#951bb9';
                color2 = '#7a0c9b';
                color3 = '#640680';
                break;
            }
        case 6:
            {
                color1 = '#712ea1';
                color2 = '#5b2384';
                color3 = '#47176a';
                break;
            }
        case 7:
            {
                color1 = '#689ad1';
                color2 = '#4e83be';
                color3 = '#3366a0';
                break;
            }
        case 8:
            {
                color1 = '#4b73bf';
                color2 = '#0061a5';
                color3 = '#1a3f85';
                break;
            }
        case 10:
            {
                color1 = '#04b0f0';
                color2 = '#0396cd';
                color3 = '#017ca9';
                break;
            }
        case 9:
            {
                color1 = '#0071c0';
                color2 = '#0061a5';
                color3 = '#004e85';
                break;
            }
        case 11:
            {
                color1 = '#475368';
                color2 = '#374256';
                color3 = '#1f2735';
                break;
            }
        case 12:
            {
                color1 = '#b02318';
                color2 = '#891309';
                color3 = '#720d05';
                break;
            }
        case 13:
            {
                color1 = '#ea3524';
                color2 = '#d51604';
                color3 = '#bb1102';
                break;
            }
        case 14:
            {
                color1 = '#92d051';
                color2 = '#7ab63c';
                color3 = '#669f2c';
                break;
            }
        case 15:
            {
                color1 = '#7fab54';
                color2 = '#6c9644';
                color3 = '#578032';
                break;
            }
        case 16:
            {
                color1 = '#02b250';
                color2 = '#019b45';
                color3 = '#01833b';
                break;
            }
        case 17:
            {
                color1 = '#feff01';
                color2 = '#fff001';
                color3 = '#fff001';
                break;
            }
        case 18:
            {
                color1 = '#f7c042';
                color2 = '#efa602';
                color3 = '#e39101';
                break;
            }
        case 19:
            {
                color1 = '#df8143';
                color2 = '#c8611e';
                color3 = '#b84c04';
                break;
            }
        case 20:
            {
                color1 = '#ff9a17';
                color2 = '#e58201';
                color3 = '#d96c02';
                break;
            }
        case 21:
            {
                color1 = '#ce0f6e';
                color2 = '#b5035b';
                color3 = '#9d004f';
                break;
            }
        default:
            break;
    }
    return {
        color1,
        color2,
        color3
    }
}
