import {
    ADD_PRODUCT,
    FETCH_PRODUCTS,
    SET_SELECTED_PRODUCT,
    SET_SELECTED_PRODUCT_COMMENTS,
    SET_PRODUCT_FILTER,
    CLEAR_PRODUCT_FILTER
} from '../constants/Constants'
import { asyncActionStart, asyncActionFinish, asyncActionError } from './asyncActions'
import agent from '../../api/agent'
import { toastr } from 'react-redux-toastr'
/*************change lang ***************/
import { messageLang } from '../../../lang/';
/*************change lang ***************/

const limit = 10

export function axiosParams(state) {
    const params = new URLSearchParams()
    params.append('category_id', state().product.filter.category)
    params.append('subcategory_id', state().product.filter.subcategory)
    // params.append('company_id', state().product.filter.company)
    params.append('q', state().product.filter.q)
    params.append('page', state().product.filter.page)
    params.append('limit', state().product.filter.limit)
    params.append('alphabetical', state().product.filter.alphabetical)
    params.append('mostVisited', state().product.filter.mostVisited)
    return params
}

export const setProductFilters = (type, value) => {
    return async function (dispatch, getState) {
        dispatch({
            type: SET_PRODUCT_FILTER,
            payload: { filter_type: type, value: value }
        })
    }
}

export const clearFilters = () => {
    return async function (dispatch) {
        dispatch({
            type: CLEAR_PRODUCT_FILTER,
        })
    }
}

export function setProduct(product) {
    return {
        type: SET_SELECTED_PRODUCT,
        payload: product
    }
}

export function setComments(comment) {
    return {
        type: SET_SELECTED_PRODUCT_COMMENTS,
        payload: comment
    }
}

export function addProduct(product) {
    return {
        type: ADD_PRODUCT,
        payload: product
    }
}

export const loadProducts = () => {
    return async function (dispatch, getState) {
        debugger
        dispatch(asyncActionStart());
        try {
            const params = axiosParams(getState)
            const response = await agent.Products.list(params)
            const companies = response.result.data

            const { current_page, last_page, per_page, total } = response.result
            dispatch({
                type: FETCH_PRODUCTS,
                payload: {
                    companies,
                    current_page, last_page, per_page, total
                }
            });
            dispatch(asyncActionFinish());
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

export const loadProduct = (id) => {
    return async function (dispatch, getState) {
        let product = getState().product.loadedProducts.find(p => p.id === id)
        let comments = []
        if (product) {
            comments = product.comments
            dispatch(setProduct(product))
        } else {
            dispatch(asyncActionStart())
            try {
                let response = await agent.Products.details(id)
                product = response
                response = await agent.Products.Comments.list(id)
                comments = response.result.data
                dispatch(setProduct(product))
                dispatch(addProduct(product))
                dispatch(setComments(comments))
                dispatch(asyncActionFinish())
            } catch (error) {
                dispatch(asyncActionError())
            }
        }
    }
}

export const addComment = (id, comment, point) => {
    return async function (dispatch, getState) {
        let comments = []
        try {
            dispatch(asyncActionStart())
            let response = await agent.Products.Comments.add(id, comment, point)
            comments = response.result
            if (response.status) {
                dispatch(setComments(comments))
                dispatch(asyncActionFinish())
                try {
                    let response = await agent.Products.Comments.list(id)
                    console.log(JSON.stringify(response))
                    comments = response.result.data
                    dispatch(setComments(comments))
                    dispatch(asyncActionFinish())
                } catch (error) {
                    dispatch(asyncActionError())
                }
                toastr.success(messageLang("product.comment.sendmsg"))
            }

        } catch (error) {
            dispatch(asyncActionError())
        }
    }
}
