import {
    FETCH_ALL_OPPORTUNITIES,
    SET_OPPORTUNITY_FILTER,
    CLEAR_OPPORTUNITIES_FILTER,
    FETCH_MY_OPPORTUNITIES,
    FETCH_PAVILION_OPPORTUNITIES,
    SET_LOADING
} from '../constants/Constants'
import { asyncActionStart, asyncActionFinish, asyncActionError } from './asyncActions'
import agent from '../../api/agent'
import { toastr } from 'react-redux-toastr';
/*************change lang ***************/
import { messageLang } from '../../../lang/';
/*************change lang ***************/

export function axiosParams(state, type) {
    const params = new URLSearchParams()
    if (type === 'job') {
        params.append('type','job')
        params.append('company_id', state().opportunities.opportuntiesFilters.company_id)
        params.append('expertise_id', state().opportunities.opportuntiesFilters.position_id)
        // params.append('location_id', state().opportunities.opportuntiesFilters.location_id)
        params.append('q', state().opportunities.opportuntiesFilters.q)
    }
    if (type=='investment') {
        params.append('type','investment')
        params.append('company_id', state().opportunities.opportuntiesFilters.company_id)
        params.append('expertise_id', state().opportunities.opportuntiesFilters.position_id)
        // params.append('location_id', state().opportunities.opportuntiesFilters.location_id)
        params.append('q', state().opportunities.opportuntiesFilters.q)
    }

    return params
}

export function fetcOpportunities(type) {       // type= job | investment
    return async function (dispatch, getState) {
        dispatch(asyncActionStart());
        try {
            const params = axiosParams(getState, type)
            const response = await agent.Opportunities.allOpportunities(params)
            const opportunities = response.result.data
            const { current_page, last_page, per_page, total } = response.result
            dispatch({
                type: FETCH_ALL_OPPORTUNITIES,
                payload: {
                    opportunities,
                    current_page, last_page, per_page, total
                }
            });
            dispatch(asyncActionFinish());
            // }
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

export function fetchpavilionopportunities(company_id,search) {
    return async function (dispatch, getState) {
        dispatch(asyncActionStart());
        try {
            // const params = axiosParams(getState, 'job')
            const params = {'pavilion_id' :company_id , 'q': search}
            // const response = await agent.Opportunities.pavilionOpportunities(company_id,'job')
            const response = await agent.Opportunities.pavilionOpportunities(params)
            const opportunities = response.result.data
            const { current_page, last_page, per_page, total } = response.result
            
            dispatch({
                type: FETCH_ALL_OPPORTUNITIES,
                payload: {
                    opportunities,
                    current_page, last_page, per_page, total
                }
            });
            dispatch(asyncActionFinish());
            // }
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

export function fetcMyOpportunities(pavilion_id, type) {
    return async function (dispatch) {
        dispatch(asyncActionStart());
        dispatch({
            type: SET_LOADING, payload: true
        });
        try {
            const response = await agent.Opportunities.MyOpportunities(pavilion_id, type);
            if (response.status) {
                const opportunities = response.result.data
                const { current_page, last_page, per_page, total } = response.result
                dispatch({
                    type: FETCH_MY_OPPORTUNITIES, payload: {
                        opportunities,
                        current_page, last_page, per_page, total
                    }
                });
                dispatch(asyncActionFinish());
            }
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}
export const createOpportunity = (opportunity_id, description,from,pavilion_id,job,file) => {
    return async function (dispatch) {
        try {
            dispatch(asyncActionStart())
            //
            const formData = new FormData()
            formData.append('opportunity_id', opportunity_id)
            formData.append('description', description)
            formData.append('document', file)
            //
            // const result = await agent.Opportunities.create(opportunity_id, description);
            const result = await agent.Opportunities.create(formData);
            
            if (from==='stand'){
                dispatch(fetchpavilionopportunities(pavilion_id))
            }
            else{
                if(job){
                    dispatch(fetcOpportunities('job'));
                    dispatch(fetcMyOpportunities(0,'job'))
                }
                else{
                    dispatch(fetcOpportunities('investment'));
                    dispatch(fetcMyOpportunities(0,'investment'))
                }
            }
            toastr.success('', 'درخواست همکاری شما ارسال شد.')
            if (result.status) {
                //dispatch({ type: ADD_NEW_MESSAGE, payload: result.result });
            }
        } catch (error) {

        }
    }
}


export function fetcPavilionOpportunities(pavilion_id, type) {
    return async function (dispatch) {
        dispatch(asyncActionStart());
        dispatch({
            type: SET_LOADING, payload: true
        });
        try {
            const response = await agent.Opportunities.pavilionOpportunities(pavilion_id, type);
            if (response.status) {
                const opportunities = response.result.data
                const { current_page, last_page, per_page, total } = response.result
                dispatch({
                    type: FETCH_PAVILION_OPPORTUNITIES, payload: {
                        opportunities,
                        current_page, last_page, per_page, total
                    }
                });
                dispatch(asyncActionFinish());
            }
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

//delete
export const deleteOpportunity = (opportunity_id,job) => { //job=true < jobopps  , job==false < invensmentopps
    return async function (dispatch) {
        try {
            dispatch(asyncActionStart())
            const result = await agent.Opportunities.delete(opportunity_id);
            if(job){
                dispatch(fetcOpportunities('job'));
                dispatch(fetcMyOpportunities(0,'job'))
            }
            else{
                dispatch(fetcOpportunities('investment'));
                dispatch(fetcMyOpportunities(0,'investment'))
            }
            toastr.success(messageLang("Oppotunity.item.delete-btn.alert"))
        } catch (error) {

        }
    }
}

//search
export const setopportunityFilters = (type, value) => {
    return async function (dispatch, getState) {
        dispatch({
            type: SET_OPPORTUNITY_FILTER,
            payload: { filter_type: type, value: value }
        })
    }
}

//clear
export const clearFilters = () => {
    return async function (dispatch) {
        dispatch({
            type: CLEAR_OPPORTUNITIES_FILTER,
        })
    }
}
