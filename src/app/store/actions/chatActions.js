import {
    FETCH_CONVERSATION,
    FETCH_MESSAGES,
    ADD_NEW_MESSAGE,
    UPDATE_CONVERSATIONS,
    UPDATE_MESSAGES
} from '../constants/Constants'
import { asyncActionStart, asyncActionFinish, asyncActionError } from './asyncActions'
import agent from '../../api/agent'
import Echo from 'laravel-echo';
import io from 'socket.io-client';
import { configureStore } from '../configureStore'

// const store = configureStore()

// let echo = new Echo({
//     broadcaster: 'socket.io',
//     client: io,
//     host: "https://79.175.176.202:6001",
//     auth: {
//         headers: {
//             Authorization: `Bearer ${store.auth.token}`,
//             Accept: 'application/json',
//         },
//     },
// });

export function fetchConversationsBySocket(data) {
    return async function (dispatch) {
        try {

            dispatch({ type: FETCH_CONVERSATION, payload: data });

        } catch (error) {

        }
    };
}

export function fetchConversations() {
    return async function (dispatch) {

        try {
            const conversations = await agent.Chats.list()
            dispatch({ type: FETCH_CONVERSATION, payload: conversations.result });

        } catch (error) {

        }
    };
}
export const createConversation = (audience_id) => {
    return async function (dispatch) {
        try {

            const result = await agent.Chats.create({ "audience_id": audience_id });

            if (result.status) {


            }
        } catch (error) {

            throw (error)
        }
    }
}

export function fetchMessages(id, loading) {
    return async function (dispatch, getState) {

        let echo = new Echo({
            broadcaster: 'socket.io',
            client: io,
            transports: ['websocket'],
            host: "https://expo.namaline.ir",
            path: "/listener",
            transports: ['websocket'],
            auth: {
                headers: {
                    Authorization: `Bearer ${getState().auth.token}`,
                    Accept: 'application/json',
                },
            },
        });
        echo.leaveChannel(`App.Conversation.${id}`)
        try {

            const channel = echo.private(`App.Conversation.${id}`)

            channel.listen('.message.posted', (data => {
                dispatch({ type: UPDATE_MESSAGES, payload: data.payload });
            }))


            //channel.perform('createMessage', { coversation_id: id, message: 'Hello world!' });

            const messages = await agent.Chats.messagesList(id)
            dispatch({ type: FETCH_MESSAGES, payload: messages.result });

        } catch (error) {

        }
    };
}

export const createMessage = (message, conversation_id) => {
    return async function (dispatch) {
        try {
            const result = await agent.Chats.createMessage(message, conversation_id);
            if (result.status) {


            }
        } catch (error) {
            throw (error)
        }
    }
}

export const updateMessagesList = (message) => {
    return async function (dispatch) {
        dispatch({ type: ADD_NEW_MESSAGE, payload: message })
    }
}
export const updateConversations = (conversations) => {
    return async function (dispatch) {
        dispatch({ type: UPDATE_CONVERSATIONS, payload: conversations })
    }
}