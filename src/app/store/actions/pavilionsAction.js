import {
    FETCH_PAVILON_DETAILS,
    FETCH_PAVILON_MEMBERS,
    FETCH_PAVILON_COMMENTS,
    FETCH_PAVILON_PRODUCTS,
    FETCH_PRODUCTS
} from '../constants/Constants'
import { asyncActionStart, asyncActionFinish, asyncActionError } from './asyncActions'
import agent from '../../api/agent'
import { toastr } from 'react-redux-toastr';
/*************change lang ***************/
import MessageLang, { messageLang } from '../../../lang/';
/*************change lang ***************/

export const loadPavilionsDetails = (id, pavilion_id, section) => {
    return async function (dispatch) {
        dispatch(asyncActionStart());
        try {
            const res = await agent.Pavilions.details(id, pavilion_id, section);
            dispatch({
                type: FETCH_PAVILON_DETAILS,
                payload: { type: section, details: res.result }
            });
            dispatch(asyncActionFinish());
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

export const loadPavilionsMembers = (pavilion_id) => {
    return async function (dispatch) {
        dispatch(asyncActionStart());
        try {
            const members = await agent.Pavilions.members(pavilion_id);
            dispatch({
                type: FETCH_PAVILON_MEMBERS, payload: members.result.data
            });
            dispatch(asyncActionFinish());
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

export const loadPavilionsProducts = (pavilion_id, category_id, subcategory_id, search) => {
    return async function (dispatch) {
        dispatch(asyncActionStart());
        try {
            const products = await agent.Pavilions.products({
                "pavilion_id": pavilion_id,
                "category_id": category_id,
                "subcategory_id": subcategory_id,
                q: search
            });
            dispatch({
                type: FETCH_PAVILON_PRODUCTS,
                payload: products.result.data
            });
            dispatch(asyncActionFinish());
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

export const loadPavilionsComments = (pavilion_id) => {
    return async function (dispatch) {
        dispatch(asyncActionStart());
        try {
            const comments = await agent.Pavilions.comments(pavilion_id);
            dispatch({
                type: FETCH_PAVILON_COMMENTS, payload: comments.result
            });
            dispatch(asyncActionFinish());
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}


export const createComment = (pavilion_id, comment) => {
    return async function (dispatch) {
        try {
            dispatch(asyncActionStart())
            const result = await agent.Pavilions.sendComments(pavilion_id, comment);
            if (result.status) {
                dispatch(loadPavilionsComments(pavilion_id))
                toastr.success('پیام شما با موفقیت ارسال شد')
                dispatch(asyncActionFinish())
            }
        } catch (error) {
            dispatch(asyncActionFinish())
            throw (error)
        }
    }
}