import {
    FETCH_COMPANIES,
    FETCH_MEMBERS,
    SET_COMPANY_FILTER,
    CLEAR_COMPANY_FILTER,
    SET_MEMBER_FILTER,
    FETCH_MEMBER_DETAILS,
    MEMBER_DETAIL_LOADING,
    SETTING_MODAL
} from '../constants/Constants'
import { asyncActionStart, asyncActionFinish, asyncActionError } from './asyncActions'
import agent from '../../api/agent'

const limit = 10

export function axiosParams(state, type) {
    const params = new URLSearchParams()
    if (type === 'company') {
        params.append('category_id', state().stand.companyFilters.category)
        params.append('tag_id', state().stand.companyFilters.subCategory)
        params.append('country_id', state().stand.companyFilters.country)
        params.append('q', state().stand.companyFilters.q)
        params.append('limit', state().stand.companyFilters.limit)
        params.append('page', state().stand.companyFilters.page)
    }
    else if (type === 'member') {
        if (state().stand.memberFilters.company !== 0)
        params.append('company_id', state().stand.memberFilters.company)
        params.append('interest_id', state().stand.memberFilters.interest)
        params.append('expertise_id', state().stand.memberFilters.ability)
        params.append('country_id', state().stand.memberFilters.country)
        params.append('title', state().stand.memberFilters.position)
        params.append('q', state().stand.memberFilters.q)
        params.append('limit', state().stand.memberFilters.limit)
        params.append('page', state().stand.memberFilters.page)
    }
    return params
}

export const setCompanyFilters = (type, value) => {
    return async function (dispatch, getState) {
        dispatch({
            type: SET_COMPANY_FILTER,
            payload: { filter_type: type, value: value }
        })
    }
}

export const setMemberFilters = (type, value) => {
    return async function (dispatch, getState) {
        dispatch({
            type: SET_MEMBER_FILTER,
            payload: { filter_type: type, value: value }
        })
    }
}

export const clearFilters = () => {
    return async function (dispatch) {
        dispatch({
            type: CLEAR_COMPANY_FILTER,
        })
    }
}

export const loadCompanies = () => {
    return async function (dispatch, getState) {
        dispatch(asyncActionStart());
        try {
            const params = axiosParams(getState, 'company')
            const response = await agent.Stands.companyList(params)
            const companies = response.result.data
            const total = response.result.total
            dispatch({
                type: FETCH_COMPANIES, payload: {
                    companies,
                    total
                }
            });
            dispatch(asyncActionFinish());
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

export const loadMembers = () => {
    return async function (dispatch, getState) {
        dispatch(asyncActionStart());
        try {
            const params = axiosParams(getState, 'member')
            const response = await agent.Stands.memberList(params)
            const members = response.result.data
            const total = response.result.total
            dispatch({
                type: FETCH_MEMBERS, payload: {
                    members,
                    total
                }
            })
            dispatch(asyncActionFinish());
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

export const loadMemberDetails = (id) => {
    return async function (dispatch) {
        try {
            const member = await agent.Stands.details(id);
            dispatch({
                type: FETCH_MEMBER_DETAILS,
                payload: member.result
            });
            // dispatch(asyncActionFinish());
        } catch (error) {
            // dispatch(asyncActionError(error));
        }
    };
}


export const SettingModal = (open) => {
    return async function (dispatch) {
        dispatch({
            type: SETTING_MODAL,
            payload: open
        });
    };
}