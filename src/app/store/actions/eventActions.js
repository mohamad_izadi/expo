import {
    SET_LECTURES, SET_WORKSHOPS,
    SET_LECTURE_ALERT, SET_WORKSHOP_ALERT,
    FETCH_EVENT_DATES
} from '../constants/Constants'
import { asyncActionStart, asyncActionFinish, asyncActionError } from './asyncActions'
import agent from '../../api/agent'

// const limit = 10

export function setEventDates(dates) {
    return {
        type: FETCH_EVENT_DATES,
        payload: dates
    }
}

export function setEventAlert(eventId, type) {
    return {
        type: type === 'lecture' ? SET_LECTURE_ALERT : SET_WORKSHOP_ALERT,
        payload: eventId
    }
}

export const setAlert = (eventId, type) => {
    return async function (dispatch, getState) {
        try {
            const response = await agent.Events.alert(eventId);
            if (response.status)
                dispatch(setEventAlert(eventId, type))
        } catch (error) {
            throw error
        }
    }
}

export const getDateRange = () => {
    return async function (dispatch, getState) {
        try {
            const response = await agent.Events.dates();
            if (response.status)
                dispatch(setEventDates({ dates: response.result }))
        } catch (error) {
            console.log(error)
        }
    }
}

export const loadEvents = (date, type) => {
    return async function (dispatch, getState) {
        dispatch(asyncActionStart());
        try {
            const params = new URLSearchParams()
            params.append('date', date)
            params.append('type', type)
            const response = await agent.Events.list(params)
            const events = response.result.data
            const { current_page, last_page, per_page, total } = response.result
            dispatch({
                type: (type === 1 ? SET_LECTURES : SET_WORKSHOPS), payload: {
                    events,
                    current_page, last_page, per_page, total
                }
            });
            dispatch(asyncActionFinish());
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}
