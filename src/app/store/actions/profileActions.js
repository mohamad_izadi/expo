import {
    SET_PROFILE_INFO,
    SET_AVATAR,
    SET_TOKEN
} from '../constants/Constants';
import agent from '../../api/agent'
import { toast } from 'react-toastify';
import { asyncActionStart, asyncActionFinish, asyncActionError } from './asyncActions';
import { toastr } from 'react-redux-toastr';

/*************change lang ***************/
import { messageLang } from '../../../lang/';
/*************change lang ***************/

export function setInfo(user) {
    return {
        type: SET_PROFILE_INFO,
        payload: user
    }
}

export function setAvatar(avatar) {
    return {
        type: SET_AVATAR,
        payload: avatar
    }
}

export function setToken(token) {
    window.localStorage.setItem('jwt', token);
    return {
        type: SET_TOKEN,
        payload: token
    }
}

export const loadProfile = () => {
    return async function (dispatch, getState) {
        dispatch(asyncActionStart());
        try {
            const response = await agent.Profile.info()

            if (response.state) {
                dispatch(setInfo(response.result))
                dispatch(asyncActionFinish())
            }

            dispatch(asyncActionFinish())
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

// edit user main info
export const editInfo = (values, type, offers, interests) => {
    return async function (dispatch, getState) {
        try {
            dispatch(asyncActionStart())
            const { first_name, last_name, company_name, job_position: job_position_id,
                city, email, linkedin, instagram } = values
            let response = null;
            if (type === 'basic') {
                response = await agent.Profile.updateInfo({
                    first_name, last_name,
                    company_name, job_position_id,
                })
                await agent.Profile.updateExtra({
                    linkedin, instagram
                })
            }
            else if (type === 'extra') {
                response = await agent.Profile.updateExtra({
                    city, email,
                    offers, interests,
                    // linkedin, instagram
                })
            }

            if (response.status) {
                dispatch(setInfo(response.result))
                dispatch(asyncActionFinish())
            }
            dispatch(asyncActionFinish())
        } catch (error) {
            dispatch(asyncActionFinish())
            throw error
        }
    }
}

//changepassword
export const ChangePassword = (values) => {
    return async (dispatch, getState) => {
        try {
            dispatch(asyncActionStart())
            // const { first_name, last_name, phone_number, code } = getState().auth.currentUser
            const response = await agent.Profile.changepassword({
                old_password:values.last_password,
                password: values.password,
                password_confirmation: values.password_confirmation
            });
            if (response.status) {
                toastr.success(response.messages)
            }
            dispatch(asyncActionFinish())
        } catch (error) {
            dispatch(asyncActionFinish())
            if (error.data.status===false) {
                toastr.error(error.data.messages)
            }
            throw error
        }
    }
}

export const uploadPhoto = (pic) => {
    return async function (dispatch, getState) {
        try {
            dispatch(asyncActionStart())
            const photo = await agent.Profile.uploadAvatar(pic);
            dispatch(setAvatar(photo.result.url))
            toastr.success(messageLang("Profile.profileuploader.uploadimg.alert"))
            dispatch(asyncActionFinish())
        } catch (error) {
            console.log(error)
            toastr.error(error.data.messages)
            dispatch(asyncActionFinish())
        }
    }
}
