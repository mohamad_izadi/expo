import {
    FETCH_MEETINGS,
    FETCH_TODAY_MEETINGS,
    FETCH_CALENDERS,
    FETCH_SELECTED_MEETING,
    FETCH_RANGE
} from '../constants/Constants'
import { asyncActionStart, asyncActionFinish, asyncActionError } from './asyncActions'
import agent from '../../api/agent'
import { toastr } from 'react-redux-toastr';
import { messageLang } from '../../../lang';

export function fetchMeetings() {
    return async function (dispatch) {
        dispatch(asyncActionStart());
        try {
            const meetings = await agent.Meetings.list();
            if (meetings.status) {
                const result = meetings.result;
                dispatch({ type: FETCH_MEETINGS, payload: result });
                dispatch(asyncActionFinish());
            }
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

export function fetchTodayMeetings() {
    return async function (dispatch) {
        dispatch(asyncActionStart());
        try {
            const meetings = await agent.Meetings.todaylist()
            if (meetings.status) {
                const result = meetings.result;
                dispatch({ type: FETCH_TODAY_MEETINGS, payload: result });
                dispatch(asyncActionFinish());
            }
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}

export const fetchCalenders = (date) => {
    return async function (dispatch, getState) {
        dispatch(asyncActionStart());
        try {
            const params = new URLSearchParams()
            params.append('date', date)
            const response = await agent.Calenders.list(date ? params : '')
            const calenders = response.result;
            dispatch({
                type: FETCH_CALENDERS, payload: calenders
            });
            dispatch(asyncActionFinish());
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}


export function fetchMeetingDetail(id) {
    return async function (dispatch) {
        dispatch(asyncActionStart());
        try {
            const meeting = await agent.Meetings.details(id);
            if (meeting.status) {
                const result = meeting.result;
                dispatch({ type: FETCH_SELECTED_MEETING, payload: result });
                dispatch(asyncActionFinish());
            }
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}
export function changeStaus(status, id) {
    return async function () {
        try {
            const result = await agent.Meetings.changeStatus(status, id)
            // if(result.){
            //     if(status==='accepted'){
            //         toastr.success(messageLang("Meeting.meetingDashboard.meetingInventation.accept_confirm.alert"))
            //     }
            //     else{
            //         toastr.error(messageLang("Meeting.meetingDashboard.meetingInventation.reject_confirm.alert"))
            //     }
            // }
            return result
        } catch (error) {
            return error
        }
    };
}


export function lockDate(datetime) {
    return async function () {

        try {
            const result = await agent.Calenders.lockDate(datetime)
            return result
        } catch (error) {
            throw error
        }
    };
}

export function changeDate(date, id) {
    return async function () {
        try {
            const result = await agent.Meetings.changeTime(date, id)
            if (result.status) {
                toastr.status(messageLang("Meeting.meetingDashboard.meetingInventation.changetime_confirm.alert"))
            }
            return result
        } catch (error) {
            return error
        }
    };
}

export function fetchRanges() {
    return async function (dispatch) {
        dispatch(asyncActionStart());
        try {
            const ranges = await agent.Meetings.ranges();
            if (ranges.status) {
                const result = ranges.result;
                dispatch({ type: FETCH_RANGE, payload: result });
                dispatch(asyncActionFinish());
            }
        } catch (error) {
            dispatch(asyncActionError(error));
        }
    };
}