import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { ThemeProvider } from '@material-ui/core';
import theme from './theme'
import GlobalStyles from './app/styles/styles'
import { create } from 'jss';
import rtl from 'jss-rtl';
import { StylesProvider, jssPreset } from '@material-ui/core/styles';
import { history } from './app/store/configureStore'
// import GlobalFonts from './assets/fonts/font'

import { Route, Switch, Redirect } from 'react-router-dom'
import DashboardLayout from './layouts/DashboardLayout/DashboardLayout'
import MainLayout from './layouts/MainLayout/MainLayout';
import Landing from './views/Landing/Landing'
import Faq from './views/FAQ/'
import Terms from './views/terms/'
import ContactUs from './views/contactUs/'
import AboutUs from './views/aboutUs'

import './styles/App.scss'
import ReduxToastr from 'react-redux-toastr'
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';

import NotFound from './views/common/NotFound';

// Configure JSS




function RTL(props) {
  const { lang } = useSelector((state) => state.lang);
  const jss = create({ plugins: [...jssPreset().plugins, rtl({ enabled: lang === "fa" || lang === "ar" })] });
  return (
    <StylesProvider jss={jss}>
      {props.children}
    </StylesProvider>
  );
}

const App = () => {
  const reduxData = useSelector((state) => ({
    lang: state.lang.lang,
    auth: state.auth.authenticated
  }));
  const { lang, auth } = reduxData;
  useEffect(() => {
    document.body.dir = (lang === "fa" || lang === "ar") ? "rtl" : "ltr";
    console.log()
  }, [lang])

  return (
    <ConnectedRouter history={history}>
      <RTL>
        <ThemeProvider theme={{ ...theme, direction: ((lang === "fa" || lang === "ar") ? 'rtl' : 'ltr') }}>
          <ReduxToastr
            timeOut={7000}
            position='bottom-right'
            transitionIn='fadeIn'
            transitionOut='fadeOut'
            progressBar
            closeOnToastrClick={false}
          />
          <GlobalStyles />
          <Switch>
            <Route exact path="/" component={Landing} />
            <Route exact path="/faq" component={Faq} />
            <Route exact path="/terms" component={Terms} />
            <Route exact path="/contactUs" component={ContactUs} />
            <Route exact path="/aboutUs" component={AboutUs} />
            <Route path="/auth" component={MainLayout} />
            <Route path="/:exhibition" component={DashboardLayout} />
            <Route exact path="/404" component={NotFound} />
            <Route component={NotFound} />
          </Switch>
        </ThemeProvider>
      </RTL>
    </ConnectedRouter>
  );
};

export default React.memo(App);
