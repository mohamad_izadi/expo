import React from 'react'
import Skeleton from '@material-ui/lab/Skeleton';
import { Box, Container, Card } from '@material-ui/core'

const ExhibitionsSkeleton = () => {
    const sample = [1, 2, 3]

    return (
            <Box className='skeletonshowexhibitions'>
                {sample.map((item, index) =>
                    <Card p={1} className='card'>
                        <Skeleton variant="rect" height={230} width='100%' />
                        <Box p={1} mt={2} className='textbox'>
                            <Skeleton  animation="wave" width='100%'/>
                            <Skeleton my={1} animation="wave" width='80%'/>
                            <Box my={2}  width='100%'>
                                <Skeleton animation="wave" />
                                <Skeleton animation="wave" />
                                <Skeleton animation="wave" width='40%' />
                            </Box>
                            <Skeleton animation="wave" width="80%" />
                            <Skeleton className={'btn'} variant="rect" height={36} width='100%' />
                        </Box>
                    </Card>
                )}
            </Box>
    )
}
export default ExhibitionsSkeleton;