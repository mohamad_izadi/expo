import React, { useState, useEffect } from 'react'
import { Box, Typography, Grid, Container, Button } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux'
import agent from '../../app/api/agent'
import ExhibitionsSkeleton from './ExhibitionsSkeleton'
import PavilionCard from '../Landing/PavilionsSlider/PavilionCard'
import { lightpath1, minilightpath } from '../Landing/images'
import { setExhibitions } from '../../app/store/actions/exhibitionAction';
/*************change lang ***************/
import MessageLang from '../../lang';
/*************change lang ***************/

const Showexhibitions = () => {

    const [data, setData] = useState([])

    const dispatch = useDispatch();
    useEffect(() => {
        (async () => {
            const { data } = await agent.Landing.list()
              if (data.status) {
                setData(data.result.data)
                dispatch(setExhibitions(data.result.data))
              }
        })()
    }, [])

    return (
        <div className='exhibitionlist'>
            <div className='pavilion__container'>
                <img src={lightpath1} className='backimg' />
                <Box className='exhibitionlist__content' >
                    <Container maxWidth="xl" >
                        {data && data.length > 0 ?
                            <>
                                <Grid container className='pavilion'>
                                    <Box className='pavilion__top'>
                                        <Box className='pavilion__top__title'>
                                            <Typography ><MessageLang id="Landing.Pavilions.title" /></Typography>
                                            <img src={minilightpath} />
                                        </Box>
                                    </Box>
                                    <Grid className='pavilion__slider'>

                                        <PavilionCard data={data} />
                                        {/* <Box className={'btnbox'}>
                                <Button variant="outlined"
                                    className='bluebtn showmore'
                                    // onClick={() => window.location.replace('/')}
                                    >
                                    <MessageLang id="DashboardLayout.header.events2.btn" />
                                </Button>
                            </Box> */}

                                    </Grid>
                                </Grid>
                            </>
                            :
                            <ExhibitionsSkeleton />
                        }
                    </Container>
                </Box>
            </div>
        </div >
    )
}

export default Showexhibitions;