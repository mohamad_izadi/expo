import React, { useEffect, useState, useRef } from 'react'
import { useSelector } from 'react-redux'
import { AppBar, CircularProgress, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Carousel from 'react-elastic-carousel'
import { useDispatch } from 'react-redux';
import { loadEvents } from '../../../app/store/actions/eventActions'

const useStyles = makeStyles((theme) => ({
    root: {
        height: 80,
        // paddingTop: 15,
        alignItems: 'center',
        justifyContent: 'center',
        '& .bTybGL,.kyJvRZ': {
            backgroundColor: '#fff',
            boxShadow: 'none',
            color: '#9f9f9f'
        },
        '& .bTybGL:hover:enabled': {
            backgroundColor: '#fff',
            boxShadow: 'none',
            color: '#9f9f9f'
        },
        '& .bTybGL:focus:enabled': {
            backgroundColor: '#fff',
            boxShadow: 'none',
            color: '#9f9f9f'
        },
        '& .rec-carousel-item': {
            color: '#9f9f9f'
        }
    },
    carousel: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        // height: '50px'
        width: '294px'
    },
    item: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '50px',
        width: '50px',
        backgroundColor: '#fff',
        border: '1px solid #eee',
        margin: '15px',
        fontSize: '17px',
        cursor: 'pointer'
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
}))

const EventHeader = ({ setLoading }) => {
    const classes = useStyles();
    const { dates } = useSelector((state) => state.persist)
    const [selected, setSelected] = useState()
    const dispatch = useDispatch()

    useEffect(() => {
        if (dates !== undefined && dates.length > 0) {
            setSelected(dates.find(date => date.today)?.id || 1)
        }
    }, [dates]);

    const handleSelect = async (index) => {
        setLoading(true)
        const selected_date = dates.find(date => date.id === index).date
        setSelected(index)
        await dispatch(loadEvents(selected_date, 1))
        await dispatch(loadEvents(selected_date, 2))
        setLoading(false)
    }
    if (selected === undefined) return <CircularProgress />

    return (
        <AppBar position="static" color="default" className={classes.root}>
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                item lg={4} xs={12}>
                <Carousel
                    onChange={(event) => handleSelect(event.index + 3)}
                    // breakPoints={breakPoints}
                    itemsToShow={5}
                    pagination={false}
                    // focusOnSelect={true}
                    // enableTilt={true}
                    // itemPadding={[0, 10, 0, 10]}
                    transitionMs={1}
                    initialActiveIndex={selected - 3}
                    className={`${classes.carousel} calendar`}
                >
                    {dates.map(date => (
                        <div key={date.id} date={date.date}
                            // onClick={() => handleSelect(date.id)} 
                            className={selected === date.id ? 'events-header-item events-header-item__selected' : 'events-header-item'}>
                            <div className={selected === date.id ? 'events-header-item__id events-header-item__id-selected' : "events-header-item__id"}>
                                {date.format.day}
                            </div>
                            <div className={selected === date.id ? 'events-header-item__title events-header-item__id-selected' : "events-header-item__title"}>
                                {date.format.month}
                            </div>
                        </div>
                    ))}
                </Carousel>
            </Grid>
        </AppBar >

    )
}

export default EventHeader
