import React, { useState } from 'react'
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import {
    Table, TableBody, TableCell, TableContainer, CircularProgress, Container, Box,
    TableHead, TableRow, Paper, Avatar, CardHeader, Typography, IconButton, Button
} from '@material-ui/core';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive'
import { setAlert } from '../../../app/store/actions/eventActions';
import TodayIcon from '@material-ui/icons/Today';

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    }, subject: {
        fontSize: '14px',
        color: '#rgba(0, 0, 0, 0.87)',
        paddingBottom: '5px'
    }, description: {
        fontSize: '17px',
        color: 'secondary'
    }, statsItem: {
        alignItems: 'center',
        display: 'flex',
        padding: '7px',
        borderRadius: '50%',
        width: 'fit-content',
        margin: '0 auto',
        cursor: 'pointer',
    },
    statsItemActive: {
        backgroundColor: '#57C0ED',
        '&:hover': {
            backgroundColor: '#57C0ED'
        },
    },
    statsItemDisabled: {
        backgroundColor: '#eee',
        '&:hover': {
            backgroundColor: '#eee'
        },
    },
    statsIcon: {
        fontSize: '25px',
    },
    image: {
        padding: '30px 0 20px',
        display: 'inline-block',
        maxWidth: '60%',
    },
});

const WorkshopList = ({workshops}) => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const [updating, setUpdating] = useState({ isUpdating: false, target: null });

    const mobileSize = window.innerWidth > 600 ? false : true;
    const exhibition = useSelector(state => state.exhibition.data);

    async function handleChangeAlert(eventId, target) {
        setUpdating({ isUpdating: true, target: target });
        try {
            await dispatch(setAlert(eventId, 'workshop'))
        } catch (error) {
            console.log(error.message);
        } finally {
            setUpdating({ isUpdating: false, target: null });
        }
    }

    return (
        <div className='event'>
            {
                workshops.length === 0 ? (
                    <div className='exhebition-emptypages tabcontent-event'>
                        <Box>
                            <img src="/static/images/backgrounds/SVG/lectures.svg" />
                            <Typography >
                                <MessageLang id="events.LectureList.nothing.WorkshopList" />
                            </Typography>
                        </Box>
                    </div>
                ) :
                    <TableContainer component={Paper} className="table_desgin eventtable">

                        <Table className={``} aria-label="simple table ">
                            <TableHead>
                                {mobileSize ?
                                    <TableRow>
                                        <TableCell align="center" width='20%'><MessageLang id="events.LectureList.table.clock" /></TableCell>
                                        <TableCell align="center" width='30%' ><MessageLang id="events.LectureList.table.teacher" /></TableCell>
                                        <TableCell align="center" width='50%'><MessageLang id="events.LectureList.table.topic" /></TableCell>
                                    </TableRow>
                                    :
                                    <TableRow>
                                        <TableCell align="center" width='15%'><MessageLang id="events.LectureList.table.clock" /></TableCell>
                                        <TableCell align="left" width='30%' ><MessageLang id="events.LectureList.table.teacher" /></TableCell>
                                        <TableCell align="left" width='55%'><MessageLang id="events.LectureList.table.topic" /></TableCell>
                                        <TableCell align="center" width='100%'></TableCell>
                                        <TableCell align="center" width='10%'></TableCell>
                                    </TableRow>
                                }
                            </TableHead>
                            <TableBody>
                                {mobileSize ?
                                    workshops.map((event) => (
                                        <TableRow key={event.id}>
                                            <TableCell align="center" valign="top" className="eventtable__time">
                                                {event.start_time} - {event.end_time}
                                                {updating.isUpdating && updating.target === String(event.id)
                                                    ? <CircularProgress color='secondary' size={26} />
                                                    : <IconButton
                                                        className={clsx(classes.statsItem, {
                                                            [classes.statsItemActive]: event.alert,
                                                            [classes.statsItemDisabled]: !event.alert,
                                                        })}
                                                        name={event.id}
                                                        onClick={(e) => handleChangeAlert(event.id, e.target.name)}>
                                                        <NotificationsActiveIcon
                                                            name={event.id}
                                                            className={classes.statsIcon}
                                                            style={event.alert ? { color: '#fff' } : { color: '#9f9f9f' }}
                                                        />
                                                    </IconButton>
                                                }
                                            </TableCell>
                                            <TableCell component="th" scope="row">
                                                <CardHeader
                                                    className="eventtable__lecturer"
                                                    avatar={
                                                        <Avatar alt="" src="/static/images/user.png" width={30} height={30} />
                                                    }
                                                    title={event.lecturer}
                                                    subheader={event.lecturer_description}
                                                />
                                            </TableCell>

                                            <TableCell align="left">
                                                <CardHeader
                                                    className="eventtable__lecturer"
                                                    title={event.subject}
                                                    subheader={event.description.replace(/(<([^>]+)>)/gi, "")}
                                                />
                                            </TableCell>
                                        </TableRow>
                                    ))
                                    :
                                    workshops.map((event) => (
                                        <TableRow key={event.id}>
                                            <TableCell align="center" valign="top" className="eventtable__time">{event.start_time} - {event.end_time}</TableCell>
                                            <TableCell component="th" scope="row">
                                                <CardHeader
                                                    className="eventtable__lecturer"
                                                    avatar={
                                                        <Avatar alt="" src="/static/images/user.png" width={30} height={30} />
                                                    }
                                                    title={event.lecturer}
                                                    subheader={event.lecturer_description}
                                                />
                                            </TableCell>

                                            <TableCell align="left">
                                                 <CardHeader
                                                    className="eventtable__lecturer"
                                                    title={event.subject}
                                                    subheader={event.description.replace(/(<([^>]+)>)/gi, "")}
                                                />
                                            </TableCell>

                                            <TableCell align="center">
                                                <Button variant="contained" size="small" disabled={event?.onPerforming} color="secondary" fullWidth disableElevation href={`/${exhibition.short_name}/conference`}>
                                                    <Typography style={{ color: "#fff" }}>مشاهده</Typography>
                                                </Button>
                                            </TableCell>

                                            <TableCell align="center">
                                                {(updating.isUpdating && updating.target === String(event.id))
                                                    ? <CircularProgress color='secondary' size={26} />
                                                    : <IconButton
                                                        className={clsx(classes.statsItem, {
                                                            [classes.statsItemActive]: event.alert,
                                                            [classes.statsItemDisabled]: !event.alert,
                                                        })}
                                                        name={event.id}
                                                        onClick={(e) => handleChangeAlert(event.id, e.target.name)}>
                                                        <NotificationsActiveIcon
                                                            name={event.id}
                                                            className={classes.statsIcon}
                                                            style={event.alert ? { color: '#fff' } : { color: '#9f9f9f' }}
                                                        />
                                                    </IconButton>}
                                            </TableCell>
                                        </TableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
            }

        </div>
    );
}

export default WorkshopList

