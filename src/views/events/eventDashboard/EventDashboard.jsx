import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import EventListSkeleton from './EventListSkeleton'
import { makeStyles } from '@material-ui/core/styles';
import { loadEvents } from '../../../app/store/actions/eventActions'
import EventHeader from './EventHeader';
import TabNavs from '../../../component/Tabs/TabNavs';
import LectureList from './LectureList';
import WorkshopList from './WorkshopList';

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/


const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTableCell-root': {
            fontFamily: 'IRANSansFa',
            // padding: '15px',
            fontSize: '14px'
        },
        '& .MuiTableCell-head': {
            padding: '15px',
            fontSize: '14px',
            fontWeight: 'normal'
        },
        '& .MuiAvatar-root': {
            marginLeft: '10px'
        },
        '& .MuiCardHeader-root': {
            padding: '2px',
        },
        '& .MuiAppBar-root': {
            backgroundColor: '#fff',
        },
    }
}))

const EventDashboard = ({ location }) => {
    const classes = useStyles()
    const dispatch = useDispatch();
    const [value, setValue] = useState(0);
    const [loading, setLoading] = useState(true)

    const handleChange = (newValue) => {
        setValue(newValue);
    };

    const { dates } = useSelector((state) => state.persist)
    // const { loading } = useSelector((state) => state.async);

    const { lectures, workshops } = useSelector((state) => state.event);

    useEffect(() => {
        async function loadContents() {
            const today = dates.find(date => date.today)?.date || 1
            dispatch(loadEvents(today, 1))
            await dispatch(loadEvents(today, 2))
            setLoading(false)
        }
        if (dates.length > 0) {
            loadContents();
        }
    }, [dispatch, dates]);
    // console.log(lectures);
    return (
        <div className={classes.root}>
            <EventHeader setLoading={setLoading} />
            <TabNavs
                components={
                    [loading ? <EventListSkeleton /> :
                        <LectureList lectures={lectures} />,

                    loading ? <EventListSkeleton /> :
                        <WorkshopList workshops={workshops} />]}

                labels={[
                    <MessageLang id="events.EventDashboard.tabnav.lectures" />
                    ,
                    <MessageLang id="events.EventDashboard.tabnav.workshops" />
                ]}
                handleChangeIndex={handleChange}
                loading={loading}
                classNames={'tab_header fftabs'} />
        </div >
    )
}

export default EventDashboard
