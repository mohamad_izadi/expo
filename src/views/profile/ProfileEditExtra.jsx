import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import {
    Box,
    Button,
    CircularProgress,
    Typography,
    Grid
} from '@material-ui/core';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import { Modal, CheckInput, TextInput } from '../../component';
import { loadAbilities, loadInterests, loadOffers } from '../../app/store/actions/persistActions';
import { loadProfile } from '../../app/store/actions/profileActions'
import { editInfo } from '../../app/store/actions/profileActions';
import GeneralDrawer from '../../component/Drawer/GeneralDrawer.jsx'

/*************change lang ***************/
import MessageLang, { messageLang } from '../../lang/';
import { toastr } from 'react-redux-toastr';
/*************change lang ***************/

const ProfileEditExtra = ({ user, open, setOpen }) => {
    const { loading } = useSelector((state) => state.async);
    const { offers, interests } = useSelector((state) => state.persist)
    const [selectedOffers, setSelectedOffers] = useState([])
    const [selectedInterests, setSelectedInterests] = useState([])

    const ScreenWidth = window.innerWidth
    const Responsive_Size = 600;

    const dispatch = useDispatch();

    const handleOfferChange = (event) => {
        let newArray = [...selectedOffers, parseInt(event.target.id)];
        if (selectedOffers.includes(parseInt(event.target.id))) {
            newArray = newArray.filter(offer => offer !== parseInt(event.target.id));
        }
        setSelectedOffers([...newArray]);
    };

    const handleInterestChange = (event) => {
        let newValue = parseInt(event.target.id.replace('ist', ''))
        let newArray = [...selectedInterests, newValue];
        if (selectedInterests.includes(newValue)) {
            newArray = newArray.filter(interest => interest !== newValue);
        }
        setSelectedInterests([...newArray]);
    };

    useEffect(() => {
        dispatch(loadOffers())
        dispatch(loadAbilities())
        dispatch(loadInterests())

        setSelectedOffers(user && [...user.offer_keys])
        setSelectedInterests(user && [...user.interest_keys])
    }, [dispatch, user])


    const EditContent =
        <Formik
            initialValues={
                {
                    city: user && user.city && user.city.name,
                    email: user && user.email,
                }}
            validationSchema={Yup.object({
                email: Yup.string().required(<MessageLang id="form.verify.register.email"/>).email(<MessageLang id="form.verify.register.email-valid"/>),
            })}
            onSubmit={async (values, { setSubmitting, setErrors }) => {
                try {
                    await dispatch(editInfo(values, "extra", selectedOffers, selectedInterests))
                    setSubmitting(false);
                    setOpen(false)
                    dispatch(loadProfile())
                    toastr.success(messageLang("Profile.profileEditExtra.addalert"))
                } catch (error) {
                    setErrors({ auth: error });
                    setSubmitting(false);
                }
                setOpen(false)
            }}
        >
            {({ submitForm,
                errors,
                handleBlur,
                handleChange,
                handleSubmit,
                isSubmitting,
                touched,
                values, isValid, dirty }) => (
                <Form className='ui form Profile_Detalis_Content'>
                    <Typography className="Profile_Detalis_Content_Title">
                        <MessageLang id="Profile.profileDetails.profileInfo.general.offer.h3" />
                    </Typography>
                    <Grid container className='profile_selectbox'>
                        {offers && offers.map(offer =>
                            <Grid key={offer.id} item xs={4} className="Profile_Detalis_Content_Items">
                                <CheckInput
                                    id={offer.id.toString()}
                                    className="custom-control-input"
                                    variant="outlined"
                                    label={offer.name}
                                    value={offer.id}
                                    name={offer.name}
                                    onChange={handleOfferChange}
                                    defaultChecked={user.offer_keys.includes(offer.id)}
                                />
                            </Grid>
                        )}
                    </Grid>
                    <Typography className="Profile_Detalis_Content_Title">
                        <MessageLang id="Profile.profileDetails.profileInfo.general.need.h3" />
                    </Typography>
                    <Grid container >
                        {interests && interests.map(interest =>
                            <Grid key={interest.id} item xs={4} className="Profile_Detalis_Content_Items">
                                <CheckInput
                                    id={"ist" + interest.id.toString()}
                                    className="custom-control-input"
                                    variant="outlined"
                                    label={interest.name}
                                    value={interest.id}
                                    name={interest.name}
                                    onChange={handleInterestChange}
                                    defaultChecked={user.interest_keys.includes(interest.id)}
                                />
                            </Grid>)}
                    </Grid>

                    <TextInput
                        error={Boolean(touched.city && errors.city)}
                        helperText={touched.city && errors.city}
                        label={<MessageLang id="Profile.profileEditExtra.textInput.lable1" />}
                        margin="normal"
                        name="city"
                        onBlur={handleBlur}
                        onChange={handleChange('city')}
                        type="text"
                        value={values.city}
                    />
                    <TextInput
                        error={Boolean(touched.email && errors.email)}
                        helperText={touched.email && errors.email}
                        label={<MessageLang id="Profile.profileEditExtra.textInput.lable2" />}
                        margin="normal"
                        name="email"
                        onBlur={handleBlur}
                        onChange={handleChange('email')}
                        type="text"
                        value={values.email}
                    />

                    <Box my={2}
                        alignItems="flex-end"
                        display="flex"
                        flexDirection="column">
                        <Button
                            size="large"
                            variant="contained"
                            className='bluebtn'
                            onClick={submitForm}
                        >
                            {loading && <CircularProgress color='secondary' size={26} />}
                            {!loading && <MessageLang id="Profile.profileEditExtra.textInput.loading" />}
                        </Button>
                    </Box>
                </Form>
            )}
        </Formik>


    return (
        <div>
            {ScreenWidth >= Responsive_Size ?
                <Modal
                    onClose={() => {
                        setOpen(false)
                    }}
                    isOpen={open}
                    title={<MessageLang id="Profile.profileEditExtra.modal.title" />}
                    subTitle={<MessageLang id="Profile.profileEditExtra.modal.subTitle" />}
                >
                    {EditContent}

                </Modal>
                : null}

            {ScreenWidth < Responsive_Size && open ?
                <GeneralDrawer from="bottom"
                    onClose={() => { setOpen(false) }}
                    isOpen={open}
                    title={<MessageLang id="Profile.profileEditExtra.modal.title" />}
                    subTitle={<MessageLang id="Profile.profileEditExtra.modal.subTitle" />}
                >
                    {EditContent}
                </GeneralDrawer>
                : null}
        </div>

    )
}

export default ProfileEditExtra
