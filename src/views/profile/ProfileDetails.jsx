import React from 'react';
import { useSelector } from 'react-redux'
import EditIcon from '@material-ui/icons/Edit';
import InstagramIcon from '@material-ui/icons/Instagram';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import IconButton from '@material-ui/core/IconButton';
import { Box, Grid } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const ProfileDetails = ({ setOpen, user, positions }) => {
    const { offers, interests } = useSelector((state) => state.persist)
    const history = useHistory()

    const handleClick = (url) => {
        const win = window.open(url, "_blank");
        win.focus();
    }

    return (
        <Grid container className="profile__info">
            <div className="profile__basic-info">
                <div className="profile__basic-info__details ">
                    <h2>
                        <MessageLang id="Profile.profileDetails.profileInfo.h2" />
                    </h2>
                    <div>
                        <MessageLang id="Profile.profileDetails.profileInfo.h2.hint" />
                    </div>
                </div>
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="row"
                    justifyContent="flex-end"
                >
                    <IconButton aria-label="edit" className="editIconBtn">
                        <EditIcon onClick={() => setOpen(true)} />
                    </IconButton>
                </Box>
            </div>
            <div className="profile__general-info">
                <Box
                    alignItems="start"
                    display="flex"
                    flexDirection="column"
                    justifyContent="flex-start"
                    className='detailsbox'
                >
                    <Box
                        alignItems="start"
                        flexDirection="row"
                        justifyContent="flex-start"
                    >
                        <h3><MessageLang id="Profile.profileDetails.profileInfo.general.offer.h3" />:</h3>
                        <h4>{user && user.offer_keys && user.offer_keys.length > 0 &&
                            offers && offers.map(offer =>
                                user.offer_keys.includes(offer.id) ? offer.name + " - " : "")}
                        </h4>
                    </Box>
                    <Box
                        alignItems="start"
                        flexDirection="row"
                        justifyContent="flex-start"
                    >
                        <h3><MessageLang id="Profile.profileDetails.profileInfo.general.need.h3" />:</h3>
                        <h4>{user && user.interest_keys && user.interest_keys.length > 0 &&
                            interests && interests.map(interest =>
                                user.interest_keys.includes(interest.id) ? interest.name + ' - ' : "")}
                        </h4>
                    </Box>
                    {user && user.company_name &&
                        <Box
                            alignItems="start"
                            flexDirection="row"
                            justifyContent="flex-start"
                        >
                            <h3><MessageLang id="Profile.profileDetails.profileInfo.companyName" />:</h3>
                            <h4>{user.company_name}</h4>
                        </Box>
                    }
                    {user && user.job_position && positions.length > 0 &&
                        <Box
                            alignItems="start"
                            flexDirection="row"
                            justifyContent="flex-start"
                        >
                            <h3><MessageLang id="Profile.profileDetails.profileInfo.jobPosition" />:</h3>
                            <h4>{positions.find(p => p.id === user.job_position).name}</h4>
                        </Box>}
                    <Box
                        alignItems="start"
                        flexDirection="row"
                        justifyContent="flex-start"
                    >
                        <h3><MessageLang id="Profile.profileEditExtra.textInput.lable2" />:</h3>
                        <h4>{user && user.email}</h4>
                    </Box>
                    {user && user.social_networks && <Box
                        alignItems="start"
                        display="inherit"
                        flexDirection="row"
                        justifyContent="flex-start"
                        className='detailsbox'
                    >
                        {user && user.social_networks && user.social_networks.instagram &&
                            <div className="profile__general-info__icon">
                                <InstagramIcon onClick={() => handleClick(user.social_networks.instagram)} />
                            </div>
                        }

                        {user && user.social_networks && user.social_networks.linkedin &&
                            <div className="profile__general-info__icon">
                                <LinkedInIcon onClick={() => handleClick(user.social_networks.linkedin)} />
                            </div>
                        }
                    </Box>}
                </Box>
            </div>
        </Grid >
    )
}

export default ProfileDetails
