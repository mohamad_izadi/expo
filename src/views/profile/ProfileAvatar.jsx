import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import PhotoUploadWidget from '../../component/photoUpload/PhotoUploadWidget'
import { Modal } from '../../component';
import { uploadPhoto , setAvatar } from '../../app/store/actions/profileActions'
import GeneralDrawer from '../../component/Drawer/GeneralDrawer.jsx';
import { toastr } from 'react-redux-toastr';
import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import ConfirmModal from '../../component/ConfirmModal/ConfirmModal'
/*************change lang ***************/
import MessageLang, { messageLang } from '../../lang/';
/*************change lang ***************/

const ProfileAvatar = ({ user, open, setOpen }) => {
    const dispatch = useDispatch();
    const { loading } = useSelector(state => state.async)
    const [confirm,setconfirm]=useState('false');

    const handleUploadImage = (photo) => {
        dispatch(uploadPhoto(photo))
    }

    const ScreenWidth = window.innerWidth
    const Responsive_Size = 600;

    const close = () => {
        setOpen(false)
    }

    const deleteavatar=()=>{
        setconfirm('false')
        dispatch(uploadPhoto({avatar:""}))
        dispatch(setAvatar("")) 
        toastr.success(messageLang("Profile.profileuploader.uploadimg.deletealert"))
        close() 
    }

    const EditContent = () => {
        return (
            <>
                <PhotoUploadWidget
                    uploadPhoto={handleUploadImage}
                    loading={loading}
                    user={user.avatar}
                    close={close} />
                {user.avatar === "" ? null : <IconButton aria-label="delete" 
                    className={'delete-button'}
                    onClick={()=> {setconfirm('true')}}>
                    <DeleteIcon />
                </IconButton>}
            </>
        )
    }
    return (
        <div>
            {ScreenWidth >= Responsive_Size ?
                <Modal
                    onClose={() => close()}
                    isOpen={open}
                    title={<MessageLang id="Profile.profileAvatar.title1" />}
                >
                    <EditContent />
                </Modal>
                : null}

            {ScreenWidth < Responsive_Size && open ?
                <GeneralDrawer from="bottom"
                    onClose={() => close()}
                    isOpen={open}
                    title={<MessageLang id="Profile.profileAvatar.title1" />}
                >
                    <EditContent />
                </GeneralDrawer>
                : null}

            {confirm==='true' ?
                <ConfirmModal title={messageLang("Profile.profileuploader.confirm.deletavatar")}
                agree={messageLang("Profile.profileuploader.confirm.deletavatar.agree")}
                disagree={messageLang("Profile.profileuploader.disagree")} 
                close={()=>close()} 
                accept={()=>deleteavatar()}
                />:null}
        </div>
    )
}

export default ProfileAvatar
