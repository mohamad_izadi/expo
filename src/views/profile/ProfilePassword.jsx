import React from 'react';
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { useDispatch, useSelector } from 'react-redux'
import {
    Box,
    Button,
    IconButton,
    InputAdornment,
    TextField,
    CircularProgress,
    LinearProgress,
} from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { ChangePassword } from '../../app/store/actions/profileActions.js';
//component
import Modal from '../../component/Modal/Modal'
import GeneralDrawer from '../../component/Drawer/GeneralDrawer'

/*************change lang ***************/
import MessageLang, { messageLang } from '../../lang/';
/*************change lang ***************/

const ProfilePassword = ({ user, open, setOpen }) => {
    const dispatch = useDispatch();

    const { loading } = useSelector((state) => state.async);
    const [label, setValues] = React.useState({
        amount: '',
        password: ' ',
        weight: '',
        weightRange: '',
        showPassword: false,
    });

    const ScreenWidth = window.innerWidth
    const Responsive_Size = 600;

    const handleClickShowPassword = () => {
        setValues({ ...label, showPassword: !label.showPassword });
    };


    const ChangePasswordForm =
        <Formik
            initialValues={
                {
                    last_password: '',
                    password: '',
                    password_confirmation: ''
                }}
            validationSchema={Yup.object({
                last_password: Yup.string().required(''),
                password: Yup.string().required(<MessageLang id="form.verify.login.password" />).min(6, <MessageLang id="Profile.profileChangePassword.length" />),
                password_confirmation: Yup.string().required(<MessageLang id="form.verify.login.two_password" />)
                    .oneOf([Yup.ref('password'), null], <MessageLang id="form.verify.login.authpassword" />)
            })}
            onSubmit={(values, { setSubmitting, setErrors }) => {
                try {
                    dispatch(ChangePassword(values))
                    setSubmitting(false);
                    setOpen(false)

                } catch (error) {
                    console.log(error);
                    setErrors({ auth: '' });
                    setSubmitting(false);
                }
            }}
        >
            {({ submitForm,
                errors,
                handleBlur,
                handleChange,
                handleSubmit,
                isSubmitting,
                touched,
                values, isValid, dirty }) => (
                <Form className='ui form changepassword'>
                    {loading && <LinearProgress color='primary' size={26} />}
                    <Field
                        component={TextField}
                        id="outlined-adornment-lastpassword"
                        error={Boolean(touched.last_password && errors.last_password)}
                        fullWidth
                        helperText={touched.last_password && errors.last_password}
                        label={<MessageLang id="form.label.register.lastpassword" />}
                        margin="normal"
                        name="last_password"
                        onBlur={handleBlur}
                        onChange={handleChange('last_password')}
                        // className={classes.textField}
                        variant="outlined"
                        type={label.showPassword ? 'text' : 'password'}
                        value={values.last_password}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{
                            shrink: true,
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton aria-label="Toggle password visibility" onClick={handleClickShowPassword}>
                                        {label.showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                    <Field
                        component={TextField}
                        id="outlined-adornment-newpassword"
                        error={Boolean(touched.password && errors.password)}
                        fullWidth
                        helperText={touched.password && errors.password}
                        label={<MessageLang id="form.label.register.password" />}
                        margin="normal"
                        name="password"
                        onBlur={handleBlur}
                        onChange={handleChange('password')}
                        // className={classes.textField}
                        variant="outlined"
                        type={label.showPassword ? 'text' : 'password'}
                        value={values.password}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{
                            shrink: true,
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton aria-label="Toggle password visibility" onClick={handleClickShowPassword}>
                                        {label.showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                    <Field
                        component={TextField}
                        id="outlined-adornment-repnewpassword"
                        error={Boolean(touched.password_confirmation && errors.password_confirmation)}
                        fullWidth
                        helperText={touched.password_confirmation && errors.password_confirmation}
                        label={<MessageLang id="form.label.register.password_replay" />}
                        margin="normal"
                        name="password_confirmation"
                        onBlur={handleBlur}
                        onChange={handleChange('password_confirmation')}
                        variant="outlined"
                        type={label.showPassword ? 'text' : 'password'}
                        value={values.password_confirmation}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{

                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton aria-label="Toggle password visibility" onClick={handleClickShowPassword}>
                                        {label.showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                    <Box my={2}
                        alignItems="flex-end"
                        display="flex"
                        flexDirection="column">
                        <Button
                            className='bluebtn'
                            size="large"
                            variant="contained"
                            onClick={submitForm}
                        >
                            {loading && <CircularProgress color='secondary' size={26} />}
                            {!loading && <MessageLang id="Profile.profileHeader.button2" />}
                        </Button>
                    </Box>
                </Form>
            )}
        </Formik>


    return (
        <>
            {ScreenWidth >= Responsive_Size ?
                <Modal
                    onClose={() => {setOpen(false)}}
                    isOpen={open}
                    title={<MessageLang id="Profile.profileChangePassword.Modal.title" />}
                    subTitle={<MessageLang id="Profile.profileChangePassword.Modal.subtitle" />}
                >
                    {ChangePasswordForm}
                </Modal>
                : null}
            {ScreenWidth < Responsive_Size && open ?
                <GeneralDrawer from="bottom"
                    onClose={() => { setOpen(false) }}
                    isOpen={open}
                    title={<MessageLang id="Profile.profileChangePassword.Modal.title" />}
                    subTitle={<MessageLang id="Profile.profileChangePassword.Modal.subtitle" />}
                >
                    {ChangePasswordForm}
                </GeneralDrawer>
                : null}
        </>
    )
}

export default ProfilePassword
