import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import ProfileDetails from './ProfileDetails'
import { Grid } from '@material-ui/core'
import { loadProfile } from '../../app/store/actions/profileActions'
import { loadPositions } from '../../app/store/actions/persistActions'
import { makeStyles } from '@material-ui/core/styles';
//component
import ProfileHeader from './ProfileHeader'
import ProfileEditInfo from './ProfileEditInfo'
import ProfileEditExtra from './ProfileEditExtra'
import ProfilePicture from './ProfileAvatar'
import ProfilePassword from './ProfilePassword'

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiInputLabel-outlined': {
            fontFamily: 'IRANSansFa'
        }
    }
}))

const Profile = () => {
    const classes = useStyles();
    const { currentUser: user } = useSelector((state) => state.auth);
    const { positions } = useSelector(state => state.persist)
    const [openEdit, setOpenEdit] = useState(false)
    const [openExtra, setOpenExtra] = useState(false)
    const [openAvatar, setOpenAvatar] = useState(false)
    const [openPassword, setOpenPassword] = useState(false)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(loadProfile())
        dispatch(loadPositions())
    }, [dispatch]);

    return (
        <div className={`${classes.root} profilebg`}>
            <Grid container className="profile" >
                <ProfileHeader user={user} setOpenEdit={setOpenEdit} setOpenAvatar={setOpenAvatar} setOpenPassword={setOpenPassword} />
                <ProfileDetails user={user} positions={positions} setOpen={setOpenExtra} />

                <ProfilePicture user={user} open={openAvatar} setOpen={setOpenAvatar} />
                <ProfileEditInfo user={user} open={openEdit} positions={positions} setOpen={setOpenEdit} />
                <ProfilePassword user={user} open={openPassword} setOpen={setOpenPassword}/>
                <ProfileEditExtra user={user} open={openExtra} setOpen={setOpenExtra} />
            </Grid>
        </div>
    )
}

export default Profile
