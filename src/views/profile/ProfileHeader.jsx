import React from 'react';

import {
    Avatar,
    Box,
    Button,
    makeStyles,
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';

/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const ProfileHeader = ({ user, setOpenEdit, setOpenAvatar ,setOpenPassword}) => {

    return (
        <div className={'profile-header'}>
            <div className="profile-header__content">
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="row"
                >
                    <Box className='avatarbox' onClick={() => setOpenAvatar(true)}>
                        <Avatar src={user && user.avatar==="" || user && user.avatar===null ? '/static/images/user.png' : user.avatar}/>
                        { user && user.avatar ==="" || user && user.avatar ===null ? null : <EditIcon className='editavatar'/>}
                    </Box>
                    
                    <div className="profile-header__info">
                        <div className="profile-header__title">{user && user.first_name + ' ' + user.last_name}</div>
                        <div className="profile-header__sub-title">{user && user.company_name}</div>
                        <div className="profile-header__sub-title">{user && user.job_title}</div>
                    </div>
                </Box>
                <Box
                    alignItems="flex-end"
                    display="flex"
                    flexDirection="row"
                    justifyContent="flex-end"
                >
                    <Button variant="contained" className='bluebtn' onClick={() => setOpenEdit(true)}>
                        <MessageLang id="Profile.profileHeader.button1"/>
                    </Button>
                    <Button variant="contained" className='bluebtn' onClick={() => setOpenPassword(true)}>
                        <MessageLang id="Profile.profileHeader.button2"/>
                    </Button> 
                </Box>
            </div>

        </div>
    )
}

export default ProfileHeader


// const useStyles = makeStyles((theme) => ({
//     root: {
//         '& label.Mui-focused': {
//             color: 'blue',
//         },
//         '& label.MuiFormLabel-root': {
//             fontFamily: 'IRANSansFa',
//         },
//         '& .MuiOutlinedInput-adornedEnd': {
//             backgroundColor: '#fff'
//         },
//         '& .MuiInputLabel-outlined': {
//             fontFamily: 'IRANSansFa'
//         },
//         '& .MuiInput-underline:after': {

//         },
//         '& .MuiOutlinedInput-root': {
//             '& fieldset': {
//             },
//             '&:hover fieldset': {
//             },
//             '&.Mui-focused fieldset': {
//             },
//         },
//         backgroundColor: '#9F9F9F',
//         height: '100%',
//         textAlign: 'center',
//         padding: 0,
//         overflow: 'hidden'
//     },
//     labelText: {
//         fontSize: '30px',
//     },
//     LoginContainer: {
//         margin: '0',
//         height: '100%'
//     },
//     LoginPanel: {
//         backgroundColor: '#fff',
//         height: '100%',
//         borderRight: '1px solid rgba(0, 0, 0, 0.12);',
//         display: 'grid',
//         [theme.breakpoints.down('xs')]: {
//             padding: '0 35px',
//         },
//     },
// }));
