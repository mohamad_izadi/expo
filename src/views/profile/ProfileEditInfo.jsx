import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import {
    Box,
    Button,
    CircularProgress,
} from '@material-ui/core';
import { TextInput, SelectInput } from '../../component'
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { Modal } from '../../component';
import { editInfo } from '../../app/store/actions/profileActions.js';
import { loadProfile } from '../../app/store/actions/profileActions'
import Select from '../../component/Select/Select'
import { makeStyles } from '@material-ui/core/styles';
import GeneralDrawer from '../../component/Drawer/GeneralDrawer.jsx';

/*************change lang ***************/
import MessageLang, { messageLang } from '../../lang/';
import { toastr } from 'react-redux-toastr';
/*************change lang ***************/


const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiInputLabel-outlined': {
            fontFamily: 'IRANSansFa'
        }
    }
}))

const ProfileEditInfo = ({ user, open, setOpen, positions }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { loading } = useSelector((state) => state.async);
    const [inpalert,setinpalert]=useState({
        instagram:false,
        linkedin:false
    })
    const ScreenWidth = window.innerWidth
    const Responsive_Size = 600;

    const checklinkedinfunc =(url)=>{
        if(url){
            var check_linkedin = '(https?:\\/\\/(www.)?linkedin.com\\/(mwlite\\/|m\\/)?in\\/[a-zA-Z0-9_.-]+\\/?)';
            var link = url.match(check_linkedin)
            if(link){ 
                setinpalert({linkedin:false}) 
                return (true) 
            }
            else{
                setinpalert({linkedin:true})
                return false
            }
        }
        else{
            return true;
        }
    }
    const checkinstagramfunc =(url)=>{
        if (url){
            var check_instagram = '(https?)?:?(www)?instagram\.com/[a-z].{3}';
            var ins = url.match(check_instagram)
            if(ins){ 
                setinpalert({instagram:false}) 
                return true;
            }
            else{
                setinpalert({instagram:true})
                return false
            }
        }
        else{
            return true;
        }
    }

    const EditContent = <Formik
        initialValues={
            {
                first_name: user && user.first_name,
                last_name: user && user.last_name,
                company_name: user && user.company_name,
                job_position: user && user.job_position,
                linkedin: user && user.social_networks && user.social_networks.linkedin,
                instagram: user && user.social_networks && user.social_networks.instagram
            }}
        validationSchema={Yup.object({

        })}
        onSubmit={async (values, { setSubmitting, setErrors }) => {

            if(checkinstagramfunc(values.instagram) && checklinkedinfunc(values.linkedin)){
                try {
                    await dispatch(editInfo(values, 'basic'))
                    setSubmitting(false)
                    setOpen(false)
                    dispatch(loadProfile())
                    toastr.success(messageLang("Profile.profileEditInfo.addalert"))
                } catch (error) {
                    setErrors({ auth: '' })
                    setSubmitting(false)
                }
                setOpen(false)
            }
        }}
    >
        {({ submitForm,
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            setFieldValue,
            touched,
            values, isValid, dirty }) => (
            <Form className='ui form'>
                <TextInput
                    error={Boolean(touched.first_name && errors.first_name)}
                    helperText={touched.first_name && errors.first_name}
                    label={<MessageLang id="form.label.register.name" />}
                    margin="normal"
                    name="name"
                    onBlur={handleBlur}
                    onChange={handleChange('first_name')}
                    type="text"
                    value={values.first_name}
                />
                <TextInput
                    error={Boolean(touched.last_name && errors.last_name)}
                    helperText={touched.last_name && errors.last_name}
                    label={<MessageLang id="form.label.register.family" />}
                    margin="normal"
                    name="name"
                    onBlur={handleBlur}
                    onChange={handleChange('last_name')}
                    type="text"
                    value={values.last_name}
                />
                <TextInput
                    error={Boolean(touched.company_name && errors.company_name)}
                    helperText={touched.company_name && errors.company_name}
                    label={<MessageLang id="form.label.register.company" />}
                    margin="normal"
                    name="name"
                    onBlur={handleBlur}
                    onChange={handleChange('company_name')}
                    type="text"
                    value={values.company_name}
                />
                <Select
                    id="job_position"
                    label={<MessageLang id="stands.MemberFilter.form.position" />}
                    name="job_position"
                    value={values.job_position}
                    options={positions}
                    onChange={(field, value) => {
                        setFieldValue(field, value);
                    }}
                />
                <TextInput
                    error={Boolean(inpalert.linkedin)}
                    helperText={inpalert.linkedin && 'https://www.linkedin.com/in/Your_account'}
                    label={<MessageLang id="Profile.profileEditExtra.textInput.lable3" />}
                    margin="normal"
                    name="linkedin"
                    onBlur={handleBlur}
                    onChange={handleChange('linkedin')}
                    type="text"
                    value={values.linkedin}
                />
                <TextInput
                    error={Boolean(inpalert.instagram)}
                    helperText={inpalert.instagram && 'https://www.instagram.com/Your_account'}
                    label={<MessageLang id="Profile.profileEditExtra.textInput.lable4" />}
                    margin="normal"
                    name="instagram"
                    onBlur={handleBlur}
                    onChange={handleChange('instagram')}
                    type="text"
                    value={values.instagram}
                />
                <Box my={2}
                    alignItems="flex-end"
                    display="flex"
                    flexDirection="column">
                    <Button
                        size="large"
                        className='bluebtn'
                        onClick={submitForm}
                    // className="Btn_Shadow Btn_FontSize"
                    >
                        {loading && <CircularProgress color='secondary' size={26} />}
                        {!loading && <MessageLang id="Profile.profileEditExtra.textInput.loading" />}
                    </Button>
                </Box>
            </Form>
        )}
    </Formik>
    return (
        <div>
            {ScreenWidth >= Responsive_Size ?
                <Modal
                    onClose={() => {setOpen(false)}}
                    isOpen={open}
                    title={<MessageLang id="Profile.profileEditInfo.Modal.title" />}
                    className={classes.root}
                >
                    {EditContent}
                </Modal>
                : null}

            {ScreenWidth < Responsive_Size && open ?
                <GeneralDrawer from="bottom"
                    onClose={() => { setOpen(false) }}
                    isOpen={open}
                    title={<MessageLang id="Profile.profileEditInfo.Modal.title" />}
                >
                    {EditContent}
                </GeneralDrawer>
                : null}
        </div>
    )
}

export default ProfileEditInfo
