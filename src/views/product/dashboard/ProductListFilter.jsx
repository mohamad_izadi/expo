import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux'
import agent from '../../../app/api/agent';
import {
    SelectInput,SelectSort
} from '../../../component'

import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { clearFilters, loadProducts, setProductFilters} from '../../../app/store/actions/productActions';
import { loadCategories, loadCompanies } from '../../../app/store/actions/persistActions'
import Grid from '@material-ui/core/Grid';
import SearchBox from '../../../component/Search/SearchBox';

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles({
    root: {
        color: '#234'
    }
});

const ProductFilter = ({ selected, number, onClick }) => {
    const [category, setCategory] = useState()
    const [subCategories, setSubCategories] = useState([])
    const [loading, setLoading] = useState(false);
    const [isDisabled, toggleDisabled] = useState(true)

    const classes = useStyles()
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(loadCategories())
        // dispatch(loadCompanies())
        dispatch(loadProducts())
        return () => {
            dispatch(clearFilters())
        }
    }, [dispatch])


    // async function loadSubCategories(categoryId) {
    //     setLoading(true)
    //     try {
    //         const subCategories = await agent.Filters.subCategories(categoryId)
    //         setSubCategories(subCategories.result)
    //         setLoading(false)
    //     } catch (error) {
    //         console.log(error)
    //         setLoading(false)
    //     }
    // }

    // function handleChangeCategory(category, type) {
    //     if (type.action === "select-option") {
    //         setCategory(category.value)
    //         loadSubCategories(category.value)
    //         dispatch(setProductFilters('category', category.value))
    //         dispatch(loadProducts())
    //         toggleDisabled(false)
    //     } else if (type.action === "clear") {
    //         setCategory(null)
    //         setSubCategories(null)
    //         dispatch(setProductFilters('category', 0))
    //         dispatch(setProductFilters('sub_ategory', 0))
    //         dispatch(loadProducts())
    //         toggleDisabled(true)
    //     }
    // }

    function handleChangeSubCategory(subCategory, type) {
        if (type.action === "select-option") {
            dispatch(setProductFilters('sub_ategory', subCategory.value))
            dispatch(loadProducts())
        } else if (type.action === "clear") {
            dispatch(setProductFilters('sub_ategory', 0))
            dispatch(loadProducts())
        }
    }

    // function handleChangeComapny(company, type) {
    //     if (type.action === "select-option") {
    //         dispatch(setProductFilters('company', company.value))
    //         dispatch(loadProducts())
    //     } else if (type.action === "clear") {
    //         dispatch(setProductFilters('company', 0))
    //         dispatch(loadProducts())
    //     }
    // }

    const [isSort, set_isSort] = useState({
        alphabetical : false,
        mostVisited : false,
    })

    const handelSelectSort = (type)=>{
        dispatch(setProductFilters(type, !isSort[type] ? "1":"0"))        
        set_isSort(val=>{
            val[type] = !val[type]
            return {...val}
        })
        dispatch(loadProducts())
    }

    return (
        <div className={classes.root}>
            <div className="product_filter-list Responsive_Filter_Mood">
                <SelectSort
                    name="mostVisited"
                    label={<MessageLang id="product.filter.mostvisited" />}
                    isDisabled={false}
                    checked={isSort.mostVisited}
                    onChange={() => handelSelectSort("mostVisited")}
                />
                <SelectSort
                    name="alphabetical"
                    label={<MessageLang id="product.filter.alphabetical" />}
                    isDisabled={false}
                    checked={isSort.alphabetical}
                    onChange={() => handelSelectSort("alphabetical")}
                />
                {/* <SelectInput
                    name="companies"
                    label={<MessageLang id="stands.StandDashboard.text.company" />}
                    isDisabled={false}
                    isLoading={loading}
                    isClearable={true}
                    options={companies && companies.map(company =>
                        ({ value: company.id, label: company.name }))}
                    onChange={(value, type) => handleChangeComapny(value, type)}
                />

                <SelectInput
                    clearValue={(e) => console.log(e)}
                    name="category"
                    label={<MessageLang id="stands.CompanyList.Table.activity" />}
                    isDisabled={false}
                    isLoading={loading}
                    isClearable={true}
                    options={categories && categories.map(cat =>
                        ({ value: cat.id, label: cat.name }))}
                    onChange={(value, type) => handleChangeCategory(value, type)}
                />
*/}
                <SelectInput
                    name="sub_categories"
                    label={<MessageLang id="stands.MemberFilter.form.categories" />}
                    isDisabled={isDisabled}
                    isLoading={loading}
                    isClearable={true}
                    options={subCategories && subCategories.map(sub =>
                        ({ value: sub.id, label: sub.name }))}
                    onChange={(value, type) => handleChangeSubCategory(value, type)}
                /> 
                
            </div>
        </div>
    )
}

export default ProductFilter
