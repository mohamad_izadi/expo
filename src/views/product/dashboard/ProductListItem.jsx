import React from 'react';
import icon from '../default.png'
import { useHistory } from 'react-router-dom';
import { StarBorder } from '@material-ui/icons';
import { useSelector } from 'react-redux'

/*************change lang ***************/
import MessageLang, { messageLang } from '../../../lang/';
/***************************************/

const ProductItem = ({ product }) => {
    const [value, setValue] = React.useState(2);
    const history = useHistory();

    const mobileSize = window.innerWidth < 600 ? true :false
    // const product_desc = mobileSize ? product.description.slice(0, 110) : product.description.slice(0, 300)
    const exhibition = useSelector(state => state.exhibition.data);
    const handleClick = (productId) => {
        history.push(`/${exhibition.short_name}/product/${productId}`)

    }
    return (
        <div onClick={() => handleClick(product.id)} className={'product-item'}>
            <div className="product-item__avatar">
                <img alt={product.name} src={(product.media && product.media.length > 0) ? product.media[0] : icon} />
            </div>
            <div className="product-item__content">
                <div className="product-item__content__title">
                    {/* <div> */}
                        <div className="product-item__content__rate">
                            <div className='product-ratingbox'>
                                <span className='count'>({product.votes} <MessageLang id='General.person'/>)</span>
                                <StarBorder/>
                                <span className='sum'>{product.points}</span>
                            </div>
                        </div>
                        <h2>
                            {product.name}
                        </h2>
                    {/* </div> */}
                </div>
                <div className="product-item__content__category">
                    {product.category}
                </div>
                <div className="product-item__content__description" dangerouslySetInnerHTML={{ __html: product.description}}>
                </div>
            </div>

        </div>
    )
}

export default ProductItem
