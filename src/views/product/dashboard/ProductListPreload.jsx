import React from 'react';
import { AppBar, CardHeader, Toolbar, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';

const ProductListPreload = () => {
    return (
        <div className={'product-list__products__show'}>
            <div className={'product-header'}>
                <AppBar position="relative" className={'AppBar'}>
                    <Toolbar>
                        <CardHeader style={{ paddingRight: 0 }}
                            avatar={<Skeleton animation="wave" variant="circle" width={40} height={40} />}
                            title={<Skeleton animation="wave" variant="text" height={20} width={160} />}
                        // subheader={<Skeleton animation="wave" height={15} width={120} />}
                        />
                    </Toolbar>
                </AppBar>
            </div>
            <div>
                <div className="preload-item__content">
                    <CardHeader style={{ padding: '20px 25px', alignItems: 'inherit' }}
                        avatar={<Skeleton animation="wave" variant="rect" width={80} height={80} />}
                        title={<Skeleton animation="wave" variant="text" height={20} width={120} />}
                        subheader={<>
                            <Skeleton style={{ margin: '10px 0' }} animation="wave" height={20} width={80} />
                            <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="95%" />
                            <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="90%" />
                            <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="95%" />
                            <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="50%" />
                        </>}
                    />
                </div>
            </div>
            <div className={'product-header'}>
                <AppBar position="relative" className={'AppBar'}>
                    <Toolbar>
                        <CardHeader style={{ paddingRight: 0 }}
                            avatar={<Skeleton animation="wave" variant="circle" width={40} height={40} />}
                            title={<Skeleton animation="wave" variant="text" height={20} width={150} />}
                        // subheader={<Skeleton animation="wave" height={15} width={120} />}
                        />
                    </Toolbar>
                </AppBar>
            </div>
            <div>
                <div className="preload-item__content">
                    <CardHeader style={{ padding: '20px 25px', alignItems: 'inherit' }}
                        avatar={<Skeleton animation="wave" variant="rect" width={80} height={80} />}
                        title={<Skeleton animation="wave" variant="text" height={20} width={120} />}
                        subheader={<>
                            <Skeleton style={{ margin: '10px 0' }} animation="wave" height={20} width={80} />
                            <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="95%" />
                            <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="90%" />
                            <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="95%" />
                            <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="50%" />
                        </>}
                    />
                </div>
            </div>
        </div>
    )
}

export default ProductListPreload
