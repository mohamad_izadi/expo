import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Avatar, CardHeader, Typography,} from '@material-ui/core'
/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const ProductHeader = ({ onBack, count, name, image }) => {

    return (
        <div className={'product-header'}>
            <AppBar position="relative" className={'AppBar'}>
                <Toolbar>
                    <CardHeader
                        className='title'
                        avatar={
                            <Avatar alt="" src={image ? image : "/static/images/default.png"} width={30} height={30} />
                        }
                        title={name}
                    />
                    <Typography variant="contained" color="secondary" className="subTitle" >{count} <MessageLang id="stands.details.Outlook.product"/></Typography>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default ProductHeader
