import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import clsx from 'clsx';
import { loadProducts, setProductFilters } from '../../../app/store/actions/productActions'
import { setTourGuide } from '../../../app/store/actions/authActions'
import { Card, makeStyles, Hidden, Button } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import SearchBox from '../../../component/Search/SearchBox';
import ProductListFilter from './ProductListFilter';
import ProductListHeader from './ProductListHeader';
import ProductListItem from './ProductListItem';
import ProductListPreload from './ProductListPreload';
import GeneralDrawer from '../../../component/Drawer/GeneralDrawer.jsx'
import { Modal } from '../../../component';
import Tour from 'reactour'
//pagination
import FooterPagination from '../../../component/FooterPagination/FooterPagination'
/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    paperExhebition: {
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

const ProductList = ({ className, ...rest }) => {

    const classes = useStyles();
    const [ShowFilter, SetShowFilter] = useState(false)
    const [isTourOpen, setTourOpen] = useState(false)
    const dispatch = useDispatch()
    const { companies, filter, total } = useSelector(state => state.product)
    const { currentUser } = useSelector(state => state.auth);
    const { loading } = useSelector(state => state.async)
    const lang = useSelector((state) => state.lang.lang);
    let mobileSize = window.innerWidth < 600 ? true : false
    const default_List_height = window.innerHeight - 58

    const steps = [
        {
            selector: '.MuiAppBar-root',
            content: <>
                <h4 className={'tour_header'}>{<MessageLang id="TourGuide.products.title" />}</h4>
                <p>{<MessageLang id="TourGuide.products.desc" />}</p>
                <Button className={'btn_close'} variant="contained" color="primary"
                    onClick={() => setTourOpen(false)}>
                    <MessageLang id="TourGuide.Button.Ok" />
                </Button>
            </>,
            style: {
                backgroundColor: '#fff',
                textAlign: 'center'
            },
        },
    ];

    useEffect(() => {
        dispatch(loadProducts())
        if (!currentUser.tour_guide.products) {
            setTourOpen(true)
            dispatch(setTourGuide("products"))
        }
    }, [dispatch])

    const handleChangePage = async (event, newPage) => {
        await dispatch(setProductFilters('page', newPage + 1))
        dispatch(loadProducts())
    };

    const handleSearchFilter = async (event) => {
        await dispatch(setProductFilters('search', event.target.value))
        dispatch(loadProducts())
    };

    return (
        <>
            <Card
                className={clsx(classes.root, className)}
                {...rest}
            >
                <Grid container className="product-list">
                    <Grid item md={4} xs={12}>
                        <Tour
                            steps={steps}
                            isOpen={isTourOpen}
                            onRequestClose={() => setTourOpen(false)}
                            showNavigation={false}
                            showNavigationNumber={false}
                            showNumber={false}
                            showButtons={false}
                            showNavigationScreenReaders={false}
                            disableDotsNavigation={true}
                            className={'tourguide'}
                        />
                        <Grid container>
                            <Grid item xs={12}>
                                <SearchBox onChange={handleSearchFilter} HandlerFilter={() => SetShowFilter(true)} />
                            </Grid>
                            {ShowFilter && mobileSize ?
                                <GeneralDrawer from="bottom"
                                    onClose={() => SetShowFilter(false)}
                                    isOpen={ShowFilter}
                                    title={<MessageLang id="product.productList.filter.products" />}
                                >
                                    <ProductListFilter />
                                </GeneralDrawer>
                                :
                                <Modal
                                    onClose={() => SetShowFilter(false)}
                                    isOpen={ShowFilter}
                                    title={<MessageLang id="product.productList.filter.products" />}
                                    className={classes.root}
                                >
                                    <ProductListFilter />
                                </Modal>
                            }
                            <Hidden only={['sm', 'xs']}>
                                <Grid item xs={12}>
                                    <ProductListFilter />
                                </Grid>
                            </Hidden>
                        </Grid>
                    </Grid>
                    <Grid item md={8} xs={12} >
                        <div className={'product-list__products'}>
                            {loading ?
                                <ProductListPreload />
                                :
                                <>
                                    <div className={'product-list__products__show'}>
                                        {companies.map((company) => (
                                            <div key={company.id}>
                                                <ProductListHeader count={company.count} name={company.name} image={company.logo} />
                                                {
                                                    company.products && company.products.map(
                                                        product => (
                                                            <ProductListItem key={product.id} product={product} />
                                                        )
                                                    )
                                                }
                                                {companies.length > 1 && <div className='product-list__products__between'></div>}
                                            </div>))}
                                    </div>
                                    <FooterPagination
                                        lang={lang}
                                        total={total}
                                        limit={filter.limit}
                                        page={filter.page - 1}
                                        ChangePage={handleChangePage}
                                    />
                                </>
                            }
                        </div>
                    </Grid>
                </Grid>
            </Card>
        </>
    )
}

export default ProductList
