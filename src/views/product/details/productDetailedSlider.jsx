import React from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

const ProductDetailedSlider = ({ product }) => {

    return (
        <div className="product-detail__slider-panel">
            <div class="carousel-wrapper">
                <Carousel showThumbs={false} showStatus={false}>
                    {product.media.length > 0 ? product.media.map(image =>
                        <div>
                            <img alt={product.name} src={image} />
                        </div>
                    ) :
                        <div>
                            <img alt="sss" src="/static/images/slider/image_3.jpg" />
                        </div>
                    }
                </Carousel>
            </div>
        </div>
    )
}

export default ProductDetailedSlider


// const arrowStyles: CSSProperties = {
//     position: 'absolute',
//     zIndex: 2,
//     top: 'calc(50% - 15px)',
//     width: 30,
//     height: 30,
//     cursor: 'pointer',
// };

// const indicatorStyles: CSSProperties = {
//     background: '#fff',
//     width: 8,
//     height: 8,
//     display: 'inline-block',
//     margin: '0 8px',
// };