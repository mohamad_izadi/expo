import React from 'react'
import { Button, Typography } from '@material-ui/core';

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/


const ProductDetailedInfo = ({ product }) => {
    return (
        <div className={'product-detail-info'}>
            <div className="product-detail-info__header">
                <Typography variant="h5" >
                    <span className={'product-detail-info__title'}>{product.name}</span>
                    <p variant="h6" className={'product-detail-info__subtitle'}>
                        {product.category}
                    </p>
                </Typography>
                {/* {product.price &&
                    <Typography className={'product-detail-info__company'} color="secondary" >
                        <Button variant="contained">{product.price} <MessageLang id="product.productList.infoprice"/></Button>
                    </Typography>
                } */}
            </div>
            <div className="product-detail-info__content"
                dangerouslySetInnerHTML={{ __html: product.description }}>
            </div>
        </div>
    )
}

export default ProductDetailedInfo
