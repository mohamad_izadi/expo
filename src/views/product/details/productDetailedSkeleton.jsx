import React from 'react'
import { Button, Toolbar, AppBar, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';


const ProductDetailedSkeleton = () => {
    return (
        <>
            <div className={'product-detail-header'}>
                <AppBar position="relative" className={'AppBar'}>
                    <Toolbar>
                        <div className={'product-detail'}>
                            <div className={'product-title'}>
                                <Skeleton align="right" animation="wave" height={20} width={120} />
                            </div>
                        </div>
                        {/* <div className={'company-detail'}>
                            <Skeleton align="left" component={Button} animation="wave" height={50} width="40%" />
                        </div> */}
                    </Toolbar>
                </AppBar>
            </div >
            <div className="product-detail__slider-preload">
                <div class="carousel-wrapper">
                    <Skeleton animation="wave" variant="rect" width="100%" height='289px' />
                </div>
            </div>
            <div className={'product-detail-info'}>
                <div className="product-detail-info__header">
                    <Typography variant="h5" className={'product-detail-info__title'}>

                    </Typography>
                    <Typography className={'product-detail-info__company'} color="secondary" >

                    </Typography>
                </div>
                <div className="product-detail-info__content">
                    <Skeleton style={{ margin: '10px 0' }} animation="wave" height={20} width={80} />
                    <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="95%" />
                    <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="90%" />
                    <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="95%" />
                    <Skeleton style={{ marginBottom: '5px' }} animation="wave" height={15} width="50%" />
                </div>
            </div>
        </>
    )
}

export default ProductDetailedSkeleton
