import React from 'react';
import { Button , Toolbar, AppBar ,Hidden,CardHeader ,makeStyles} from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { useHistory } from 'react-router-dom';
import { StarBorder } from '@material-ui/icons';


/*************change lang ***************/
import MessageLang from '../../../lang/index.js';
/*************change lang ***************/

const theme = makeStyles({
    breakpoints: {
        values: {
            xs: 0,
            sm: 600,
            md: 960,
            lg: 1280,
            xl: 1920,
        },
    },
})
const useStyles = makeStyles((theme) =>{})

const ProductDetailedHeader = ({ product }) => {
    const classes = useStyles();

    const history = useHistory()
    return (
        <div className={'product-detail-header'}>
            <AppBar position="relative" className={'AppBar'}>
                <Toolbar>
                    <div className={'product-detail'}>
                        <div className={'product-title'}>
                            {product.name}
                            <span>
                                {product.company}
                            </span>
                        </div>
                        <div className='product-ratingbox'>
                            <span className='count'>({product.votes} <MessageLang id='General.person'/>)</span>
                            <StarBorder/>
                            <span className='sum'>{product.points}</span>
                        </div>
                    </div>
                    <div className={'company-detail'}>
                    <Button variant="contained" className='title'>{product.company}</Button>
                    <Button className={'back-icon'} onClick={() => { history.goBack() }} >
                        <ArrowBackIosIcon fontSize="small" />
                    </Button>
                    </div>
                </Toolbar>
            </AppBar>
        </div >
    )
}

export default ProductDetailedHeader

