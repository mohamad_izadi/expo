import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import clsx from 'clsx';
import ProductDetailedHeader from './productDetailedHeader'
import ProductDetailedSlider from './productDetailedSlider'
import {Card,Grid,Hidden} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SearchBox from '../../../component/Search/SearchBox';
import ProductListFilter from '../dashboard/ProductListFilter';
import ProductDetailedInfo from './productDetailedInfo';
import { loadProduct } from '../../../app/store/actions/productActions'
import TabNavs from '../../../component/Tabs/TabNavs';
import ProductDetailedSkeleton from './productDetailedSkeleton';
import ProductDetailedComments from './productDetailedComments';
import GeneralDrawer from '../../../component/Drawer/GeneralDrawer.jsx'
import { Modal } from '../../../component';

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTableCell-root': {
            fontFamily: 'IRANSansFa',
            padding: '8px'
        },
        '& .MuiTableCell-head': {
            padding: '16px'
        },
        '& .MuiTabs-root': {
            height: '100%'
        },
        '& .MuiTabs-flexContainer': {
            height: '100%'
        },
        '& .makeStyles-search-36': {
            borderRadius: '0'
        },
        '& .MuiCardHeader-root': {
            padding: '0'
        },
        '& .MuiSelect-selectMenu': {
            padding: '10px 10px'
        }
    },
    // standHeader: {
    //     height: '60px',
    //     boxShadow: 'none',
    //     backgroundColor: '#fff',
    //     borderBottom: '1px solid #eee'
    // },
    swipeableViews: {
        margin: '0 auto'
    },
    avatar: {
        marginRight: theme.spacing(2)
    }
}));

const ProductDetails = ({ className, match, history, ...rest }) => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const { selectedProduct: product, selectedComments: comments } = useSelector(state => state.product)
    const [value, setValue] = useState(0);
    const [ShowFilter,SetShowFilter] = useState(false)
    let mobileSize = window.innerWidth < 600 ? true : false

    const pageheight = window.innerHeight - 58

    const handleChange = (newValue) => {
        setValue(newValue);
    };

    useEffect(() => {
        dispatch(loadProduct(match.params.id))
    }, [dispatch, match.params.id, history]);

    return (
        <>
            <Card
                className={clsx(classes.root, className)}
                {...rest}
                style={{height: pageheight, maxHeight: pageheight }}
            >
                <Grid container className="product-detail">
                    <Hidden only={['sm','xs']}>
                        <Grid item md={3}>
                            <Grid container>
                                <Grid item xs={12}>
                                    <SearchBox/>
                                </Grid>
                            
                                <Grid item xs={12}>
                                    <ProductListFilter />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Hidden>
                    <Grid item md={9} xs={12} className='product-detail-left' 
                    style={{height: pageheight, maxHeight: pageheight }}
                    >
                        {product ?
                            <>
                                <ProductDetailedHeader product={product} />
                                <ProductDetailedSlider product={product} />
                                <TabNavs
                                    components={
                                        [<ProductDetailedInfo product={product} />,
                                        <ProductDetailedComments product={product} comments={comments} />]}
                                    labels={
                                        [
                                        <MessageLang id="stands.details.StandDetails.introduction"/>
                                        ,
                                        <MessageLang id="stands.details.StandDetails.comments"/>
                                        ]
                                    }
                                    handleChangeIndex={handleChange}
                                    classNames={'tab_header fftabs'} />
                            </> : <ProductDetailedSkeleton />}

                    </Grid>
                </Grid>
            </Card>
        </>
    )
}

export default ProductDetails