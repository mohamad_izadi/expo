import React, { useState, useEffect, useRef } from 'react'
import { useSelector } from 'react-redux';
import Rating from '@material-ui/lab/Rating';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { Avatar, BottomNavigation, CircularProgress, CardHeader, ListItemAvatar, ListItemText, ListItem } from '@material-ui/core'
import SendIcon from '@material-ui/icons/Send';
import { addComment } from '../../../app/store/actions/productActions'
import { Box, Typography } from '@material-ui/core'
import GeneralDrawer from '../../../component/Drawer/GeneralDrawer'
import Modal from '../../../component/Modal/Modal'
import Voting from '../../../component/Voting/Voting'
import Sendbox_input from '../../../component/SendBox_Input/Sendbox_input'
/*************change lang ***************/
import MessageLang, { messageLang } from '../../../lang/index.js';
import { TrainRounded } from '@material-ui/icons';
/*************change lang ***************/


const useStyles = makeStyles({
    root: {
        width: 500,
    },
});

const ProductDetailedComments = ({ product, comments }) => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const [value, setValue] = React.useState(0);
    const [msg, setMsg] = useState('')
    const [open, setopen] = useState(false)
    const CommentRef = useRef(null)
    const { loading } = useSelector(state => state.async)

    const mobileSize = window.innerWidth < 600 ? true : false

    useEffect(() => {
        Scrolltolastcomment()
    }, [CommentRef])

    const Scrolltolastcomment =()=>{
        if (CommentRef.current) {
            const target = CommentRef.current;
            target.scroll({ top: target.scrollHeight, behavior: 'smooth' });
        }
    }

    const CloseVoting = () => {
        setopen(false)
        setMsg('')
    }

    const handlesendmsg = (msg) => {
        setMsg(msg)
        setopen(true)
    }
    const handleKeyDown = (event, msg) => {
        if (event.key === 'Enter') {
            handlesendmsg(msg)
        }
    }

    return (
        <>
            <div className='comments'>
                <div className='comments__list' ref={CommentRef}>
                    {comments && comments.length > 0 ?
                        comments.map(comment =>
                            <div key={comment.id} className={'comment-item'}>
                                <div className="comment-item__content">
                                    <ListItem alignItems="flex-start">
                                        <ListItemAvatar>
                                            <Avatar alt={comment.user.full_name} src={comment.user.avatar ? comment.user.avatar : "/static/images/user.png"} width={15} height={15} />
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={
                                                <Box className='comment-item__content__top'>
                                                    <Box className='comment-item__content__title'>
                                                        <Typography>{comment.user.full_name}</Typography>
                                                        <div className="comment-item__content__rate">
                                                            <Rating name="read-only" value={comment.points} className={'comment-rate'} readOnly />
                                                        </div>
                                                    </Box>
                                                    <Typography className='comment-item__content__date'>{comment.created_at}</Typography>
                                                </Box>
                                            }
                                            secondary={
                                                <React.Fragment>
                                                    <div className="comment-item__job-title">
                                                        {comment.user.job_title ?
                                                            comment.user.company_name + '-' + comment.user.job_title
                                                            :
                                                            comment.user.company_name
                                                        }
                                                    </div>
                                                    <div className="comment-item__desc">
                                                        {comment.comment}
                                                    </div>
                                                    {/* {comment.user.full_name ==} */}
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItem>
                                </div>
                            </div>)
                        :
                        <Box className='nomessage-product'>
                            <img src={"/static/images/backgrounds/SVG/message.svg"} />
                            <Typography >
                                <MessageLang id="product.detailedcomments.alert.nothing" />
                            </Typography>
                        </Box>
                    }
                </div>
                <Sendbox_input
                    Value={value}
                    Msg={msg}
                    onchange={e => setMsg(e.target.value)}
                    Placeholder={messageLang("form.label.placeholder.enterText")}
                    Send={handlesendmsg}
                    Sendbykey={handleKeyDown}
                />
            </div>
            {mobileSize && open ?
                <GeneralDrawer
                    from="bottom"
                    onClose={() => setopen(false)}
                    isOpen={open}
                    title={<MessageLang id="product.detailedcomments.vote.title" />}
                    subTitle={<MessageLang id="product.detailedcomments.vote.subtitle" />}>
                    <Voting msg={msg} close={CloseVoting} product={product} from='product' back={Scrolltolastcomment} />
                </GeneralDrawer>
                :
                <Modal
                    onClose={() => {
                        setopen(false)
                    }}
                    isOpen={open}
                    title={<MessageLang id="product.detailedcomments.vote.title" />}
                    subTitle={<MessageLang id="product.detailedcomments.vote.subtitle" />}
                >
                    <Voting msg={msg} close={CloseVoting} product={product} from='product' back={Scrolltolastcomment} />
                </Modal>
            }
        </>
    )
}

export default ProductDetailedComments
