import React , {useState , useEffect,useRef} from 'react';
import { toastr } from 'react-redux-toastr';

/*******material *******************/
import { CardHeader , Grid ,AppBar , Toolbar , Button , Box } from '@material-ui/core';
// import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import CheckIcon from '@material-ui/icons/Check';
import { useTheme } from '@material-ui/core/styles';
/***end****material *******************/
import MessagesSkeleton from './MessagesSkeleton';
import agent from '../../app/api/agent'
import SendBox from './SendBox';
import emptyIcon from './empty.svg';
import Echo from 'laravel-echo';
import io from 'socket.io-client';
import InfiniteScroll from 'react-infinite-scroll-component';
/***********MeetingInvitationCreate **********/
import { loadMemberDetails } from '../../app/store/actions/standActions'
import MeetingInvitationCreate from '../meeting/meetingDashboard/MeetingInvitationCreate';
import { useDispatch } from 'react-redux'
/***********MeetingInvitationCreate **********/
import {loadNotificationCounter} from '../../app/store/actions/persistActions';
/*************change lang ***************/
import MessageLang , {messageLang} from '../../lang/';
/*************change lang ***************/

const ChatContent = ({selectedUser,memberId,user,handelClose}) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(true);
    const [data,setData] = useState(false);
    const [scrollData, setScrollData] = useState({
        page : 1,
        limit : 20,
        more : true,
        length : 0
    })
    const theme = useTheme();
    const chatlistRef= useRef(null);

    useEffect(() => {
        (async()=>{
            setLoading(true)
            const params = {
                page : 1,
                limit : scrollData.limit,
            }
            const messages = await agent.Chats.messagesList(selectedUser,params);
            if(messages.status){
                setData(messages.result)
                handelLength(messages.result)
                dispatch(loadNotificationCounter());
                setTimeout(() => {
                    hadnelScrollDown()
                }, 150);
                
            }
            setLoading(false)
        })()
    }, [selectedUser]);

    const handelLength = (items)=>{
        let count = 0
        for (const x of items.data) {
            for (const y of x.messages) {
                count += 1
            }
        }
        setScrollData(val=>{
            val.length = count
            return {...val}
        })
    }

    const hadnelScrollDown = ()=>{
        const target= chatlistRef.current;
        target.scroll({ top: target.scrollHeight, behavior: 'smooth' });
    }

    const handelSendMsg = async(value)=>{
        const response = await agent.Chats.createMessage(value, selectedUser);
        if(response.status){
            hadnelScrollDown()
        }
        else {

            toastr.success('', messageLang('General.alert.toast.errorServer'))
        }
    }
    
    useEffect(() => {
        let echo = new Echo({
            broadcaster: 'socket.io',
            client: io,
            host: "https://expo.namaline.ir",
            path: "/listener",
            transports:['websocket'],
            auth: {
                headers: {
                    Authorization: `Bearer ${user.token}`,
                    Accept: 'application/json',
                },
            },
        });
        if(selectedUser){
            const channelB = echo.private(`App.Conversation.${selectedUser}`)
            channelB.listen('.message.posted', (res => {
                if(data.data.length===0){
                    handelLength(res.payload)
                    setData({...res.payload})
                }
                else{
                    data.data[0].messages = [...data.data[0].messages,res.payload.data[0].messages[0]]
                    handelLength({...data})
                    setData({...data})
                }
            }))
        }
        return () => {
            echo.leave(`App.Conversation.${selectedUser}`);
        };
    }, [selectedUser,data])

    /***********MeetingInvitationCreate **********/
    const [openModal, setOpenModal] = useState(false);
    const [loadingHandel, setloadingHandel] = useState(false)
    const handelOpenMeeting = async(e)=>{
        e.stopPropagation();
        setloadingHandel(true)
        await dispatch(loadMemberDetails(memberId));
        setOpenModal(true);
        setloadingHandel(false)
    }
    const handelMoreMessage = async()=>{
        setLoading(false)
        const params = {
            page : scrollData.page + 1,
            limit : scrollData.limit,
        }
        const messages = await agent.Chats.messagesList(selectedUser,params);
        if(messages.status){
            data.data = [...data.data,...messages.result.data]
            setData({...data})
            handelLength({...data})
            setScrollData(item=>{
                item.page = item.page + 1
                return {...item}
            }) 
        }
    }
    /****end*******MeetingInvitationCreate **********/
    if(loading){
        return <MessagesSkeleton />
    }
    return (
        <Grid container className={'chat-list-container__box'}>
            
            <Grid item xs={12} className='chat-list-container__box__header'>
                <div className="header-chatroom">
                <AppBar position="relative" className="AppBar">
                    {data.audience && (
                        <Toolbar>
                            <CardHeader className='chattitle'
                                avatar={
                                    <div className="avatar" >
                                        <div className={data.audience.is_online === true ? "avatar__status" : "avatar__status avatar__status-offline" }></div>
                                        <img src={data && data.audience.avatar}/>
                                    </div>
                                }
                                title={<div className='chatlist-cardheader'>
                                    <span className="chattitle__title">{data && data.audience.full_name}</span>
                                    <span className="chattitle__subtitle">
                                        <MessageLang id="chat.chattitle__subtitle.lastVisit"/>: {data && data.audience.activity}
                                        </span>

                                </div>}
                            />
                            <Box className='livemeetingbtn'>
                                <Button variant="contained" className="bluebtn" color="primary" disabled={loadingHandel} onClick={handelOpenMeeting}><MessageLang id="chat.chatHeader.setVideoMeeting" /></Button>
                            </Box>
                            <div className="chattitle__back">
                                {theme.direction === 'ltr' ? <ChevronRightIcon style={{color:"#000"}} onClick={handelClose}/> : <ChevronLeftIcon style={{color:"#000"}} onClick={handelClose}/>}
                            </div>
                        </Toolbar>
                    )}
                </AppBar>
                </div>
            </Grid>
            <div className='chat-list-container__chatlists' id="chatlistRef" ref={chatlistRef}
                style={{
                    display: 'flex',
                    flexDirection: 'column-reverse',
                }}
                >
                {data.data.length === 0 ?
                    <div className="chat-list-container__img">
                        <img src={emptyIcon} alt="namaline" />
                    </div>
                :   
                <InfiniteScroll
                        dataLength={scrollData.length}
                        next={handelMoreMessage}
                        style={{ display: 'flex', flexDirection: 'column-reverse'}} 
                        inverse={true} 
                        hasMore={scrollData.length<=data.total}
                        scrollableTarget="chatlistRef"
                >
                <>
                {data.data.map((item, index) =>
                <Grid container className="chat-container" key={index}>
                    {item.date && (
                            <Grid item xs={12}>
                                <div className="chat-date">{item.date}</div>
                            </Grid>
                        )
                    }
                    {
                        item.messages && item.messages.map((msg, j) => {
                            if (msg.sender.id===user.currentUser.id) {
                                return (
                                    <Grid item xs={12}>
                                        <div className="send-msg">
                                            <div className={!msg.message.isFirst ? 'send-msg-content send-msg-content-space' : "send-msg-content"}>
                                                {msg.message.text}
                                                <span className="send-message-time">
                                                {msg.message.time}
                                                {msg.message.read ? <span className="read"><CheckIcon style={{ color:'#90caf9' }} fontSize="small" /></span>:<span><CheckIcon fontSize="small" /></span>}
                                                </span>
                                            </div>
                                        </div>
                                    </Grid>
                                )
                            }
                            else {
                                return (
                                    <div className="recieve-msg">
                                        <div className={"recieve-msg-content"}>
                                            {msg.message.text}
                                            <span className="recieve-msg-time">
                                                {msg.message.time}
                                            </span>
                                        </div>
                                    </div>
                                )
                            }
                        })
                    }
                </Grid>
                )}
                </>
                </InfiniteScroll>
                }
            </div>
            <SendBox onClick={(value) => handelSendMsg(value)}/>


            <MeetingInvitationCreate
                open={openModal}
                handleClose={()=>setOpenModal(false)}
            />
        </Grid>
    );
};

export default ChatContent
