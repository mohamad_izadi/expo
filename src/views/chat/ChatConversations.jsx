import React , {useState , useEffect} from 'react';

/*******material *******************/
import { CardHeader , Grid,Box } from '@material-ui/core';
/***end****material *******************/

/*********My custom Component*********************/
import SearchBox from '../../component/Search/SearchBox';
/***end******My custom Component*********************/

import ChatItemListSkeleton from './ChatItemListSkeleton';
import agent from '../../app/api/agent'
import Echo from 'laravel-echo';
import io from 'socket.io-client';
const ChatConversations = ({selectedUser,handelSelectUser,user}) => {
    const [loading, setLoading] = useState(false);
    const [data, setData] = useState([]);
    const [filterData, setFilter] = useState("");
    useEffect(() => {
        (async()=>{
            setLoading(true)
            const conversations = await agent.Chats.list();
            if(conversations.status){
                setData(conversations.result);
            }
            setLoading(false)
        })()
    }, [])
    useEffect(() => {
        let echo = new Echo({
            broadcaster: 'socket.io',
            client: io,
            host: "https://expo.namaline.ir",
            path: "/listener",
            transports:['websocket'],
            auth: {
                headers: {
                    Authorization: `Bearer ${user.token}`,
                    Accept: 'application/json',
                },
            },
        });
        if(user.currentUser){
            const channel = echo.private(`App.Chats.${user.currentUser && user.currentUser.id}`)
            channel.listen('.chat.created', (data => {
                setData(data.chats);
            }))
        }
      return () => {
        echo.leave(`App.Chats.${user.currentUser && user.currentUser.id}`);
      };
    }, [user.currentUser]);
    if(loading){
        return <ChatItemListSkeleton />
    }
    return (
        <>
        <Grid item xs={12}> 
            <SearchBox onChange={(ev)=>setFilter(ev.target.value.toLowerCase())}/> 
        </Grid>
        {data && data.filter((val)=>{
            if(filterData.length===0){
                return true
            }
            else{
                return val.audience.full_name.toLowerCase().includes(filterData)
            }
        }).map((item, index) => 
            <Grid item xs={12} key={index}>
                <Item item={item} selected={item.id === selectedUser} handelSelectUser={handelSelectUser}/>
            </Grid>
        )}
        </>
    );
};


const Item =({ selected, item ,handelSelectUser})=>{
    return(
        <div className={selected ? 'chat-item selected-chat-item' : "chat-item"} onClick={()=>handelSelectUser({chatId :item.id,memberId : item.audience.member_id})}>
                <CardHeader
                    avatar={
                        <div className="avatar" className="chat-item-img">
                            <div className={item.audience.is_online===true ? "chat-item-img__status" : "chat-item-img__status chat-item-img__status-offline" }></div>
                            <img src={item.audience.avatar} />
                        </div>
                    }
                    title={<div className='chatlist-cardheader'>
                        <Box>
                            <span className="chatlist-cardheader__title">{item.audience.full_name}</span>
                            <div className="chat-item-date">{item.message && item.message.created_at}</div>
                        </Box>
                        <span className="chatlist-cardheader__subtitle">{item.message && item.message.text}</span>
                    </div>}
                />
            
            
            {(item.unread > 0) && <div className="chat-item-not-read-count">{item.unread}</div>}

        </div>
    )
}

export default ChatConversations
