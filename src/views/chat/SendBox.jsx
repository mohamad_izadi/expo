import React, { useState } from 'react';

import 'emoji-mart/css/emoji-mart.css';
import data from './emoji.json'
import { NimblePicker  } from 'emoji-mart';
// import Picker from 'emoji-picker-react';
import { Popover } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';

/*************change lang ***************/
import {messageLang} from '../../lang/';
/*************change lang ***************/


function Imoji() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="23"
            height="23"
            x="0"
            y="0"
            enableBackground="new 0 0 510 510"
            version="1.1"
            viewBox="0 0 510 510"
            xmlSpace="preserve"
        >
            <path d="M344.25 229.5c20.4 0 38.25-17.85 38.25-38.25S364.65 153 344.25 153 306 170.85 306 191.25s17.85 38.25 38.25 38.25zm-178.5 0c20.4 0 38.25-17.85 38.25-38.25S186.15 153 165.75 153s-38.25 17.85-38.25 38.25 17.85 38.25 38.25 38.25zM255 408c66.3 0 122.4-43.35 145.35-102h-290.7C132.6 364.65 188.7 408 255 408zm0-408C114.75 0 0 114.75 0 255s114.75 255 255 255 255-114.75 255-255S395.25 0 255 0zm0 459c-112.2 0-204-91.8-204-204S142.8 51 255 51s204 91.8 204 204-91.8 204-204 204z" fill="#9f9f9f"></path>
        </svg>
    );
}
const SendBox = ({onClick}) => {
    const [value,setValue] = useState("");
    const handelSend = ()=>{
        if(value.length>0){
            onClick(value);
            setValue("");
        }
    }
    const handelOnKeyDown = (event)=>{
        if (event.key === 'Enter') {
            handelSend()
        }
    }
    
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [chosenEmoji, setChosenEmoji] = useState(null);
    const open = Boolean(anchorEl);
    const handleClickEmoji = (event) => {
        if(open){
            handleClose()
        }
        else{
            setAnchorEl(event.currentTarget);
        }
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const onSelectEmojiClick = (emoji, event) => {
        setValue(val=> `${val} ${emoji.native}`);
        handleClose()
    };
    
    return (
        <>
            <div className="send-box">
                <div className="send-box-emoji" onClick={handleClickEmoji}>
                    <Imoji />
                </div>
                <Popover
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                    onClose={handleClose}
                    open={open}
                    anchorEl={anchorEl}
                >
                    {/* <Picker
                        onEmojiClick={onSelectEmojiClick} 
                        disableSkinTonePicker={true} 
                        disableSearchBar={true}
                        groupVisibility={{
                            flags: false,
                            animals_nature : false
                        }}
                    /> */}
                    <NimblePicker 
                        data={data} 
                        emoji={null}
                        showPreview={false}
                        showSkinTones={false}
                        i18n={{
                            search : messageLang('form.label.placeholder.search'),
                            notfound : "",
                        }}
                        onClick={onSelectEmojiClick}
                    />
                </Popover>
                <input placeholder={messageLang("chat.sendBox.placeholder")} value={value} onChange={(ev)=>setValue(ev.target.value)} onKeyDown={handelOnKeyDown}/>
                <div className="send-box-icon" onClick={handelSend}>
                    <SendIcon />
                </div>

            </div>
        </>

    )
}

export default SendBox
