import React, { useEffect, useState, useRef } from 'react';

import {
    Card,
    makeStyles,
    Box,
    Grid
} from '@material-ui/core';

import ChatConversations from './ChatConversations';
import ChatContent from './ChatContent';
import emptyIcon from './empty.svg';

import { useSelector } from 'react-redux';

const ChatList = () => {
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
            height : "100%"
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },
        right: {
            height: '100vh',
            borderRight: '1px solid #eaeaea'
    
        },
        page: {
            height: '100%'
        }
    }));
    const classes = useStyles();

    const [selectedUser, setSelectedUser] = useState(false)
    const handelSelectUser = (id)=>{
        setSelectedUser(false)
        setTimeout(() => {
            setSelectedUser({...id})
        }, 1);
    }
    const { token, currentUser } = useSelector(state => state.auth);
    return (
    <Card className={classes.root}>
        <Grid container className={classes.page}>
            <Grid item md={4} xs={12}
                className={classes.right}
                className={`sidepage chat ${selectedUser===false ? "active":""}`}
            >
                <ChatConversations selectedUser={selectedUser.chatId} user={{currentUser,token}} handelSelectUser={(id)=>handelSelectUser(id)}/>
            </Grid>
            <Grid item md={8} xs={12} className={`chat-list-container ${selectedUser!==false ? "active":""}`}>
                {!selectedUser ?
                    (
                        <div className="chat-list-container">
                            <img src={emptyIcon} alt="namaline" />
                        </div>
                    ) :
                    <ChatContent selectedUser={selectedUser.chatId} memberId={selectedUser.memberId}  handelClose={()=>setSelectedUser(false)} user={{currentUser,token}}/>
                }
            </Grid>
        </Grid>
    </Card>
    )
}

export default React.memo(ChatList);


// fix redux , link ghorfe azaaa yadet bashe