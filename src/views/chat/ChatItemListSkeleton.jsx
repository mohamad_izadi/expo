import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    card: {
        maxWidth: 345,
        margin: theme.spacing(2),
    },
    media: {
        height: 190,
    },
}));

const ChatItemListSkeleton = () => {
    const classes = useStyles();

    return (
        <>
         <div className={"chat-item"}>
            <div className="chat-item-img">
                <Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />
            </div>
            <div className="chat-item-content">
                <h1><Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" /></h1>
                <p>
                     <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                    <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                </p>
            </div>
            <div className="chat-item-date">
                <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
            </div>
        </div>
            <div className={"chat-item"}>
                <div className="chat-item-img">
                    <Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />
                </div>
                <div className="chat-item-content">
                    <h1><Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" /></h1>
                    <p>
                        <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                        <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                    </p>
                </div>
                <div className="chat-item-date">
                    <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                </div>
            </div>
            <div className={"chat-item"}>
                <div className="chat-item-img">
                    <Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />
                </div>
                <div className="chat-item-content">
                    <h1><Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" /></h1>
                    <p>
                        <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                        <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                    </p>
                </div>
                <div className="chat-item-date">
                    <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                </div>
            </div>
            <div className={"chat-item"}>
                <div className="chat-item-img">
                    <Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />
                </div>
                <div className="chat-item-content">
                    <h1><Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" /></h1>
                    <p>
                        <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                        <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                    </p>
                </div>
                <div className="chat-item-date">
                    <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                </div>
            </div>
            <div className={"chat-item"}>
                <div className="chat-item-img">
                    <Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />
                </div>
                <div className="chat-item-content">
                    <h1><Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" /></h1>
                    <p>
                        <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                        <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                    </p>
                </div>
                <div className="chat-item-date">
                    <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="80%" />
                </div>
            </div>
       </>
    );
}

export default ChatItemListSkeleton
