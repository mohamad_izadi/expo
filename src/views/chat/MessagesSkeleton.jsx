import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Skeleton from '@material-ui/lab/Skeleton';
import Grid from '@material-ui/core/Grid';
const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    card: {
        maxWidth: 345,
        margin: theme.spacing(2),
    },
    media: {
        height: 190,
    },
}));

const MessagesSkeleton = () => {
    const classes = useStyles();

    return (
        <>
            <Grid item xs={12}>
            <div className={'header-chatroom'}>
                <AppBar position="relative" className={'AppBar'}>
                    <Toolbar>
                        <Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />
                        <Typography variant="h6" className={'title'}>
                            <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="20%" />
                            <Typography variant="h1" className={'subTitle'}>
                                <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={15} width="20%" />
                            </Typography>
                        </Typography>
                      
                        <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={50} width="20%" />
                    </Toolbar>
                </AppBar>
            </div>
                <Grid container className="chat-container">
                    <Grid item xs={12}>
                        <div className="chat-date-skeleton">
                            <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={45} width="30%" />
                        </div>
                    </Grid>
                    <div className="send-msg">
                        <Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />
                        <div className={"send-msg-content-skeleton"}>
                            <Skeleton style={{ marginRight: '10px',marginTop:'-10px',borderRadius:15 }} align="center" animation="wave" height={60} width="100%" />
                        </div>
                    </div>
                    <div className="recieve-msg-skeleton">
                        <div className={"send-msg-content-skeleton"}>
                            <Skeleton style={{ marginRight: '10px', marginTop: '-10px', borderRadius: 15 }} align="center" animation="wave" height={60} width="100%" />
                        </div>
                    </div>
                    <div className="recieve-msg-skeleton">
                        <div className={"send-msg-content-skeleton"}>
                            <Skeleton style={{ marginRight: '10px', marginTop: '-10px', borderRadius: 15 }} align="center" animation="wave" height={60} width="100%" />
                        </div>
                    </div>
                    <div className="recieve-msg-skeleton">
                        <div className={"send-msg-content-skeleton"}>
                            <Skeleton style={{ marginRight: '10px', borderRadius: 15 }} align="center" animation="wave" height={60} width="100%" />
                        </div>
                    </div>
                    <Grid item xs={12}>
                        <div className="chat-date-skeleton">
                            <Skeleton style={{ marginRight: '10px' }} align="center" animation="wave" height={45} width="30%" />
                        </div>
                    </Grid>
                    <div className="send-msg">
                        <Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />
                        <div className={"send-msg-content-skeleton"}>
                            <Skeleton style={{ marginRight: '10px', marginTop: '-10px', borderRadius: 15 }} align="center" animation="wave" height={60} width="100%" />
                        </div>
                    </div>
                    <div className="recieve-msg-skeleton">
                        <div className={"send-msg-content-skeleton"}>
                            <Skeleton style={{ marginRight: '10px', borderRadius: 15 }} align="center" animation="wave" height={60} width="100%" />
                        </div>
                    </div>
                </Grid>
            </Grid>
       </>
    );
}

export default MessagesSkeleton
