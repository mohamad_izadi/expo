import React from "react";
import {
  Grid,
  Container,
  Typography,
  Box,
} from "@material-ui/core";
import Header from "../FAQ/Header";
import Footer from "../Landing/Footer";
import Header_Menu from "../Landing/Header_Menu";
import imageHeader from "./header.jpg";


import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import { useSelector } from "react-redux";
import { dataEn, dataFa } from "./data.js";

import MessageLang from '../../lang';
import {messageLang} from '../../lang';
const Terms = () => {
  const { lang } = useSelector((state) => state.lang);
  return (
    <>
      <Header_Menu />
      <Header
        img={imageHeader}
        title={<MessageLang id="Terms.header.title"/>}
        text={{ text: <MessageLang id="Terms.header.text"/> }}
      >
        <Paper component="form" className="header-index__wrapper__box__root">
          <IconButton type="submit" className="iconButton" aria-label="search">
            <SearchIcon />
          </IconButton>
          <InputBase
            className="input"
            placeholder={messageLang("Terms.header.input")}
            inputProps={{ "aria-label": <MessageLang id="Terms.header.input"/>}}
          />
        </Paper>
      </Header>
      <Grid container className="index__faq">
        <Container maxWidth="lg">
          <Grid
            container
            alignItems="center"
            justify="center"
            direction="column"
            className="index__faq__top"
          >
            <Typography variant="h4" className="index__faq__top__title">
            <MessageLang id="Terms.body.title"/>
            </Typography>
            <Typography variant="body2" gutterBottom>
            <MessageLang id="Terms.body.subtitle"/>
            </Typography>
          </Grid>
          <Grid>
            <Box mt={5}>
            {lang === "en" ? (
              <Typography paragraph={true} variant="body2" dangerouslySetInnerHTML={{ __html: dataEn }} />
            ) : (
              <Typography paragraph={true} variant="body2" dangerouslySetInnerHTML={{ __html: dataFa }} />
            )}
            </Box>
          </Grid>
          {/* <Grid container justify="center" className="index__faq__questions">
            <Button variant="contained" className="index__faq__questions__btn">
              سایر قوانین و شرایط
            </Button>
          </Grid> */}
        </Container>
      </Grid>
      <Footer />
    </>
  );
};

export default Terms;
