import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import {
    Table, Typography, TableCell, TableContainer, TableHead, IconButton,
    TableRow, Paper, Box
} from '@material-ui/core'
import OpportunityItem from './OpportunityItem'
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import Sendrequest from './Sendrequest';
//pagination
import FooterPagination from '../../../component/FooterPagination/FooterPagination'

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles((theme) => ({
    root: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
    },
    table: {
        minWidth: '820px',
    },

}))


const ALLOpportunities = ({job}) => {
    const classes = useStyles();
    const { opportunities } = useSelector((state) => state.opportunities);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(3);
    const lang = useSelector((state) => state.lang.lang);

    const[open,setOpen]=useState(false)
    const[requestid,setrequestid]=useState(0)

    const showrequestFunc=(id)=>{
        setrequestid(id)
        setOpen(true)
    }

    const showallopportunities=(x)=>{
        setOpen(false)
    }
    const handleChangePage = async (event, newPage) => {
        // await dispatch(setCompanyFilters('page', newPage))
        // dispatch(loadCompanies())
    };

    return (
        <>
        {!open ?
            <Box className='opportunitypage'>
                <FooterPagination
                    className='products_footer' 
                    lang={lang}
                    total={'10'} 
                    limit={'10'} 
                    page={'1'}
                    ChangePage={handleChangePage}
                />
                <Box className='opportunitypage__content'>
                    <TableContainer component={Paper} className="table_desgin opportunitypage__content__table">
                        <Table aria-label="simple table ">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left" width='60%'>
                                        {job ? 
                                            <MessageLang id="Oppotunity.allOportunities.jobPosition" />
                                        :
                                            <MessageLang id="Oppotunity.allOportunities.investment" />
                                        }
                                    </TableCell>
                                    <TableCell align="left" width='20%' ><MessageLang id="Oppotunity.allOportunities.company" /></TableCell>
                                    <TableCell align="left" width='20%'>
                                        {job ? 
                                            <MessageLang id="Oppotunity.allOportunities.City" />
                                        :
                                            <MessageLang id="Oppotunity.allOportunities.Category" />
                                        }
                                        
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                        </Table>
                    </TableContainer>
                    <Paper >
                        {opportunities && opportunities.length>0 ? opportunities.map((row, index) => (
                            <OpportunityItem item={row} key={index} requestform={showrequestFunc} job={job}/>))
                            :
                            <div className='exhebition-emptypages tabcontent-stand'>
                                <Box>
                                    <img src={"/static/images/backgrounds/SVG/lectures.svg"} />
                                    <Typography >
                                        <MessageLang id="Oppotunity.allOportunities.alert" />
                                    </Typography>
                                </Box>
                            </div>
                        }
                    </Paper >
                </Box>
            </Box>
        :
            <Sendrequest item={requestid} back={showallopportunities} job={job}/>
         }
        </>
    );
}

export default ALLOpportunities




















