import React, { useState, useEffect } from 'react';
import {
    Card,
    Grid,
    AppBar,
    Tabs,
    Tab,
    Hidden
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import SearchBox from '../../../component/Search/SearchBox';
import SwipeableViews from 'react-swipeable-views';
import OpportunityFilter from './OpportunityFilter';
import SuggestionsList from './SuggestionsList';
import ALLOpportunities from './ALLOpportunities';
import Sendrequest from './Sendrequest';
import { fetcOpportunities, fetcMyOpportunities, setopportunityFilters } from '../../../app/store/actions/opportunitiesActions'

import StandListPreload from '../../exhibition/CoorporationSkeleton'
import GeneralDrawer from '../../../component/Drawer/GeneralDrawer.jsx'
import { Modal } from '../../../component';

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTableCell-root': {
            fontFamily: 'IRANSansFa',
            padding: '8px'
        },
        '& .MuiTableCell-head': {
            padding: '16px'
        },
        '& .MuiTabs-root': {
            height: '100%'
        },
        '& .MuiTabs-flexContainer': {
            height: '100%'
        },
        '& .makeStyles-search-36': {
            borderRadius: '0'
        },
        '& .MuiCardHeader-root': {
            padding: '0'
        },
        '& .MuiSelect-selectMenu': {
            padding: '10px 10px'
        }
    },
    swipeableViews: {
        width: '100%'
    },
    avatar: {
        marginRight: theme.spacing(2)
    },
    '& .MuiTab-wrapper': {
        fontSize: 16
    },
    shadow: {
        boxShadow: 'none'
    }
}));

export default function OpportunityDashboard({ className, ...rest }) {
    const classes = useStyles();
    const theme = useTheme();
    const dispatch = useDispatch();
    const [ShowFilter, SetShowFilter] = useState(false)
    const { opportunities, loading } = useSelector((state) => state.opportunities);

    const pageheight = window.innerHeight - 130
    let mobileSize = window.innerWidth < 600 ? true : false

    let job = true
    if ((window.location.pathname).includes('investmentopps')) {
        job = false
    }


    const [value, setValue] = useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    function handleSearch(tab, type) {
        dispatch(setopportunityFilters('search', type))
        if (job) {
            if (tab == 0) {
                dispatch(fetcOpportunities('job'))
            }
            else {
                dispatch(fetcMyOpportunities(0, 'job'))
            }
        }
        else {
            dispatch(fetcOpportunities('investment'));
        }
    }


    useEffect(() => {
        if (job) {
            dispatch(fetcOpportunities('job'));
            dispatch(fetcMyOpportunities(0, 'job'))
        }
        else {
            dispatch(fetcOpportunities('investment'));
            dispatch(fetcMyOpportunities(0, 'investment'))
        }

    }, [dispatch])

    function TabPanel(props) {
        const { children, value, index, ...other } = props;

        return (
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`full-width-tabpanel-${index}`}
                aria-labelledby={`full-width-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <>
                        {children}
                    </>
                )}
            </div>
        );
    }

    TabPanel.propTypes = {
        children: PropTypes.node,
        index: PropTypes.any.isRequired,
        value: PropTypes.any.isRequired,
    };

    function a11yProps(index) {
        return {
            id: `full-width-tab-${index}`,
            'aria-controls': `full-width-tabpanel-${index}`,
        };
    }

    const Filter = job ?
        value === 0 ? <OpportunityFilter job={job}
            onChange={(event) => {
                dispatch(fetcOpportunities('job'));
            }}
        /> : <OpportunityFilter
            onChange={(event) => {
                dispatch(fetcMyOpportunities(1, event.value))
                // dispatch(fetcMyOpportunities(1, 'job'))
            }}
        />
        :
        value === 0 ? <OpportunityFilter job={job}
            onChange={(event) => {
                dispatch(fetcOpportunities('investment'));
            }}
        /> : <OpportunityFilter
            onChange={(event) => {
                dispatch(fetcMyOpportunities(1, event.value))
            }}
        />

    return (
        <>
            <Card
                className={clsx(classes.root, className)}
                {...rest}
            >
                <Grid container>
                    <Grid container >
                        <AppBar position="static" className={classes.shadow}>
                            <Tabs
                                value={value}
                                onChange={handleChange}
                                className='tab_header fftabs'
                            >
                                <Tab label={<MessageLang id="Oppotunity.opportunityDashboard.tab1" />} {...a11yProps(0)} />
                                <Tab label={<MessageLang id="Oppotunity.opportunityDashboard.tab2" />} {...a11yProps(1)} />
                            </Tabs>
                        </AppBar>
                    </Grid>
                    <Grid item md={3} xs={12} className="stand-list__filter-panel">
                        <Grid container>
                            <Grid item xs={12}>
                                <SearchBox HandlerFilter={() => SetShowFilter(true)} onChange={(e) => handleSearch(value, e.target.value)} />
                            </Grid>
                            {ShowFilter && mobileSize ?
                                <GeneralDrawer from="bottom"
                                    onClose={() => SetShowFilter(false)}
                                    isOpen={ShowFilter}
                                    title={job ? <MessageLang id="Oppotunity.OppotunityDashboard.searchtitle.job" /> : <MessageLang id="Oppotunity.OppotunityDashboard.searchtitle.investment" />}
                                >
                                    {Filter}
                                </GeneralDrawer>
                                :
                                <Modal
                                    onClose={() => SetShowFilter(false)}
                                    isOpen={ShowFilter}
                                    title={job ? <MessageLang id="Oppotunity.OppotunityDashboard.searchtitle.job" /> : <MessageLang id="Oppotunity.OppotunityDashboard.searchtitle.investment" />}
                                    className={classes.root}
                                >
                                    {Filter}
                                </Modal>
                            }
                            <Hidden only={['sm', 'xs']}>
                                <Grid item xs={12}>
                                    {Filter}
                                </Grid>
                            </Hidden>
                        </Grid>
                    </Grid>
                    <Grid item md={9} xs={12} className={`${classes.standContent} rightside`}>
                        <SwipeableViews
                            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                            index={value}
                            onChangeIndex={handleChangeIndex}
                            className={classes.swipeableViews}
                        >
                            <TabPanel value={value} index={0} dir={theme.direction}>
                                {loading ? <StandListPreload /> : <ALLOpportunities job={job} />}
                            </TabPanel>
                            <TabPanel value={value} index={1} dir={theme.direction}>
                                {loading ? <StandListPreload /> : <SuggestionsList job={job} />}
                            </TabPanel>
                        </SwipeableViews>
                    </Grid>
                </Grid>
            </Card >
        </>
    );
}