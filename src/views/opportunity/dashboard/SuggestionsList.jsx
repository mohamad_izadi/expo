import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {
    Table, TableBody, TableCell, TableContainer, TableHead, IconButton,
    TableRow, Paper, Typography, Box
} from '@material-ui/core'
import OpportunityItem from './OpportunityItem'
import Sendrequest from './Sendrequest'
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import { fetcMyOpportunities } from '../../../app/store/actions/opportunitiesActions'
//pagination
import FooterPagination from '../../../component/FooterPagination/FooterPagination'
/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%'
    },
    table: {
        minWidth: '820px',
    },
}))



const ALLOpportunities = ({job}) => {
    const classes = useStyles();
    const { myOpportunities } = useSelector((state) => state.opportunities);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(3);
    const lang = useSelector((state) => state.lang.lang);

    const [open, setOpen] = useState(false)
    const [requestid, setrequestid] = useState(0)

    const dispatch = useDispatch();

    const showrequestFunc = (id) => {
        setrequestid(id)
        setOpen(true)
    }

    const showallopportunities = (x) => {
        setOpen(false)
    }

    const handleChangePage = async (event, newPage) => {
        // await dispatch(setCompanyFilters('page', newPage))
        // dispatch(loadCompanies())
    };

    return (
        <Box className='opportunitypage'>
            {myOpportunities && myOpportunities[0] ?
                <div>
                    {!open ?
                        <div>
                            <FooterPagination
                                className='products_footer' 
                                lang={lang}
                                total={'10'} 
                                limit={'10'} 
                                page={'1'}
                                ChangePage={handleChangePage}
                            />
                            <Box className='opportunitypage__content' >

                                <>
                                    <TableContainer component={Paper} className="table_desgin opportunitypage__content__table">
                                        <Table aria-label="simple table ">
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell align="left" width='60%'><MessageLang id="Oppotunity.allOportunities.jobPosition" /></TableCell>
                                                    <TableCell align="left" width='20%' ><MessageLang id="Oppotunity.allOportunities.company" /></TableCell>
                                                    <TableCell align="left" width='20%'><MessageLang id="Oppotunity.allOportunities.City" /></TableCell>
                                                </TableRow>
                                            </TableHead>
                                        </Table>
                                    </TableContainer>
                                    <Paper >
                                        {(myOpportunities).map((row, index) => (
                                            <OpportunityItem item={row.opportunity} data={row} key={index} from={'suggest'} requestform={showrequestFunc} job={job}/>
                                        ))}
                                    </Paper >
                                </>
                            </Box>
                        </div>
                        :
                        <Sendrequest item={requestid} back={showallopportunities} from={'suggestionlist'} job={job}/>}
                </div>
                :
                <div className='exhebition-emptypages tabcontent-stand'>
                    <Box>
                        <img src={"/static/images/backgrounds/SVG/lectures.svg"} />
                        <Typography >
                            <MessageLang id="Oppotunity.allOportunities.alert" />
                        </Typography>
                    </Box>
                </div>
            }

        </Box>
    );
}

export default ALLOpportunities




















