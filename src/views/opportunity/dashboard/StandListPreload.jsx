import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import {
    Table, TableBody, TableCell, TableContainer, TableHead,
    TableRow, Paper, Avatar, CardHeader
} from '@material-ui/core'
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles({
    root: {},
    head: { padding: '20px 16px 21px !important' },
    table: {
        minWidth: '820px',
    },
});

const StandListPreload = () => {
    const classes = useStyles();

    return (
        <TableContainer className={classes.root} component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell className={classes.head} align="left" width='30%'>
                            <Skeleton align="left" animation="wave" height={15} width="40%" />
                        </TableCell>
                        <TableCell align="center" width='35%'>
                            <Skeleton style={{ margin: '0 auto' }} align="center" animation="wave" height={15} width="80%" />
                        </TableCell>
                        <TableCell align="center" width='15%'>
                            <Skeleton style={{ margin: '0 auto' }} align="center" animation="wave" height={15} width="80%" />
                        </TableCell>
                        <TableCell align="center" width='20%'>
                            <Skeleton style={{ margin: '0 auto' }} align="center" animation="wave" height={15} width="80%" />
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow>
                        <TableCell component="th" scope="row">
                            <CardHeader
                                avatar={<Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />}
                                title={<Skeleton animation="wave" height={15} width="80%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="40%" />}
                            />
                        </TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="80%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell component="th" scope="row">
                            <CardHeader
                                avatar={<Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />}
                                title={<Skeleton animation="wave" height={15} width="80%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="40%" />}
                            />
                        </TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="80%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell component="th" scope="row">
                            <CardHeader
                                avatar={<Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />}
                                title={<Skeleton animation="wave" height={15} width="80%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="40%" />}
                            />
                        </TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="80%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell component="th" scope="row">
                            <CardHeader
                                avatar={<Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />}
                                title={<Skeleton animation="wave" height={15} width="80%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="40%" />}
                            />
                        </TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="80%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell component="th" scope="row">
                            <CardHeader
                                avatar={<Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />}
                                title={<Skeleton animation="wave" height={15} width="80%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="40%" />}
                            />
                        </TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="80%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell component="th" scope="row">
                            <CardHeader
                                avatar={<Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />}
                                title={<Skeleton animation="wave" height={15} width="80%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="40%" />}
                            />
                        </TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="80%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell component="th" scope="row">
                            <CardHeader
                                avatar={<Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />}
                                title={<Skeleton animation="wave" height={15} width="80%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="40%" />}
                            />
                        </TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="80%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell component="th" scope="row">
                            <CardHeader
                                avatar={<Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />}
                                title={<Skeleton animation="wave" height={15} width="80%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="40%" />}
                            />
                        </TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="80%" style={{ margin: '0 auto' }} /></TableCell>
                        <TableCell align="center"><Skeleton align="center" animation="wave" height={15} width="40%" style={{ margin: '0 auto' }} /></TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default StandListPreload