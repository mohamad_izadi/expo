import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux'
import {
    SelectInput, Switch
} from '../../../component'
import { makeStyles } from '@material-ui/core/styles';
import { MenuItem } from '@material-ui/core'
import { useDispatch } from 'react-redux';
import { clearFilters, fetcOpportunities, setopportunityFilters } from '../../../app/store/actions/opportunitiesActions';
/*************change lang ***************/
import MessageLang from '../../../lang';
/*************change lang ***************/


const useStyles = makeStyles({
    root: {
    },
});

const OpportunityFilter = ({ job }) => { //({ selected, number, onClick }) => {
    const { companies, abilities, interests, positions, countries } = useSelector(state => state.persist)
    const [sort, setsort] = useState(false)
    const classes = useStyles()
    const dispatch = useDispatch()

    // useEffect(() => {
    //     dispatch(fetcOpportunities())
    //     return () => {
    //         dispatch(clearFilters())
    //     }
    // }, [dispatch])


    function handleChangeFilter(value, type) {
        if (job) {
            if (type.action === "select-option") {
                dispatch(setopportunityFilters(type.name, value.value))
                dispatch(fetcOpportunities('job'));
            } else if (type.action === "clear") {
                dispatch(setopportunityFilters(type.name, 0))
                dispatch(fetcOpportunities('job'));

            }
        }
        else {
            if (type.action === "select-option") {
                dispatch(setopportunityFilters(type.name, value.value))
                dispatch(fetcOpportunities('investment'));
            } else if (type.action === "clear") {
                dispatch(setopportunityFilters(type.name, 0))
                dispatch(fetcOpportunities('investment'));
            }
        }
    }

    const switchhandleChange = (event) => {
        // if(event.target.checked === true){
        //     alert('true')
        // }
        setsort(event.target.checked);
    };

    return (
        <div className={classes.root}>
            <div className="stand-list__filter-list Responsive_Filter_Mood">
                <div className="stand-list_filter-member">
                    {/* <Switch 
                        // onChange={(e)=>switchhandleChange()}
                        label={<MessageLang id="Oppotunity.opportunityDashboard.switch"/>} 
                        /> */}

                    <SelectInput
                        name="company"
                        label={<MessageLang id="Oppotunity.opportunityDashboard.company" />}
                        isClearable={true}
                        options={companies && companies.map(company =>
                            ({ value: company.id, label: company.name }))}
                        onChange={(value, type) => handleChangeFilter(value, type)}
                    />

                    <SelectInput
                        name="position"
                        label={<MessageLang id="Oppotunity.allOportunities.jobPosition" />}
                        isClearable={true}
                        options={positions && positions.map((pos, index) =>
                            ({ value: pos.id, label: pos.name }))}
                        onChange={(value, type) => handleChangeFilter(value, type)}
                    />

                    {/* <SelectInput
                        name="location"
                        label={<MessageLang id="Oppotunity.opportunityDashboard.location"/>}
                        isClearable={true}
                        options={countries && countries.map(country =>
                            ({ value: country.id, label: country.name }))}
                        onChange={(value, type) => handleChangeFilter(value, type)}
                    /> */}

                </div>
            </div >
        </div >
    )
}

export default OpportunityFilter
