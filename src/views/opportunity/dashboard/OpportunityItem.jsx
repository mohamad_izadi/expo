import React,{useState,useEffect} from 'react';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Button from '@material-ui/core/Button';
import { Typography , Box } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useDispatch } from 'react-redux';
import { deleteOpportunity , fetcMyOpportunities} from '../../../app/store/actions/opportunitiesActions'
import ConfirmModal from '../../../component/ConfirmModal/ConfirmModal'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
//icon
import ApartmentIcon from '@material-ui/icons/Apartment';
import EmailIcon from '@material-ui/icons/Email';
import Done from '@material-ui/icons/Done';
import ViewList from '@material-ui/icons/ViewList';

/*************change lang ***************/
import MessageLang ,{messageLang} from '../../../lang/';
/*************change lang ***************/

const OpportunityItem = ({ item , from , requestform ,data,Delet_id ,job}) => {
    const[open,setOpen]=useState(false)
    const [loading, setLoading] = useState(false)
    const [confirm,setconfirm]=useState(false)
    const [cancleitem,setcancleitem]=useState(0)
    const dispatch = useDispatch();
  const theme = useTheme();
    
    const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

    const showconfirm=(id)=>{
        setconfirm(true)
        setcancleitem(id)
    }

    const close =()=>{
        setconfirm(false)
    }

    async function Canclerequest() {
        try {
            setLoading(true)
            dispatch(await deleteOpportunity(cancleitem , job))
        } catch (error) {
            console.log(error.message);
        } finally {
            setLoading(false)
        }
    }

    return (
        <div className="opportunity-item">
            <div className="opportunity-item__top">
                <div className="opportunitiestitle">{item.title}</div>
                <div className="opportunity-item__top__job">
                    <div className="opportunity-item__top__location">
                        <div><ApartmentIcon /></div> <div className='opportunity-item__top__location__overflow'>
                            {item.company.name ? item.company.name : <MessageLang id="DashboardLayout.header.pearl_petrochemical"/>}</div>
                    </div>
                    {job ?
                        <div className="opportunity-item__top__location"><div><LocationOnIcon /></div> <div>{item.location ? item.location.name : <MessageLang id="Oppotunity.opportunityDashboard.location.tehran"/>}</div></div>
                    :
                        item.company.category ?<div className="opportunity-item__top__location"><div><ViewList /></div> <div>{ item.company.category }</div></div> :null
                    }

                </div>
            </div>
            <div className="opportunity-item__description">
                
                {
                    item.description ? item.description : <MessageLang id="Oppotunity.opportunityItem.description"/>
                }
            </div>
            {from == 'suggest' ?
                <Box>
                    <Button
                        className='mysuggest'
                        variant="contained"
                        onClick={() => requestform(data)}
                        startIcon={<Done />}
                    >
                        {
                            loading ? <CircularProgress className="opportunity-item__loading" /> : <MessageLang id="Oppotunity.item.btn-sendrequest"/>
                        }
                    </Button>
                    <Button className="canclebtn" 
                    onClick={()=>showconfirm(data.id)}><MessageLang id="Oppotunity.item.btn-cancele"/></Button>

                </Box>
            :
                item.is_requested ?
                
                <Button
                    className='mysuggest autowith'
                    variant="contained"
                    startIcon={<Done />}
                >
                    {
                        loading ? <CircularProgress className="opportunity-item__loading" /> : <MessageLang id="Meeting.meetingDashboard.meetingInventation.alertOne"/>
                    }
                </Button>
                :
                <Button
                    className='allsuggest'
                    variant="contained"
                    onClick={() => {
                        if (!item.is_requested) {
                            requestform(item)
                        }
                    }}
                    startIcon={ item.is_requested ? <Done/> : <EmailIcon />}
                >
                    {
                        loading ? <CircularProgress className="opportunity-item__loading" /> :<MessageLang id="exhibition.cooperationItem.loadingtwo"/>
                    }
                </Button>
            }
            {confirm?
            <ConfirmModal title={messageLang("exhibition.cooperationItem.loadingThere")} agree={messageLang("exhibition.cooperationItem.loadingYes")} disagree={messageLang("exhibition.cooperationItem.loadingNo")} close={close} accept={Canclerequest}/> :null} 
        </div>
    )
}

export default OpportunityItem
