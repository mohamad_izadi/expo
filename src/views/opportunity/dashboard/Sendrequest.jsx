import React, { useState, useEffect } from 'react'
import { Button, Toolbar, AppBar, Box, Typography, IconButton, TextField, OutlinedInput, FormControl } from '@material-ui/core';
import Chip from '@material-ui/core/Chip';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { createOpportunity } from '../../../app/store/actions/opportunitiesActions'
import CircularProgress from '@material-ui/core/CircularProgress';

import { deleteOpportunity } from '../../../app/store/actions/opportunitiesActions'
import { fetcMyOpportunities, fetcJobOpportunities } from '../../../app/store/actions/opportunitiesActions'
import ConfirmModal from '../../../component/ConfirmModal/ConfirmModal'
//icon
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import ApartmentIcon from '@material-ui/icons/Apartment';
import EmailIcon from '@material-ui/icons/Email';
import ViewList from '@material-ui/icons/ViewList';
//alert
import { Alert, AlertTitle } from '@material-ui/lab';
//file download
import investmentoppsform from '../files/investmentopps.pdf'
import joboppsform from '../files/jobopps.pdf'
//upload
import FileUpload from '../../../component/FileUpload/FileUpload'

/*************change lang ***************/
import MessageLang, { messageLang } from '../../../lang/';
/*************change lang ***************/



const Sendrequest = ({ item, back, from, pavilion_id, job }) => {
    const [loading, setLoading] = useState(false)
    const [confirm, setconfirm] = useState(false)
    const [file, setfile] = useState();
    const [fileName, setfileName] = useState();
    const [showfilename, setshowfilename] = useState('');

    const [description, setdescription] = useState(from == 'suggestionlist' ? item.description : '')
    var item = from == 'suggestionlist' ? item.opportunity : item

    const history = useHistory()
    const dispatch = useDispatch();


    async function createItem(id, description) {
        try {
            setLoading(true)
            dispatch(await createOpportunity(id, description, from, pavilion_id, job, file))
            if (from == 'stand') {
                back()
            }
        } catch (error) {
            console.log(error.message);
        } finally {
            setLoading(false)
            setdescription('')
        }
    }

    const ShowConfirm = () => {
        setconfirm(true)
    }
    const close = () => {
        setconfirm(false)
    }


    const Upload = (e, v) => {
        if (e.target.files[0].name) {
            let file_type = e.target.files[0].type
            if ((file_type && file_type.split("/")[1]) === 'pdf' ||
                file_type.split("/")[1] === 'docx' ||
                file_type.split("/")[1] === 'doc' ||
                file_type.split("/")[1] === 'application/msword' ||
                file_type.split("/")[1] === 'vnd.openxmlformats-officedocument.wordprocessingml.document') {
                setfile(e.target.files[0])
                setshowfilename(e.target.files[0].name)
            }
            else {
                setshowfilename("type")
            }
        }
    }

    return (
        <Box className='opportunitypage'>
            <div className='send-request'>
                <Box className='send-request__header'>
                    <Typography className="send-request__title">
                        {job ?
                            <MessageLang id="Oppotunity.allOportunities.sendrequest_job" />
                            :
                            <MessageLang id="Oppotunity.allOportunities.sendrequest_investment" />
                        }
                    </Typography>
                    <IconButton className={'back-icon'} onClick={() => back(false)}>
                        <ArrowBackIosIcon fontSize="small" />
                    </IconButton>
                </Box>
                <Box className='send-request__iconinfo'>
                    <div className="opportunity-item__top__job">
                        <div className="opportunity-item__top__location">
                            <div><ApartmentIcon /></div> <div>{item.company ? item.company.name : <MessageLang id="DashboardLayout.header.pearl_petrochemical" />}</div>
                        </div>

                        {job ?
                            <div className="opportunity-item__top__location">
                                <div><LocationOnIcon /></div>
                                <div>{item.location ? item.location.name : <MessageLang id="exhibition.cooperationItem.tehrann" />}</div>
                            </div>
                            :
                            item.company.category ? <div className="opportunity-item__top__location"><div><ViewList /></div> <div>{item.company.category}</div></div> : null
                        }
                    </div>
                </Box>
                <Box className='send-request__content'>
                    {item.expertises ?
                        <Box className='send-request__content__tags'>
                            {item.expertises.map((item, index) => (<Chip key={index} label={item.name} variant="outlined" />))}
                        </Box>
                        : null}
                    <Typography className='send-request__content__desc'>
                        {item.description ? item.description : <MessageLang id="Oppotunity.opportunityItem.description" />}
                    </Typography>
                    <Box className="oppsdownloadbtn text-center">
                        <Alert severity="info">
                            <AlertTitle>{job ? <MessageLang id="exhibition.rightSide.lable2" /> : <MessageLang id="exhibition.rightSide.lable3" />}</AlertTitle>
                            <span style={{ lineHeight: "1.7" }}>
                                {job ? <MessageLang id="Oppotunity.item.btn-sendrequest-alert_desc_job" /> : <MessageLang id="Oppotunity.item.btn-sendrequest-alert_desc_invesment" />}
                                {job ? "" : <a type="button"
                                    href={job ? joboppsform : investmentoppsform}
                                    download="OpportunityInvesment.pdf">
                                    <strong><MessageLang id="Oppotunity.item.btn-sendrequest-alert_download" /></strong>
                                </a>}
                            </span>
                        </Alert>
                    </Box>
                    <Box className='send-request__content__input'>
                        <p><MessageLang id="Oppotunity.sendrequest.requestbox" /></p>
                        <FormControl fullWidth variant="outlined">
                            <OutlinedInput
                                id="outlined-adornment-amount"
                                value={description}
                                onChange={(event) => {
                                    setdescription(event.target.value)
                                }}
                                multiline
                                rows={8}
                            />
                        </FormControl>

                    </Box>
                    {from === 'suggestionlist' ?
                        <Box> </Box>
                        :
                        <Box className='uploadbox'>
                            <FileUpload
                                formatfile={"application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"}
                                multiple={false}
                                showfilename={showfilename}
                                Upload_Func={Upload}
                            />
                            <Button
                                className={from === 'stand' ?
                                    showfilename === '' || description === '' || showfilename == "type" ? 'standsuggestdisable standsuggest' : 'standsuggest' :
                                    showfilename === '' || description === '' || showfilename == "type" ? 'disabledbtn' : 'allsuggest'}
                                variant="contained"
                                onClick={() => ShowConfirm()}
                                disabled={showfilename === '' || description === '' || showfilename == "type" ? true : false}
                                startIcon={<EmailIcon />}
                            >
                                {
                                    loading ? <CircularProgress className="opportunity-item__loading" /> : <MessageLang id="exhibition.cooperationItem.loadingtwo" />
                                }
                            </Button>
                        </Box>
                    }

                </Box>
            </div >
            {confirm ?
                <ConfirmModal title={messageLang("Oppotunity.sendrequest.confirm")}
                    agree={messageLang("Oppotunity.sendrequest.agree")}
                    disagree={messageLang("Oppotunity.sendrequest.disagree")}
                    close={close}
                    accept={() => createItem(item.id, description)} />
                : null}

        </Box>
    );
}

export default Sendrequest




















