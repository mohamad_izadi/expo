import React from 'react';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import { Box, CircularProgress } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux'
import { requestCode } from '../../app/store/actions/authActions.js';
import { useStyles } from './_auth.js'
import { Dots, TextInput, Button } from '../../component'
import SelectLanguage from './SelectLanguage'
//phone
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/material.css'
/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const RegisterStepOne = ({ history }) => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const { loading } = useSelector((state) => state.async);
    const lang = useSelector((state) => state.lang.lang);
    return (
        <>
            <Formik
                initialValues={
                    {
                        last_name: '',
                        first_name: '',
                        phone_number: '',
                        email: '',
                        state: lang,
                    }}
                validationSchema={lang === 'fa' ?
                    Yup.object({
                        first_name: Yup.string().required(<MessageLang id="form.verify.register.name" />),
                        last_name: Yup.string().required(<MessageLang id="form.verify.register.lastname" />),
                        phone_number: Yup.string().required(<MessageLang id="form.verify.register.phone" />),
                        // phone_number: Yup.string().required(lang ==="fa"?<MessageLang id="form.verify.register.phone"/>:<MessageLang id="form.verify.register.email"/>),
                    })
                    :
                    Yup.object({
                        first_name: Yup.string().required(<MessageLang id="form.verify.register.name" />),
                        last_name: Yup.string().required(<MessageLang id="form.verify.register.lastname" />),
                        email: Yup.string().required(<MessageLang id="form.verify.register.email" />).email(<MessageLang id="form.verify.register.email-valid" />),
                    })
                }
                onSubmit={async (values, { setSubmitting, setErrors }) => {
                    try {
                        await dispatch(requestCode(values))
                        setSubmitting(false);
                    } catch (error) {
                        setErrors({ auth: error.data.messages[0] });
                        setSubmitting(false);
                    }
                }}
            >
                {({ submitForm,
                    errors,
                    handleBlur,
                    handleChange,
                    handleSubmit,
                    isSubmitting,
                    touched,
                    values, isValid, dirty }) => (
                    <Form className='ui form countrycode'>
                        <div className="auth__title" >
                            <MessageLang id="register.panel.head.form_head" />
                        </div>
                        {/* {loading && <LinearProgress color='primary' size={26} />} */}
                        {errors.auth && <span className={'error-alert'}>{errors.auth}</span>}
                        <TextInput
                            error={Boolean(touched.first_name && errors.first_name)}
                            helperText={touched.first_name && errors.first_name}
                            className={classes.labelText}
                            label={<MessageLang id="form.label.register.name" />}
                            margin="normal"
                            name="name"
                            onBlur={handleBlur}
                            onChange={handleChange('first_name')}
                            type="text"
                            value={values.first_name}

                        />
                        <TextInput
                            error={Boolean(touched.last_name && errors.last_name)}
                            helperText={touched.last_name && errors.last_name}
                            className={classes.labelText}
                            label={<MessageLang id="form.label.register.family" />}
                            margin="normal"
                            name="last_name"
                            onBlur={handleBlur}
                            onChange={handleChange('last_name')}
                            type="text"
                            value={values.last_name}

                        />
                        {lang === "fa" ?
                            <PhoneInput
                                error={Boolean(touched.phone_number && errors.phone_number)}
                                helperText={touched.phone_number && errors.phone_number}
                                className={classes.labelText}
                                margin="normal"
                                name="phone_number"
                                onBlur={handleBlur}
                                onChange={handleChange('phone_number')}
                                type="number"
                                value={values.phone_number}
                                variant="outlined"
                                country='ir'
                                autoFormat={true}
                                enableSearch={true}
                                disableSearchIcon={false}
                                autocompleteSearch={true}
                                countryCodeEditable={false}
                                specialLabel={<MessageLang id="form.label.login.phone" />}
                                inputProps={{
                                    required: true,
                                    autoFocus: false,
                                }}
                            // isValid={(inputNumber, country, countries) => {
                            //     setcode(country.dialCode)
                            // }}
                            />
                            :
                            <TextInput
                                error={Boolean(touched.email && errors.email)}
                                helperText={touched.email && errors.email}
                                className={classes.labelText}
                                label={<MessageLang id="Profile.profileEditExtra.textInput.lable2" />}
                                margin="normal"
                                name="email"
                                onBlur={handleBlur}
                                onChange={handleChange('email')}
                                type="text"
                                value={values.email}
                            />
                        }
                        <Box my={2}>
                            <Button
                                className={'darkbtn'}
                                fullWidth
                                size="large"
                                variant="contained"
                                onClick={submitForm}
                                disabled={loading}
                                label={loading ? <CircularProgress style={{ color: '#fff' }} size={26} /> : <MessageLang id="form.label.register.btn_signup" />}
                            />
                        </Box>
                        <Dots stepOne />
                    </Form>
                )}
            </Formik>
            <SelectLanguage />
        </>
    )
}

export default RegisterStepOne
