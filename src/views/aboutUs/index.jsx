import React, { useState } from "react";
import {
  Grid,
  Container,
  Typography,
  Button,
  CardMedia,
  IconButton,
  Box,
} from "@material-ui/core";
import Header from "../FAQ/Header";
import imageHeader from "./header.jpg";
import OnlineConection from "./OnlineConection.jpg";
import TeamBackground from "./teamBg.svg";
import MiladPic from "./milad.jpg";
import MohammadReza from "./MohammadReza.jpg";
import Venus from "./venus.jpg";
import Majid from "./Majid.jpg";
import Mohsen from "./Mohsen.jpg";
import Sarah from "./Sarah.jpg";
import storyImage from "./bg-image.svg";

import PermContactCalendarIcon from "@material-ui/icons/PermContactCalendar";
import EmailIcon from "@material-ui/icons/Email";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";

import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.scss";

import Header_Menu from "../Landing/Header_Menu";
import Footer from "../Landing/Footer";

import GeneralDrawer from '../../component/Drawer/GeneralDrawer'
import Modal from '../../component/Modal/Modal'
import FileUpload from '../../component/FileUpload/FileUpload'
import MessageLang from '../../lang';





const AboutUs = () => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const params = {
    spaceBetween: 52,
    slidesPerView: 4,
    freeMode: true,
    // shouldSwiperUpdate: true,
    breakpoints: {
      1366: {
        slidesPerView: 4,
      },
      768: {
        slidesPerView: 3,
      },
      468: {
        slidesPerView: 2,
      },
      0: {
        slidesPerView: 1,
      },
    },
  };
  const mobileSize = window.innerWidth < 600 ? true : false
  return (
    <>
      <Header_Menu />
      <Header
        img={imageHeader}
        title={<MessageLang id="About.header.title" />}
        text={{ text: <MessageLang id="About.header.text" /> }}
        className="header-index-aboutUs"
      >
        <Grid
          container
          alignItems="center"
          justify="center"
          className="index-aboutUs__btnBox"
        >
          <Button
            variant="contained"
            className="index-aboutUs__btnBox__buttonTeam"
            startIcon={<PermContactCalendarIcon />}
          >
            <MessageLang id="About.header.button" />

          </Button>
        </Grid>
      </Header>
      <Grid className="index-aboutUs__TabBox">
        <Paper className="root">
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="secondary"
            textColor="secondary"
            centered
            className="tabBoxes"
          >
            <Tab label={<MessageLang id="About.tabBox.label1" />} />
            <Tab label={<MessageLang id="About.tabBox.label2" />} />
            <Tab label={<MessageLang id="About.tabBox.label3" />} />
            <Tab label={<MessageLang id="About.tabBox.label4" />} />
          </Tabs>
        </Paper>
      </Grid>
      <Container maxWidth="xl" className="index-aboutUs__info">
        <Grid container>
          <Grid item xs={12} md={6}>
            <Paper className="index-aboutUs__info__infoPic">
              <CardMedia className="media" image={OnlineConection} />
            </Paper>
          </Grid>
          <Grid
            item
            xs={12}
            md={6}
            className="index-aboutUs__info__infoTextBox"
          >
            <Typography
              variant="h3"
              gutterBottom
              className="index-aboutUs__info__infoTextBox__infoTopic"
            >
              <MessageLang id="About.sectionOne.title1" />
              <br />
              <MessageLang id="About.sectionOne.title2" />
              <br />
              <MessageLang id="About.sectionOne.title3" />
            </Typography>
            <Typography
              variant="body2"
              gutterBottom
              className="index-aboutUs__info__infoTextBox__infoText"
            >
              <MessageLang id="About.sectionOne.text" />
            </Typography>
            <Button variant="contained"><MessageLang id="About.sectionOne.signup.btn" /></Button>
          </Grid>
        </Grid>
      </Container>
      <Box className="index-aboutUs__jobBackground">
        <Container maxWidth="xl">
          <Grid container>
            <Grid
              item
              sm={12}
              md={5}
              className="index-aboutUs__jobBackground__text"
            >
              <Typography
                variant="h3"
                gutterBottom
                className="index-aboutUs__jobBackground__text__title"
              >
                <MessageLang id="About.jobBackground.title" />
              </Typography>
              <Typography
                variant="body2"
                gutterBottom
                className="index-aboutUs__jobBackground__text__content"
              >
                <MessageLang id="About.jobBackground.text" />
              </Typography>
            </Grid>
            <Grid
              item
              sm={12}
              md={7}
              className="index-aboutUs__jobBackground__image"
            >
              <div className="index-aboutUs__jobBackground__image__pic">
                <img src={storyImage} alt="stroy image" />
              </div>
            </Grid>
          </Grid>
        </Container>
      </Box>

      <Box className="index-aboutUs__team">
        <Container maxWidth="xl">
          <Grid container alignItems="center" justify="center">
            <Grid
              item
              lg={4}
              sm={8}
              className="index-aboutUs__team__textBox"
              alignItems="center"
              justify="center"
              direction="column"
            >
              <Typography
                variant="h2"
                className="index-aboutUs__team__textBox__title"
                align="center"
              >
                <MessageLang id="About.team.title" />
              </Typography>
              <Typography
                variant="body2"
                className="index-aboutUs__team__textBox__text"
              >
                <MessageLang id="About.team.text" />
              </Typography>
            </Grid>
          </Grid>
        </Container>
        <Grid
          item
          sm={12}
          className="index-aboutUs__team__memberBox"
          alignItems="center"
          justify="center"
        >
          <Container maxWidth="xl">
            <Swiper {...params}>
              <Box className="index-aboutUs__team__memberBox__member">
                <CardMedia
                  className="index-aboutUs__team__memberBox__member__PersonPic"
                // image=
                />
                <Typography
                  className="index-aboutUs__team__memberBox__member__name"
                  align="center"
                >
                  <MessageLang id="About.team.name.navid" />
                </Typography>
                <Typography
                  className="index-aboutUs__team__memberBox__member__position"
                  align="center"
                >
                  <MessageLang id="About.team.position.navid" />
                </Typography>
                <Grid
                  className="index-aboutUs__team__memberBox__member__socials"
                  align="center"
                >
                  <IconButton aria-label="mail" className="icon">
                    <EmailIcon />
                  </IconButton>
                  <IconButton aria-label="instagram" className="icon">
                    <InstagramIcon />
                  </IconButton>
                  <IconButton aria-label="linkedin" className="icon">
                    <LinkedInIcon />
                  </IconButton>
                </Grid>
              </Box>

              <Box className="index-aboutUs__team__memberBox__member">
                <CardMedia
                  className="index-aboutUs__team__memberBox__member__PersonPic"
                  image={MiladPic}
                />
                <Typography
                  className="index-aboutUs__team__memberBox__member__name"
                  align="center"
                >
                  <MessageLang id="About.team.name.milad" />
                </Typography>
                <Typography
                  className="index-aboutUs__team__memberBox__member__position"
                  align="center"
                >
                  <MessageLang id="About.team.position.milad" />
                </Typography>
                <Grid
                  className="index-aboutUs__team__memberBox__member__socials"
                  align="center"
                >
                  <IconButton aria-label="mail" className="icon">
                    <EmailIcon />
                  </IconButton>
                  <IconButton aria-label="instagram" className="icon">
                    <InstagramIcon />
                  </IconButton>
                  <IconButton aria-label="linkedin" className="icon">
                    <LinkedInIcon />
                  </IconButton>
                </Grid>
              </Box>
              <Box className="index-aboutUs__team__memberBox__member">
                <CardMedia
                  className="index-aboutUs__team__memberBox__member__PersonPic"
                  image={MohammadReza}
                />
                <Typography
                  className="index-aboutUs__team__memberBox__member__name"
                  align="center"
                >
                  <MessageLang id="About.team.name.mohammadreza" />

                </Typography>
                <Typography
                  className="index-aboutUs__team__memberBox__member__position"
                  align="center"
                >
                  <MessageLang id="About.team.position.mohammadreza" />
                </Typography>
                <Grid
                  className="index-aboutUs__team__memberBox__member__socials"
                  align="center"
                >
                  <IconButton aria-label="mail" className="icon">
                    <EmailIcon />
                  </IconButton>
                  <IconButton aria-label="instagram" className="icon">
                    <InstagramIcon />
                  </IconButton>
                  <IconButton aria-label="linkedin" className="icon">
                    <LinkedInIcon />
                  </IconButton>
                </Grid>
              </Box>
              <Box className="index-aboutUs__team__memberBox__member">
                <CardMedia
                  className="index-aboutUs__team__memberBox__member__PersonPic"
                  image={Venus}
                />
                <Typography
                  className="index-aboutUs__team__memberBox__member__name"
                  align="center"
                >
                  <MessageLang id="About.team.name.venus" />
                </Typography>
                <Typography
                  className="index-aboutUs__team__memberBox__member__position"
                  align="center"
                >
                  <MessageLang id="About.team.position.venus" />

                </Typography>
                <Grid
                  className="index-aboutUs__team__memberBox__member__socials"
                  align="center"
                >
                  <IconButton aria-label="mail" className="icon">
                    <EmailIcon />
                  </IconButton>
                  <IconButton aria-label="instagram" className="icon">
                    <InstagramIcon />
                  </IconButton>
                  <IconButton aria-label="linkedin" className="icon">
                    <LinkedInIcon />
                  </IconButton>
                </Grid>
              </Box>
              <Box className="index-aboutUs__team__memberBox__member">
                <CardMedia
                  className="index-aboutUs__team__memberBox__member__PersonPic"
                // image=
                />
                <Typography
                  className="index-aboutUs__team__memberBox__member__name"
                  align="center"
                >
                  <MessageLang id="About.team.name.mohammad" />
                </Typography>
                <Typography
                  className="index-aboutUs__team__memberBox__member__position"
                  align="center"
                >
                  <MessageLang id="About.team.name.mohammad" />
                </Typography>
                <Grid
                  className="index-aboutUs__team__memberBox__member__socials"
                  align="center"
                >
                  <IconButton aria-label="mail" className="icon">
                    <EmailIcon />
                  </IconButton>
                  <IconButton aria-label="instagram" className="icon">
                    <InstagramIcon />
                  </IconButton>
                  <IconButton aria-label="linkedin" className="icon">
                    <LinkedInIcon />
                  </IconButton>
                </Grid>
              </Box>
              <Box className="index-aboutUs__team__memberBox__member">
                <CardMedia
                  className="index-aboutUs__team__memberBox__member__PersonPic"
                  image={Majid}
                />
                <Typography
                  className="index-aboutUs__team__memberBox__member__name"
                  align="center"
                >
                  <MessageLang id="About.team.name.majid" />

                </Typography>
                <Typography
                  className="index-aboutUs__team__memberBox__member__position"
                  align="center"
                >
                  <MessageLang id="About.team.position.majid" />

                </Typography>
                <Grid
                  className="index-aboutUs__team__memberBox__member__socials"
                  align="center"
                >
                  <IconButton aria-label="mail" className="icon">
                    <EmailIcon />
                  </IconButton>
                  <IconButton aria-label="instagram" className="icon">
                    <InstagramIcon />
                  </IconButton>
                  <IconButton aria-label="linkedin" className="icon">
                    <LinkedInIcon />
                  </IconButton>
                </Grid>
              </Box>
            </Swiper>
            {/* <Grid
              container
              alignItems="center"
              justify="center"
              className="index-aboutUs__btnBox"
            >
              <Button
                variant="contained"
                className="index-aboutUs__btnBox__buttonTeam"
                startIcon={<PermContactCalendarIcon />}
              >
                به تیم ما بپیوندید
              </Button>
            </Grid> */}
          </Container>
        </Grid>
      </Box>
      <Footer />
      {/* {mobileSize && open?
                <GeneralDrawer
                    from="bottom"
                    onClose={() => setopen(false)}
                    isOpen={open}
                    title={<MessageLang id="product.detailedcomments.vote.title" />}
                    subTitle={<MessageLang id="product.detailedcomments.vote.subtitle" />}>
                    <Voting msg={msg} close={CloseVoting} product={product} from='product'/>
                </GeneralDrawer>
            :
                <Modal
                    onClose={() => {
                        setopen(false)
                    }}
                    isOpen={open}
                    title={<MessageLang id="product.detailedcomments.vote.title" />}
                    subTitle={<MessageLang id="product.detailedcomments.vote.subtitle" />}
                >
                    <Voting msg={msg} close={CloseVoting} product={product} from='product'/>
                </Modal>
            } */}
      {/* <FileUpload
                            formatfile={"application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"}
                            multiple={false}
                            showfilename={showfilename} //showfilename==="type" ? <MessageLang id="Oppotunity.item.btn-sendrequest-upload_alertformat" /> : showfilename
                            Upload_Func={Upload}
                        /> */}
    </>
  );
};
export default AboutUs;