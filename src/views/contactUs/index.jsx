import React, { useState } from 'react'
import { Grid, Container, Typography, Button, CardMedia, Link, Box } from '@material-ui/core';
import Header from '../FAQ/Header';
import Footer from '../Landing/Footer';
import Header_Menu from '../Landing/Header_Menu'
import imageWorkPlace from './workplace.jpg'

import ChatIcon from '@material-ui/icons/Chat';
import PhoneEnabledIcon from '@material-ui/icons/PhoneEnabled';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import MailIcon from '@material-ui/icons/Mail';
import PublicIcon from '@material-ui/icons/Public';
import LinkedIn from '@material-ui/icons/LinkedIn';
import Instagram from '@material-ui/icons/Instagram';
import Paper from '@material-ui/core/Paper';
import { useSelector } from 'react-redux';
import MessageLang from '../../lang';
import {messageLang} from '../../lang';

import Map from './map'





const ContactUs = () => {
  return (
    <>
      <Header_Menu/>
      <Header
        title={ <MessageLang id="Contact.header.title" />}
        text={{ text: <MessageLang id="Contact.header.text"/> }}
        className="header-index-conatactUs"
      >
        <Grid container alignItems="center" justify="center" className="index__contact__btnBox">
          <Button
            variant="contained"
            className="index__contact__btnBox__buttonChat"
            startIcon={<ChatIcon />}
          >
            <MessageLang id="Contact.header.chatbtn"/>
          </Button>
          <Button
            variant="contained"
            className="index__contact__btnBox___buttonCall"
            startIcon={<PhoneEnabledIcon />}
          >
            <MessageLang id="Contact.header.callbtn"/>
          </Button>
        </Grid>
      </Header>
      <Grid container alignItems="center" justify="center" className="index__contact">
        <Grid className="index-contactUs__map">
          {/* <Map/> */}
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d291.86285554745285!2d51.38854233325273!3d35.724222861889245!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e00d5709a32b1%3A0x7c675d0ffc9b0681!2sUniversity%20of%20Tehran%2C%20College%20of%20Engineering!5e0!3m2!1sen!2s!4v1620463311447!5m2!1sen!2s" width="100%" height="100%" style={{border:0}} allowfullscreen="" ></iframe>
        </Grid>
        <Container maxWidth="xl">
          <Grid container >
            <Grid item xs="12" sm={4}>
              <Paper className="index__contact__workPlaceImage">
                <CardMedia
                  className="media"
                  image={imageWorkPlace}
                />
              </Paper>
            </Grid>
            <Grid item xs="12" sm={8}>
              <Paper className="index__contact__info">
                <Box display="flex" alignItems="center">
                  <LocationOnIcon />
                  <Typography className="text">
                  <MessageLang id="Contact.body.address"/>
                  </Typography>
                </Box>
                <Box display="flex" alignItems="center">
                  <Link href="mailto:info@namaline.ir" className="index__contact__info__link">
                    <MailIcon />
                    <Typography className="text">
                      info@namaline.ir
                </Typography>
                  </Link>
                </Box>
                <Box display="flex" alignItems="center">
                  <Link href="tel:02112345678" className="index__contact__info__link">
                    <PhoneEnabledIcon />
                    <Typography className="text">
                      021-91070105
                  </Typography>
                  </Link>
                </Box>
              </Paper>
            </Grid >
          </Grid >
        </Container>
        <Grid container display="flex" alignItems="center" justify="center" className="index__contact__info__socialLinks">
          <Box display="flex" alignItems="center" className="socialBox">
            <Link href="https://www.linkedin.com/company/namaline" className="index__contact__info__link" display="flex" alignItems="center">
              <Typography className="item">
                namaline@
                </Typography>
              <LinkedIn />
            </Link>
          </Box>
          <Box display="flex" alignItems="center" className="socialBox">
            <Link href="https://www.instagram.com/namaline.ir/" className="index__contact__info__link" display="flex" alignItems="center">
              <Typography className="item">
                namaline@
                </Typography>
              <Instagram />
            </Link>
          </Box>
        </Grid>
      </Grid >
      <Footer/>
    </>

  )
}

export default ContactUs;