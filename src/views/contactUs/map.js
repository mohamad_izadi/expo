// import React, { useRef, useEffect } from 'react';

// import mapboxgl from 'mapbox-gl';
// const Map = () => {

// }

// mapboxgl.accessToken =
//   'pk.eyJ1IjoibmFtYWxpbmUiLCJhIjoiY2ttN3Vtc3d5MDdsMTJ3cGhucWx6d2ZxYyJ9.DAtpbMb3abMv5sC3ZA1qqQ';
//   mapboxgl.setRTLTextPlugin(
//     'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.3/mapbox-gl-rtl-text.js',
//     null,
//     true
//   );
// const Map = () => {
//     const mapEl = useRef(null);
//     let mapRef = useRef();
//     const initMap = value => {
//       var geojson = {
//         type: 'FeatureCollection',
//         features: [
//           {
//             type: 'Feature',
//             properties: {
//               iconSize: [50, 50]
//             },
//             geometry: {
//               type: 'Point',
//               coordinates: value.latlong
//             }
//           }
//         ]
//       };
//       mapRef.current = new mapboxgl.Map({
//         container: mapEl.current,
//         center: value.latlong,
//         zoom: value.zoom,
//         scrollZoom : true,
//         keyboard : false,
//         attributionControl: false,
//         style: 'mapbox://styles/mapbox/light-v10'
//       });
//       geojson.features.forEach(function (marker) {
//         var el = document.createElement('span');
//         el.innerHTML = `<img src="./static/images/logo/logoMini.svg"/>`;
//         el.addEventListener('mouseenter', function () {
//           this.classList.add('active');
//         });
//         el.addEventListener('mouseleave', function () {
//           this.classList.remove('active');
//         });
//         new mapboxgl.Marker(el)
//           .setLngLat(marker.geometry.coordinates)
//           .addTo(mapRef.current);
//       });
//     };
//     useEffect(() => {
//       const data = {
//         latlong: [51.3880757,35.7242546],
//         zoom: 15
//       };
//       initMap(data);
//     }, []);

//   return <div ref={mapEl} style={{ width: '100%', height: '100%' }} />
// }
// export default Map