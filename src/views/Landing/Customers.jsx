import React from 'react'
import { Grid, Box, Typography, Container } from '@material-ui/core';
//imgs
import { minisilverpath3 } from './images'
import oneImage from './images/customers/11.png'
import twoImage from './images/customers/12.png'
import thereImage from './images/customers/13.png'
import fourImage from './images/customers/14.png'
import fiveImage from './images/customers/15.png'
import sixImage from './images/customers/16.png'
/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const Customers = () => {
    const data = [oneImage, twoImage, thereImage, fourImage, fiveImage, sixImage]
    return (
        <div className='customers__container'>
            <Container maxWidth="xl">
                <Grid wrap="wrap" container className='customers'>
                    <Box className='customers__title'>
                        <Typography ><MessageLang id="Landing.Customers.title" /></Typography>
                        <img src={minisilverpath3} />
                    </Box>
                    <Grid wrap="wrap" container className='customers__content'>
                        {data.map((item, index) => (
                            <img key={index} src={item} />
                        ))}
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}
export default Customers;