import React from 'react'
import { Grid, Typography, Container, Box } from '@material-ui/core';
import { bluepath3, lightpath2, minisilverpath2 } from './images'
/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const Statistics = () => {
    const data = [
        {
            'title': <MessageLang id="Landing.statistics.sec1.title" />,
            'rate': '5000+'
        },
        {
            'title': <MessageLang id="Landing.statistics.sec2.title" />,
            'rate': '12'
        },
        {
            'title': <MessageLang id="Landing.statistics.sec3.title" />,
            'rate': '100+'
        },
        {
            'title': <MessageLang id="Landing.statistics.sec4.title" />,
            'rate': '1000+'
        },
    ]
    return (
        <div className="statistics__container">
            <Container maxWidth="lg" className='statistics'>
                <Grid container wrap="wrap" justify="center">
                    <Grid container xs={12} justify="center" >
                        <Box item mb={6} className='statistics__title' display="inline-bBlock">
                            <img src={minisilverpath2} />
                            <Typography ><MessageLang id="Landing.statistics.title" /></Typography>
                        </Box>
                    </Grid>
                    <Grid container item wrap="wrap" xs={12} lg={8} className='statistics__content'>
                        {data.map((item, index) => (
                            <div className='statistics__content__box' key={index}>
                                <div className='border'>
                                    <Typography className='number'>
                                        {item.rate}
                                    </Typography>
                                    <Typography className='title'>
                                        {item.title}
                                    </Typography>
                                </div>
                            </div>
                        ))}
                    </Grid>
                </Grid>
                <img src={bluepath3} className='bluearrow' />
                <img src={lightpath2} className='lightarrow' />
            </Container>
        </div>

    )
}
export default Statistics;