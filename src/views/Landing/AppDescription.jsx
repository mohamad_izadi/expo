import React from 'react'
import { Grid, Box, Typography, Button, Container } from '@material-ui/core';
import { dlapp, bluepath2 } from './images'
/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const AppDescription = () => {
    return (
        <div className='appdesc__container'>
            <Container maxWidth="xl" >
                <Grid container wrap="wrap" className='appdesc'>
                    <Grid item md={5} xs={12} className='appdesc__right'>
                        <img src={dlapp} />
                    </Grid>
                    <Grid item md={7} xs={12} className='appdesc__left'>
                        <Box className='appdesc__left__box'>
                            <Typography className='appdesc__left__title'><MessageLang id="Landing.appdescription.title" /></Typography>
                            <Typography className='appdesc__left__desc'>
                                <MessageLang id="Landing.appdescription.desc" /></Typography>
                            <Button variant="contained">
                                <MessageLang id="Landing.appdescription.btn" />
                            </Button>
                        </Box>
                    </Grid>
                </Grid>
                <img src={bluepath2} className='arrowleft' />
            </Container>
        </div>
    )
}
export default AppDescription;