import React from 'react'
import FullPageModall from '../../../component/FullPageModal/FullPageModal'
import { Grid, Button, Typography, Box } from '@material-ui/core';
import { Person, Today, Contacts, VerifiedUser, KeyboardArrowLeft } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';

/*************change lang ***************/
import MessageLang from '../../../lang';
/*************change lang ***************/

const LoginForm_Dialog = ({ close }) => {
    const history = useHistory();

    const [btnGuest, setBtnGuest] = React.useState('login_btn');
    const [btnPavilion, setBtnPavilion] = React.useState('login_btn');

    return (
        <FullPageModall close={close} back={'blue'}>
            <Grid container className='loginform'>

                <Grid className='loginBoxs'
                    onMouseEnter={() => setBtnGuest('login_btn_bold')}
                    onMouseLeave={() => setBtnGuest('login_btn')}
                    onClick={() =>
                        window.location.replace('https://namaline.ir/auth/login')
                    }>
                    <Button className='btn'
                        onClick={() =>
                            window.location.replace('https://namaline.ir/auth/login')
                        }>
                        <Box className='btn-text'>
                            <span>
                                <Person />
                                <Typography className={btnGuest}>
                                    <MessageLang id="Landing.loginform.sec1" />
                                </Typography>
                            </span>
                            <KeyboardArrowLeft />
                        </Box>
                    </Button>
                </Grid>
                <Grid className='loginBoxs'
                    onMouseEnter={() => setBtnPavilion('login_btn_bold')}
                    onMouseLeave={() => setBtnPavilion('login_btn')}
                    onClick={() =>
                        window.location.replace('https://app.namaline.ir/')
                    }>
                    <Button className='btn' onClick={() =>
                        window.location.replace('https://app.namaline.ir/')
                    }>
                        <Box className='btn-text'>
                            <span>
                                <Contacts />
                                <Typography className={btnPavilion}>
                                    <MessageLang id="Landing.loginform.sec2" />
                                </Typography>
                            </span>
                            <KeyboardArrowLeft />
                        </Box>
                    </Button>
                </Grid>
            </Grid>
        </FullPageModall>
    )
}
export default LoginForm_Dialog;