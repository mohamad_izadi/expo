import React from 'react'
import FullPageModall from '../../../component/FullPageModal/FullPageModal'
import { Grid, Button, Typography, Box } from '@material-ui/core';
import { Person, Today, Contacts, VerifiedUser, KeyboardArrowLeft } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';

/*************change lang ***************/
import MessageLang from '../../../lang';
/*************change lang ***************/

const RegisterForm_Dialog = ({ close }) => {
    const history = useHistory();


    const [btnGuest, setBtnGuest] = React.useState('register_btn');
    const [btnPavilion, setBtnPavilion] = React.useState('register_btn');


    return (
        <FullPageModall close={close} back={'light'}>
            <Grid container className='loginform'>
                <Grid className='registerbox'
                    onMouseEnter={() => setBtnGuest('register_btn_bold')}
                    onMouseLeave={() => setBtnGuest('register_btn')}
                    onClick={() => window.location.replace('https://namaline.ir/auth/register')
                    }>
                    <Button className='btn'
                        onClick={() => window.location.replace('https://namaline.ir/auth/register')
                        }>
                        <Box className='btn-text'>
                            <span>
                                <Person />
                                <Typography className={btnGuest}>
                                    <MessageLang id="Landing.registerform.sec1" />
                                </Typography>
                            </span>
                            <KeyboardArrowLeft />
                        </Box>
                    </Button>
                </Grid>
                <Grid className='registerbox'
                    onMouseEnter={() => setBtnPavilion('register_btn_bold')}
                    onMouseLeave={() => setBtnPavilion('register_btn')}
                    onClick={() => window.location.replace('https://app.namaline.ir/user/register')}>
                    <Button className='btn' onClick={() =>
                        window.location.replace('https://app.namaline.ir/user/register')
                    }>
                        <Box className='btn-text'>
                            <span>
                                <Contacts />
                                <Typography className={btnPavilion}>
                                    <MessageLang id="Landing.registerform.sec2" />
                                </Typography>
                            </span>
                            <KeyboardArrowLeft />
                        </Box>
                    </Button>
                </Grid>
            </Grid>
        </FullPageModall >
    )
}
export default RegisterForm_Dialog;