import React from 'react'
import { Grid, Typography, Container, Box } from '@material-ui/core';
import { about_sec1, about_sec2, about_sec3, minisilverpath1 } from './images'
/*************change lang ***************/
import MessageLang from '../../lang';
/*************change lang ***************/

const About = () => {
    const data = [
        {
            'img': about_sec1,
            'title': <MessageLang id="Landing.about.sec1.title" />,
            'desc': <MessageLang id="Landing.about.sec1.desc" />,
        },
        {
            'img': about_sec2,
            'title': <MessageLang id="Landing.about.sec2.title" />,
            'desc': <MessageLang id="Landing.about.sec2.desc" />,
        },
        {
            'img': about_sec3,
            'title': <MessageLang id="Landing.about.sec3.title" />,
            'desc': <MessageLang id="Landing.about.sec3.desc" />,
        }]

    return (
        <div className='about__container'>
        <Container maxWidth="xl" >
            <Grid wrap="wrap" container className='about'>
                <Grid item xs={12} container justify="center" >
                    <Box className='about__box' display="flex">
                        <Typography className='about__box__title'><MessageLang id="Landing.about.title" /></Typography>
                        <img src={minisilverpath1} />
                    </Box>
                </Grid>
                <Grid wrap="wrap" container spacing={3} className='about__content'>
                    {data.map((item, index) => (
                        <Grid item md={4} xs={12} className='about__content__box' key={index}>
                            <img src={item.img} />
                            <Typography className='title'>
                                {item.title}
                            </Typography>
                            <Typography className='desc'>
                                {item.desc}
                            </Typography>
                        </Grid>
                    ))}
                </Grid>
            </Grid>
        </Container>
        </div>
    )
}
export default About;