import React from 'react';
import { Card, CardMedia, CardContent, Typography, Box, Chip, Button } from '@material-ui/core';
import { Today, Done, Cached, EventBusy } from '@material-ui/icons';
import { useHistory } from "react-router-dom";
//icon
import Assignment from '@material-ui/icons/Assignment'

import { useDispatch } from 'react-redux'
import { setExhibition } from '../../../app/store/actions/exhibitionAction'
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/navigation/navigation.min.css"

// import Swiper core and required modules
import SwiperCore, {
  Navigation
} from 'swiper/core';


/*************change lang ***************/
import MessageLang from '../../../lang';
/*************change lang ***************/

// install Swiper modules
SwiperCore.use([Navigation]);

const PavilionCard = (props) => {
  const { data } = props;
  const history = useHistory()
  const dispatch = useDispatch()

  const selecttagstyle = (title) => {
    switch (title) {
      case 'به زودی':
        return 'pavilioncard__tags__left tags-backcolor-blue'
      case 'onRegistering':
        return 'pavilioncard__tags__left tags-backcolor-yellow'
      case 'onPerforming':
        return 'pavilioncard__tags__left tags-backcolor-green'
      case 'onPast':
        return 'pavilioncard__tags__left tags-backcolor-gray'
      default:
        break;
    }
  }
  const selecttagtext = (title) => {
    switch (title) {
      case 'onComing':
        return <MessageLang id="Landing.pavilioncard.title." />
      case 'onRegistering':
        return <MessageLang id="Landing.pavilioncard.title.onregister" />
      case 'onPerforming':
        return <MessageLang id="Landing.pavilioncard.title.onperforming" />
      case 'onPast':
        return <MessageLang id="Landing.pavilioncard.title.onpast" />
      default:
        break;
    }
  }

  const selecttagicon = (title) => {
    switch (title) {
      case 'onComing':
        return <Today />
      case 'onRegistering':
        return <Cached />
      case 'onPerforming':
        return <Done />
      case 'onPast':
        return <EventBusy />
      default:
        break;
    }
  }

  const params = {
    slidesPerView: 3,
    spaceBetween: 30,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    }
  }

  // const params = {
  //   spaceBetween: 30,
  //   slidesPerView: 'auto',
  //   freeMode: false,
  //   pagination: {
  //     el: '.swiper-pagination',
  //     clickable: true
  //   },
  //   navigation: {
  //     nextEl: '.swiper-button-next',
  //     prevEl: '.swiper-button-prev'
  //   }
  // }

  const Showexhibition = (data) => {
    dispatch(setExhibition(data))
    setTimeout(() => {
      history.push(`/${data.short_name}/home`)
    }, 10);
  }


  const breakpoints = {
    4000: {
      slidesPerView: 4,
      spaceBetween: 50
    },
    1024: {
      slidesPerView: 3,
      spaceBetween: -50
    },
    768: {
      slidesPerView: 3,
      spaceBetween: -30
    },
    640: {
      slidesPerView: 2,
      spaceBetween: -50
    },
    320: {
      slidesPerView: 1,
      spaceBetween: 10
    }
  }

  // const params = {
  //   spaceBetween: 50,
  //   slidesPerView: 3,
  //   breakpoints: breakpoints,
  //   rtl: true,
  //   pagination: { clickable: true },
  //   scrollbar: { draggable: true },
  //   navigation: {
  //     nextEl: '.swiper-button-next',
  //     prevEl: '.swiper-button-prev'
  //   },
  //   renderPrevButton: () => <CustomButton className="swiper-button-prev">Prev</CustomButton>,
  //   renderNextButton: () => <CustomButton className="swiper-button-next">Next</CustomButton>,
  // }

  return (

    <Swiper
      spaceBetween={50}
      slidesPerView={3}
      breakpoints={breakpoints}
      navigation={true}
      rtl={true}
      pagination={{ clickable: true }}
      scrollbar={{ draggable: true }}
      onSwiper={(swiper) => console.log(swiper)}
      onSlideChange={() => console.log('slide change')}
    >

      {data.map((item, index) => {
        return (
          <SwiperSlide>
            <Card className={'pavilioncard'} key={index}>
              <Box className='pavilioncard__tags'>
                <Box className='pavilioncard__tags__right'>
                  <span className='pavilioncard__tags__right__date'>{item.starting_date.split(" ")[1]}</span>
                  <span className='pavilioncard__tags__right__month'>{item.starting_date.split(" ")[2]}</span>
                </Box>
                <Box className={selecttagstyle(item.status)} >
                  {selecttagicon(item.status)}
                  {selecttagtext(item.status)}
                </Box>
              </Box>
              <CardMedia
                className={'pavilioncard__media'}
                image={item.media.poster}
                title="Paella dish"
              >
                {item.status == 'onPast' ? <div className='pavilioncard__media__disable'></div> : null}
              </CardMedia>
              <CardContent className={'pavilioncard__content'}>
                <Box className='circle'>
                  <img src={item.media.logo} />
                </Box>
                <Box className='pavilioncard__content__box'>
                  <Typography className='pavilioncard__content__title' component="p">{item.title}</Typography>
                  <Typography className='pavilioncard__content__subtitle' component="p"> <Today /> {item.starting_date} {"تا"}{item.ending_date}</Typography>
                  <Typography className='pavilioncard__content__desc' component="p"><p dangerouslySetInnerHTML={{ __html: item.description.substring(0, 60) }}></p></Typography>
                  <Box className='pavilioncard__content__tags'>{item.tags.slice(0, 3).map((tag, index) => (<Chip key={index} label={tag.substring(0, 15)} variant="outlined" />))}</Box>
                </Box>
                <Button variant="contained" disabled={item.status !== "onPerforming"} onClick={() => Showexhibition(item)} >
                  <Assignment />
                  <MessageLang id="Landing.Pavilions.cardbtn" />
                </Button>
              </CardContent>
            </Card>
          </SwiperSlide>
        )
      })}
    </Swiper>
  );
}
export default PavilionCard;