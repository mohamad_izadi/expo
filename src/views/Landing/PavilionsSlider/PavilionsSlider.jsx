import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Grid, Box, Typography, Button, Container } from '@material-ui/core';
import PavilionCard from './PavilionCard'
import { ArrowLeft } from '@material-ui/icons';
import 'swiper/swiper.scss';
import "swiper/components/navigation/navigation.scss";
import "swiper/components/pagination/pagination.scss";


//img
import { lightpath1, minilightpath } from '../images'
/*************change lang ***************/
import MessageLang from '../../../lang';
/*************change lang ***************/

import agent from '../../../app/api/agent'
import { setExhibitions } from '../../../app/store/actions/exhibitionAction';

const PavilionsSlider = () => {
  const [data, setData] = useState([])
  const dispatch = useDispatch();

  useEffect(() => {
    (async () => {
      const { data } = await agent.Landing.list()
      if (data.status) {
        setData(data.result.data)
        dispatch(setExhibitions(data.result.data))
      }
    })()
  }, [])

  return (
    <div className='pavilion__container'>
      <Container maxWidth="xl" >
        <Grid container className='pavilion'>
          <img src={lightpath1} className='backimg' />
          <Box className='pavilion__top'>
            <Box className='pavilion__top__title'>
              <Typography ><MessageLang id="Landing.Pavilions.title" /></Typography>
              <img src={minilightpath} />
            </Box>
            {/* <Button endIcon={<ArrowLeft />}>
                        <MessageLang id="Landing.Pavilions.more" />
                    </Button> */}
          </Box>
          <Grid className='pavilion__slider'>
            <PavilionCard data={data} />
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}
export default PavilionsSlider;