import React, { useState } from 'react'
import { Grid, Box, Typography, Button, Container } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import { home } from './images/'
import { useHistory } from "react-router-dom";
import LoginForm_Dialog from './Form/LoginForm_Dialog'
import RegisterForm_Dialog from './Form/RegisterForm_Dialog'
/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const FirstSection = ({ movebottom }) => {
    const [showlogin, setshowlogin] = useState(false)
    const [showregister, setshowregister] = useState(false)
    const history = useHistory()

    const CloseRegister = () => {
        setshowregister(false)
    }

    const CloseLogin = () => {
        setshowlogin(false)
    }

    return (
        <div className="firstsec">
            <div className='imgarrow'></div>
            <Container maxWidth="xl" className='firstsec__container'>
                <Grid container wrap="wrap">
                    <Grid item md={4} xs={12} className='firstsec__right'>
                        <Box className='firstsec__right__box'>
                            <Typography className='firstsec__right__title'>
                                <MessageLang id="Landing.firstsection.rightside.title" />
                            </Typography>
                            <Typography className='firstsec__right__desc'>
                                <MessageLang id="Landing.firstsection.rightside.desc" />
                            </Typography>
                            <Box className='firstsec__right__btnbox'>
                                <Button variant="contained" color="primary" className='exhibition' onClick={() => movebottom()}>
                                    <MessageLang id="Landing.firstsection.bar.btn-exhibition" />
                                </Button>
                            </Box>
                        </Box>
                    </Grid>
                    <Grid item sm={8} xs={12} className='firstsec__left'>
                        <img src={home} />
                    </Grid>
                    <KeyboardArrowDownIcon className='' onClick={() => movebottom()} />
                </Grid>
                {/* <img src={home} className='homearrow' /> */}
                {showlogin && <LoginForm_Dialog close={CloseLogin} />}
                {showregister && <RegisterForm_Dialog close={CloseRegister} />}
            </Container>
        </div>
    )
}
export default FirstSection;