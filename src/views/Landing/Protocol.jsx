import React, { useState, useRef } from 'react'
import { Grid, Container, Typography, Button, ButtonGroup, Box } from '@material-ui/core';
import { view_sec1, view_sec2, view_sec3, view_sec4, org_sec1, org_sec3, darkpath } from './images'
import { ArrowLeft, TramRounded } from '@material-ui/icons';
import { minipath, minilightpath, line } from './images'
import Swiper from 'react-id-swiper';
/*************change lang ***************/
import MessageLang from '../../lang';
/*************change lang ***************/

const Protocol = () => {
    const [select, setselect] = useState('view')
    const swiperRef = useRef(null);

    const params = {
        // loop: true,
        spaceBetween: 30,
        slidesPerView: 'auto',
        freeMode: false,
        shouldSwiperUpdate: true,
        reverseDirection: true,
        rtl: true
    }
    const dataorganizer = [
        {
            'img': org_sec1,
            'title': <MessageLang id="Landing.protocol.organizer.sec1.title" />,
            'desc': <MessageLang id="Landing.protocol.organizer.sec1.desc" />,
        },
        {
            'img': view_sec2,
            'title': <MessageLang id="Landing.protocol.organizer.sec2.title" />,
            'desc': <MessageLang id="Landing.protocol.organizer.sec2.desc" />,
        },
        {
            'img': org_sec3,
            'title': <MessageLang id="Landing.protocol.organizer.sec3.title" />,
            'desc': <MessageLang id="Landing.protocol.organizer.sec3.desc" />,
        },
        {
            'img': view_sec1,
            'title': <MessageLang id="Landing.protocol.organizer.sec4.title" />,
            'desc': <MessageLang id="Landing.protocol.organizer.sec4.desc" />,
        }]

    const dataview = [
        {
            'img': view_sec1,
            'title': <MessageLang id="Landing.protocol.view.sec1.title" />,
            'desc': <MessageLang id="Landing.protocol.view.sec1.desc" />,
        },
        {
            'img': view_sec2,
            'title': <MessageLang id="Landing.protocol.view.sec2.title" />,
            'desc': <MessageLang id="Landing.protocol.view.sec2.desc" />,
        },
        {
            'img': view_sec3,
            'title': <MessageLang id="Landing.protocol.view.sec3.title" />,
            'desc': <MessageLang id="Landing.protocol.view.sec3.desc" />,
        },
        {
            'img': view_sec4,
            'title': <MessageLang id="Landing.protocol.view.sec4.title" />,
            'desc': <MessageLang id="Landing.protocol.view.sec4.desc" />,
        }]

    // Manipulate swiper from outside
    const goNext = () => {
        if (swiperRef.current && swiperRef.current.swiper) {
            swiperRef.current.swiper.slidePrev();
        }
        setselect('view')
    };

    const goPrev = () => {
        if (swiperRef.current && swiperRef.current.swiper) {
            swiperRef.current.swiper.slideNext();
        }
        setselect('organazer')
    };

    return (
        <div className='protocol__container'>
            <Container maxWidth="xl" >
                <div className="protocol__container__path"></div>
                <Grid item xs={12}>
                    <Box className='protocol__container__title'>
                        <img src={minilightpath} />
                        <Typography><MessageLang id="Landing.protocol.title" /></Typography>
                    </Box>
                    <Box justifyContent="center" display="flex">
                        <ButtonGroup aria-label="outlined button group">
                            <Button className={select === 'view' ? 'active' : ''}
                                onClick={() => goNext()}
                            ><MessageLang id="Landing.protocol.btngroup.sec1" /></Button>
                            <Button className={select === 'organazer' ? 'active' : ''}
                                onClick={() => goPrev()}
                            ><MessageLang id="Landing.protocol.btngroup.sec2" /></Button>
                        </ButtonGroup>
                    </Box>
                </Grid>
                <Grid item xs={12} className="protocol__container__content">
                    <Swiper {...params} ref={swiperRef}>
                        <Grid item xs={12} wrap="wrap" container>
                            {dataview.map((item, index) => (
                                <Grid item xs={12} md={3} lg className='protocol__container__content__box' key={index} attr-index={index}>
                                    <Grid xs={4} md={12} className="protocol__container__content__box__image">
                                        <img src={item.img} />
                                    </Grid>
                                    <Grid item xs={8} md={12}>
                                        <Typography className='title'>
                                            {item.title}
                                        </Typography>
                                        <Typography className='desc'>
                                            {item.desc}
                                        </Typography>
                                    </Grid>

                                    {/* <div className="protocol__container__content__box__arrow" attr-index={index}></div> */}
                                </Grid>
                            ))}
                            <div className="protocol__container__content__image-between"></div>
                        </Grid>
                        <Grid item xs={12} wrap="wrap" container>
                            {dataorganizer.map((item, index) => (
                                <Grid item xs={12} md={3} lg className='protocol__container__content__box' key={index}>
                                    <Grid item xs={4} md={12} className="protocol__container__content__box__image">
                                        <img src={item.img} />
                                    </Grid>
                                    <Grid item xs={8} md={12} className="protocol__container__content__box__image">
                                        <Typography className='title'>
                                            {item.title}
                                        </Typography>
                                        <Typography className='desc'>
                                            {item.desc}
                                        </Typography>
                                    </Grid>

                                    {/* <div className="protocol__container__content__box__arrow" attr-index={index}></div> */}
                                </Grid>
                            ))}
                            <div className="protocol__container__content__image-between"></div>
                        </Grid>
                    </Swiper>
                </Grid>
                <Grid container alignItems="center" justify="center">
                    {select === 'view' ?
                        <Grid className='protocol__container__footer' onClick={() => window.location.replace(`https://namaline.ir/auth/register`)}>
                            <ArrowLeft />
                            <Typography><MessageLang id="Landing.protocol.footer.title-view" /></Typography>
                        </Grid>
                        :
                        <Grid className='protocol__container__footer' onClick={() => window.location.replace(`https://app.namaline.ir`)}>
                            <ArrowLeft />
                            <Typography><MessageLang id="Landing.protocol.footer.title-organazer" /></Typography>
                        </Grid>
                    }
                </Grid>
            </Container>
        </div>
    )
}
export default Protocol;