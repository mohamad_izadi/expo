import home from './home.svg'
import imgcard from './imgcard.svg'
import sliderpath from './sliderpath.svg'
import dlapp from './dlapp.svg'
import about_sec1 from './about_sec1.svg'
import about_sec2 from './about_sec2.svg'
import about_sec3 from './about_sec3.svg'
import silverarrow from './silverarrow.svg'

//test
import sample from './sample.svg'

//path
import bluepath1 from './bluepath1.svg'
import bluepath2 from './bluepath2.svg'
import bluepath3 from './bluepath3.svg'
import lightpath1 from './lightpath1.svg'
import lightpath2 from './lightpath2.svg'
import darkpath from './darkpath.svg'
import minipath from './minipath.svg'
import minilightpath from './minilightpath.svg'
import minisilverpath1 from './minisilverpath1.svg'
import minisilverpath2 from './minisilverpath2.svg'
import minisilverpath3 from './minisilverpath3.svg'

//dark section
import view_sec1 from './view_sec1.svg'
import view_sec2 from './view_sec2.svg'
import view_sec3 from './view_sec3.svg'
import view_sec4 from './view_sec4.svg'
import org_sec1 from './org_sec1.svg'
import org_sec3 from './org_sec3.svg'

//line
import line from './line.svg'

export {
    home, imgcard, sliderpath, dlapp, about_sec1, about_sec2, about_sec3, silverarrow,
    //test
    sample,
    //path
    bluepath1, bluepath2, bluepath3, lightpath1, lightpath2, darkpath, minipath,
    minilightpath, minisilverpath1, minisilverpath2, minisilverpath3,
    line,
    //dark section
    view_sec1, view_sec2, view_sec3, view_sec4, org_sec1, org_sec3
}