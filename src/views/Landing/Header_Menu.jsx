import React, { useState, useRef, useEffect } from 'react'
import { Grid, Typography, Button, IconButton, FormControl, NativeSelect, Select } from '@material-ui/core';
import { Call, Menu } from '@material-ui/icons';
//
import LoginForm_Dialog from './Form/LoginForm_Dialog'
import RegisterForm_Dialog from './Form/RegisterForm_Dialog'
//menu
import { makeStyles, withStyles } from '@material-ui/core/styles';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import InputBase from '@material-ui/core/InputBase';
import { ManageModal } from '../../app/store/actions/asyncActions';

/*************change lang ***************/
import MessageLang from '../../lang/';
/****************************************/

import { useSelector, useDispatch } from 'react-redux'
import { changeLanguage } from '../../app/store/actions/changelanguage'


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  paper: {
    marginRight: theme.spacing(2),
  },

}));

const Header_Menu = ({ style, background }) => {
  const [headerbgcolor, setheaderbgcolor] = useState(background)
  const [show, setshow] = useState(false)
  const [open, setOpen] = useState(false);
  const [opnLng, setOpnLng] = React.useState(false);
  const prevOpen = React.useRef(open);
  const prevOpenLng = React.useRef(opnLng)

  const anchorRef = React.useRef(null);
  const anchorRefLng = React.useRef(null);
  const headerRef = useRef(null)
  const dispatch = useDispatch()
  const { lang } = useSelector((state) => state.lang);
  const { modal } = useSelector((state) => state.async);

  const classes = useStyles();

  useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }
    prevOpen.current = open;
  }, [open]);

  const BootstrapInput = withStyles((theme) => ({
    root: {
      'label + &': {
        marginTop: theme.spacing(3),
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      border: '1px solid #FAFAFA',
      fontSize: 16,
      padding: '5px 5px 5px 5px',
      paddingRight: '5px!important',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      color: '#FAFAFA',
      fontWeight: 'bold',
      // Use the system font instead of the default Roboto font.
      '&:focus': {
        borderRadius: 4,
        border: '1px solid #FAFAFA',
        color: '#FAFAFA',
        fontWeight: 'bold',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',

      },
    },
  }))(InputBase);

  const ShowRegisterFunc = () => {
    if (modal.state === false || modal.type === 'login') {
      setOpen(false)
      dispatch(ManageModal({ state: true, type: 'register' }))
      setshow(true)
      setheaderbgcolor(true)
    } else if (modal.state === true || modal.type === 'register') {
      setOpen(true)
      dispatch(ManageModal({ state: false, type: '' }))
      setshow(false)
      setheaderbgcolor(false)
    }
  }

  const ShowLoginFunc = () => {
    if (modal.state === false || modal.type === 'register') {
      setOpen(false)
      dispatch(ManageModal({ state: true, type: 'login' }))
      setshow(true)
      setheaderbgcolor(false)
    }
    else if (modal.state === true || modal.type === 'login') {
      setOpen(true)
      dispatch(ManageModal({ state: false, type: '' }))
      setshow(false)
      setheaderbgcolor(true)
    }
  }

  const handlemodal = () => {
    setshow(false)
    dispatch(ManageModal({ state: false, type: '' }))
  }

  //menu
  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleToggleLng = () => {
    setOpnLng((prevOpenLng) => !prevOpenLng);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const handleCloseLng = (event) => {
    if (anchorRefLng.current && anchorRefLng.current.contains(event.target)) {
      return;
    }

    setOpnLng(false);
  };

  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  const handleChange = (event) => {
    if (anchorRefLng.current && anchorRefLng.current.contains(event.target)) {
      return;
    }
    setOpnLng(false);
    dispatch(changeLanguage(event.target.innerText))
  };

  return (
    <Grid className={show ? headerbgcolor ? 'header-bluemode landing-header' : 'header-sticky landing-header' : !style ? 'header-sticky landing-header' : 'landing-header'} ref={headerRef}>
      <Grid item xs={5} className='landing-header__logo'>
        <a title="Namaline" href="https://namaline.ir/"><img className='whitelogo' src={'/static/images/logo/logo.svg'} /></a>
        <a title="Namaline" href="https://namaline.ir/"><img className='bluelogo' src={'/static/images/logo/logo_full.svg'} /></a>
      </Grid>
      <Grid item xs={2} className='landing-header__lang'>
        {/* <Language /> */}
        {/* <Button variant="outlined" onClick={() => dispatch(changeLanguage(lang === "fa" ? "en" : "fa"))}>
          <MessageLang id="Landing.firstsection.bar.btn-lang" />
        </Button> */}
        <FormControl className='lang_section'>
          <Button
            ref={anchorRefLng}
            aria-controls={opnLng ? 'menu-list-grow' : undefined}
            aria-haspopup="true"
            onClick={handleToggleLng}
          >
            {lang}
          </Button>
          <Popper open={opnLng} anchorEl={anchorRefLng.current} role={undefined} transition disablePortal>
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={handleCloseLng}>
                    <MenuList autoFocusItem={opnLng} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                      <MenuItem value={"fa"} onClick={handleChange}>fa</MenuItem>
                      <MenuItem value={"en"} onClick={handleChange}>en</MenuItem>
                      <MenuItem value={"ar"} onClick={handleChange}>ar</MenuItem>
                      <MenuItem value={"ru"} onClick={handleChange}>ru</MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
          {/* <Select
            labelId="demo-customized-select-label"
            id="demo-customized-select"
            value={lang}
            onChange={handleChange}
            input={<BootstrapInput />}
          >
            <MenuItem value={"fa"}>fa</MenuItem>
            <MenuItem value={"en"}>en</MenuItem>
            <MenuItem value={"ar"}>ar</MenuItem>
            <MenuItem value={"ru"}>ru</MenuItem>
          </Select> */}
          {/* <NativeSelect
            id="demo-customized-select-native"
            value={lang}
            onChange={handleChange}
            input={<BootstrapInput />}
          >
            <option value={"fa"}>fa</option>
            <option value={"en"}>en</option>
            <option value={"ar"}>ar</option>
          </NativeSelect> */}
        </FormControl>

      </Grid>
      <Grid item xs={5} className='landing-header__action'>
        <Grid >
          <a className='call' href="tel:02191070105"><Call /> <Typography >021-9107-0105</Typography></a>
        </Grid>
        <Button variant="outlined" className='register' onClick={() => ShowRegisterFunc()}>
          <MessageLang id="Landing.firstsection.bar.btn-Register" />
        </Button>
        <Button variant="contained" color="primary" className='login' onClick={() => ShowLoginFunc()}>
          <MessageLang id="Landing.firstsection.bar.btn-login" />
        </Button>
        <IconButton aria-label="" className='menu' onClick={handleToggle} ref={anchorRef}>
          <Menu />
        </IconButton>
        <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal className={'spacetop_minimenu'}>
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                    <MenuItem onClick={() => ShowLoginFunc()}><MessageLang id="Landing.firstsection.bar.btn-login" /></MenuItem>
                    <MenuItem onClick={() => ShowRegisterFunc()}><MessageLang id="Landing.firstsection.bar.btn-Register" /></MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </Grid>
      {show && modal.type === 'login' && <LoginForm_Dialog close={() => handlemodal()} />}
      {show && modal.type === 'register' && <RegisterForm_Dialog close={() => handlemodal()} />}
    </Grid>
  )
}


export default Header_Menu;