import React from 'react'
import { Grid, Typography, Container } from '@material-ui/core';
import { Instagram, LinkedIn } from '@material-ui/icons'
import { Link } from 'react-router-dom'
/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const Footer = ({ footerRef }) => {
    const data = [{
        'title': <MessageLang id="Landing.footer.help" />,
        'link': '/faq'
    },
    {
        'title': <MessageLang id="Landing.footer.about" />,
        'link': '/aboutUs'
    },
    {
        'title': <MessageLang id="Landing.footer.contact" />,
        'link': '/contactus'
    },
    {
        'title': <MessageLang id="Landing.footer.term" />,
        'link': '/terms'
    },
    ]

    const handleClickSocial = (link) => {
        const win = window.open(link, "_blank");
        win.focus();
    }

    return (
        <div className='footer__container'>
            <Container maxWidth="xl" >
                <Grid container wrap="wrap" className='footer' ref={footerRef}>
                    <Grid item md={4} xs={12} className='logo'>
                        <img src={'/static/images/logo/logo_full.svg'} />
                        <MessageLang id="Landing.footer.logo" />
                    </Grid>
                    <Grid item md={4} xs={12} className='icons'>
                        <LinkedIn onClick={() => handleClickSocial('https://www.linkedin.com/company/namaline')} />
                        <Instagram onClick={() => handleClickSocial('https://www.instagram.com/namaline.ir/')} />
                    </Grid>
                    <Grid item md={4} xs={12} container wrap="wrap" className='menu'>
                        <ul>
                            {data.map((item, index) => (
                                <li key={index}><Link to={item.link}>{item.title}</Link></li>
                            ))}
                        </ul>
                        <Typography ><MessageLang id="Landing.footer.namaline" /></Typography>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}
export default Footer;