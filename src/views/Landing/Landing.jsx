import React, { useState, useEffect, useRef } from 'react'
import ReactFullpage from '@fullpage/react-fullpage';
//Pages
import Header_Menu from './Header_Menu'
import FirstSection from './FirstSection'
import About from './About'
import Protocol from './Protocol'
import PavilionsSlider from './PavilionsSlider/PavilionsSlider'
import AppDescription from './AppDescription'
import Statistics from './Statistics'
import Customers from './Customers'
import Footer from './Footer'

const Landing = () => {
  const [headerstyle, setheaderstyle] = useState(true)
  const fullpageRef = useRef(null)
  const firstsection = useRef(null)
  const mobilesize = window.innerWidth > 900 ? false : true

  useEffect(() => {
    document.body.addEventListener('scroll', function () {
      if (mobilesize && document.body.scrollTop > 50) {
        setheaderstyle(false)
      }
      else {
        setheaderstyle(true)
      }
    })
  }, [])

  const onLeave = (origin, destination, direction) => {
    if (destination.isFirst) {
      setheaderstyle(true)
    }
    else {
      setheaderstyle(false)
    }
  }


  return (
    <div >
      <Header_Menu style={headerstyle} />
      <ReactFullpage
        ref={fullpageRef}
        scrollOverflow={true}
        scrollingSpeed={1200}
        onLeave={onLeave}
        offsetSections={true}
        afterRender={async () => {
          await fullpageRef.current.fullpageApi.reBuild()
          if (mobilesize) {
            setTimeout(() => {
              fullpageRef.current.fullpageApi.destroy('all')
            }, 100);
          }
        }}
        offsetSectionsKey={'YWx2YXJvdHJpZ28uY29tX2ZZM2IyWm1jMlYwVTJWamRHbHZibk09MWpN'}
        render={({ state, fullpageApi }) => {
          return (
            <>
              <div className="section" ref={firstsection}>
                <FirstSection movebottom={() => fullpageApi.moveSectionDown()} />
              </div>
              <div className="section section1 fp-auto-height">
                <PavilionsSlider />
                <Protocol />
                <About />
                <AppDescription />
                <Statistics />
                <Customers />
                <Footer />
              </div>
            </>
          );
        }}
      />
    </div>
  )
}

export default Landing;