import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { TwitchEmbed, TwitchChat, TwitchClip, TwitchPlayer } from 'react-twitch-embed';
import { Button, Paper } from '@material-ui/core'
import MessageLang from '../../lang/';
import { setTourGuide } from '../../app/store/actions/authActions'
import Tour from 'reactour'

import $ from 'jquery'
window.jQuery = window.$ = $

const ConferenceComponent = () => {
    const [isTourOpen, setTourOpen] = useState(false)
    const { currentUser } = useSelector(state => state.auth);
    const dispatch = useDispatch()

    const steps = [
        {
            selector: '.MuiAppBar-root',
            content: <>
                <h4 className={'tour_header'}>{<MessageLang id="TourGuide.seminar.title" />}</h4>
                <p>{<MessageLang id="TourGuide.seminar.desc" />}</p>
                <Button className={'btn_close'} variant="contained" color="primary"
                    onClick={() => setTourOpen(false)}>
                    <MessageLang id="TourGuide.Button.Ok" />
                </Button>
            </>,
            style: {
                backgroundColor: '#fff',
                textAlign: 'center'
            },
        },
        // ...
    ];

    useEffect(() => {
        if (!currentUser.tour_guide.seminars) {
            setTourOpen(true)
            dispatch(setTourGuide("seminars"))
        }
        $.getJSON('https://streamlabs.com/widgets/viewer-count?token=1D31FE1550905D5724C6', function (data) {
            $('#viewerCount').html(data.stream.viewers);
        });
    })

    return (
        <Paper className={'center conference'}>
            <Tour
                steps={steps}
                isOpen={isTourOpen}
                onRequestClose={() => setTourOpen(false)}
                showNavigation={false}
                showNavigationNumber={false}
                showNumber={false}
                showButtons={false}
                showNavigationScreenReaders={false}
                disableDotsNavigation={true}
                className={'tourguide'}
            />
            {/* <TwitchEmbed
                channel="namaline"
                id="namaline"
                theme="dark"
                muted
                onVideoPause={() => console.log(':(')}
            /> */}
            {/* <TwitchChat channel="moonstar_x" theme="dark" /> */}
            {/* <TwitchClip
                clip="WealthyBumblingKimchiItsBoshyTime"
                parent={['mycoolsite.com, anotherawesomesite.net']} /> */}
            <div id='namaline' className='video'>
                <iframe scrolling="no" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="https://www.aparat.com/embed/live/navidasdf"></iframe>
            </div>
            {/* <div id='viewerCount'></div>
            <TwitchPlayer
                channel="namaline"
                id="namaline"
                className="video"
                video="333014765" /> */}
        </Paper>
    )
}

export default ConferenceComponent
