import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux'
import {
    Box
} from '@material-ui/core'
import InstagramIcon from '@material-ui/icons/Instagram';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import { useHistory } from "react-router-dom";

import { createConversation } from '../../../app/store/actions/chatActions'

import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles({
    list: {
        width: 390,
    },
    fullList: {
        width: 'auto',
    },
    drawerPaper: {
        left: 223,
        width: 400
    }
});

const AddMeetingPreload = (props) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    return (
        <div className="add-meeting">

            <div
                className={classes.list}
                role="presentation"
            >
                <div className="add-meeting__top-info">
                    <div className="add-meeting__top-info__title">
                        <Skeleton align="center" animation="wave" height={35} width="30%" />
                    </div>
                    <div className="add-meeting__top-info__position">
                        <Skeleton align="center" animation="wave" height={30} width="40%" />
                    </div>
                    <div className="add-meeting__top-info__avatar">
                        <Skeleton animation="wave" variant="circle" width={60} height={60} />
                    </div>
                </div>
                <div className="add-meeting__desc">
                    <div className="add-meeting__desc__details-info">
                        <div className="add-meeting__desc__details-info__actions">
                            <Skeleton align="center" animation="wave" height={50} width="30%" />
                        </div>
                        <ul>
                            <li>
                                <Skeleton align="center" animation="wave" height={25} width="50%" />
                                <Skeleton align="center" animation="wave" height={25} width="60%" />
                                <Skeleton align="center" animation="wave" height={25} width="55%" />
                                <Skeleton align="center" animation="wave" height={25} width="45%" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="add-meeting__divider"></div>
            <Box
                alignItems="start"
                display="flex"
                flexDirection="row"
                justifyContent="flex-start"
                className="add-meeting__details-info__social"
            >
                <div className="add-meeting__details-info__compnay-name">
                    <h1> <Skeleton align="center" animation="wave" height={15} width="80%" /></h1>
                    <span> <Skeleton align="center" animation="wave" height={15} width="80%" /></span>
                </div>
            </Box>
        </div>
    );
}
export default AddMeetingPreload;