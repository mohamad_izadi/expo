import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux'
import {
    Grid
} from '@material-ui/core';
import SearchBox from '../../../component/Search/SearchBox';
import {
    SelectInput
} from '../../../component'
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { clearFilters, loadMembers, setMemberFilters } from '../../../app/store/actions/standActions';
import { loadCompanies, loadAbilities, loadInterests, loadCountries, loadPositions } from '../../../app/store/actions/persistActions'

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/


const useStyles = makeStyles({
    root: {
    },
});

const MemberFilter = ({ selected, number, onClick }) => {
    const { companies, abilities, interests, positions, countries } = useSelector(state => state.persist)

    const classes = useStyles()
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(loadCompanies())
        dispatch(loadPositions())
        dispatch(loadCountries())
        dispatch(loadAbilities())
        dispatch(loadInterests())
        return () => {
            dispatch(clearFilters())
        }
    }, [dispatch])

    function handleChangeFilter(value, type) {
        if (type.action === "select-option") {
            dispatch(setMemberFilters(type.name, value.value))
            dispatch(loadMembers())
        } else if (type.action === "clear") {
            // dispatch(setMemberFilters(type.name, null))
            dispatch(setMemberFilters(type.name, 0))
            dispatch(loadMembers())
        }
    }

    return (
        <>
            <div className={classes.root}>
                <div className="stand-list__filter-list Responsive_Filter_Mood">
                    <div className="stand-list_filter-member">
                        <SelectInput
                            name="company"
                            label={<MessageLang id="stands.MemberFilter.form.company" />}
                            isClearable={true}
                            options={companies && companies.map(company =>
                                ({ value: company.id, label: company.name }))}
                            onChange={(value, type) => handleChangeFilter(value, type)}
                        />

                        <SelectInput
                            name="position"
                            label={<MessageLang id="stands.MemberFilter.form.position" />}
                            isClearable={true}
                            options={positions && positions.map((pos, index) =>
                                ({ value: pos.id, label: pos.name }))}
                            onChange={(value, type) => handleChangeFilter(value, type)}
                        />

                        <SelectInput
                            name="country"
                            label={<MessageLang id="stands.MemberFilter.form.country" />}
                            isClearable={true}
                            options={countries && countries.map(country =>
                                ({ value: country.id, label: country.name }))}
                            onChange={(value, type) => handleChangeFilter(value, type)}
                        />

                        <SelectInput
                            name="ability"
                            label={<MessageLang id="stands.MemberFilter.form.ability" />}
                            isClearable={true}
                            options={abilities && abilities.map(ability =>
                                ({ value: ability.id, label: ability.name }))}
                            onChange={(value, type) => handleChangeFilter(value, type)}
                        />

                        <SelectInput
                            name="interest"
                            label={<MessageLang id="stands.MemberFilter.form.interest" />}
                            isClearable={true}
                            options={interests && interests.map(interest =>
                                ({ value: interest.id, label: interest.name }))}
                            onChange={(value, type) => handleChangeFilter(value, type)}
                        />

                    </div>
                </div >
            </div >
        </>
    )
}

export default MemberFilter
