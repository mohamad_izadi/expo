import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AddMeetingPreload from './AddMeetingPreload'
import { useSelector, useDispatch } from 'react-redux'
import {
    SwipeableDrawer,
    IconButton,
    Button,
    Box
} from '@material-ui/core'
import Close from '@material-ui/icons/Close';
import InstagramIcon from '@material-ui/icons/Instagram';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import { useHistory } from "react-router-dom";

import { createConversation } from '../../../app/store/actions/chatActions'

import { loadMemberDetails } from '../../../app/store/actions/standActions'

import MeetingInvitationCreate from '../../meeting/meetingDashboard/MeetingInvitationCreate';

import Loading from '../../../component/Loading/Loading'


/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const AddMeeting = (props) => {
    const dispatch = useDispatch();
    const useStyles = makeStyles({
        list: {
            width: '100%',
        },
        fullList: {
            width: 'auto',
        },
        deskdrawerPaper: {
            left: 240
        },
        mobdrawerPaper: {
            left: 0
        },
        drawerPaper: {
            left: 223,
            width: 400
        }
    });
    const classes = useStyles();
    let history = useHistory();
    const { details } = useSelector((state) => state.stand);
    const exhibition = useSelector(state => state.exhibition.data);
    const [openModal, setOpenModal] = React.useState(false);
    const [loading, setLoading] = React.useState(true);
    const desk = window.innerWidth > 600 ? true : false;

    useEffect(() => {
        (async () => {
            await dispatch(loadMemberDetails(props.open));
            setLoading(false);
        })()
    }, [])

    async function createNewConversation(userId) {
        setLoading(true);
        try {
            await dispatch(createConversation(userId))
        } catch (error) {
            setLoading(false);
        } finally {
            history.push(`/${exhibition.short_name}/chat`)
            setLoading(false);
        }
    }

    if (openModal) {
        return (
            <MeetingInvitationCreate
                open={openModal}
                handleClose={() => setOpenModal(false)}
            />
        )
    }

    const handleClick = (url) => {
        const win = window.open(url, "_blank");
        win.focus();
    }

    return (
        <SwipeableDrawer
            anchor={'left'}
            open={props.open === false ? false : true}
            onClose={props.onClose}
            docked={true}
            classes={desk ? { paper: classes.deskdrawerPaper } : { paper: classes.mobdrawerPaper }}
        >
            <div className="add-meeting">
                {loading ? <AddMeetingPreload /> :
                    <div
                        className={classes.list}
                        role="presentation"
                    >
                        <div className="add-meeting__top-info">
                            <IconButton aria-label="close" onClick={() => props.onClose()} className="add-meeting__top-info__close">
                                <Close />
                            </IconButton>
                            <div className="add-meeting__top-info__title">
                                {details && details.full_name}
                            </div>
                            <div className="add-meeting__top-info__position">
                                {details.job_title}
                            </div>
                            <div className="add-meeting__top-info__avatar">
                                <img alt='' src={details.avatar ? details.avatar : "/static/images/user.png"} />
                            </div>

                        </div>
                        <div className="add-meeting__desc">
                            <div className="add-meeting__desc__details-info">
                                <div className="add-meeting__desc__details-info__actions">
                                    <Button className="darkbtn" onClick={() => setOpenModal(true)}>
                                        <MessageLang id="stands.StandDashboard.add-meeting.setting" />
                                    </Button>
                                    <Button className="add-meeting__desc__details-info__actions__chat bluebtn" onClick={() => createNewConversation(details.id)}>
                                        <MessageLang id="stands.StandDashboard.add-meeting.conversation" />
                                    </Button>
                                </div>
                                <ul>
                                    <li><MessageLang id="Profile.profileEditExtra.textInput.lable2" /><span>{details.email}</span></li>
                                </ul>
                                <Box
                                    alignItems="start"
                                    display="flex"
                                    flexDirection="row"
                                    justifyContent="flex-start"
                                    className="add-meeting__desc__details-info__social"
                                >
                                    {details && details.social_networks && details.social_networks.instagram &&
                                        <div className="profile__general-info__icon">
                                            <InstagramIcon onClick={() => handleClick(details.social_networks.instagram)} />
                                        </div>
                                    }
                                    {details && details.social_networks && details.social_networks.linkedin &&
                                        <div className="profile__general-info__icon">
                                            <LinkedInIcon onClick={() => handleClick(details.social_networks.linkedin)} />
                                        </div>
                                    }
                                </Box>
                            </div>
                            {/* <div className="add-meeting__divider"></div> */}
                            <Box
                                alignItems="start"
                                display="flex"
                                flexDirection="row"
                                justifyContent="flex-start"
                                className="add-meeting__desc__details-info__compnay"
                            >
                                <div className="add-meeting__desc__details-info__compnay__compnay-img">
                                    <img alt='' src={details.company.logo ? details.company.logo : "/static/images/logo/logo.svg"} />
                                </div>
                                <div className="add-meeting__desc__details-info__compnay__compnay-name">
                                    <h1 className="add-meeting__desc__details-info__compnay__compnay-name__title">{details.company.name}</h1>
                                    <span className="add-meeting__desc__details-info__compnay__compnay-name__subtitle">{details.company.category}</span>
                                </div>
                            </Box>
                        </div>
                    </div>
                }
            </div>
        </SwipeableDrawer>
    );
}
export default AddMeeting;