import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux'
import {
    Grid
} from '@material-ui/core';
import SearchBox from '../../../component/Search/SearchBox';
import agent from '../../../app/api/agent';
import {
    SelectInput
} from '../../../component'

import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { clearFilters, loadCompanies, setCompanyFilters } from '../../../app/store/actions/standActions';
import { loadCategories, loadCountries } from '../../../app/store/actions/persistActions'

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles({
    root: {
        color: '#234'
    },
    iconStyle: {
        position: 'absolute',
        top: '9px',
        right: '10px',
        display: 'inline-block',
        width: '20px',
        height: '20px',
        pointerEvents: 'none'
    },
});

const CompanyFilter = ({ selected, number, onClick}) => {
    const [category, setCategory] = useState()
    const [subCategories, setSubCategories] = useState([])
    const [loading, setLoading] = useState(false);
    const [isDisabled, toggleDisabled] = useState(true)

    const classes = useStyles()
    const dispatch = useDispatch()

    const { categories, countries } = useSelector(state => state.persist)
    
    useEffect(() => {
        dispatch(loadCategories())
        dispatch(loadCountries())
        return () => {
            dispatch(clearFilters())
        }
    }, [dispatch])

    async function loadSubCategories(categoryId) {
        setLoading(true)
        try {
            const subCategories = await agent.Filters.subCategories(categoryId)
            setSubCategories(subCategories.result)
            setLoading(false)
        } catch (error) {
            console.log(error)
            setLoading(false)
        }
    }

    function handleChangeCategory(category, type) {
        if (type.action === "select-option") {
            setCategory(category.value)
            loadSubCategories(category.value)
            dispatch(setCompanyFilters('category', category.value))
            dispatch(loadCompanies())
            toggleDisabled(false)
        } else if (type.action === "clear") {
            setCategory(null)
            setSubCategories(null)
            dispatch(setCompanyFilters('category', null))
            dispatch(setCompanyFilters('sub_ategory', null))
            dispatch(loadCompanies())
            toggleDisabled(true)
        }
    }

    function handleChangeSubCategory(subCategory, type) {
        if (type.action === "select-option") {
            dispatch(setCompanyFilters('sub_ategory', subCategory.value))
            dispatch(loadCompanies())
        } else if (type.action === "clear") {
            dispatch(setCompanyFilters('sub_ategory', null))
            dispatch(loadCompanies())
        }
    }

    function handleChangeCountry(country, type) {
        if (type.action === "select-option") {
            dispatch(setCompanyFilters('country', country.value))
            dispatch(loadCompanies())
        } else if (type.action === "clear") {
            dispatch(setCompanyFilters('country', null))
            dispatch(loadCompanies())
        }
    }

    return (
        <>
            <div className={classes.root}>
                <div className="stand-list__filter-list Responsive_Filter_Mood">
                    <div className="stand-list_filter-company">
                        <SelectInput
                            data-tut="reactour__iso_a"
                            clearValue={(e) => console.log(e)}
                            name="category"
                            label={<MessageLang id="stands.CompanyList.Table.activity" />}
                            isDisabled={false}
                            isLoading={loading}
                            isClearable={true}
                            options={categories && categories.map(cat =>
                                ({ value: cat.id, label: cat.name }))}
                            onChange={(value, type) => handleChangeCategory(value, type)}
                        />

                        <SelectInput
                            name="sub_categories"
                            label={<MessageLang id="stands.MemberFilter.form.categories" />}
                            isDisabled={isDisabled}
                            isLoading={loading}
                            isClearable={true}
                            options={subCategories && subCategories.map(sub =>
                                ({ value: sub.id, label: sub.name }))}
                            onChange={(value, type) => handleChangeSubCategory(value, type)}
                        />

                        <SelectInput
                            name="countries"
                            label={<MessageLang id="stands.CompanyList.Table.country" />}
                            isDisabled={false}
                            isLoading={loading}
                            isClearable={true}
                            options={countries && countries.map(country =>
                                ({ value: country.id, label: country.name }))}
                            onChange={(value, type) => handleChangeCountry(value, type)}
                        />
                    </div>
                </div >
            </div >
        </>
    )
}

export default CompanyFilter

