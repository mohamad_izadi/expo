import React, { useState, useEffect } from 'react';
import {
    Button,
    Card,
    Grid,
    Hidden
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import CompanyFilter from './CompanyFilter';
import MemberFilter from './MemberFilter'
import MemberList from './MemberList';
import CompanyList from './CompanyList';
import TabNavs from '../../../component/Tabs/TabNavs'
import GeneralDrawer from '../../../component/Drawer/GeneralDrawer.jsx'
import SearchBox from '../../../component/Search/SearchBox';
import { Modal } from '../../../component';
import { loadMembers, loadCompanies, setMemberFilters, setCompanyFilters } from '../../../app/store/actions/standActions';
import Tour from 'reactour'
import { setTourGuide } from '../../../app/store/actions/authActions'

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

function StandDashboard({ location }) {
    const useStyles = makeStyles((theme) => ({
        root: {
            height: '100%',
            '& .MuiTableCell-root': {
                fontFamily: 'IRANSansFa',
                padding: '8px'
            },
            '& .MuiTableCell-head': {
                padding: '16px'
            },
            '& .MuiTabs-root': {
                height: '100%'
            },
            '& .MuiTabs-flexContainer': {
                height: '100%'
            },
            '& .makeStyles-search-36': {
                borderRadius: '0'
            },
            '& .MuiCardHeader-root': {
                padding: '0'
            },
            '& .MuiSelect-selectMenu': {
                padding: '10px 10px'
            }
        },
        standHeader: {
            height: '72px',
            width: '100%',
            boxShadow: 'none',
            backgroundColor: '#fff',
            borderBottom: '1px solid #eee',

        },
        swipeableViews: {
            margin: '0 auto'
        },
        avatar: {
            marginRight: theme.spacing(2)
        }
    }));

    const classes = useStyles();
    const dispatch = useDispatch()

    const [ShowFilter, SetShowFilter] = useState(false)
    const [isTourOpen, setTourOpen] = useState(false)
    let mobileSize = window.innerWidth < 600 ? true : false
    const { currentUser } = useSelector(state => state.auth);
    const [value, setValue] = useState(location.state?.value || 0);

    const { categories, countries } = useSelector(state => state.persist)

    const handleChange = (newValue) => {
        setValue(newValue);
    };

    const steps = [
        {
            selector: '.MuiAppBar-root',
            content: <>
                <h4 className={'tour_header'}>{<MessageLang id="TourGuide.pavillions.title" />}</h4>
                <p>{<MessageLang id="TourGuide.pavillions.desc" />}</p>
                <Button className={'btn_close'} variant="contained" color="primary"
                    onClick={() => setTourOpen(false)}>
                    <MessageLang id="TourGuide.Button.Ok" />
                </Button>
            </>,
            style: {
                backgroundColor: '#fff',
                textAlign: 'center'
            },
        },
        // ...
    ];

    useEffect(() => {
        if (!currentUser.tour_guide.pavilions) {
            setTourOpen(true)
            dispatch(setTourGuide("pavilions"))
        }
    }, [])

    function handleSearch(value, tab) {

        if (tab === 1) {
            dispatch(setMemberFilters('search', value))
            dispatch(loadMembers())
        }
        else {
            dispatch(setCompanyFilters('search', value))
            dispatch(loadCompanies())
        }
    }

    return (
        <Card className={classes.root}>
            <Grid container>
                <Grid item lg={4} md={4} xs={12} className="stand-list__filter-panel">
                    <Tour
                        steps={steps}
                        isOpen={isTourOpen}
                        onRequestClose={() => setTourOpen(false)}
                        showNavigation={false}
                        showNavigationNumber={false}
                        showNumber={false}
                        showButtons={false}
                        showNavigationScreenReaders={false}
                        disableDotsNavigation={true}
                        className={'tourguide'}
                    />
                    <Grid container>
                        <Grid item xs={12}>
                            <SearchBox onChange={(e) => handleSearch(e.target.value, value)} HandlerFilter={() => SetShowFilter(true)} />
                        </Grid>
                        {ShowFilter && mobileSize && (value === 0 ?
                            <GeneralDrawer from="bottom"
                                onClose={() => SetShowFilter(false)}
                                isOpen={ShowFilter}
                                title={<MessageLang id="stands.StandDashboard.filter.company" />}
                            >
                                <CompanyFilter />
                            </GeneralDrawer>
                            :
                            <GeneralDrawer from="bottom"
                                onClose={() => SetShowFilter(false)}
                                isOpen={ShowFilter}
                                title={<MessageLang id="stands.StandDashboard.filter.parts" />}
                            >
                                <MemberFilter />
                            </GeneralDrawer>
                        )}
                        {ShowFilter && !mobileSize && (value === 0 ?
                            <Modal
                                onClose={() => SetShowFilter(false)}
                                isOpen={ShowFilter}
                                title={<MessageLang id="stands.StandDashboard.filter.company" />}
                                className={classes.root}
                            >
                                <CompanyFilter />
                            </Modal>
                            :
                            <Modal
                                onClose={() => SetShowFilter(false)}
                                isOpen={ShowFilter}
                                title={<MessageLang id="stands.StandDashboard.filter.parts" />}
                                className={classes.root}
                            >
                                <MemberFilter />
                            </Modal>
                        )}
                        <Hidden only={['sm', 'xs']}>
                            <Grid item xs={12}>
                                {value === 0 ? <CompanyFilter /> : <MemberFilter />}
                            </Grid>
                        </Hidden>
                    </Grid>
                </Grid>
                <Grid item lg={8} md={8} xs={12} className={classes.standContent} >
                    <Grid container  >
                        <TabNavs
                            components={[<CompanyList />, <MemberList />]}
                            labels={[
                                <MessageLang id="stands.StandDashboard.text.company" />
                                ,
                                <MessageLang id="stands.StandDashboard.text.people" />
                            ]}
                            defualtValue={value}
                            handleChangeIndex={handleChange}
                            classNames={'tab_header fftabs'} />
                    </Grid>
                </Grid>
            </Grid>
        </Card>
    );
}

export default StandDashboard;