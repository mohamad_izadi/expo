import React, { useEffect } from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
    Table, TableBody, TableCell, TableContainer, TableHead, TextField,
    TableRow, Paper, Avatar, CardHeader, TablePagination,
} from '@material-ui/core'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import * as locales from '@material-ui/core/locale';
import { useDispatch } from 'react-redux';
import { history } from '../../../app/store/configureStore'
import Rating from '@material-ui/lab/Rating';
import { loadCompanies, setCompanyFilters } from '../../../app/store/actions/standActions';
import StandListPreload from './StandListPreload'
//pagination
import FooterPagination from '../../../component/FooterPagination/FooterPagination'
/*************change lang ***************/
import MessageLang, { messageLang } from '../../../lang/';
/*************change lang ***************/


const CompanyList = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(loadCompanies())
    }, [])
    const { loading } = useSelector((state) => state.async);
    const { companies, companyFilters } = useSelector((state) => state.stand, shallowEqual);
    const lang = useSelector((state) => state.lang.lang);

    const handleChangePage = async (event, newPage) => {
        await dispatch(setCompanyFilters('page', newPage + 1))
        dispatch(loadCompanies())
    };

    return (
        <Paper>
            {loading ? <StandListPreload /> :
                <TableContainer className="table_desgin">
                    <Table style={{ width: '100%' }} >
                        <TableHead>
                            {window.innerWidth > 600 ?
                                <TableRow>
                                    <TableCell width='30%'><MessageLang id="stands.CompanyList.Table.company" /></TableCell>
                                    <TableCell align="center" width='20%'><MessageLang id="stands.CompanyList.Table.activity" /></TableCell>
                                    <TableCell align="center" width='15%'><MessageLang id="stands.CompanyList.Table.country" /></TableCell>
                                    <TableCell align="center" width='15%'><MessageLang id="stands.CompanyList.Table.counter" /></TableCell>
                                    <TableCell align="center" width='20%'><MessageLang id="stands.CompanyList.TableCell.rate" /></TableCell>
                                </TableRow>
                                :
                                <TableRow>
                                    <TableCell width='60%'><MessageLang id="stands.CompanyList.Table.company" /></TableCell>
                                    <TableCell width='40%'><MessageLang id="stands.CompanyList.Table.activity" /></TableCell>
                                    {/* <TableCell align="center" width='33%'>امتیاز بازدید کنندگان</TableCell> */}
                                </TableRow>
                            }
                        </TableHead>
                        <TableBody>
                            {window.innerWidth > 600 ?
                                companies.map((row) => (
                                    <TableRow onClick={() => { history.push({ pathname: `exhibition/${row.id}` }) }} key={row.id} className={'company-row'}>
                                        <TableCell component="th" scope="row">
                                            <CardHeader
                                                avatar={
                                                    <Avatar alt="" src={row.logo ? row.logo : "/static/images/default.png"} width={30} height={30} />
                                                }
                                                title={row.name}
                                            />
                                        </TableCell>
                                        <TableCell align="center">{row.category}</TableCell>
                                        <TableCell align="center">{row.country}</TableCell>
                                        <TableCell align="center">{`${row.members}  ${messageLang("stands.CompanyList.TableCell.people")}`}</TableCell>
                                        <TableCell align="center">
                                            <Rating
                                                name="simple-controlled"
                                                value={row.points}
                                                size="small"
                                            />
                                        </TableCell>
                                    </TableRow>
                                ))
                                :
                                companies.map((row) =>
                                    <TableRow onClick={() => { history.push(`exhibition/${row.id}`) }} key={row.id} className={'company-row'}>
                                        <TableCell component="th" scope="row">
                                            <CardHeader
                                                avatar={
                                                    <Avatar alt="" src={row.logo ? row.logo : "/static/images/default.png"} width={30} height={30} />
                                                }
                                                title={row.name}
                                                subheader={`${row.members}  ${messageLang("stands.CompanyList.TableCell.people")}`}
                                            />
                                        </TableCell>
                                        <TableCell>
                                            <div>
                                                {row.category}
                                                <span>{row.country}</span>
                                            </div>
                                        </TableCell>
                                        {/* <TableCell align="center">{row.}</TableCell> */}
                                    </TableRow>
                                )
                            }
                        </TableBody>
                    </Table>
                </TableContainer>}
            <FooterPagination
                lang={lang}
                total={companyFilters.total}
                limit={companyFilters.limit}
                page={companyFilters.page - 1}
                ChangePage={handleChangePage}
            />
        </Paper>
    );
}

export default React.memo(CompanyList);



