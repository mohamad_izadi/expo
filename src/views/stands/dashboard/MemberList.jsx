import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {
    Table, TableBody, TableCell, TableContainer, TableHead,
    TableRow, Paper, Avatar, CardHeader, TablePagination
} from '@material-ui/core';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import * as locales from '@material-ui/core/locale';
import { makeStyles } from '@material-ui/core/styles';
import { loadMembers, setMemberFilters } from '../../../app/store/actions/standActions'
import StandListPreload from './StandListPreload'
import AddMeeting from './AddMeeting';
//pagination
import FooterPagination from '../../../component/FooterPagination/FooterPagination'
/*************change lang ***************/
import MessageLang, { messageLang } from '../../../lang/';
/*************change lang ***************/



const useStyles = makeStyles((theme) => ({
    root: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
        height: '100%'
    },
    table: {
        minWidth: '100%',
        width: '100%',
        // minHeight: '440px'
    },
    container: {
        // maxHeight: 440,
        //   minHeight:window.innerHeight
    },
}))


const MemberList = (props) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const { members, memberFilters } = useSelector((state) => state.stand);
    const lang = useSelector((state) => state.lang.lang);
    const { loading } = useSelector((state) => state.async);
    const [openDrawer, setOpenDrawer] = useState(false);

    const desk = window.innerWidth > 600 ? true : false

    const handleChangePage = async (event, newPage) => {
        await dispatch(setMemberFilters('page', newPage + 1))
        dispatch(loadMembers())
    };
    useEffect(() => {
        dispatch(loadMembers())
    }, [dispatch])

    return (
        <Paper>
            {openDrawer && <AddMeeting open={openDrawer} onClose={() => setOpenDrawer(false)} />}
            {loading ? <StandListPreload /> :
                <TableContainer component={Paper} className='table_desgin'>
                    <Table className={`${classes.table} `} aria-label="simple table" style={{ width: '100%' }}>
                        <TableHead>
                            {desk ?
                                <TableRow>
                                    <TableCell width='30%'><MessageLang id="stands.MemberFilter.form.family" /></TableCell>
                                    <TableCell width='35%' align="center"><MessageLang id="stands.MemberFilter.form.company" /></TableCell>
                                    <TableCell width='15%' align="center"><MessageLang id="stands.MemberFilter.form.position" /></TableCell>
                                    <TableCell width='20%' align="center"><MessageLang id="stands.MemberFilter.form.country" /></TableCell>
                                </TableRow>
                                :
                                <TableRow>
                                    <TableCell width='30%'><MessageLang id="stands.MemberFilter.form.spec" /></TableCell>
                                    <TableCell width='20%' align="center"><MessageLang id="stands.MemberFilter.form.company" /></TableCell>
                                    <TableCell width='30%' align="center"><MessageLang id="stands.MemberFilter.form.position" /></TableCell>
                                </TableRow>
                            }
                        </TableHead>
                        <TableBody>
                            {desk ?
                                members.map((row) =>
                                    <TableRow key={row.id} className={'company-row'}>
                                        <TableCell
                                            component="th" scope="row"
                                            onClick={() => setOpenDrawer(row.id)}>
                                            <CardHeader
                                                avatar={
                                                    <Avatar alt="" src={row.avatar ? row.avatar : "/static/images/user.png"} width={30} height={30} />
                                                }
                                                title={row.fullname}
                                            />
                                        </TableCell>
                                        <TableCell align="center">{row.company}</TableCell>
                                        <TableCell align="center">{row.title}</TableCell>
                                        <TableCell align="center">{row.country}</TableCell>
                                    </TableRow>
                                )
                                :
                                members.map((row) =>
                                    <TableRow key={row.id} className={'company-row'}>
                                        <TableCell
                                            component="th" scope="row"
                                            onClick={() => setOpenDrawer(row.id)}>
                                            <CardHeader
                                                avatar={
                                                    <Avatar alt="" src={row.avatar ? row.avatar : "/static/images/user.png"} width={30} height={30} />
                                                }
                                                title={row.fullname}
                                            />
                                        </TableCell>
                                        <TableCell align="center">{row.company}</TableCell>
                                        <TableCell align="center">{row.title}</TableCell>
                                    </TableRow>
                                )
                            }
                        </TableBody>
                    </Table>
                </TableContainer>}
            <FooterPagination
                lang={lang}
                total={memberFilters.total}
                limit={memberFilters.limit}
                page={memberFilters.page - 1}
                ChangePage={handleChangePage}
            />
        </Paper>
    );
}

export default React.memo(MemberList);

