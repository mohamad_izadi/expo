import React from 'react';

import Rating from '@material-ui/lab/Rating';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

const CommentItem = ({ item,key }) => {
    const [value, setValue] = React.useState(2);
    return (
        <div className="comments-item-pavilion" key={key}>
            <ListItem alignItems="flex-start">
                <ListItemAvatar>
                <Avatar alt="" src={item.user.avatar} />
                </ListItemAvatar>
                <ListItemText
                primary={item.user.full_name}
                secondary={
                    <React.Fragment>
                    <Typography
                        component="span"
                        variant="body2"
                        // className={classes.inline}
                        style={{display:"block"}}
                        color="textPrimary"
                    >
                    {item.user.company_name}
                    </Typography>
                    {item.comment}
                    </React.Fragment>
                }
                />
            </ListItem>
            <div className="comments-item-pavilion__date">
                {item.created_at}
            </div>

        </div>
    )
}

export default CommentItem
