import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import StandInfoIntroduction from './StandInfoIntroduction';
import StandInfoMembers from './StandInfoMembers';
import StandInfoProduction from './StandInfoProduction';
import StandInfoComments from './StandInfoComments';
import StandInfoCooperation from './StandInfoCooperation';
import TabNavs from '../../../component/Tabs/TabNavs'
import { Grid } from '@material-ui/core';
import BookIcon from '@material-ui/icons/Book';
import ChromeReaderModeIcon from '@material-ui/icons/ChromeReaderMode';
import LocalMallIcon from '@material-ui/icons/LocalMall';
import ModeCommentIcon from '@material-ui/icons/ModeComment';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import { loadPavilionsDetails, loadPavilionsMembers, loadPavilionsComments, loadPavilionsProducts } from '../../../app/store/actions/pavilionsAction'
import { fetchpavilionopportunities, fetcJobOpportunities } from '../../../app/store/actions/opportunitiesActions';

import { IntroductionPreloadings, MemberPreloadings } from './StandInfoPreloadings'

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const StandInfo = ({ match, history }) => {
    function TabPanel(props) {
        const { children, value, index, ...other } = props;

        return (
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`full-width-tabpanel-${index}`}
                aria-labelledby={`full-width-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Box>
                        <Typography>{children}</Typography>
                    </Box>
                )}
            </div>
        );
    }

    TabPanel.propTypes = {
        children: PropTypes.node,
        index: PropTypes.any.isRequired,
        value: PropTypes.any.isRequired,
    };

    function a11yProps(index) {
        return {
            id: `full-width-tab-${index}`,
            'aria-controls': `full-width-tabpanel-${index}`,
        };
    }


    /*----------------------------------------------*/
    const dispatch = useDispatch();
    const { details, members, products } = useSelector((state) => state.pavilion);
    const { opportunities } = useSelector((state) => state.opportunities);
    const [abtLoading, setAbtLoading] = useState(true)
    const [mbrLoading, setMbrLoading] = useState(true)
    const [prdLoading, setPrdLoading] = useState(true)

    useEffect(() => {
        async function loadContents() {
            dispatch(loadPavilionsDetails(match.params.id, match.params.id, 'about'))
            setAbtLoading(false)
            await dispatch(loadPavilionsMembers(match.params.id))
            setMbrLoading(false)
            await dispatch(loadPavilionsProducts(match.params.id))
            setPrdLoading(false)
            await dispatch(loadPavilionsComments(match.params.id))
            setPrdLoading(false)
            dispatch(await fetchpavilionopportunities(match.params.id))
            setPrdLoading(false)
        }
        loadContents();
    }, [])

    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    return (
        <div className={"stand-info-stand"} >
            <AppBar position="static" color="default" style={{ maxWidth: "100%" }}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    aria-label="full width tabs example"
                    fullWidth
                    scrollable
                    scrollButtons={"on"}
                >
                    <Tab label={<MessageLang id="stands.details.StandDetails.introduction" />} icon={<ChromeReaderModeIcon />} {...a11yProps(0)} />
                    <Tab label={<MessageLang id="stands.details.StandDetails.people" />} icon={<PeopleAltIcon />} {...a11yProps(1)} />
                    <Tab label={<MessageLang id="stands.details.StandDetails.products" />} icon={<LocalMallIcon />} {...a11yProps(2)} />
                    <Tab label={<MessageLang id="stands.details.StandDetails.conversations" />} icon={<ModeCommentIcon />} {...a11yProps(3)} />
                    <Tab label={<MessageLang id="stands.details.StandDetails.cooperations" />} icon={<BookIcon />} {...a11yProps(4)} />
                </Tabs>
            </AppBar>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={value} index={0} dir={theme.direction}>
                    {abtLoading ? <IntroductionPreloadings /> : <StandInfoIntroduction details={details} />}
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                    {mbrLoading ? <MemberPreloadings /> : <StandInfoMembers members={members} />}
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                    <StandInfoProduction id={match.params.id} loading={prdLoading} products={products} />
                </TabPanel>
                <TabPanel value={value} index={3} dir={theme.direction}>
                    <StandInfoComments id={match.params.id} />
                </TabPanel>
                <TabPanel value={value} index={4} dir={theme.direction}>
                    <StandInfoCooperation opportunities={opportunities} pavilion_id={match.params.id} />
                </TabPanel>
            </SwipeableViews>
        </div>
    );
}

export default StandInfo
