import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import clsx from 'clsx';
import { loadPavilionsProducts } from '../../../app/store/actions/pavilionsAction'
import {
    Card,
    makeStyles,
    Hidden,
    Box, Typography, IconButton

} from '@material-ui/core';

import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import Grid from '@material-ui/core/Grid';
import SearchBox from '../../../component/Search/SearchBox';
import ProductListFilter from '../../product/dashboard/ProductListFilter';
import ProductListHeader from '../../product/dashboard/ProductListHeader';
import ProductListItem from '../../product/dashboard/ProductListItem';
import ProductListPreload from '../../product/dashboard/ProductListPreload';
import GeneralDrawer from '../../../component/Drawer/GeneralDrawer.jsx'
import { Modal } from '../../../component';
import { loadProducts, setProductFilters } from '../../../app/store/actions/productActions'

/*************change lang ***************/
import MessageLang, { messageLang } from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    paperExhebition: {
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

const StandInfoProduction = ({ className, id, products, loading, ...rest }) => {

    const classes = useStyles();
    const [ShowFilter, SetShowFilter] = useState(false)
    const dispatch = useDispatch()

    let mobileSize = window.innerWidth < 600 ? true : false
    const default_List_height = mobileSize ? window.innerHeight - 130 : window.innerHeight - 58
    const pageheight = window.innerHeight - 58

    const handleSearchFilter = async (event) => {
        // dispatch(loadPavilionsProducts(id,null,null,event.target.value))
        // await dispatch(setProductFilters('search', event.target.value))
        // dispatch(loadProducts())
    };


    return (
        <>
            <Card
                className={`${clsx(classes.root, className)} tabcontent-stand `}
                {...rest}
            >
                <Grid container className="product-list"
                    style={{ height: pageheight, maxHeight: pageheight }}>

                    <Grid item md={12} xs={12} >
                        {loading ?
                            <ProductListPreload /> :
                            products && products.length > 0 ?
                                <div className={'product-list__products stand-product'} >
                                    <div className={'product-list__products__show stand-product__show'}
                                        style={{ height: default_List_height, maxHeight: default_List_height, position: 'relative' }}>
                                        {products.map(
                                            product => (
                                                <ProductListItem
                                                    key={product.id} product={product}
                                                />
                                            )
                                        )}
                                    </div>
                                </div>
                                :
                                <div className='exhebition-emptypages tabcontent-stand'>
                                    <Box>
                                        <img src={"/static/images/backgrounds/SVG/lectures.svg"} />
                                        <Typography >
                                            <MessageLang id="stands.info.products.nothings" />
                                        </Typography>
                                    </Box>
                                </div>
                        }
                    </Grid>
                </Grid>
            </Card>
        </>
    )
}

export default StandInfoProduction