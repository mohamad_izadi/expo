import React from 'react'
import Skeleton from '@material-ui/lab/Skeleton';

export const IntroductionPreloadings = () => {
    return (
        <div className="exhebition-introduction tabcontent-stand">
            {/* <div className="exhebition__slider">
                <div class="carousel-wrapper">
                    <Skeleton animation="wave" variant="rect" width="100%" height='200px' />
                </div>
            </div> */}

            <div className="exhebition-introduction__content">
                <div className="exhebition-introduction__title">
                </div>
                <div className="exhebition-introduction__description">
                    <Skeleton style={{ margin: '10px 0' }} animation="wave" height={25} width="15%" />
                    <Skeleton style={{ marginBottom: '8px' }} animation="wave" height={20} width="95%" />
                    <Skeleton style={{ marginBottom: '8px' }} animation="wave" height={20} width="90%" />
                    <Skeleton style={{ marginBottom: '8px' }} animation="wave" height={20} width="95%" />
                    <Skeleton style={{ marginBottom: '8px' }} animation="wave" height={20} width="50%" />
                    <Skeleton style={{ marginBottom: '8px' }} animation="wave" height={20} width="90%" />
                    <Skeleton style={{ marginBottom: '8px' }} animation="wave" height={20} width="95%" />
                </div>
            </div>
        </div>
    )
}

export const MemberPreloadings = () => {
    return (
        <div></div>
    )
}

