import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {
    Table, TableBody, TableCell, TableContainer, TableHead,
    TableRow, Paper, Avatar, CardHeader, IconButton, Box
} from '@material-ui/core';
import { People } from '@material-ui/icons';
import { useHistory } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import { VoiceChat, Message } from '@material-ui/icons';
import { createConversation } from '../../../app/store/actions/chatActions'
import { loadMemberDetails } from '../../../app/store/actions/standActions'
import MeetingInvitationCreate from '../../meeting/meetingDashboard/MeetingInvitationCreate';
import { SettingModal } from '../../../app/store/actions/standActions'
import MessageLang from '../../../lang/';


const useStyles = makeStyles({
    root: {
        width: '100%',
        '& .MuiCardHeader-root': {
            padding: 0
        }
    },
    container: {
        // maxHeight: 440,
        //   minHeight:window.innerHeight
    },
});



const StandInfoMembers = ({ members }) => {

    const classes = useStyles();
    const dispatch = useDispatch();
    let history = useHistory();
    const [loadingHandel, setloadingHandel] = useState(false)
    // const [openModal, setOpenModal] = useState(false);
    const exhibition = useSelector(state => state.exhibition.data);
    const { openModal } = useSelector(state => state.stand)

    const createNewConversation = async (e, userId) => {
        e.stopPropagation();
        setloadingHandel(true)
        try {
            await dispatch(createConversation(userId))
        } catch (error) {
            console.log(error.message);
        } finally {
            history.push(`/${exhibition.short_name}/chat`)
            setloadingHandel(false)
        }
    }

    const handelOpenMeeting = async (e, id) => {
        e.stopPropagation();
        setloadingHandel(true)
        await dispatch(loadMemberDetails(id));
        dispatch(SettingModal(true))
        setloadingHandel(false)
    }


    return (
        // <Div100vh>
            <Paper className={`${classes.root} tabcontent-stand`}>
                {openModal && <MeetingInvitationCreate
                    open={openModal}
                    handleClose={() => dispatch(SettingModal(false))} />
                }
                <TableContainer className={`${classes.container} table_desgin `}>
                    <Table style={{ width: '100%' }} >
                        <TableHead>
                            {window.innerWidth > 600 ?
                                <TableRow>
                                    <TableCell width='20%' align="center"><MessageLang id="exhibition.members.TableCell1"/></TableCell>
                                    <TableCell width='20%' align="center"><MessageLang id="exhibition.members.TableCell2"/></TableCell>
                                    <TableCell width='20%' align="center"><MessageLang id="exhibition.members.TableCell3"/></TableCell>
                                    <TableCell width='20%' align="center"><MessageLang id="exhibition.members.TableCell4"/></TableCell>
                                    <TableCell width='20%' align="center"></TableCell>
                                </TableRow>
                                :
                                <TableRow>
                                    <TableCell width='40%' className='membertab-title'><MessageLang id="exhibition.members.TableCell6"/><People /></TableCell>
                                    <TableCell width='30%' align="center"><MessageLang id="exhibition.members.TableCell2"/></TableCell>
                                    <TableCell width='30%' align="center"><MessageLang id="exhibition.members.TableCell3"/></TableCell>
                                </TableRow>
                            }
                        </TableHead>
                        <TableBody>
                            {members && members.map((row, index) => {
                                return (
                                    window.innerWidth > 600 ?
                                        <TableRow key={index}>
                                            <TableCell
                                                component="th" scope="row"
                                                align="center"
                                            >
                                                <CardHeader
                                                    avatar={
                                                        <Avatar alt="" src={row.avatar ? row.avatar : "/static/images/user.png"} width={30} height={30} />
                                                    }
                                                    title={row.name}
                                                />
                                            </TableCell>
                                            {/* <TableCell align="left">{row.name}</TableCell> */}
                                            <TableCell align="center">{row.title}</TableCell>
                                            <TableCell align="center">{row.phone ? row.phone : '-'}</TableCell>
                                            <TableCell align="center">{row.country ? row.country : '-'}</TableCell>
                                            <TableCell align="center">
                                                <Box className="memberlist-icon">
                                                {/* <IconButton disabled={loadingHandel} onClickCapture={(e) => handelOpenMeeting(e, row.member_id)}> */}
                                                <IconButton disabled={loadingHandel} onClickCapture={(e) => handelOpenMeeting(e, row.member_id)}>
                                                    <VoiceChat />
                                                </IconButton >
                                                <IconButton disabled={loadingHandel} onClickCapture={(e) => createNewConversation(e, row.id)}>
                                                    <Message />
                                                </IconButton >
                                            </Box>
                                        </TableCell>
                                    </TableRow>
                                    :
                                    <TableRow key={index} >
                                        <TableCell
                                            component="th" scope="row">
                                            <CardHeader
                                                avatar={
                                                    <Avatar alt="" src={row.avatar ? row.avatar : "/static/images/user.png"} width={30} height={30} />
                                                }
                                                title={row.name}
                                            />
                                        </TableCell>
                                        <TableCell align="center">{row.title}</TableCell>
                                        <TableCell align="center">{row.phone ? row.phone : '-'}</TableCell>
                                    </TableRow>

                            )
                        })
                        }
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>
        // </Div100vh>
    )
}

export default StandInfoMembers
