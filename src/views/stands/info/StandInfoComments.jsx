import React, { useState , useRef , useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { Box, Typography, Avatar, BottomNavigation, CircularProgress, CardHeader } from '@material-ui/core'
import {createComment} from '../../../app/store/actions/pavilionsAction'
import SendIcon from '@material-ui/icons/Send';
import Rating from '@material-ui/lab/Rating';
import ChatIcon from '@material-ui/icons/Chat';
import Loading from '../../../component/Loading/Loading';
import CommentItem from './CommentItem'
import { addComment } from '../../../app/store/actions/productActions'
import Div100vh from 'react-div-100vh'
import Sendbox_input from '../../../component/SendBox_Input/Sendbox_input'
/*************change lang ***************/
import MessageLang, { messageLang } from '../../../lang/';
/*************change lang ***************/

const StandInfoComments = ({id}) => {
    const dispatch = useDispatch()
    const [value, setValue] = React.useState(0);
    const [msg, setMsg] = useState('')
    const { loading } = useSelector(state => state.async)
    const { comments, loadingComments } = useSelector((state) => state.pavilion);
    const commentlist = useRef(null)

    useEffect(()=>{
        if(commentlist.current){
            const target= commentlist.current;
            target.scroll({ top: target.scrollHeight, behavior: 'smooth' });
        }
    },[commentlist])

    const createComment_func=(msg)=>{
        dispatch(createComment(id, msg))
    }

    const handleKeyDown =(event,msg)=>{
        if (event.key === 'Enter') {
            createComment_func(msg)
        }
    }

    return (
        // <Div100vh>
            <Box className='exhebition-emptypages-cover '>
                <div className={comments && comments.total>0  ? 'tabcontent-stand' :'exhebition-emptypages tabcontent-stand'}>
                    {comments && comments.total>0 ?
                    /*-------------------------*/
                    <>
                        <div className="exhebition-comments__header">
                            <div className="exhebition-comments__header__icon">
                                <ChatIcon />
                            </div>

                            <div className="exhebition-comments__header__title">
                                <h1><MessageLang id="exhibition.coments.title"/></h1>
                                <h2>{window.innerWidth > 600 ? <MessageLang id="exhibition.comments.subtitle"/> : <MessageLang id="exhibition.comments.subtitle-small"/>}</h2>
                            </div>
                        </div>
                        {
                            loadingComments && <Loading />
                        }
                        <div className="exhebition-comments__list" ref={commentlist}>
                            {
                                comments && comments.data.map((item, index) => {
                                    return (
                                        <CommentItem item={item} key={index} />
                                    )
                                })
                            }
                        </div>
                    </> 
                    /*-------------------------*/
                    :
                    <Box>
                        <img src={"/static/images/backgrounds/SVG/message.svg"} />
                        <Typography >
                            <MessageLang id="stands.info.comment.nothings1" />
                            <br/>
                            <MessageLang id="stands.info.comment.nothings2" />
                        </Typography>
                    </Box>
                    }
                </div>
                <Sendbox_input
                    Value={value}
                    Msg={msg}
                    onchange={e=>setMsg(e.target.value)}
                    Placeholder={
                        window.innerWidth > 600 ? messageLang('stands.info.comment.placeholder.enterText') : messageLang('stands.info.comment.placeholder.enterText-minsize')}
                    Send={createComment_func}
                    Sendbykey={handleKeyDown}
                />
            </Box>
        // {/* </Div100vh> */}
    )
}

export default StandInfoComments
