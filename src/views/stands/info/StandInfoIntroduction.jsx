import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { loadPavilionsDetails } from '../../../app/store/actions/pavilionsAction'
import {Call,Public,Instagram,LinkedIn} from '@material-ui/icons';
import {Box,Typography,withStyles} from '@material-ui/core';
import Carousel from 'react-elastic-carousel'
import Div100vh from 'react-div-100vh'
import Tooltip from '@material-ui/core/Tooltip';
import ShowTellModal from './TellModal';


/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const HtmlTooltip = withStyles((theme) => ({
    tooltip: {
        backgroundColor: '#f5f5f9',
        color: 'rgba(0, 0, 0, 0.87)',
        maxWidth: 220,
        fontSize: theme.typography.pxToRem(12),
        border: '1px solid #dadde9',
    },
}))(Tooltip);

const StandInfoIntroduction = ({ details }) => {
    const [showphonenumber,setshowphonenumber]=useState(false)
    const [phonenumber,setphonenumber]=useState(null)
    var phones=[]
    if(details.phone_numbers){
        phones=details.phone_numbers.split(" ")
    }

    const handleClick = (url) => {
        if(url !== null){
            const win = window.open(`http://${url}`);
            win.focus();
        }
    }

    const openTellModal=()=>{
        setphonenumber(phones)
        if(phones.length>1){
            setshowphonenumber(true)
        }
    }


    return (
        <Div100vh className='tabcontent-stand'>
        {showphonenumber && <ShowTellModal Values={phonenumber} open={true} Close={()=>setshowphonenumber(false)} />}
        <div div className="exhebition-introduction tabcontent-stand" >
            <div className="exhebition__slider">
                {/* <div class="carousel-wrapper">
                    {(details && details.media && details.media.videos.length > 0) ?
                        <Carousel showThumbs={false} showStatus={false}>
                            {details.media.catalogs.map(image =>
                                <div>
                                    <img alt={details.name} src={image} />
                                </div>
                            )}
                        </Carousel> : <img width='100%' alt='' src='/static/images/backgrounds/SVG/slider.png' />}
                </div> */}
            </div>

            <div className="exhebition-introduction__content">
                <div className="exhebition-introduction__title">
                    {details.name}
                </div>
                <div className="exhebition-introduction__description"
                    dangerouslySetInnerHTML={{ __html: details.about }}>
                </div>
                
            </div>
            <div className="exhebition-introduction__footer">
                <a href={phones.length>1 ? null : `tel:${details.phone_numbers}`} className='exhebition-introduction__footer__tell'>
                    <HtmlTooltip
                        title={
                            <React.Fragment>
                                <Typography color="inherit">
                                    {phones.length>1? 
                                    <MessageLang id="stands.info.introduction.call.tooltip"/>:details.phone_numbers}
                            </Typography>
                            </React.Fragment>
                        }
                    >
                            <Box className='exhebition-introduction__footer__item' onClick={()=>openTellModal()}>
                                <span className={details.phone_numbers===undefined ? `logo-cover disable` : `logo-cover active`}><Call/></span>
                                <Typography><MessageLang id="stands.info.introduction.call"/></Typography>
                            </Box>
                    </HtmlTooltip>
                </a>

                <Box className='exhebition-introduction__footer__item'  onClick={() => handleClick(details.website_url)}>
                    <span className={details.website_url===undefined ? `logo-cover disable` :`logo-cover active`}><Public/></span>
                    <Typography><MessageLang id="stands.info.introduction.site"/></Typography>
                </Box>
                <Box className='exhebition-introduction__footer__item' onClick={() => handleClick(details.social_networks.linkedin)}>
                    <span className={details.social_networks && details.social_networks.linkedin !==null ? `logo-cover active` : `logo-cover disable`}><LinkedIn/></span>
                    <Typography><MessageLang id="stands.info.introduction.linkedin"/></Typography>
                </Box>
                <Box className={'exhebition-introduction__footer__item'} onClick={() => handleClick(details.social_networks.instagram)} >
                    <span className={details.social_networks && details.social_networks.instagram !== null ? `logo-cover active` : `logo-cover disable`}><Instagram/></span>
                    <Typography><MessageLang id="stands.info.introduction.instagram"/></Typography>
                </Box>
                
            </div>
        </div>
        </Div100vh>
    )
}

export default StandInfoIntroduction
