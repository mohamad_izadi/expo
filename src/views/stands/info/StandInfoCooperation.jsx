import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import {
    Button, Hidden, Box, Typography, Grid, Table, TableCell, TableContainer, TableHead, IconButton,
    TableRow, Paper,
} from '@material-ui/core';
import { SelectInput, SelectSort } from '../../../component'
import MessageLang from '../../../lang/'
import { setopportunityFilters, fetcJobOpportunities } from '../../../app/store/actions/opportunitiesActions';
import StandListPreload from '../../exhibition/CoorporationSkeleton'
import LocationOnIcon from '@material-ui/icons/LocationOn';
import SearchBox from '../../../component/Search/SearchBox';
import EmailIcon from '@material-ui/icons/Email';
import Done from '@material-ui/icons/Done';
import CircularProgress from '@material-ui/core/CircularProgress';
import Sendrequest from '../../opportunity/dashboard/Sendrequest'
import { fetcMyOpportunities, fetcOpportunities } from '../../../app/store/actions/opportunitiesActions'
import { fetchpavilionopportunities } from '../../../app/store/actions/opportunitiesActions';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
//Search
import { Modal } from '../../../component';
import GeneralDrawer from '../../../component/Drawer/GeneralDrawer.jsx'
import { Category } from 'emoji-mart';
//pagination
import FooterPagination from '../../../component/FooterPagination/FooterPagination'

const StandInfoCooperation = ({ opportunities, pavilion_id }) => {
    const [ShowFilter, SetShowFilter] = useState(false)
    const [loading, setLoading] = useState(false)
    const [open, setOpen] = useState(false)
    const [requestid, setrequestid] = useState(0)
    const lang = useSelector((state) => state.lang.lang);

    const dispatch = useDispatch();

    let mobileSize = window.innerWidth < 600 ? true : false

    const showrequestFunc = (id) => {
        setrequestid(id)
        setOpen(true)
    }
    const showallopportunities = (x) => {
        setOpen(false)
    }

    // function handleSearch(search, type) {
    //     // dispatch(setopportunityFilters('search', value))
    //     // dispatch(fetcJobOpportunities());
    //     dispatch(fetchpavilionopportunities(pavilion_id, search))
    // }
    const handleSearchFilter = async (event) => {
        await dispatch(setopportunityFilters('search', event.target.value))
        dispatch(fetcOpportunities('job'));
    };

    return (
        <>
            <Grid container className='tabcontent-stand'>
                <Grid item md={4} xs={12} className="stand-list__filter-panel">
                    <Grid container>
                        <Grid item xs={12}>
                            {/* <SearchBox HandlerFilter={() => SetShowFilter(true)} onChange={(e) => handleSearch(e.target.value)}/> */}
                            <SearchBox onChange={e => handleSearchFilter(e)} HandlerFilter={() => SetShowFilter(true)} />
                        </Grid>
                        {ShowFilter && mobileSize ?
                            <GeneralDrawer from="bottom"
                                onClose={() => SetShowFilter(false)}
                                isOpen={ShowFilter}
                                title="فیلتر کردن بین فرصت های همکاری"
                            >
                                <SelectSort
                                    name="mostVisited"
                                    label={<MessageLang id="exhibition.rightSide.switchLable" />}
                                    isDisabled={false}
                                // checked={isSort.mostVisited}
                                // onChange={() => handelSelectSort("mostVisited")}
                                />
                                <SelectInput
                                    name="company"
                                    label={<MessageLang id="Oppotunity.opportunityDashboard.company" />}
                                    isClearable={true}
                                // options={companies && companies.map(company =>
                                //     ({ value: company.id, label: company.name }))}
                                // onChange={(value, type) => handleChangeFilter(value, type)}
                                />
                            </GeneralDrawer>
                            :
                            <Modal
                                onClose={() => SetShowFilter(false)}
                                isOpen={ShowFilter}
                                title="فیلتر کردن بین فرصت های شغلی"
                            // className={classes.root}
                            >
                                {/* <OpportunityFilter
                                        onChange={(event) => {
                                            dispatch(fetcJobOpportunities())
                                        }}
                                     />  */}
                            </Modal>
                        }
                        <Hidden only={['sm', 'xs']}>
                            <Grid item xs={12}>
                                {
                                    // <OpportunityFilter
                                    // onChange={(event) => {
                                    //     dispatch(fetcJobOpportunities())
                                    // }}
                                    //  /> 
                                }

                            </Grid>
                        </Hidden>
                    </Grid>
                </Grid>
                <Grid item md={8} xs={12} className={` rightside`}>
                    {opportunities && opportunities.length > 0 ?
                        <>
                            {!open ?
                                <div width={'100%'}>
                                    {/* <div style={{ width: '100%' }} className='table_footer products_footer'>
                                        <span>
                                            <MessageLang id="General.page" />
                                            {1}
                                            <MessageLang id="General.from" />
                                            {1}
                                        </span>
                                        <div>
                                            <IconButton aria-label="close"
                                            // onClick={handleClose} 
                                            >
                                                <KeyboardArrowRight />
                                            </IconButton>
                                            <IconButton aria-label="close"
                                            // onClick={handleClose} 
                                            >
                                                <KeyboardArrowLeft />
                                            </IconButton>
                                        </div>
                                    </div> */}
                                    <FooterPagination
                                        className='products_footer'
                                        lang={lang}
                                        total={'10'}
                                        limit={'10'}
                                        page={'1'}
                                    // ChangePage={handleChangePage}
                                    />
                                    <Box className='opportunitypage__content' height="100%">

                                        <TableContainer component={Paper} className="table_desgin opportunitypage__content__table">
                                            <Table aria-label="simple table ">
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell align="left" width={mobileSize ? '50%' : '70%'}><MessageLang id="stands.info.cooperation.SpecificationsColleague" /></TableCell>
                                                        <TableCell align="right" width={mobileSize ? '50%' : '30%'} ><MessageLang id="stands.info.cooperation.SpecificationsUser" /></TableCell>
                                                    </TableRow>
                                                </TableHead>
                                            </Table>

                                            <Paper className='opportunity-list'>
                                                {opportunities.map((item, index) => (
                                                    <div className="opportunity-item">
                                                        <div className="opportunity-item__top cooperation_top">
                                                            <div className="opportunity_standtitle">{item.title}</div>
                                                            <Box className='stand-opportunity'>
                                                                <div className="opportunity-item__top__type-stand">
                                                                    {item.type == 'job' ? <MessageLang id="exhibition.coopeationItem.opportunities" /> : <MessageLang id="exhibition.coopeationItem.opportunitiestwo" />}
                                                                </div>
                                                                <div className="opportunity-item__top__location">
                                                                    <div><LocationOnIcon /></div> <div>{item.location ? item.location.name : 'تهران'}</div>
                                                                </div>
                                                            </Box>

                                                        </div>
                                                        <div className="opportunity-item__description">
                                                            {
                                                                item.description ? item.description : <MessageLang id="Oppotunity.opportunityItem.description" />
                                                            }
                                                        </div>
                                                        {item.is_requested ?
                                                            <Button
                                                                className='mysuggest autowith'
                                                                variant="contained"
                                                                startIcon={<Done />}
                                                            >
                                                                {
                                                                    loading ? <CircularProgress className="opportunity-item__loading" /> : <MessageLang id="exhibition.cooperationItem.loading" />
                                                                }
                                                            </Button>
                                                            :
                                                            <Button
                                                                className='standsuggest'
                                                                variant="contained"
                                                                onClick={() => {
                                                                    if (!item.is_requested) {
                                                                        showrequestFunc(item)
                                                                    }
                                                                }}
                                                                startIcon={item.is_requested ? <Done /> : <EmailIcon />}
                                                            >
                                                                {
                                                                    loading ? <CircularProgress className="opportunity-item__loading" /> : <MessageLang id="exhibition.cooperationItem.loadingtwo" />
                                                                }
                                                            </Button>}
                                                    </div>
                                                ))}
                                            </Paper >
                                        </TableContainer>

                                    </Box>
                                </div>
                                :
                                <Sendrequest item={requestid} back={showallopportunities} from='stand' pavilion_id={pavilion_id} job={requestid.type === "job" ? true : false} />
                            }
                        </>
                        :
                        <div className='exhebition-emptypages tabcontent-stand' style={{ width: '100%' }}>
                            <Box>
                                <img src={"/static/images/backgrounds/SVG/opportunity.svg"} />
                                <Typography >
                                    <MessageLang id="stands.info.cooperation.nothings" />
                                </Typography>
                            </Box>
                        </div>
                    }
                </Grid>
            </Grid>


        </>
    )
}

export default StandInfoCooperation
