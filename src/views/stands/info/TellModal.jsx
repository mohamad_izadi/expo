
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';
import CallIcon from '@material-ui/icons/Call';
import { blue } from '@material-ui/core/colors';

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles({
  avatar: {
    backgroundColor:'#475974',
    color: '#fff',
  },
  item:{
      cursor:'pointer'
  }
});

const TellModal=({Values,Close})=> {
  const classes = useStyles();

  const handleClose = () => {
    Close()
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={true} className='tellmodal'>
      <DialogTitle id="simple-dialog-title">
        <MessageLang id="stands.details.phonenumber-title" />
      </DialogTitle>
      <List>
        {Values.map((phone_number,index) => (
            <ListItem button  key={index} className={classes.item}>
                <a href={`tel:${phone_number}`}>
                <ListItemAvatar>
                <Avatar className={classes.avatar}>
                    <CallIcon />
                </Avatar>
                </ListItemAvatar>
                <ListItemText primary={phone_number} />
                </a>
          </ListItem>
        ))}
      </List>
    </Dialog>
  );
}

export default  TellModal;