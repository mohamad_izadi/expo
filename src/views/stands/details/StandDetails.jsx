
import React, { useState, useEffect, useRef, createRef } from 'react';
import {
    Card,
    Grid,
    Divider,
    makeStyles,
    AppBar,
    Hidden,
    Box,
    Tab, Tabs, Typography
} from '@material-ui/core';
import { ScrollPage, Section } from 'react-scrollpage';
import StandDetailedInfo from './StandDetailedInfo'
import StandDetailedOutlook from './StandDetailedOutlook';
import StandDetailedStatistic from './StandDetailedStatistic';
import StandDetailedOnline from './StandDetailedOnline';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import ModeCommentIcon from '@material-ui/icons/ModeComment';
import BookIcon from '@material-ui/icons/Book';
import { loadPavilionsDetails } from '../../../app/store/actions/pavilionsAction'
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom'
import { StandDetailedPreload, StandOnlineMembersPreload, StandOutlookPreload } from './StandDetailedPreload';
import Loading from '../../../component/Loading/Loading'
import TypeC from '../pictures/typec';
import TypeB from '../pictures/typeb';
import TypeA from '../pictures/typea';
import StandInfo from '../info/StandInfo'
import AddMeeting from '../dashboard/AddMeeting'
import Div100vh from 'react-div-100vh'


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        '& .makeStyles-statsIcon-32': {
            margin: 0
        },
        height: '100%',
        // justifyContent: 'space-between',
        // overflowY:'scroll'
        borderRadius: 0,
    },
    AppBar: {
        backgroundColor: '#fff !important',
        color: 'black',
        height: 59,
        paddingTop: 5,
        bottom: 0,
        top: 'initial',
        borderBottom: '1px solid #9f9f9f'
    },
    image: {
        height: '100%',
    }
}))

const StandDetail = ({ match }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { loading } = useSelector((state) => state.async);
    const { details, outlook, statistic, media } = useSelector((state) => state.pavilion);
    const [info, setInfo] = useState(true)
    const [loadOutlook, setLoadOutlook] = useState(true)
    const [loadStatistic, setLoadStatistic] = useState(true)
    const [loadMedia, setLoadMedia] = useState(true)
    const [showdrawer, setshowdrawer] = useState(false)
    const [userid, setuserid] = useState('')
    var page_height = document.getElementById('mainpage');
    var header_height;
    // const mainheight = window.innerWidth > 1200 ? window.innerHeight > 753 ? 645 : window.innerHeight - 115 : 'auto'
    // const rightsideheight = window.innerWidth > 1200 ? window.innerHeight > 753 ? 645 : 'auto' : window.innerWidth > 960 ? 'auto' :300  
    // const leftsideheight = window.innerWidth > 1200 ? window.innerHeight > 753 ? 645 : window.innerHeight - 115 : 'auto'

    const mainheight = 'auto'
    const rightsideheight = window.innerWidth > 960 ? 'inherit' : 300;
    const leftsideheight = 'auto'

    const HeaderRef = React.createRef()

    useEffect(() => {
        function loadAbout() {
            dispatch(loadPavilionsDetails(match.params.id, match.params.id, 'about'))
            if (details.about)
                setInfo(false)
        }

        function loadMedia() {
            dispatch(loadPavilionsDetails(match.params.id, match.params.id, 'media'))
            if (media.type && details.about)
                setLoadMedia(false)
        }
        function loadOutlook() {
            dispatch(loadPavilionsDetails(match.params.id, match.params.id, 'outlook'))
            if (outlook.members && details.about && media.type)
                setLoadOutlook(false)
        }

        function loadStatistics() {
            dispatch(loadPavilionsDetails(match.params.id, match.params.id, 'statistics'))
            if (statistic.conversions !== undefined && outlook.members && details.about && media.type)
                setLoadStatistic(false)
        }

        // const Onlinemembers = await agent.Pavilions.members(match.params.id);
        loadAbout()
        loadMedia()
        loadOutlook()
        loadStatistics()

    }, [dispatch, match.params.id, details.about, media.type, outlook.members, statistic.conversions])

    useEffect(() => {
        if (HeaderRef.current) {
            header_height = HeaderRef.current.offsetTop
            page_height.addEventListener('scroll', function () {
                if (page_height.scrollTop > header_height - 200) {
                    //  console.log("???")
                }
            })
        }
    }, [])

    const Showcontent = () => {
        HeaderRef.current.scrollIntoView({ behavier: 'smooth' })
    }

    const chathandler = (id) => {
        setuserid(id)
        setshowdrawer(true)
    }

    if (info) return <StandDetailedPreload />
    return (
        <Div100vh >
            <div style={{ position: 'relative' }}>
                <Card className="stand-container non-radius" style={{ height: mainheight, maxHeight: mainheight }} >
                    <Grid container className="stand-grid" >
                        <Grid item md={8} xs={12} className="non-radius"
                            style={{ height: rightsideheight, maxHeight: rightsideheight }}>
                            <Box
                                display="flex"
                                flexDirection="column"
                                height="100%"
                                justifyContent="start"
                            >
                                <Box id='showheader' textAlign="center"
                                    className={classes.image}>
                                    {loadMedia && <Loading />}
                                    {(media.typeLetter === 'A') && (loadMedia ? <Loading /> : <TypeA media={media} logo={details.media && details.media.logo} />)}
                                    {(media.typeLetter === 'B') && (loadMedia ? <Loading /> : <TypeB media={media} logo={details.media && details.media.logo} />)}
                                    {(media.typeLetter === 'C') && (loadMedia ? <Loading /> : <TypeC media={media} logo={details.media && details.media.logo} />)}
                                </Box>
                            </Box>
                        </Grid>
                        <Grid item md={4} xs={12} className="stand-detail"
                            style={{ height: leftsideheight, maxHeight: leftsideheight }}
                        >
                            <Card className={`${classes.root} non-radius`}>
                                <Hidden only={['xs', 'sm']}>
                                    <Box className="stand-detail__head">
                                        <StandDetailedInfo details={details} info_more={Showcontent} />
                                    </Box>
                                </Hidden>
                                {/* <Divider /> */}
                                <Box className="stand-detail__details">
                                    <Box className='stand-detail__details__box'>
                                        {loadOutlook ? <StandOutlookPreload /> : <StandDetailedOutlook outlook={outlook} />}
                                        {loadStatistic ? <StandOutlookPreload /> : <StandDetailedStatistic statistic={statistic} />}
                                        {loadStatistic ? <StandOnlineMembersPreload /> : <StandDetailedOnline statistic={statistic.online_members} chat={chathandler} />}
                                    </Box>
                                </Box>
                            </Card>
                        </Grid>
                    </Grid>
                </Card>
                <Box id='info_header'
                    class={"stand-header stickyheader"}
                    onClick={Showcontent}
                    ref={HeaderRef}
                >
                    <StandInfo match={match} />
                </Box>
            </div>
            {showdrawer && <AddMeeting open={userid} onClose={() => setshowdrawer(false)} />}
        </Div100vh>
    )
}

export default StandDetail
