import React from 'react'
import {
    Box,
    Grid,
    makeStyles,
    Avatar,
    Typography
} from '@material-ui/core';

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: '10px auto'
    },
}))

const StandDetailedMembers = ({ statistic, chat }) => {
    const classes = useStyles()
    const handelCounterOnline = () => {
        let x = 0
        for (const iterator of statistic) {
            if (iterator.is_online) {
                x = + 1;
            }
        }
        return x
    }
    // alert(JSON.stringify(statistic))

    return (
        <Box >
            <Grid item xs={12} className='iconsbox'>
                <Typography className='iconsbox__lasttitle'>
                    <MessageLang id="stands.details.Online.title" />
                    ({statistic ? handelCounterOnline() : 0})
                </Typography>

                <Grid container justify="right" className='iconsbox__online' >
                    <ul>
                        {statistic && statistic.map((item, index) =>

                            <li key={index}>
                                <Avatar
                                    className={item.is_online ? "active" : ""}
                                    key={index} alt="Remy Sharp"
                                    src={!item.avatar || item.avatar === 'https://app.namaline.ir/assets/img/user-default.png' ? "/static/images/user.png" : item.avatar}
                                    onClick={() => chat(item.member_id)}
                                />
                                <Typography>
                                    {item.name}
                                </Typography>
                            </li>
                        )}
                    </ul>
                </Grid>

            </Grid>
        </Box>
    )
}

export default StandDetailedMembers
