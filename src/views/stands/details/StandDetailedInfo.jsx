import React, { useState } from 'react'
import {
    Avatar,
    Box,
    CardContent,
    Grid,
    Typography,
    makeStyles,
    withStyles
} from '@material-ui/core';
import CallIcon from '@material-ui/icons/Call';
import PublicIcon from '@material-ui/icons/Public';
import InstagramIcon from '@material-ui/icons/Instagram';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import Tooltip from '@material-ui/core/Tooltip';
import ShowTellModal from '../info/TellModal';
/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        '& .makeStyles-statsIcon-32': {
            margin: 0
        },
        '& .MuiCardContent-root': {
            padding: '8px 16px 8px'
        }
    },

    statsIcon: {
        fontSize: '25px',
        color: '#9f9f9f',
    },
    statsIconActive: {
        fontSize: '25px',
        color: '#475974', //theme.palette.primary.lightBlue,
        cursor: 'pointer'

    },
    statsText: {
        fontSize: '10px',
        cursor: 'pointer'
    },

    active: {
    }
}));

const HtmlTooltip = withStyles((theme) => ({
    tooltip: {
        backgroundColor: '#f5f5f9',
        color: 'rgba(0, 0, 0, 0.87)',
        maxWidth: 220,
        fontSize: theme.typography.pxToRem(12),
        border: '1px solid #dadde9',
    },
}))(Tooltip);

const StandDetailedInfo = ({ className, info_more, details, ...rest }) => {
    const classes = useStyles();
    const [showphonenumber, setshowphonenumber] = useState(false)
    const [phonenumber, setphonenumber] = useState(null)
    var phones = []
    if (details.phone_numbers) {
        phones = details.phone_numbers.split(" ")
    }

    const handleClick = (url) => {
        const win = window.open(`http://${url}`);
        win.focus();
    }

    const openTellModal = () => {
        setphonenumber(phones)
        if (phones.length > 1) {
            setshowphonenumber(true)
        }
    }

    return (
        <>
            {showphonenumber && <ShowTellModal Values={phonenumber} open={true} Close={() => setshowphonenumber(false)} />}
            <CardContent className='info-company'>
                <Box
                    display="flex"
                    justifyContent="center"
                    mb={3}
                    className={'info-company__logo'}
                >
                    <Avatar
                        alt="Product"
                        src={details && details.media && details.media.logo}
                        variant="circle"
                        width={50}
                        height={50}
                        className={'info-company__logo__avatar'}

                    />
                </Box>
                <Typography
                    align="center"
                    color="textPrimary"
                    gutterBottom
                    className='info-company__title'
                >
                    {details && details.name}
                </Typography>
                <Typography
                    align="left"
                    color="textPrimary"
                    variant="body1"
                    className='stand-detail-infoBox'
                >
                    {
                        <>
                            <div className="stand-detail-info__content"
                                dangerouslySetInnerHTML={{ __html: details && details.about }}>
                            </div>
                            <div className='stand-detail-info__content__more'
                                onClick={() => info_more()}>
                                <MessageLang id="stands.details.StandDetails.morebtn" />
                            </div>
                        </>

                    }
                </Typography>
            </CardContent>
            <Box flexGrow={1} />
            <Box className='info-company__about'>
                <Grid
                    container
                    className='info-company__about__comm'
                >
                    <HtmlTooltip
                        title={
                            <React.Fragment>
                                <Typography color="inherit">
                                    {phones.length > 1 ? <MessageLang id="stands.info.introduction.call.tooltip" /> : details.phone_numbers}
                                </Typography>
                            </React.Fragment>
                        }
                    >
                        <a href={phones.length > 1 ? null : `tel:${details.phone_numbers}`}>
                            <Grid className={'info-company__about__comm__icon'} onClick={() => openTellModal()}>
                                {details && details.phone_numbers ?
                                    <CallIcon
                                        className={details &&
                                            details.phone_numbers &&
                                            classes.statsIconActive} />
                                    :
                                    <CallIcon
                                        className={classes.statsIcon}
                                        color="action" />
                                }
                            </Grid>
                        </a>
                    </HtmlTooltip>
                    <Grid className={'info-company__about__comm__icon'} >
                        {details && details.website_url ?
                            <PublicIcon
                                className={details &&
                                    details.website_url &&
                                    classes.statsIconActive}
                                onClick={() => handleClick(details.website_url)}
                            />

                            :
                            <PublicIcon
                                className={classes.statsIcon}
                                color="action"
                            />}
                    </Grid>
                    <Grid className={'info-company__about__comm__icon'} >
                        {(details &&
                            details.social_networks &&
                            details.social_networks.instagram) ?
                            <InstagramIcon
                                className={details &&
                                    details.social_networks &&
                                    details.social_networks.instagram &&
                                    classes.statsIconActive}
                                onClick={() => handleClick(details.social_networks.instagram)}
                            />
                            :
                            <InstagramIcon
                                className={classes.statsIcon}
                                color="action"
                            />}
                    </Grid>
                    <Grid className={'info-company__about__comm__icon'} >
                        {(details &&
                            details.social_networks &&
                            details.social_networks.linkedin) ?
                            <LinkedInIcon
                                className={details &&
                                    details.social_networks &&
                                    details.social_networks.linkedin &&
                                    classes.statsIconActive}
                                onClick={() => handleClick(details.social_networks.linkedin)}
                            />
                            :
                            <LinkedInIcon
                                className={classes.statsIcon}
                                color="action"
                            />}
                    </Grid>
                </Grid>
            </Box>
        </>
    )
}

export default StandDetailedInfo