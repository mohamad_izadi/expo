import React from 'react'
import clsx from 'clsx';
import {
    Box,
    Grid,
    Typography,
    Paper,
    makeStyles
} from '@material-ui/core';
import { Book, LocalMall, People, Business, Message } from '@material-ui/icons';

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    control: {
        padding: theme.spacing(2),
    },
    perContent: {
        border: '1px solid #eee'
    },
}));

const StandDetailedOutlook = ({ outlook }) => {
    const classes = useStyles()
    return (
        <Box className='iconsbox'>
            <Typography className='iconsbox__title'><MessageLang id="stands.details.Outlook.title"/></Typography>
            <Grid item xs={12} className={`${classes.outlook} iconsbox__icons `}>
                <Grid container justify="center" spacing={2} >
                    <Grid item className='iconsbox__icons__item'>
                        <Paper className='iconsbox__icons__item__box' >
                            <Business color="action" />
                            <Typography
                                align="center" className='boxnumber'
                                variant="body1" > {outlook.hall}</Typography>
                            <Typography
                                color="textSecondary" 
                                variant="body2" >
                                <MessageLang id="stands.details.Outlook.hall" />
                            </Typography>
                        </Paper >
                    </Grid>
                    <Grid item className='iconsbox__icons__item'>
                        <Paper className='iconsbox__icons__item__box' >
                            <People color="action" />
                            <Typography
                                align="center" className='boxnumber'
                                variant="body1" >{outlook.members}</Typography>
                            <Typography
                                color="textSecondary" 
                                variant="body2" >
                                <MessageLang id="stands.details.Outlook.member" />
                            </Typography>
                        </Paper >
                    </Grid>
                    <Grid item className='iconsbox__icons__item'>
                        <Paper className='iconsbox__icons__item__box' >
                            <LocalMall color="action" />
                            <Typography
                                align="center"  className='boxnumber'
                                variant="body1" >{outlook.products}</Typography>
                            <Typography
                                color="textSecondary"
                                variant="body2" >
                                <MessageLang id="stands.details.Outlook.product" />
                            </Typography>
                        </Paper >
                    </Grid>
                    <Grid item className='iconsbox__icons__item'>
                        <Paper className='iconsbox__icons__item__box' >
                            <Book color="action" />
                            <Typography
                                align="center"  className='boxnumber'
                                variant="body1" >{outlook.opportunities}</Typography>
                            <Typography
                                color="textSecondary"
                                variant="body2" ><MessageLang id="stands.details.Outlook.opportunity" /></Typography>
                        </Paper >
                    </Grid>
                </Grid>
            </Grid>
        </Box>
    )
}

export default StandDetailedOutlook
