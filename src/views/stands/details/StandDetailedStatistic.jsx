import React from 'react'
import {
    Box,
    Grid,
    Typography,
    Paper,
    makeStyles
} from '@material-ui/core';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import StarIcon from '@material-ui/icons/Star';
import PersonIcon from '@material-ui/icons/Person';
import VoiceChatIcon from '@material-ui/icons/VoiceChat';

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    control: {
        padding: theme.spacing(2),
    }
    , perspective: {
        paddingBottom: '10px'
    }, perContainer: {
        padding: '10px 5px'
    }, perContent: {
        border: '1px solid #eee'
    },

}));

const StandDetailedStatistic = ({ statistic }) => {
    const classes = useStyles();
    return (
        <Box className='iconsbox'>
            <Typography className='iconsbox__title'><MessageLang id="stands.details.Statistic.title"/></Typography>
            <Grid item xs={12} className={`${classes.outlook} iconsbox__icons `}>
                <Grid container justify="center" spacing={2} >
                    <Grid item className='iconsbox__icons__item'>
                        <Paper className='iconsbox__icons__item__box'>
                            <PersonIcon color="action" />
                            <Typography
                                align="center" className='boxnumber'
                                variant="body1" >{statistic.visists}</Typography>
                            <Typography
                                color="textSecondary"
                                variant="body2" className={classes.perText}>
                                <MessageLang id="stands.details.Statistic.visit" />
                            </Typography>
                        </Paper >
                    </Grid>
                    <Grid item className='iconsbox__icons__item'>
                        <Paper className='iconsbox__icons__item__box'>
                            <StarIcon color="action" />
                            <Typography
                                align="center" className='boxnumber'
                                variant="body1" >{statistic.points}</Typography>
                            <Typography
                                color="textSecondary"
                                variant="body2" >
                                <MessageLang id="stands.details.Statistic.score" />
                            </Typography>
                        </Paper >
                    </Grid>
                    <Grid item className='iconsbox__icons__item'>
                        <Paper className='iconsbox__icons__item__box' >
                            <ChatBubbleIcon color="action" />
                            <Typography
                                align="center" className='boxnumber'
                                variant="body1"  >{statistic.conversions}</Typography>
                            <Typography
                                color="textSecondary"
                                variant="body2">
                                <MessageLang id="stands.details.Statistic.conversation" />
                            </Typography>
                        </Paper >
                    </Grid>
                    <Grid item className='iconsbox__icons__item'>
                        <Paper className='iconsbox__icons__item__box' >
                            <VoiceChatIcon color="action" />
                            <Typography
                                align="center" className='boxnumber'
                                variant="body1" >{statistic.meetings}</Typography>
                            <Typography
                                color="textSecondary"
                                variant="body2" >
                                <MessageLang id="stands.details.Statistic.meeting" />
                            </Typography>
                        </Paper >
                    </Grid>
                </Grid>
            </Grid>
        </Box>
    )
}

export default StandDetailedStatistic
