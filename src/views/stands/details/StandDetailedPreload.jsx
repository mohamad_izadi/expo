import React from 'react'
import {
    Card,
    Grid,
    Paper,
    CardContent,
    Box,
    makeStyles,
    Hidden
} from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import Loading from '../../../component/Loading/Loading';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        '& .makeStyles-statsIcon-32': {
            margin: 0
        }
    },
    statsContent: {
        textAlign: 'center'
    },
    statsItem: {
        alignItems: 'center',
        display: 'flex',
        padding: '10px',
        backgroundColor: '#fff',
        borderRadius: '50%',
        boxShadow: '0 0 0 2px #ddd',
        marginBottom: '5px',
        width: 'fit-content',
        margin: '0 auto'
    },
    statsIcon: {
        fontSize: '25px',
    },
    statsText: {
        fontSize: '10px',
        cursor: 'pointer'
    },
    companyLogo: {
        height: '60px',
        width: '60px'
    },
    logoContainer: {
        marginBottom: '10px'
    },
    companyTitle: {
        marginBottom: '8px',
        fontSize: '14px'
    },
    companyDescription: {
        color: theme.palette.primary.darkGray,
        fontSize: '12px',
        lineHeight: '25px'
    },
    contactInfo: {
        padding: '10px 50px;'
    },
    paper: {
        height: 75,
        width: 75,
        border: '1px solid #ddd',
        borderRadius: '5px'
    },
    control: {
        padding: theme.spacing(2),
    }
    , perspective: {
        paddingBottom: '10px'
    }, perContainer: {
        padding: '10px 5px'
    }, perImage: {
        padding: '10px 25px'
    }, perContent: {
        border: '1px solid #eee'
    },
    perDescription: {
        fontSize: '12px',
        fontWeight: 'bold'
    },
    perText: {
        fontSize: '11px',
        textAlign: 'center'
    },
    perIcon: {
        alignItems: 'center',
        display: 'flex',
        marginBottom: '5px',
        marginTop: '5px',
        margin: '0 auto',
    },
}));
const rightsideheight = window.innerWidth > 960 ? 'inherit' : 300;

export const StandDetailedPreload = () => {
    const classes = useStyles();
    return (
        <Card className="stand-container">
            <Grid container className="stand-grid">
                <Hidden only={['sm', 'xs']}>
                    <Grid item md={8} xs={12} className="non-radius"
                        style={{ height: rightsideheight, maxHeight: rightsideheight }}>
                        <Box
                            display="flex"
                            flexDirection="column"
                            height="100%"
                            justifyContent="start"
                        >
                            <Box id='showheader' textAlign="center"
                                className={classes.image}>
                                <Loading />
                            </Box>
                        </Box>
                    </Grid>
                </Hidden>
                <Grid item lg={4} xs={12} className="stand-detail">
                    <Card>
                        <CardContent style={{ paddingBottom: '10px' }}>
                            <Box
                                display="flex"
                                justifyContent="center"
                                mb={3}
                                className={classes.logoContainer}
                            >
                                <Skeleton animation="wave" variant="circle" width={60} height={60} />
                            </Box>
                            <Skeleton align="center" style={{ margin: '0 auto 10px' }} animation="wave" height={20} width={70} />
                            <Skeleton align="right" style={{ margin: '0 0 7px' }} animation="wave" height={17} width='90%' />
                            <Skeleton align="right" style={{ margin: '0 0 7px' }} animation="wave" height={17} width='96%' />
                            <Skeleton align="right" style={{ margin: '0 0 7px' }} animation="wave" height={17} width='94%' />
                            <Skeleton align="right" style={{ margin: '0 0 10px' }} animation="wave" height={17} width='70%' />
                        </CardContent>
                        <Box flexGrow={1} />
                        <Box className={classes.contactInfo}>
                            <Grid
                                container
                                justify="space-between"
                                spacing={2}
                            >
                                <Grid lg={3}>
                                    <Grid className={classes.statsItem} style={{ padding: '3px' }}>
                                        <Skeleton animation="wave" variant="circle" width={40} height={40} />
                                    </Grid>
                                    <Skeleton align="center" style={{ margin: '0 auto 7px' }} animation="wave" height={15} width={60} />
                                </Grid>
                                <Grid lg={3}>
                                    <Grid className={classes.statsItem} style={{ padding: '3px' }}>
                                        <Skeleton animation="wave" variant="circle" width={40} height={40} />
                                    </Grid>
                                    <Skeleton align="center" style={{ margin: '0 auto 7px' }} animation="wave" height={15} width={60} />
                                </Grid>
                                <Grid lg={3}>
                                    <Grid className={classes.statsItem} style={{ padding: '3px' }}>
                                        <Skeleton animation="wave" variant="circle" width={40} height={40} />
                                    </Grid>
                                    <Skeleton align="center" style={{ margin: '0 auto 7px' }} animation="wave" height={15} width={60} />
                                </Grid>
                                <Grid lg={3}>
                                    <Grid className={classes.statsItem} style={{ padding: '3px' }}>
                                        <Skeleton animation="wave" variant="circle" width={40} height={40} />
                                    </Grid>
                                    <Skeleton align="center" style={{ margin: '0 auto 7px' }} animation="wave" height={15} width={60} />
                                </Grid>
                            </Grid>
                        </Box>
                        <StandOutlookPreload />
                        <StandOutlookPreload />
                        <StandOnlineMembersPreload />
                    </Card>
                </Grid>
            </Grid>

        </Card>
    )
}

export const StandOutlookPreload = () => {
    const classes = useStyles();
    return (
        <Box className={classes.perspective}>
            <Grid item xs={12}>
                <Grid container justify="center" spacing={2} className={classes.perContainer}>
                    <Grid item>
                        <Paper className={classes.paper} >
                            <Skeleton style={{ margin: '5px auto' }} animation="wave" variant="square" width={25} height={25} />
                            <Skeleton align="center" style={{ margin: '15px auto' }} animation="wave" height={15} width={50} />
                        </Paper >
                    </Grid>
                    <Grid item>
                        <Paper className={classes.paper} >
                            <Skeleton style={{ margin: '5px auto' }} animation="wave" variant="square" width={25} height={25} />
                            <Skeleton align="center" style={{ margin: '15px auto' }} animation="wave" height={15} width={50} />
                        </Paper >
                    </Grid>
                    <Grid item>
                        <Paper className={classes.paper} >
                            <Skeleton style={{ margin: '5px auto' }} animation="wave" variant="square" width={25} height={25} />
                            <Skeleton align="center" style={{ margin: '15px auto' }} animation="wave" height={15} width={50} />
                        </Paper >
                    </Grid>
                    <Grid item>
                        <Paper className={classes.paper} >
                            <Skeleton style={{ margin: '5px auto' }} animation="wave" variant="square" width={25} height={25} />
                            <Skeleton align="center" style={{ margin: '15px auto' }} animation="wave" height={15} width={50} />
                        </Paper >
                    </Grid>
                </Grid>
            </Grid>
        </Box>
    )
}


export const StandOnlineMembersPreload = () => {
    const classes = useStyles();
    return (
        <Box className={classes.perspective}>
            <Grid container justify="right" spacing={2} className={classes.perImage}>
                <Grid item>
                    <Grid className={classes.statsItem} style={{ padding: '3px' }}>
                        <Skeleton animation="wave" variant="circle" width={40} height={40} />
                    </Grid>
                </Grid>
                <Grid item>
                    <Grid className={classes.statsItem} style={{ padding: '3px' }}>
                        <Skeleton animation="wave" variant="circle" width={40} height={40} />
                    </Grid>
                </Grid>
                <Grid item>
                    <Grid className={classes.statsItem} style={{ padding: '3px' }}>
                        <Skeleton animation="wave" variant="circle" width={40} height={40} />
                    </Grid>
                </Grid>
            </Grid>
        </Box >
    )
}
