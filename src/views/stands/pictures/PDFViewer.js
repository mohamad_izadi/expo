import React,{useRef, useEffect} from 'react'
import WebViewer from '@pdftron/webviewer'


const PDFViewer=({src})=>{

  const viewer = useRef(null);

  useEffect(() => {
    WebViewer(
      {
        path: '/WebViewer/lib',
        initialDoc:src,
      },
      viewer.current,
    ).then((instance) => {
      instance.disableElements(['highlightToolGroupButton']);
      instance.disableElements(['underlineToolGroupButton']);
      instance.disableElements(['strikeoutToolGroupButton']);
      instance.disableElements(['squigglyToolGroupButton']);
      instance.disableElements(['stickyToolGroupButton']);
      instance.disableElements(['freeTextToolGroupButton']);
      instance.disableElements(['shapeToolGroupButton']);
      instance.disableElements(['freeHandToolGroupButton']);
      instance.disableElements(['eraserToolButton']);
      instance.disableElements(['printButton']);
      instance.disableElements(['viewControlsButton']);
      instance.disableElements(['leftPanelButton']);
      instance.disableElements(['menuButton']);
      instance.disableElements(['header']);
    });
  }, [])

    return (
      <div ref={viewer} id='viewer' style={{ width: '100%', height: '100%' }}>
      </div>
    )
  }

  export default PDFViewer

 