import React, { useState } from 'react'
import VideoPlayer from 'react-video-markers';
import QierPlayer from 'qier-player';

const Player = ({ videoUrl }) => {

    return (

        <QierPlayer
            // width={740}
            // height={420}
            width={'100%'}
            height={'100%'}
            language="en"
            // showVideoQuality={true}
            themeColor="#abc123"
            srcOrigin={videoUrl}
        />
    )
}

export default Player

