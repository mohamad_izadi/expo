import React, { useEffect, useState, useRef } from 'react'
import { Paper, Typography } from '@material-ui/core'
import { VectorMap } from '../../../component/JVectorMap/lib'
import Slide from '@material-ui/core/Slide';
import Player from './player';
import { getColor } from '../../../app/common/util/color'
//modal
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
//preview pdf
import PDFViewer from './PDFViewer';
//dl file
import Downloadbtn from '../../../component/Downloadbtn';

import $ from 'jquery'
// import 'viewerjs-react/dist/index.css'
window.jQuery = window.$ = $


const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const TypeA = ({ media, logo }) => {
    const [openModal, setOpenModal] = useState(false);
    const [isType, setIsType] = useState(true)
    const [image, setImage] = useState('')
    const [video, setVideo] = useState('')
    const [pdf, setPdf] = useState({
        numPages: null,
        pageNumber: 1,
        file: null,
    })
    const handleClose = () => {
        setImage(null)
        setVideo(null)
        setOpenModal(false);
    };

    useEffect(() => {

        //#region BASIC INITIALS

        if (media) {
            var colors = getColor(media.color.number + 1)
        }

        document.documentElement.style
            .setProperty('--light-color', colors.color1);

        document.documentElement.style
            .setProperty('--main-color', colors.color2);

        document.documentElement.style
            .setProperty('--dark-color', colors.color3);

        var svgMap = $('.jvectormap-container > svg').get(0);
        var svgNS = 'http://www.w3.org/2000/svg';
        var svgNSXLink = 'http://www.w3.org/1999/xlink';

        svgMap.setAttribute('xmlns', svgNS);
        svgMap.setAttribute('xmlns:link', svgNSXLink);
        svgMap.setAttribute('xmlns:ev', 'http://www.w3.org/2001/xml-events');

        var defs =
            svgMap.querySelector('defs') ||
            svgMap.insertBefore(document.createElementNS(svgNS, 'defs'), svgMap.firstChild);

        //#endregion

        // #region LOGOS

        // Create pattern for markers.
        var pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'logo_1');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        var image = document.createElementNS(svgNS, 'image');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');
        image.setAttribute("class", 'alogo_a');
        image.setAttribute('preserveAspectRatio', 'none');
        // image.setAttributeNS(svgNSXLink, 'xlink:href', media && media.media && media.media.poster);
        image.setAttributeNS(svgNSXLink, 'xlink:href', logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'logo_2');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');
        image.setAttribute("class", 'alogo_b');
        image.setAttribute('preserveAspectRatio', 'none');
        // image.setAttributeNS(svgNSXLink, 'xlink:href', media && media.media && media.media.poster);
        image.setAttributeNS(svgNSXLink, 'xlink:href', logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        //#endregion

        // #region POSTER 1

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'poster_1');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'aposter');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.pst_radius288x512.length > 0) ?
                media.media.pst_radius288x512[0] :
                '/static/images/default/typeA/poster-down.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // show icon only if there is an image
        if (media && media.media && media.media.pst_radius288x512.length > 0) {
            pattern = document.createElementNS(svgNS, 'pattern');
            pattern.setAttribute('id', 'poster_icon_1');
            pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
            pattern.setAttribute('width', '100%');
            pattern.setAttribute('height', '100%');

            // Create image for pattern.
            image = document.createElementNS(svgNS, 'image');
            image.setAttribute('x', '0');
            image.setAttribute('y', '0');
            image.setAttribute('width', '1');
            image.setAttribute('height', '1');

            image.setAttribute('preserveAspectRatio', 'none');
            image.setAttribute("class", 'aposter_icon');
            image.setAttributeNS(svgNSXLink, 'xlink:href',
                '/static/images/default/typeA/poster_icon.png');

            // Put it together
            pattern.appendChild(image);
            defs.appendChild(pattern);
        }

        // #endregion

        // #region POSTER 2

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'poster_2');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'aposter');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.pst_sharp288x512.length > 0) ?
                media.media.pst_sharp288x512[0] :
                '/static/images/default/typeA/poster-down.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // show icon only if there is an image
        if (media && media.media && media.media.pst_sharp288x512.length > 0) {
            pattern = document.createElementNS(svgNS, 'pattern');
            pattern.setAttribute('id', 'poster_icon_2');
            pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
            pattern.setAttribute('width', '100%');
            pattern.setAttribute('height', '100%');

            // Create image for pattern.
            image = document.createElementNS(svgNS, 'image');
            image.setAttribute('x', '0');
            image.setAttribute('y', '0');
            image.setAttribute('width', '1');
            image.setAttribute('height', '1');

            image.setAttribute('preserveAspectRatio', 'none');
            image.setAttribute("class", 'aposter_icon');
            image.setAttributeNS(svgNSXLink, 'xlink:href',
                '/static/images/default/typeA/poster_icon.png');

            // Put it together
            pattern.appendChild(image);
            defs.appendChild(pattern);
        }

        // #endregion

        // #region POSTER 3

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'poster_3');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'aposter_left');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.pst_polygon288x512) ?
                media.media.pst_polygon288x512 :
                '/static/images/default/typeA/poster-hexagonal.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // show icon only if there is an image
        if (media && media.media && media.media.pst_polygon288x512.length > 0) {
            pattern = document.createElementNS(svgNS, 'pattern');
            pattern.setAttribute('id', 'poster_icon_left');
            pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
            pattern.setAttribute('width', '100%');
            pattern.setAttribute('height', '100%');

            // Create image for pattern.
            image = document.createElementNS(svgNS, 'image');
            image.setAttribute('x', '0');
            image.setAttribute('y', '0');
            image.setAttribute('width', '1');
            image.setAttribute('height', '1');

            image.setAttribute('preserveAspectRatio', 'none');
            image.setAttribute("class", 'aposter_icon_left');
            image.setAttributeNS(svgNSXLink, 'xlink:href',
                '/static/images/default/typeA/poster_icon.png');

            // Put it together
            pattern.appendChild(image);
            defs.appendChild(pattern);
        }

        // #endregion

        // #region POSTER 4

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'poster_4');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'aposter_top');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.pst_sharp512x410) ?
                media.media.pst_sharp512x410 :
                '/static/images/default/typeA/poster-top.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // show icon only if there is an image
        if (media && media.media && media.media.pst_sharp512x410.length > 0) {
            pattern = document.createElementNS(svgNS, 'pattern');
            pattern.setAttribute('id', 'poster_icon_top');
            pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
            pattern.setAttribute('width', '100%');
            pattern.setAttribute('height', '100%');

            // Create image for pattern.
            image = document.createElementNS(svgNS, 'image');
            image.setAttribute('x', '0');
            image.setAttribute('y', '0');
            image.setAttribute('width', '1');
            image.setAttribute('height', '1');

            image.setAttribute('preserveAspectRatio', 'none');
            image.setAttribute("class", 'aposter_icon_top');
            image.setAttributeNS(svgNSXLink, 'xlink:href',
                '/static/images/default/typeA/poster_icon.png');

            // Put it together
            pattern.appendChild(image);
            defs.appendChild(pattern);
        }

        // #endregion

        // #region VIDEO

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'video_1');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'avideotop');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            '/static/images/default/typeA/cover-video-blur.png');

        pattern.appendChild(image);
        defs.appendChild(pattern);


        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'video_2');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'avideo');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            '/static/images/default/typeA/cover-video-blur.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'video_icon');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'avideo_icon');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            '/static/images/default/typeA/video_icon.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // #endregion

        // #region CATALOG COVER

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'catalog_cover');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'docs-cover');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.catalog_cover.length > 0) ?
                media.media.catalog_cover[0] :
                '/static/images/default/typeA/docs-cover.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'cc_top_left');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'docs-cover-top-left');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.catalog_cover.length > 1) ?
                media.media.catalog_cover[1] :
                '/static/images/default/typeA/docs-cover.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'cc_top_right');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'docs-cover-top-right');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.catalog_cover.length > 1) ?
                media.media.catalog_cover[1] :
                '/static/images/default/typeA/docs-cover.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // #endregion

        // #region Gradient

        var gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_1');
        var stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', 'rgb(208, 208, 221)');
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', '#8d8f99');
        gradient.appendChild(stop);
        defs.appendChild(gradient);

        gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_2');
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', `${colors.color2}`);
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', `${colors.color3}`);
        gradient.appendChild(stop);
        defs.appendChild(gradient);

        gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_3');
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', `${colors.color3}`);
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', `${colors.color2}`);
        gradient.appendChild(stop);
        defs.appendChild(gradient);

        // #endregion

        // #region PATHES

        $('path[data-code="STDA049"]').attr("fill", 'url(#logo_1)')
        $('path[data-code="STDA450"]').attr("fill", 'url(#logo_2)')

        $('path[data-code="STDA000"]').attr("fill", 'url(#poster_1)')
        $('path[data-code="STDAIC000"]').attr("fill", 'url(#poster_icon_1)')
        $('path[data-code="STDA101"]').attr("fill", 'url(#poster_2)')
        $('path[data-code="STDAIC101"]').attr("fill", 'url(#poster_icon_2)')
        $('path[data-code="STDA415"]').attr("fill", 'url(#poster_3)')
        $('path[data-code="STDAIC415"]').attr("fill", 'url(#poster_icon_left)')
        $('path[data-code="STDA433"]').attr("fill", 'url(#poster_4)')
        $('path[data-code="STDAIC433"]').attr("fill", 'url(#poster_icon_top)')

        $('path[data-code="STDA432"]').attr("fill", 'url(#video_1)')
        $('path[data-code="STDA459"]').attr("fill", 'url(#video_2)')
        $('path[data-code="STDA502"]').attr("fill", 'url(#video_icon)')
        $('path[data-code="STDA503"]').attr("fill", 'url(#video_icon)')

        $('path[data-code="STDA176"]').attr("fill", 'url(#catalog_cover)')
        $('path[data-code="STDA267"]').attr("fill", 'url(#cc_top_left)')
        $('path[data-code="STDA484"]').attr("fill", 'url(#cc_top_right)')

        //#endregion

        //#region EVENTS
        $('path[data-code="STDA176"]').click(function () {
            if (media.media.catalog.length === 0)
                return
            const url = media.media.catalog[0];
            setPdf(val => {
                val.pdf = url;
                return { ...val }
            })
            setIsType("pdf")
            setOpenModal(true)
        });

        $('path[data-code="STDA267"]').click(function () {
            if (media.media.catalog.length < 2)
                return
            const url = media.media.catalog[1];
            setPdf(val => {
                val.pdf = url;
                return { ...val }
            })
            setIsType("pdf")
            setOpenModal(true)
        });

        $('path[data-code="STDA484"]').click(function () {
            if (media.media.catalog.length < 2)
                return
            const url = media.media.catalog[1];
            setPdf(val => {
                val.pdf = url;
                return { ...val }
            })
            setIsType("pdf")
            setOpenModal(true)
        });

        $('path[data-code="STDA450"]').click(function () {
            setIsType("image")
            setImage(logo)
            setOpenModal(true)
        });

        $('path[data-code="STDA049"]').click(function () {
            setIsType("image")
            setImage(logo)
            setOpenModal(true)
        });

        $('path[data-code="STDA000"], path[data-code="STDAIC000"]').click(function () {
            if (media.media.pst_radius288x512.length === 0)
                return
            setIsType("image")
            setImage(media.media.pst_radius288x512[0])
            setOpenModal(true)
        });

        $('path[data-code="STDA101"], path[data-code="STDAIC101"]').click(function () {
            if (media.media.pst_sharp288x512.length === 0)
                return
            setIsType("image")
            setImage(media.media.pst_sharp288x512[0])
            setOpenModal(true)
        });

        $('path[data-code="STDA415"], path[data-code="STDAIC415"]').click(function () {
            if (media.media.pst_polygon288x512 === 0)
                return
            setIsType("image")
            setImage(media.media.pst_polygon288x512[0])
            setOpenModal(true)
        });

        $('path[data-code="STDA433"], path[data-code="STDAIC433"]').click(function () {
            if (!media.media.pst_sharp512x410)
                return
            setIsType("image")
            setImage(media.media.pst_sharp512x410)
            setOpenModal(true)
        });

        $('path[data-code="STDA432"]').click(function () {
            if (media.media.video.length === 0)
                return
            setIsType("video")
            setVideo(media.media.video[0])
            setOpenModal(true)
        });

        $('path[data-code="STDA502"]').click(function () {
            if (media.media.video.length === 0)
                return
            setIsType("video")
            setVideo(media.media.video[0])
            setOpenModal(true)
        });

        $('path[data-code="STDA459"]').click(function () {
            if (media.media.video.length !== 2)
                return
            setIsType("video")
            setVideo(media.media.video[1])
            setOpenModal(true)
        });

        $('path[data-code="STDA503"]').click(function () {
            if (media.media.video.length !== 2)
                return
            setIsType("video")
            setVideo(media.media.video[1])
            setOpenModal(true)
        });

        //#endregion
    })
    return (
        <Paper className={'typea_container non-radius'}>
            <Dialog
                open={openModal}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                className={'video-modal'}
                fullScreen={isType === "pdf"}
            >
                <MuiDialogTitle disableTypography className='modal-title'>
                    <Typography variant="h6">{ }</Typography>
                    <IconButton aria-label="close" onClick={handleClose}>
                        <CloseIcon />
                    </IconButton>
                </MuiDialogTitle >
                <DialogContent className={isType ? 'modal-content modal-img' : 'modal-content'}>
                    {isType === "image" && <img alt='type-a' src={image} />}
                    {isType === "video" && <div className='content'><Player videoUrl={video} /></div>}
                    {isType === "pdf" && <div className='content'>
                        <PDFViewer src={media.media.catalog[0]} />
                    </div>}
                    {isType === "image" ? null :
                        <Downloadbtn
                            type={isType === "pdf" ? "Pdffile" : "Video"}
                            src={isType === "pdf" ? media.media.catalog[0] : video} />
                    }
                </DialogContent>
            </Dialog>
            <VectorMap map={'typea'}
                backgroundColor="transparent"
                zoomOnScroll={true}
                // markers={markers}
                containerStyle={{
                    width: "100%",
                    height: "100%"
                }}
                containerClassName="map"
                regionStyle={{
                    // initial: {
                    //     fill: "rgb(2, 178, 255)",
                    //     "fill-opacity": 0.5,
                    //     stroke: "white",
                    //     "stroke-width": 1.5,
                    //     "stroke-opacity": 1.5
                    // },
                    initial: {
                        // fill: getColor(media.color.number + 1).color3,
                        fill: "transparent",
                        "fill-opacity": 1,
                        stroke: "grey",
                        "stroke-width": 0.2,
                        "stroke-opacity": 1
                    },
                    // initial: {
                    //     fill: getColor(media.color.number + 1).color2,
                    //     "fill-opacity": 1,
                    //     stroke: "transparent",
                    //     "stroke-width": 0,
                    //     "stroke-opacity": 0
                    // },
                    hover: {
                        "fill-opacity": 1,
                        cursor: "pointer"
                    },
                    selected: {
                        fill: "#2938bc" //color for the clicked region
                    },
                    selectedHover: {}
                }}
                regionsSelectable={false}
                // series={series}
                zoomMax={2}
                onRegionTipShow={(e) => {
                    e.preventDefault()
                }}
            // ref="map"
            // onRegionClick={handleClick}
            />
        </Paper>
    )
}

export default TypeA