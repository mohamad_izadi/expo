import React, { useEffect, useState, useRef } from 'react'
import { Paper, Typography } from '@material-ui/core'
import { VectorMap } from '../../../component/JVectorMap/lib'
//modal
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
//preview pdf
import PDFViewer from './PDFViewer';
//dl file
import Downloadbtn from '../../../component/Downloadbtn';

import Fade from '@material-ui/core/Fade';
import Player from './player';

import $ from 'jquery'
import { getColor } from '../../../app/common/util/color';
window.jQuery = window.$ = $

const Transition = React.forwardRef(function Transition(props, ref) {
    // return <Slide direction="up" ref={ref} {...props} />;
    return <Fade in={true} timeout={1000} ref={ref} {...props} />;
});

const TypeC = ({ media, logo }) => {
    const [openModal, setOpenModal] = useState(false);
    const [isType, setIsType] = useState(true)
    const [image, setImage] = useState('')
    const [video, setVideo] = useState('')
    const [pdf, setPdf] = useState({
        numPages: null,
        pageNumber: 1,
        file: null,
    })


    const handleClose = () => {
        setImage(null)
        setVideo(null)
        setOpenModal(false);
    };


    useEffect(() => {
        if (media) {
            var colors = getColor(media.color.number + 1)
        }

        document.documentElement.style
            .setProperty('--light-color', colors.color1);

        document.documentElement.style
            .setProperty('--main-color', colors.color2);

        document.documentElement.style
            .setProperty('--dark-color', colors.color3);


        var svgMap = $('.jvectormap-container > svg').get(0);
        var svgNS = 'http://www.w3.org/2000/svg';
        var svgNSXLink = 'http://www.w3.org/1999/xlink';

        svgMap.setAttribute('xmlns', svgNS);
        svgMap.setAttribute('xmlns:link', svgNSXLink);
        svgMap.setAttribute('xmlns:ev', 'http://www.w3.org/2001/xml-events');

        var defs =
            svgMap.querySelector('defs') ||
            svgMap.insertBefore(document.createElementNS(svgNS, 'defs'), svgMap.firstChild);

        // #region LOGO 
        var pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'logo');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        var image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'clogo');
        image.setAttributeNS(svgNSXLink, 'xlink:href', logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // #endregion

        // #region POSTER

        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'poster');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'cposter');

        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.pst_radius512x288.length > 0) ?
                media.media.pst_radius512x288[0] :
                '/static/images/default/typeC/poster.png');

        pattern.appendChild(image);
        defs.appendChild(pattern);

        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'poster_icon');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'cposter_icon');

        image.setAttributeNS(svgNSXLink, 'xlink:href',
            '/static/images/default/typeC/poster_icon.png');

        pattern.appendChild(image);
        defs.appendChild(pattern);

        // #endregion

        // #region VIDEO

        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'video');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'cvideo');

        image.setAttributeNS(svgNSXLink, 'xlink:href',
            '/static/images/default/typeC/cover-video-blur.png');

        pattern.appendChild(image);
        defs.appendChild(pattern);

        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'video_icon');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'cvideo_icon');

        image.setAttributeNS(svgNSXLink, 'xlink:href',
            '/static/images/default/typeC/video_icon.png');

        pattern.appendChild(image);
        defs.appendChild(pattern);

        // #endregion

        // #region Gradient

        var gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_1');
        var stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', '#dde0e8');
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', '#80828e');
        gradient.appendChild(stop);
        defs.appendChild(gradient);

        gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_2');
        gradient.setAttribute('x1', '0%');
        gradient.setAttribute('y1', '0%');
        gradient.setAttribute('x2', '0%');
        gradient.setAttribute('y2', '100%');
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', `${colors.color1}`);
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', `${colors.color3}`);
        gradient.appendChild(stop);
        defs.appendChild(gradient);

        gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_3');
        gradient.setAttribute('x1', '0%');
        gradient.setAttribute('y1', '0%');
        gradient.setAttribute('x2', '0%');
        gradient.setAttribute('y2', '100%');
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', `${colors.color1}`);
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', `${colors.color2}`);
        gradient.appendChild(stop);
        defs.appendChild(gradient);

        gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_4');
        gradient.setAttribute('x1', '0%');
        gradient.setAttribute('y1', '0%');
        gradient.setAttribute('x2', '0%');
        gradient.setAttribute('y2', '100%');
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', '#5c5e68');
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', '#bdbfc6');
        gradient.appendChild(stop);
        defs.appendChild(gradient);

        gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_5');
        gradient.setAttribute('x1', '0%');
        gradient.setAttribute('y1', '0%');
        gradient.setAttribute('x2', '0%');
        gradient.setAttribute('y2', '100%');
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', '#5c5e68');
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', '#80828e');
        gradient.appendChild(stop);
        defs.appendChild(gradient);

        gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_6');
        gradient.setAttribute('x1', '0%');
        gradient.setAttribute('y1', '0%');
        gradient.setAttribute('x2', '0%');
        gradient.setAttribute('y2', '100%');
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', `${colors.color1}`);
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', `${colors.color3}`);
        gradient.appendChild(stop);
        defs.appendChild(gradient);


        // #endregion

        // #region CATALOG COVER

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'catalog');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'catalog_cover_c');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.catalog_cover.length > 0) ?
                media.media.catalog_cover[0] :
                '/static/images/default/typeB/catalog.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'catalog_icon');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'ccatalog_icon');

        image.setAttributeNS(svgNSXLink, 'xlink:href',
            '/static/images/default/typeC/catalog_icon.png');

        pattern.appendChild(image);
        defs.appendChild(pattern);

        // #endregion

        $('path[data-code="STDC036"]').attr("fill", 'url(#logo)')
        $('path[data-code="STDC008"]').attr("fill", 'url(#poster)')
        $('path[data-code="STDC088"]').attr("fill", 'url(#poster_icon)')
        $('path[data-code="STDC005"]').attr("fill", 'url(#video)')
        $('path[data-code="STDC055"]').attr("fill", 'url(#video_icon)')
        $('path[data-code="STDC050"]').attr("fill", 'url(#catalog)')
        $('path[data-code="STDC099"]').attr("fill", 'url(#catalog_icon)')

        $('path[data-code="STDC050"], path[data-code="STDC099"]').click(function () {
            if (!media.media.catalog)
                return
            const url = media.media.catalog[0];
            setPdf(val => {
                val.pdf = url;
                return { ...val }
            })
            setIsType("pdf")
            setOpenModal(true)
        });

        $('path[data-code="STDC036"]').click(function () {
            setIsType("image")
            setImage(logo)
            setOpenModal(true)
        });

        $('path[data-code="STDC008"], path[data-code="STDC088"]').click(function () {
            if (!media.media.pst_radius512x288)
                return
            setIsType("image")
            setImage(media.media.pst_radius512x288)
            setOpenModal(true)
        });

        $('path[data-code="STDC005"]').click(function () {
            if (!media.media.video)
                return
            setIsType("video")
            setVideo(media.media.video)
            setOpenModal(true)
        });

        $('path[data-code="STDC055"]').click(function () {
            if (!media.media.video)
                return
            setIsType("video")
            setVideo(media.media.video)
            setOpenModal(true)
        });
    })

    return (
        <Paper className={`typec_container non-radius color-${media && media.color.number}`}>
            <Dialog
                open={openModal}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                className={'video-modal'}
                fullScreen={isType === "pdf"}
            >
                <MuiDialogTitle disableTypography className='modal-title'>
                    <Typography variant="h6">{ }</Typography>
                    <IconButton aria-label="close" onClick={handleClose}>
                        <CloseIcon />
                    </IconButton>

                </MuiDialogTitle >
                <DialogContent className={isType ? 'modal-content modal-img' : 'modal-content'}>
                    {isType === "image" && <img alt='type-c' src={image} />}
                    {isType === "video" && <div className='content'><Player videoUrl={video} /></div>}
                    {isType === "pdf" && <div className='content'>
                        <PDFViewer src={media.media.catalog[0]} />
                    </div>}
                    {isType === "image" ? null :
                        <Downloadbtn
                            type={isType === "pdf" ? "Pdffile" : "Video"}
                            src={isType === "pdf" ? media.media.catalog[0] : video} />
                    }
                </DialogContent>
            </Dialog>
            <VectorMap map={'typec'}
                backgroundColor="transparent"
                zoomOnScroll={true}
                // markers={markers}
                containerStyle={{
                    width: "100%",
                    height: "100%"
                }}
                containerClassName="map"
                regionStyle={{
                    initial: {
                        fill: "#eee",
                        "fill-opacity": 0.4,
                        stroke: "white",
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    },
                    // hover: {
                    //     "fill-opacity": 0.5,
                    //     cursor: "pointer"
                    // },
                    selected: {
                        fill: "#2938bc" //color for the clicked region
                    },
                    selectedHover: {}
                }}
                regionsSelectable={false}
                // series={series}
                zoomMax={2}
                onRegionTipShow={(e) => {
                    e.preventDefault()
                }}
            // ref="map"
            // onRegionClick={handleClick}
            />

        </Paper >
    )
}

export default TypeC
