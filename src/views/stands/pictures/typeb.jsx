import React, { useEffect, useState, useRef } from 'react'
import { Paper, Typography } from '@material-ui/core'
import { VectorMap } from '../../../component/JVectorMap/lib'
//modal
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
//preview pdf
import PDFViewer from './PDFViewer';
//dl file
import Downloadbtn from '../../../component/Downloadbtn';

import Slide from '@material-ui/core/Slide';
import Player from './player';


import $ from 'jquery'
import { getColor } from '../../../app/common/util/color';
window.jQuery = window.$ = $



const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const TypeB = ({ media, logo }) => {
    const [openModal, setOpenModal] = useState(false);
    const [isType, setIsType] = useState(true)
    const [image, setImage] = useState('')
    const [video, setVideo] = useState('')
    const [pdf, setPdf] = useState({
        numPages: null,
        pageNumber: 1,
        file: null,
    })
    const handleClose = () => {
        setImage(null)
        setVideo(null)
        setOpenModal(false);
    };

    useEffect(() => {
        if (media) {
            var colors = getColor(media.color.number + 1)
        }

        document.documentElement.style
            .setProperty('--light-color', colors.color1);

        document.documentElement.style
            .setProperty('--main-color', colors.color2);

        document.documentElement.style
            .setProperty('--dark-color', colors.color3);

        var svgMap = $('.jvectormap-container > svg').get(0);
        var svgNS = 'http://www.w3.org/2000/svg';
        var svgNSXLink = 'http://www.w3.org/1999/xlink';

        svgMap.setAttribute('xmlns', svgNS);
        svgMap.setAttribute('xmlns:link', svgNSXLink);
        svgMap.setAttribute('xmlns:ev', 'http://www.w3.org/2001/xml-events');

        var defs =
            svgMap.querySelector('defs') ||
            svgMap.insertBefore(document.createElementNS(svgNS, 'defs'), svgMap.firstChild);



        // #region LOGO

        // Create pattern for markers.
        var pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'logo');
        // pattern.setAttribute('patternUnits', 'objectBoundingBox');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '1');
        pattern.setAttribute('height', '1');
        // pattern.setAttribute('viewBox', '0 0 100 150');
        // pattern.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        // Create image for pattern.
        var image = document.createElementNS(svgNS, 'image');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');
        image.setAttribute('preserveAspectRatio', 'xMidYMid meet');
        image.setAttribute("class", 'blogo');
        image.setAttributeNS(svgNSXLink, 'xlink:href', logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // #endregion

        // #region POSTER 1

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'poster_1');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'bposter');

        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.pst_sharp288x512.length > 0) ?
                media.media.pst_sharp288x512[0] :
                '/static/images/default/typeB/poster.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // show icon only if there is an image
        if (media && media.media && media.media.pst_sharp288x512.length > 0) {
            pattern = document.createElementNS(svgNS, 'pattern');
            pattern.setAttribute('id', 'poster_icon_left');
            pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
            pattern.setAttribute('width', '100%');
            pattern.setAttribute('height', '100%');

            // Create image for pattern.
            image = document.createElementNS(svgNS, 'image');
            image.setAttribute('x', '0');
            image.setAttribute('y', '0');
            image.setAttribute('width', '1');
            image.setAttribute('height', '1');

            image.setAttribute('preserveAspectRatio', 'none');
            image.setAttribute("class", 'bposter_icon');
            image.setAttributeNS(svgNSXLink, 'xlink:href',
                '/static/images/default/typeB/poster_icon.png');

            // Put it together
            pattern.appendChild(image);
            defs.appendChild(pattern);
        }

        // #endregion

        // #region POSTER 2

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'poster_2');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'bposter');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.pst_polygon288x512.length > 0) ?
                media.media.pst_polygon288x512[0] :
                '/static/images/default/typeB/poster.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // show icon only if there is an image
        if (media && media.media && media.media.pst_polygon288x512.length > 0) {
            pattern = document.createElementNS(svgNS, 'pattern');
            pattern.setAttribute('id', 'poster_icon_right');
            pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
            pattern.setAttribute('width', '100%');
            pattern.setAttribute('height', '100%');

            // Create image for pattern.
            image = document.createElementNS(svgNS, 'image');
            image.setAttribute('x', '0');
            image.setAttribute('y', '0');
            image.setAttribute('width', '1');
            image.setAttribute('height', '1');

            image.setAttribute('preserveAspectRatio', 'none');
            image.setAttribute("class", 'bposter_icon_right');
            image.setAttributeNS(svgNSXLink, 'xlink:href',
                '/static/images/default/typeB/poster_icon.png');

            // Put it together
            pattern.appendChild(image);
            defs.appendChild(pattern);
        }

        // #endregion

        // #region VIDEO

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'video');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'bvideo');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            '/static/images/default/typeB/cover-video-blur.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'video_icon');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'bvideo_icon');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            '/static/images/default/typeB/video_icon.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // #endregion

        // #region Gradient

        var gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_1');
        var stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', colors.color2);
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', colors.color3);
        gradient.appendChild(stop);
        defs.appendChild(gradient);

        gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_2');
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', `${colors.color3}`);
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', `${colors.color2}`);
        gradient.appendChild(stop);
        defs.appendChild(gradient);

        gradient = document.createElementNS(svgNS, 'linearGradient');
        gradient.setAttribute('id', 'gradient_3');
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '0%');
        stop.setAttribute('stop-color', '#80828e');
        gradient.appendChild(stop);
        stop = document.createElementNS(svgNS, 'stop');
        stop.setAttribute('offset', '100%');
        stop.setAttribute('stop-color', '#bdbfc6');
        gradient.appendChild(stop);
        defs.appendChild(gradient);

        //#endregion

        // #region CATALOG COVER

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'catalog');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'catalog_cover');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            '/static/images/default/typeB/catalog.png');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            (media && media.media && media.media.catalog_cover.length > 0) ?
                media.media.catalog_cover[0] :
                '/static/images/default/typeB/catalog.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'catalog_icon');
        pattern.setAttribute('patternContentUnits', 'objectBoundingBox');
        pattern.setAttribute('width', '100%');
        pattern.setAttribute('height', '100%');

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '0');
        image.setAttribute('y', '0');
        image.setAttribute('width', '1');
        image.setAttribute('height', '1');

        image.setAttribute('preserveAspectRatio', 'none');
        image.setAttribute("class", 'bcatalog_icon');
        image.setAttributeNS(svgNSXLink, 'xlink:href',
            '/static/images/default/typeB/catalog_icon.png');

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);

        // #endregion

        $('path[data-code="STDB067"]').attr("fill", 'url(#logo)')
        $('path[data-code="STDB059"]').attr("fill", 'url(#poster_1)')
        if (media && media.media && media.media.pst_sharp288x512.length > 0) {
            $('path[data-code="STDBI059"]').attr("fill", 'url(#poster_icon_left)')
        }

        $('path[data-code="STDB060"]').attr("fill", 'url(#poster_2)')
        if (media && media.media && media.media.pst_polygon288x512.length > 0) {
            $('path[data-code="STDBI060"]').attr("fill", 'url(#poster_icon_right)')
        }

        $('path[data-code="STDB074"]').attr("fill", 'url(#video)')
        $('path[data-code="STDB075"]').attr("fill", 'url(#video_icon)')

        $('path[data-code="STDB024"]').attr("fill", 'url(#catalog)')
        $('path[data-code="STDBI024"]').attr("fill", 'url(#catalog_icon)')

        $('path[data-code="STDB024"], path[data-code="STDBI024"]').click(function () {
            if (!media.media.catalog)
                return
            const url = media.media.catalog;
            setPdf(val => {
                val.pdf = url;
                return { ...val }
            })
            setIsType("pdf")
            setOpenModal(true)
        });


        $('path[data-code="STDB067"]').click(function () {
            setIsType("image")
            setImage(logo)
            setOpenModal(true)
        });

        $('path[data-code="STDB059"], path[data-code="STDBI059"]').click(function () {
            if (media.media.pst_sharp288x512.length === 0)
                return
            setIsType("image")
            setImage(media.media.pst_sharp288x512[0])
            setOpenModal(true)
        });


        $('path[data-code="STDB060"], path[data-code="STDBI060"]').click(function () {
            if (media.media.pst_polygon288x512.length === 0)
                return
            setIsType("image")
            setImage(media.media.pst_polygon288x512[0])
            setOpenModal(true)
        });

        $('path[data-code="STDB074"]').click(function () {
            if (!media.media.video)
                return
            setIsType("video")
            setVideo(media.media.video[0])
            setOpenModal(true)
        });

        $('path[data-code="STDB075"]').click(function () {
            if (!media.media.video)
                return
            setIsType("video")
            setVideo(media.media.video[0])
            setOpenModal(true)
        });
    })

    return (
        <Paper className={'typeb_container non-radius'}>
            <Dialog
                open={openModal}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                className={'video-modal'}
                fullScreen={isType === "pdf"}
            >
                <MuiDialogTitle disableTypography className='modal-title'>
                    <Typography variant="h6">{ }</Typography>
                    <IconButton aria-label="close" onClick={handleClose}>
                        <CloseIcon />
                    </IconButton>
                </MuiDialogTitle >
                <DialogContent className={isType ? 'modal-content modal-img' : 'modal-content'}>
                    {isType === "image" && <img alt='type-b' src={image} />}
                    {isType === "video" && <div className='content'><Player videoUrl={video} /></div>}
                    {isType === "pdf" && <div className='content'>
                        <PDFViewer src={media.media.catalog[0]} />
                    </div>}
                    {isType === "image" ? null :
                        <Downloadbtn
                            type={isType === "pdf" ? "Pdffile" : "Video"}
                            src={isType === "pdf" ? media.media.catalog[0] : video} />
                    }
                </DialogContent>
            </Dialog>
            <VectorMap map={'typeb'}
                backgroundColor="transparent"
                zoomOnScroll={true}
                // markers={markers}
                containerStyle={{
                    width: "100%",
                    height: "100%"
                }}
                containerClassName="map"
                regionStyle={{
                    initial: {
                        fill: "transparent",
                        "fill-opacity": 1,
                        stroke: "transparent",
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    },
                    hover: {
                        "fill-opacity": 1,
                        cursor: "pointer"
                    },
                    selected: {
                        fill: "#2938bc" //color for the clicked region
                    },
                    selectedHover: {}
                }}
                regionsSelectable={false}
                // series={series}
                zoomMax={2}
                onRegionTipShow={(e) => {
                    e.preventDefault()
                }}
            // ref="map"
            // onRegionClick={handleClick}
            />
        </Paper>
    )
}

export default TypeB
