import React, { useState, useEffect } from 'react';
import {
    Card,
    Grid
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import SearchBox from '../../../component/Search/SearchBox';
import JobList from './JobList';
// import { loadJobOpportunities } from '../../../app/store/actions/opportunityActions'
import OpportunityPreload from './OpportunityPreload'
import TabNavs from '../../../component/Tabs/TabNavs'
import JobFilter from './JobFilter'

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/


const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTableCell-root': {
            fontFamily: 'IRANSansFa',
            padding: '8px'
        },
        '& .MuiTableCell-head': {
            padding: '16px'
        },
        '& .MuiTabs-root': {
            height: '100%'
        },
        '& .MuiTabs-flexContainer': {
            height: '100%'
        },
        '& .makeStyles-search-36': {
            borderRadius: '0'
        },
        '& .MuiCardHeader-root': {
            padding: '0'
        },
        '& .MuiSelect-selectMenu': {
            padding: '10px 10px'
        }
    }, header: {
        height: '72px',
        boxShadow: 'none',
        backgroundColor: '#fff',
        borderBottom: '1px solid #eee'
    },
    swipeableViews: {
        margin: '0 auto'
    },
    avatar: {
        marginRight: theme.spacing(2)
    }
}))

const OpportunityDashboard = ({ className, ...rest }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { loading } = useSelector((state) => state.async);

    const [value, setValue] = useState(0);

    const handleChange = (newValue) => {
        setValue(newValue);
    };

    // useEffect(() => {
    //     // dispatch(loadJobOpportunities())
    // }, [])

    return (
        <>
            <Card
                className={clsx(classes.root, className)}
                {...rest}
            >
                <Grid container>
                    <Grid item lg={3} xs={12} className="stand-list__filter-panel">
                        <Grid container>
                            <Grid item xs={12}>
                                <SearchBox />
                            </Grid>
                            <Grid item xs={12}>
                                <JobFilter />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item lg={9} xs={12} className={classes.standContent}>
                        <Grid container >
                            <TabNavs
                                components={
                                    [loading ? <OpportunityPreload /> :
                                        <JobList />]}
                                labels={[<MessageLang id="DashboardLayout.header.jobs"/>]}
                                loading={loading}
                                handleChangeIndex={handleChange}
                                classNames={classes.header} />
                        </Grid>
                    </Grid>
                </Grid>
            </Card >
        </>
    );
}

export default OpportunityDashboard
