import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux'
import {
    SelectInput
} from '../../../component'
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { clearFilters, loadMembers, setMemberFilters } from '../../../app/store/actions/standActions';
import { loadCompanies, loadAbilities, loadInterests, loadCountries, loadPositions } from '../../../app/store/actions/persistActions'

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles({
    root: {
    },
});

const JobFilter = ({ selected, number, onClick }) => {
    const [category, setCategory] = useState()
    const [subCategories, setSubCategories] = useState([])
    const [loading, setLoading] = useState(false);
    const [isDisabled, toggleDisabled] = useState(true)

    const classes = useStyles()
    const dispatch = useDispatch()

    const { categories, countries } = useSelector(state => state.persist)

    useEffect(() => {
        dispatch(loadCountries())
        return () => {
            dispatch(clearFilters())
        }
    }, [dispatch])

    return (
        <div className={classes.root}>
            <div className="stand-list__filter-list">
                <div className="stand-list_filter-company">
                    <SelectInput
                        clearValue={(e) => console.log(e)}
                        name="category"
                        label={<MessageLang id="stands.CompanyList.Table.activity"/>}
                        isDisabled={false}
                        isLoading={loading}
                        isClearable={true}
                        options={categories && categories.map(cat =>
                            ({ value: cat.id, label: cat.name }))}
                    // onChange={(value, type) => handleChangeCategory(value, type)}
                    />

                    <SelectInput
                        name="sub_categories"
                        label={<MessageLang id="stands.MemberFilter.form.categories"/>}
                        isDisabled={isDisabled}
                        isLoading={loading}
                        isClearable={true}
                        options={subCategories && subCategories.map(sub =>
                            ({ value: sub.id, label: sub.name }))}
                    // onChange={(value, type) => handleChangeSubCategory(value, type)}
                    />

                    <SelectInput
                        name="countries"
                        label={<MessageLang id="stands.CompanyList.Table.country"/>}
                        isDisabled={false}
                        isLoading={loading}
                        isClearable={true}
                        options={countries && countries.map(country =>
                            ({ value: country.id, label: country.name }))}
                    // onChange={(value, type) => handleChangeCountry(value, type)}
                    />
                </div>
            </div >
        </div >
    )
}

export default JobFilter
