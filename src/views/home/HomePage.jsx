import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import App from '../../component/JVectorMap/demo/App';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        height: 140,
        width: 100,
    },
    control: {
        padding: theme.spacing(2),
    },
}));


const handleClick = (event, code) => {
    console.log(`You have click ${code}`)
}
const HomePage = () => {
    return (
        <Grid container spacing={2} >
            <Grid item xs={12}>
                <Paper>
                    <div style={{ width: 1000, height: 500 }}>
                        {/* <VectorMap map={'world_mill'}
                backgroundColor="#3b96ce"
                ref="map"
                containerStyle={{
                  width: '100%',
                  height: '100%'
                }}
                onRegionClick={handleClick}
                containerClassName="map"
              /> */}
                        {/* <App /> */}
                    </div>
                </Paper>
            </Grid>
        </Grid>
    )
}

export default HomePage
