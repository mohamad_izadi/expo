import React, { useEffect } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import AppBar from '@material-ui/core/AppBar';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import { loadPavilionsDetails } from '../../app/store/actions/pavilionsAction';
import CallIcon from '@material-ui/icons/Call';
import PublicIcon from '@material-ui/icons/Public';
import InstagramIcon from '@material-ui/icons/Instagram';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
const useStyles = makeStyles((theme) => ({
    AppBar: {
        backgroundColor: '#fff !important',
        color: 'black',
        height: 60,
        paddingTop: 5,
        bottom: 0,
        top: 'initial',
    },
}));

const Introduction = ({ className, ...rest }) => {
    const classes = useStyles();
    const { details } = useSelector((state) => state.pavilion);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(loadPavilionsDetails(1, 1, 'about'))
    }, [dispatch])
    return (
        <div className="exhebition-introduction">
            {/* {details && details.media && details.media.catalogs.length > 0 &&
                <div className="exhebition__slider">
                    <div class="carousel-wrapper">
                        <Carousel showThumbs={false} showStatus={false}>
                            {details.media.catalogs.map(image =>
                                <div>
                                    <img alt={details.name} src={image} />
                                </div>
                            )}
                        </Carousel>
                    </div>
                </div>} */}

            <div className="exhebition-introduction__content">
                <div className="exhebition-introduction__title">
                    {details && details.name}
                </div>
                <div className="exhebition-introduction__description"
                    dangerouslySetInnerHTML={{ __html: details.about }}>
                </div>
            </div>
            <AppBar position="fixed" className={classes.AppBar}>
                <div className="exhebition-introduction__contact">
                    <div className="exhebition-introduction__contact__item"><div className="exhebition-introduction__contact__item__icon"><CallIcon /></div></div>
                    <div className="exhebition-introduction__contact__item"><div className="exhebition-introduction__contact__item__icon"><PublicIcon /></div></div>
                    <div className="exhebition-introduction__contact__item"><div className="exhebition-introduction__contact__item__icon"><InstagramIcon /></div></div>
                    <div className="exhebition-introduction__contact__item"><div className="exhebition-introduction__contact__item__icon"><LinkedInIcon /></div></div>
                </div>
            </AppBar>
        </div>

    );
};
export default Introduction;