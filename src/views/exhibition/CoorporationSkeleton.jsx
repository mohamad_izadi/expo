import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import TableContainer from '@material-ui/core/TableContainer';

import { Paper, CardHeader } from '@material-ui/core'
import Skeleton from '@material-ui/lab/Skeleton';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Button from '@material-ui/core/Button';
import EmailIcon from '@material-ui/icons/Email';

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    card: {
        maxWidth: 345,
        margin: theme.spacing(2),
    },
    media: {
        height: 190,
    },
}));

const CoorporationSkeleton = () => {
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            <div className="cooperation-item">
                <div className="cooperation-item__top">
                    <div className="cooperation-item__top__title"> <Skeleton align="center" animation="wave" height={15} width="20%" /></div>
                    <div className="cooperation-item__top__job">
                        <Button variant="contained"> <Skeleton align="center" animation="wave" height={15} width="40%" /></Button>
                        <div className="cooperation-item__top__location">
                            <div><LocationOnIcon /></div>
                        </div>
                    </div>
                </div>
                <div className="cooperation-item__description">
                    <Skeleton align="center" animation="wave" height={15} width="100%" />
                    <Skeleton align="center" animation="wave" height={15} width="100%" />
                    <Skeleton align="center" animation="wave" height={15} width="100%" />
                </div>
                <div>
                    <Skeleton align="center" animation="wave" height={15} width="50%" />
                </div>
            </div>
            <div className="cooperation-item">
                <div className="cooperation-item__top">
                    <div className="cooperation-item__top__title"> <Skeleton align="center" animation="wave" height={15} width="20%" /></div>
                    <div className="cooperation-item__top__job">
                        <Button variant="contained"> <Skeleton align="center" animation="wave" height={15} width="40%" /></Button>
                        <div className="cooperation-item__top__location">
                            <div><LocationOnIcon /></div>
                        </div>
                    </div>
                </div>
                <div className="cooperation-item__description">
                    <Skeleton align="center" animation="wave" height={15} width="100%" />
                    <Skeleton align="center" animation="wave" height={15} width="100%" />
                    <Skeleton align="center" animation="wave" height={15} width="100%" />
                </div>
                <div>
                    <Skeleton align="center" animation="wave" height={15} width="50%" />
                </div>
            </div>
            <div className="cooperation-item">
                <div className="cooperation-item__top">
                    <div className="cooperation-item__top__title"> <Skeleton align="center" animation="wave" height={15} width="20%" /></div>
                    <div className="cooperation-item__top__job">
                        <Button variant="contained"> <Skeleton align="center" animation="wave" height={15} width="40%" /></Button>
                        <div className="cooperation-item__top__location">
                            <div><LocationOnIcon /></div>
                        </div>
                    </div>
                </div>
                <div className="cooperation-item__description">
                    <Skeleton align="center" animation="wave" height={15} width="100%" />
                    <Skeleton align="center" animation="wave" height={15} width="100%" />
                    <Skeleton align="center" animation="wave" height={15} width="100%" />
                </div>
                <div>
                    <Skeleton align="center" animation="wave" height={15} width="50%" />
                </div>
            </div>

         
        </TableContainer>
    );
}

export default CoorporationSkeleton
