import React, { Fragment, useState } from 'react';
import {
    SelectInput,
    Switch
} from '../../component'
/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/


const RightSide = ({ onChange }) => {

    const options = [
        { value: '', label: <MessageLang id="exhibition.rightSide.lable1"/> },
        { value: 'job', label: <MessageLang id="exhibition.rightSide.lable2"/> },
        { value: 'investment', label: <MessageLang id="exhibition.rightSide.lable3"/> },
    ];
    return (
        <div className="product-list__right-side">
            <Switch label={<MessageLang id="exhibition.rightSide.switchLable"/>} />
            <SelectInput label={<MessageLang id="exhibition.rightSide.selectInput"/>} options={options} onChange={onChange} />
        </div>
    )
}

export default RightSide
