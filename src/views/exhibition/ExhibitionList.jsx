import React, { Fragment, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { exhibitionData } from '../../app/data/sampleData'
import {
    Container,
    Box,
    Button,
    Card,
    CardHeader,
    Divider,
    IconButton,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText,
    makeStyles
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';

/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const useStyles = makeStyles(({
    root: {
        height: '100%'
    },
    image: {
        height: 48,
        width: 48
    }
}));

const ExhibitionList = ({ className, ...rest }) => {
    const classes = useStyles();
    const [products] = useState(exhibitionData);

    return (
        <>
            <Card
                className={clsx(classes.root, className)}
                {...rest}
            >
                <CardHeader
                    subtitle={`${products.length} in total`}
                    title={<MessageLang id="exhibition.exhibitionlist.title"/>}
                />
                <Divider />
                <List>
                    {products.map((product, i) => (
                        <ListItem
                            divider={i < products.length - 1}
                            key={product.id}
                        >
                            <ListItemAvatar>
                                <img
                                    alt="Product"
                                    className={classes.image}
                                    src={product.imageUrl}
                                />
                            </ListItemAvatar>
                            <ListItemText
                                primary={product.name}
                                secondary={`Updated ${product.updatedAt.fromNow()}`}
                            />
                            <IconButton
                                edge="end"
                                size="small"
                            >
                                <MoreVertIcon />
                            </IconButton>
                        </ListItem>
                    ))}
                </List>
                <Divider />
                <Box
                    display="flex"
                    justifyContent="flex-end"
                    p={2}
                >
                    <Button
                        color="primary"
                        endIcon={<ArrowRightIcon />}
                        size="small"
                        variant="text"
                    >
                    </Button>
                </Box>
            </Card>
        </>
    )
}

export default ExhibitionList
