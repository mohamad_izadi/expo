import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import clsx from 'clsx';

import {
    Card,
    makeStyles
} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';

import SearchBox from '../../component/Search/SearchBox';
import RightSide from './RightSide';
import CooperationItem from './CooperationItem';

import { fetcPavilionOpportunities } from '../../app/store/actions/opportunitiesActions';
import CoorporationSkeleton from './CoorporationSkeleton'


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    paperExhebition: {
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

const CooperationList = ({ className, ...rest }) => {

    const classes = useStyles();
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetcPavilionOpportunities(1))

    }, [dispatch])
    const { pavilionOpportunities, pavilionOpportunitiesLoading } = useSelector((state) => state.opportunities);
    console.log('pavilionOpportunities', pavilionOpportunities)
    return (
        <>
            <Card
                className={clsx(classes.root, className)}
                {...rest}
            >
                <Grid container className="product-list">
                    <Grid item lg={3} xs={12}>
                        <Grid container>
                            <Grid item xs={12}>
                                <SearchBox />
                            </Grid>
                            <Grid item xs={12}>
                                <RightSide onChange={(event) => {
                                    dispatch(fetcPavilionOpportunities(1, event.value))
                                }} />
                            </Grid>

                        </Grid>
                    </Grid>

                    <Grid item lg={9} xs={12}>
                        {
                            pavilionOpportunitiesLoading ? <CoorporationSkeleton /> :<>
                            {
                                pavilionOpportunities.map((item, index) => {
                                    return (<CooperationItem item={item} />)
                                })
                            }

                        </>
                        }

                    </Grid>

                </Grid>

            </Card>
        </>
    )
}

export default CooperationList
