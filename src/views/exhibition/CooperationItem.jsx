import React, { useState } from 'react';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Button from '@material-ui/core/Button';
import ApartmentIcon from '@material-ui/icons/Apartment';
import EmailIcon from '@material-ui/icons/Email';
import { createOpportunity } from '../../app/store/actions/opportunitiesActions'
import CircularProgress from '@material-ui/core/CircularProgress';
import { useDispatch } from 'react-redux';

/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const CooperationItem = ({ item }) => {
    const [open, setOpen] = useState(false)
    const [loading, setLoading] = useState(false)
    const [description, setDescription] = useState('')
    const dispatch = useDispatch();
    async function createItem(id, description) {
        try {
            setLoading(true)
            await dispatch(createOpportunity(id, description))
        } catch (error) {
            console.log(error.message);
        } finally {
            setLoading(false)
            setDescription('')
        }
    }
    return (
        <div className="cooperation-item">
            <div className="cooperation-item__top">
                <div className="cooperation-item__top__title">{item.title && item.title}</div>
                <div className="cooperation-item__top__job">
                    <Button variant="contained">{item.type==='job' ?
                     <MessageLang id="exhibition.coopeationItem.opportunities"/>:
                     <MessageLang id="exhibition.coopeationItem.opportunitiestwo"/>
                      }</Button>
                    <div className="cooperation-item__top__location">
                        <div><LocationOnIcon /></div> <div>{item.location}</div>
                    </div>
                  
                </div>

            </div>
            <div className="cooperation-item__description">
               {
                    item.description
               }
            </div>

            {
                open && (
                    <div className="opportunity-item__create">
                        <h1><MessageLang id="exhibition.coopeationItem.create"/></h1>
                        <textarea
                            value={description}
                            onChange={(event) => {
                                setDescription(event.target.value)
                            }}
                        />
                    </div>
                )
            }
            <Button
                variant="contained"
                className="coopration_btn"
                onClick={() => {
                    if (item.is_requested){
                        if (open) {
                            createItem(1, description)
                        } else {
                            setOpen(!open)
                        }
                    }
             

                }}

                startIcon={<EmailIcon />}
            >
                {
                    loading ? <CircularProgress className="opportunity-item__loading" /> : !item.is_requested ? 
                    <MessageLang id="exhibition.coopeationItem.loading"/> : 
                    <MessageLang id="exhibition.coopeationItem.loadingtwo"/> 
                }

            </Button>

        </div>
    )
}

export default CooperationItem
