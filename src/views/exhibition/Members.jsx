import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux'
import {
    Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Avatar, CardHeader
} from '@material-ui/core';


import { loadPavilionsMembers } from '../../app/store/actions/pavilionsAction';
import VoiceChatIcon from '@material-ui/icons/VoiceChat';
import MessageIcon from '@material-ui/icons/Message';
import Loading from '../../component/Loading/Loading'

/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const useStyles = makeStyles({
    table: {
        minWidth: 650,
        '& .MuiTableHead-root': {
            '& .MuiTableRow-root': {
                height: 70,
                color: '#9f9f9f',
                fontSize: 14,
                fontWeight: 'bold'
            },
        },

        '& .MuiCardHeader-root': {
            padding: 0
        }
    },
});


const MemberList = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { members } = useSelector((state) => state.pavilion);

    useEffect(() => {
        dispatch(loadPavilionsMembers(1))
    }, [dispatch])
    return (
        <div className="exhebition-members">
            <TableContainer>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell><MessageLang id="exhibition.members.TableCell1"/></TableCell>
                            <TableCell><MessageLang id="exhibition.members.TableCell2"/></TableCell>
                            <TableCell><MessageLang id="exhibition.members.TableCell3"/></TableCell>
                            <TableCell><MessageLang id="exhibition.members.TableCell4"/></TableCell>
                            <TableCell ></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {members && members.map((row, index) => {
                            return (
                                <TableRow key={index}>
                                    <TableCell>
                                        <CardHeader
                                            avatar={
                                                <Avatar alt="" src={row.avatar} width={30} height={30} />
                                            }
                                            title={row.name}
                                        // subheader="September 14, 2016"
                                        />
                                    </TableCell>
                                    <TableCell >{row.title}</TableCell>
                                    <TableCell>{row.phone}</TableCell>

                                    <TableCell >{row.country}</TableCell>
                                    <TableCell >
                                        <div className="exhebition-members__groupicons">
                                            <div className="exhebition-members__icons">
                                                <VoiceChatIcon />
                                            </div>
                                            <div className="exhebition-members__icons">
                                                <MessageIcon />
                                            </div>
                                        </div>
                                    </TableCell>
                                </TableRow>
                            )
                        })
                        }
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default MemberList

