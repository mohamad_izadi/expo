import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Introduction from './Introduction';
import Members from './Members';
import CooperationList from './CooperationList';
import Comments from './Comments'
import ProductList from '../product/dashboard/ProductList';
import PhoneIcon from '@material-ui/icons/Phone';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import ModeCommentIcon from '@material-ui/icons/ModeComment';
import BookIcon from '@material-ui/icons/Book';
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Typography>{children}</Typography>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: '#fff !important',
        height: 70,

    },
    AppBar: {
        backgroundColor: '#fff !important',
        color: 'black',
        height: 70,
        //position:'fixed'

    },
    Tabs: {
        height: 70,
        borderBottom: '1px solid #eee',
        '& .MuiTab-root ': {
            color: '#9f9f9f',
            fontSize: 16,
            fontWeight: 900,


        },
        '& .Mui-selected': {
            color: '#475974',
            borderBottom: '2px solid #475974'
        },
        '& .MuiTab-wrapper': {
            display: 'flex',
            flexDirection: 'row'
        },
        '& .MuiTabs-indicator': {
            borderBottom: '2px solid #475974'
        }


    },
    tab: {
        '& .PrivateTabIndicator-colorSecondary': {
            backgroundColor: '#475974'
        },
        '& .MuiTab-wrapper': {
            display: 'flex',
            flexDirection: 'row',
            '& .MuiSvgIcon-root': {
                width: 20,
                height: 20
            }
        }
    }

}));

export default function SimpleTabs() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <AppBar position="static" className={classes.AppBar}>
                <Tabs value={value} onChange={handleChange} aria-label="" className={classes.Tabs}>
                    <Tab label="معرفی" icon={<PhoneIcon />} {...a11yProps(0)} className={classes.tab} />
                    <Tab label="اعضا" icon={<PeopleAltIcon />} {...a11yProps(1)} className={classes.tab} />
                    <Tab label="محصولات و خدمات" icon={<PhoneIcon />} {...a11yProps(2)} className={classes.tab} />
                    <Tab label="گفت و گوها" icon={<ModeCommentIcon />} {...a11yProps(3)} className={classes.tab} />
                    <Tab label="همکاری ها" icon={<BookIcon />} {...a11yProps(4)} className={classes.tab} />
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                <Introduction />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <Members />
            </TabPanel>
            <TabPanel value={value} index={2}>
                <ProductList isExhebition />
            </TabPanel>
            <TabPanel value={value} index={3}>
                <Comments isExhebition />
            </TabPanel>

            <TabPanel value={value} index={4}>
                <CooperationList isExhebition />
            </TabPanel>
        </div>
    );
}