import React from 'react';

import Rating from '@material-ui/lab/Rating';

const CommentItem = ({ item,key }) => {
    const [value, setValue] = React.useState(2);
    return (
        <div className="comments-item-pavilion" key={key}>
            <div className="comments-item-pavilion__img"><img src={item.user.avatar} alt=""/></div>
            <div className="comments-item-pavilion__details">
                <div className="comments-item-pavilion__details__name">{item.user.full_name}</div>
                <div>{item.user.company_name}</div>
                <p>{item.comment}</p>
            </div>
            <div className="comments-item-pavilion__date">
                {item.created_at}
            </div>

        </div>
    )
}

export default CommentItem
