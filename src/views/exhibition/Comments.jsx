import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux'

import AppBar from '@material-ui/core/AppBar';
import { loadPavilionsComments, createComment } from '../../app/store/actions/pavilionsAction';
import Rating from '@material-ui/lab/Rating';
import ChatIcon from '@material-ui/icons/Chat';
import CommentItem from './CommentItem';
import Loading from '../../component/Loading/Loading';
import SendBox from './SendBox'

/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/

const useStyles = makeStyles({
    AppBar: {
        backgroundColor: '#fff !important',
        color: 'black',
        height: 70,
        paddingTop: 5,
        bottom: 0,
        top: 'initial',
        paddingLeft: 240

    },
});


const CommentsComponent = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { comments, loadingComments } = useSelector((state) => state.pavilion);
    const [value, setValue] = useState(3)
    const [msg, setMsg] = useState('')

    useEffect(() => {
        dispatch(loadPavilionsComments(1))
    }, [dispatch])

    async function create() {
        try {
            await dispatch(createComment(1, msg))
        } catch (error) {
            console.log(error.message);
        } finally {
            dispatch(loadPavilionsComments(1))
            setMsg('')
        }
    }

    return (
        <div className="exhebition-comments">
            <div className="exhebition-comments__header">
                <div className="exhebition-comments__header__icon">
                    <ChatIcon />
                </div>

                <div className="exhebition-comments__header__title">
                    <h1><MessageLang id="exhibition.coments.title"/></h1>
                    <h2><MessageLang id="exhibition.coments.h2"/></h2>
                </div>
                <div className="exhebition-comments__header__rate">
                    <Rating
                        name="simple-controlled"
                        value={value}
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                    />
                    <p><MessageLang id="exhibition.comments.vote.p"/></p>
                </div>

            </div>
            {
                loadingComments && <Loading />
            }
            <div className="exhebition-comments__list">
                {
                    comments && comments.map((item, index) => {
                        return (
                            <CommentItem item={item} key={index} />
                        )
                    })
                }

            </div>

            <AppBar position="fixed" className={classes.AppBar}>
                <SendBox
                    msg={msg}
                    onClick={create}
                    onChange={(event) => {
                        setMsg(event.target.value)
                    }} />
            </AppBar>
        </div>
    );
}

export default CommentsComponent

