import React, { useState } from 'react';

import 'emoji-mart/css/emoji-mart.css';
import { Picker } from 'emoji-mart';
import SendIcon from '@material-ui/icons/Send';

/*************change lang ***************/
import MessageLang from '../../lang/';
/*************change lang ***************/
function Icon() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="41"
            height="41"
            x="0"
            y="0"
            enableBackground="new 0 0 535.5 535.5"
            version="1.1"
            viewBox="0 0 535.5 535.5"
            xmlSpace="preserve"
        >
            <path d="M0 497.25L535.5 267.75 0 38.25 0 216.75 382.5 267.75 0 318.75z" fill="#ffffff"></path>
        </svg>
    );
}



function Imoji() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="23"
            height="23"
            x="0"
            y="0"
            enableBackground="new 0 0 510 510"
            version="1.1"
            viewBox="0 0 510 510"
            xmlSpace="preserve"
        >
            <path d="M344.25 229.5c20.4 0 38.25-17.85 38.25-38.25S364.65 153 344.25 153 306 170.85 306 191.25s17.85 38.25 38.25 38.25zm-178.5 0c20.4 0 38.25-17.85 38.25-38.25S186.15 153 165.75 153s-38.25 17.85-38.25 38.25 17.85 38.25 38.25 38.25zM255 408c66.3 0 122.4-43.35 145.35-102h-290.7C132.6 364.65 188.7 408 255 408zm0-408C114.75 0 0 114.75 0 255s114.75 255 255 255 255-114.75 255-255S395.25 0 255 0zm0 459c-112.2 0-204-91.8-204-204S142.8 51 255 51s204 91.8 204 204-91.8 204-204 204z" fill="#9f9f9f"></path>
        </svg>
    );
}
const SendBox = ({ msg, onChange, onClick, onSelect }) => {
    const [open, setOpen] = useState(false);

    return (
        <>
            <div className="send-box">
                <div className="send-box-emoji" onClick={() => setOpen(!open)}>
                    <Imoji />
                </div>

                <input placeholder={<MessageLang id="exhibition.sendBox.placeHolder"/>} value={msg} onChange={onChange} />

                <div className="send-box-icon" onClick={onClick}>
                    <SendIcon />
                </div>


            </div>
            {/*
                open && <Picker

                    skin={1}
                    size={48}
                    set='google' onSelect={(emoji) => {
                        onSelect(emoji)

                    }} />
                */ }

        </>

    )
}

export default SendBox
