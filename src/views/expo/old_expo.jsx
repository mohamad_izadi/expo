import React, { useEffect } from 'react'
import { Paper } from '@material-ui/core'
import { VectorMap } from '../../component/JVectorMap/lib';
import { history } from '../../app/store/configureStore'
import { useDispatch, useSelector } from 'react-redux'
import { series } from './regions'
import { loadExpos } from '../../app/store/actions/persistActions'

import $ from 'jquery'
window.jQuery = window.$ = $

const handleClick = (event, code) => {

    if (event)
        console.log(event)

    if (code.includes('C1'))
        history.push(`/expo/exhibition/1`);
    else if (code.includes('C2'))
        history.push(`/expo/exhibition/2`);
    else if (code.includes('C3'))
        history.push(`/expo/exhibition/3`);
    else if (code.includes('C4'))
        history.push(`/expo/exhibition/4`);
    else if (code.includes('C5'))
        history.push(`/expo/exhibition/5`);
    else if (code.includes('C6'))
        history.push(`/expo/exhibition/6`);
    else if (code.includes('C7'))
        history.push(`/expo/exhibition/7`);

    if (code.includes('B1'))
        history.push(`/expo/exhibition/1`);
    else if (code.includes('B2'))
        history.push(`/expo/exhibition/2`);
}


const ExpoComponent = () => {
    const dispatch = useDispatch()
    const expos = useSelector(state => state.persist.expos)

    useEffect(() => {
        if (expos.length === 0)
            dispatch(loadExpos())

        var svgMap = $('.jvectormap-container > svg').get(0);
        var svgNS = 'http://www.w3.org/2000/svg';
        var svgNSXLink = 'http://www.w3.org/1999/xlink';

        svgMap.setAttribute('xmlns', svgNS);
        svgMap.setAttribute('xmlns:link', svgNSXLink);
        svgMap.setAttribute('xmlns:ev', 'http://www.w3.org/2001/xml-events');

        var defs =
            svgMap.querySelector('defs') ||
            svgMap.insertBefore(document.createElementNS(svgNS, 'defs'), svgMap.firstChild);



        // #region STAND 1

        // Create pattern for markers.
        var pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'typec_top_1');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('x', '0');
        pattern.setAttribute('y', '0');
        pattern.setAttribute('width', '1');
        pattern.setAttribute('height', '1');
        // pattern.setAttribute('viewBox', '0 0 100 150');
        // pattern.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        // Create image for pattern.
        var image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '26');
        image.setAttribute('y', '-305');
        image.setAttribute('width', '7%');
        image.setAttribute('height', '100%');
        image.setAttribute("class", 'ctop');
        image.setAttributeNS(svgNSXLink, 'xlink:href', expos.length > 0 && expos[0].logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);



        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'typec_left_1');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('x', '0');
        pattern.setAttribute('y', '0');
        pattern.setAttribute('width', '1');
        pattern.setAttribute('height', '1');
        // pattern.setAttribute('viewBox', '0 0 100 150');
        // pattern.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '1');
        image.setAttribute('y', '-295');
        image.setAttribute('width', '3%');
        image.setAttribute('height', '100%');
        image.setAttribute('class', 'cleft');
        image.setAttributeNS(svgNSXLink, 'xlink:href', expos.length > 0 && expos[0].logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern)

        //#endregion

        // #region STAND 2

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'typec_top_2');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('x', '0');
        pattern.setAttribute('y', '0');
        pattern.setAttribute('width', '1');
        pattern.setAttribute('height', '1');
        // pattern.setAttribute('viewBox', '0 0 100 150');
        // pattern.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '26');
        image.setAttribute('y', '-305');
        image.setAttribute('width', '7%');
        image.setAttribute('height', '100%');
        image.setAttribute("class", 'ctop');
        image.setAttributeNS(svgNSXLink, 'xlink:href', expos.length > 0 && expos[1].logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);




        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'typec_left_2');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('x', '0');
        pattern.setAttribute('y', '0');
        pattern.setAttribute('width', '1');
        pattern.setAttribute('height', '1');
        // pattern.setAttribute('viewBox', '0 0 100 150');
        // pattern.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '1');
        image.setAttribute('y', '-295');
        image.setAttribute('width', '3%');
        image.setAttribute('height', '100%');
        image.setAttribute('class', 'cleft');
        image.setAttributeNS(svgNSXLink, 'xlink:href', expos.length > 0 && expos[1].logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern)

        //#endregion

        // #region STAND 3

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'typec_top_3');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('x', '0');
        pattern.setAttribute('y', '0');
        pattern.setAttribute('width', '1');
        pattern.setAttribute('height', '1');
        // pattern.setAttribute('viewBox', '0 0 100 150');
        // pattern.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '26');
        image.setAttribute('y', '-305');
        image.setAttribute('width', '7%');
        image.setAttribute('height', '100%');
        image.setAttribute("class", 'ctop');
        image.setAttributeNS(svgNSXLink, 'xlink:href', expos.length > 0 && expos[2].logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);




        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'typec_left_3');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('x', '0');
        pattern.setAttribute('y', '0');
        pattern.setAttribute('width', '1');
        pattern.setAttribute('height', '1');
        // pattern.setAttribute('viewBox', '0 0 100 150');
        // pattern.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '1');
        image.setAttribute('y', '-295');
        image.setAttribute('width', '3%');
        image.setAttribute('height', '100%');
        image.setAttribute('class', 'cleft');
        image.setAttributeNS(svgNSXLink, 'xlink:href', expos.length > 0 && expos[2].logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern)

        // #endregion

        // #region STAND 4 

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'typec_top_4');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('x', '0');
        pattern.setAttribute('y', '0');
        pattern.setAttribute('width', '1');
        pattern.setAttribute('height', '1');
        // pattern.setAttribute('viewBox', '0 0 100 150');
        // pattern.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '26');
        image.setAttribute('y', '-305');
        image.setAttribute('width', '7%');
        image.setAttribute('height', '100%');
        image.setAttribute("class", 'ctop');
        image.setAttributeNS(svgNSXLink, 'xlink:href', expos.length > 0 && expos[3].logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);




        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'typec_left_4');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('x', '0');
        pattern.setAttribute('y', '0');
        pattern.setAttribute('width', '1');
        pattern.setAttribute('height', '1');
        // pattern.setAttribute('viewBox', '0 0 100 150');
        // pattern.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '1');
        image.setAttribute('y', '-295');
        image.setAttribute('width', '3%');
        image.setAttribute('height', '100%');
        image.setAttribute('class', 'cleft');
        image.setAttributeNS(svgNSXLink, 'xlink:href', expos.length > 0 && expos[3].logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern)

        // #endregion

        // #region STAND 5

        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'typec_top_5');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('x', '0');
        pattern.setAttribute('y', '0');
        pattern.setAttribute('width', '1');
        pattern.setAttribute('height', '1');
        // pattern.setAttribute('viewBox', '0 0 100 150');
        // pattern.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '26');
        image.setAttribute('y', '-305');
        image.setAttribute('width', '7%');
        image.setAttribute('height', '100%');
        image.setAttribute("class", 'ctop');
        image.setAttributeNS(svgNSXLink, 'xlink:href', expos.length > 0 && expos[4].logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern);




        // Create pattern for markers.
        pattern = document.createElementNS(svgNS, 'pattern');
        pattern.setAttribute('id', 'typec_left_5');
        // pattern.setAttribute('patternUnits', 'userSpaceOnUse');
        pattern.setAttribute('x', '0');
        pattern.setAttribute('y', '0');
        pattern.setAttribute('width', '1');
        pattern.setAttribute('height', '1');
        // pattern.setAttribute('viewBox', '0 0 100 150');
        // pattern.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        // Create image for pattern.
        image = document.createElementNS(svgNS, 'image');
        image.setAttribute('x', '1');
        image.setAttribute('y', '-295');
        image.setAttribute('width', '3%');
        image.setAttribute('height', '100%');
        image.setAttribute('class', 'cleft');
        image.setAttributeNS(svgNSXLink, 'xlink:href', expos.length > 0 && expos[4].logo);

        // Put it together
        pattern.appendChild(image);
        defs.appendChild(pattern)

        // #endregion

        // #region STAND IMAGES

        $('path[data-code="C5-002"]').attr("fill", 'url(#typec_top_1)')
        $('path[data-code="C5-001"]').attr("fill", 'url(#typec_left_1)')

        $('path[data-code="C2-002"]').attr("fill", 'url(#typec_top_2)')
        $('path[data-code="C2-001"]').attr("fill", 'url(#typec_left_2)')

        $('path[data-code="C1-002"]').attr("fill", 'url(#typec_top_3)')
        $('path[data-code="C1-001"]').attr("fill", 'url(#typec_left_3)')

        $('path[data-code="C6-002"]').attr("fill", 'url(#typec_top_4)')
        $('path[data-code="C6-001"]').attr("fill", 'url(#typec_left_4)')

        $('path[data-code="C7-002"]').attr("fill", 'url(#typec_top_5)')
        $('path[data-code="C7-001"]').attr("fill", 'url(#typec_left_5)')

        // #endregion

    }, [dispatch, expos]);

    return (
        <Paper className={'expo_container'}>
            <VectorMap map={'placement'}
                backgroundColor="transparent"
                zoomOnScroll={true}
                // markers={markers}
                containerStyle={{
                    width: "100%",
                    height: "100%"
                }}
                containerClassName="map"
                regionStyle={{
                    initial: {
                        fill: "transparent",
                        "fill-opacity": 0,
                        stroke: "transparent",
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    },
                    hover: {
                        "fill-opacity": 0,
                        cursor: "pointer"
                    },
                    selected: {
                        fill: "#2938bc" //color for the clicked region
                    },
                    selectedHover: {}
                }}
                regionsSelectable={false}
                series={series}
                zoomMax={16}
                onRegionTipShow={(e) => {
                    e.preventDefault()
                }}
            // ref="map"
            // onRegionClick={handleClick}
            />
        </Paper>
    )
}

export default ExpoComponent
