import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import logo from './TemplateData/img/Logo.png'
import iranplast_logo from './TemplateData/img/iranplast-loading.png'
import gewiran_logo from './TemplateData/img/gewiran-loading.png'
import package_logo from './TemplateData/img/package-loading.png'
import buisipo_logo from './TemplateData/img/buisipo-loading.png'
import yalda_logo from './TemplateData/img/yalda-loading.png'
import MessageLang from '../../lang/';
import { setTourGuide } from '../../app/store/actions/authActions'
import Tour from 'reactour'
import { Button } from '@material-ui/core';

function ExpoComponent(props) {
    const containerRef = useRef(null)
    const [styleProgress, setStyleProgress] = useState({
        warningBrowserRef: "none",
        warningMobileRef: "none",
        errorRef: "none",
        loadingBlock: "flex",
        emptyBar: "0%",
        fullBar: "0%",
    })
    var getUrl = window.location;
    var baseUrl = getUrl.protocol + "//" + getUrl.host;
    const exhibition = useSelector(state => state.exhibition.data);
    const [isTourOpen, setTourOpen] = useState(false)
    const { currentUser } = useSelector(state => state.auth);
    const dispatch = useDispatch()

    const initUnity = async (callback) => {
        var gameInstance;
        gameInstance = await window.UnityLoader.instantiate(containerRef.current, `${baseUrl}/Build/${exhibition.short_name}/index.json`, {
            onProgress: UnityProgress,
            compatibilityCheck: CheckCompatibility,
            Module: {
                //TOTAL_MEMORY: 268435456,
                onRuntimeInitialized: RuntimeInitialized,
            },
        });
        callback(gameInstance)
        function CheckCompatibility(gameInstance, onsuccess, onerror) {
            if (!window.UnityLoader.SystemInfo.hasWebGL) {
                setStyleProgress((val) => ({
                    ...val,
                    errorRef: "inherit",
                }))
                // errorRef.current.style.display = "inherit";
                onerror();
            } else if (window.UnityLoader.SystemInfo.mobile) {
                // warningMobileRef.current.style.display = "inherit";
                onsuccess();
            } else if (["Firefox", "Chrome", "Safari"].indexOf(window.UnityLoader.SystemInfo.browser) === -1) {
                // warningBrowserRef.current.style.display = "inherit";
                setStyleProgress((val) => ({
                    ...val,
                    warningBrowserRef: "inherit",
                }))
                onsuccess();
            } else {
                onsuccess();
            }
        }

        function RuntimeInitialized() {
        }

        function UnityProgress(gameInstance, progress) {
            if (!gameInstance.Module)
                return;
            setStyleProgress((val) => ({
                ...val,
                loadingBlock: "flex",
                fullBar: `${(100 * progress)}%`,
                emptyBar: `${(100 * (1 - progress))}%`,
            }))
            if (progress == 1) {
                setTimeout(function () {
                    setStyleProgress((val) => ({
                        ...val,
                        loadingBlock: "none",
                    }))
                }, 3000);
            }
        }

    }

    const ToggleFullScreen = () => {
        var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
            (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
            (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
            (document.msFullscreenElement && document.msFullscreenElement !== null);

        var element = document.body.getElementsByClassName("webgl-content")[0];

        if (!isInFullScreen) {
            document.getElementById("fullScreenButton").style.backgroundImage = `url('${baseUrl}/TemplateData/img/fullScreen_off.png')`;
            return (element.requestFullscreen ||
                element.webkitRequestFullscreen ||
                element.mozRequestFullScreen ||
                element.msRequestFullscreen).call(element);
        }
        else {
            document.getElementById("fullScreenButton").style.backgroundImage = `url('${baseUrl}/TemplateData/img/fullScreen_on.png')`;
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }
    }
    const loadGMaps = (callback) => {
        const existingScript = document.getElementById('gameContainerScript');
        if (!existingScript) {
            const script = document.createElement('script');
            script.src = `${baseUrl}/Build/${exhibition.short_name}/UnityLoader.js`;
            script.id = 'gameContainerScript';
            document.body.appendChild(script);
            script.onload = () => {
                if (callback) {
                    callback();
                }
            };
        }
        if (existingScript && callback) callback();
    };

    const steps = [
        {
            selector: '.MuiAppBar-root',
            content: <>
                <h4 className={'tour_header'}>{<MessageLang id="TourGuide.halls.title" />}</h4>
                <p>{<MessageLang id="TourGuide.halls.desc" />}</p>
                <Button className={'btn_close'} variant="contained" color="primary"
                    onClick={() => setTourOpen(false)}>
                    <MessageLang id="TourGuide.Button.Ok" />
                </Button>
            </>,
            style: {
                backgroundColor: '#fff',
                textAlign: 'center'
            },
        },
        // ...
    ];

    useEffect(() => {
        var gameInstance;
        loadGMaps(() => {
            initUnity((back) => gameInstance = back)
        })
        if (!currentUser.tour_guide.halls) {
            setTourOpen(true)
            dispatch(setTourGuide("halls"))
        }
        return () => {
            gameInstance.Quit();
            gameInstance = null;
            const existingScript = document.getElementById('gameContainerScript');
            existingScript.remove();
        };
    }, [])
    return (
        <div className="expo-container">
            <div className="keepRatio">
                <Tour
                    steps={steps}
                    isOpen={isTourOpen}
                    onRequestClose={() => setTourOpen(false)}
                    showNavigation={false}
                    showNavigationNumber={false}
                    showNumber={false}
                    showButtons={false}
                    showNavigationScreenReaders={false}
                    disableDotsNavigation={true}
                    className={'tourguide'}
                />
                <div className="webgl-content">
                    <button id="fullScreenButton" onClick={ToggleFullScreen}></button>
                    <div id="gameContainer" ref={containerRef}></div>
                </div>

                <div id="loadingBlock" style={{ display: styleProgress.loadingBlock }}>

                    {exhibition.short_name === "wevex" || exhibition.short_name === "prime" ?
                        <>
                            <img alt='' className="logo" src={logo} />
                            <div id="progressBar">
                                <div className="centered">
                                    <div id="emptyBar" style={{ width: styleProgress.emptyBar }}></div>
                                    <div id="fullBar" style={{ width: styleProgress.fullBar }}></div>
                                </div>
                            </div>
                        </> : <></>
                    }
                    {exhibition.short_name === "iranplast" ?
                        <>
                            <img alt='' className="logo" src={iranplast_logo} />
                            <div id="progressBar">
                                <div className="centered">
                                    <div id="emptyBar" style={{ width: styleProgress.emptyBar }}></div>
                                    <div id="fullBar" style={{ width: styleProgress.fullBar }}></div>
                                </div>
                            </div>
                        </> : <></>
                    }
                    {exhibition.short_name === "gewiran" ?
                        <>
                            <img alt='' className="logo" src={gewiran_logo} />
                            <div id="progressBar">
                                <div className="centered">
                                    <div id="emptyBar" style={{ width: styleProgress.emptyBar }}></div>
                                    <div id="fullBar" style={{ width: styleProgress.fullBar }}></div>
                                </div>
                            </div>
                        </> : <></>
                    }
                    {exhibition.short_name === "print_package" ?
                        <>
                            <img alt='' className="logo" src={package_logo} />
                            <div id="progressBar">
                                <div className="centered">
                                    <div id="emptyBar" style={{ width: styleProgress.emptyBar }}></div>
                                    <div id="fullBar" style={{ width: styleProgress.fullBar }}></div>
                                </div>
                            </div>
                        </> : <></>
                    }
                    {exhibition.short_name === "buisipo" ?
                        <>
                            <img alt='' className="logo" src={buisipo_logo} />
                            <div id="progressBar">
                                <div className="centered">
                                    <div id="emptyBar" style={{ width: styleProgress.emptyBar }}></div>
                                    <div id="fullBar" style={{ width: styleProgress.fullBar }}></div>
                                </div>
                            </div>
                        </> : <></>
                    }
                    {exhibition.short_name === "yaldasummit" ?
                        <>
                            <img alt='' className="logo" src={yalda_logo} />
                            <div id="progressBar">
                                <div className="centered">
                                    <div id="emptyBar" style={{ width: styleProgress.emptyBar }}></div>
                                    <div id="fullBar" style={{ width: styleProgress.fullBar }}></div>
                                </div>
                            </div>
                        </> : <></>
                    }

                    <div id="warningBrowserBlock" style={{ display: styleProgress.warningBrowserRef }}>
                        <div className="warningBrowserText">
                            لطفا یکی از این مرورگرها رو برای مشاده وبسایت نصب نمایید.
                        </div>
                        <div className="browserIcons">
                            <a href="https://www.mozilla.org/firefox" target="_blank"><img src="https://expo.namaline.ir/TemplateData/img/browser-firefox.png" alt="Firefox browser" /></a>
                            <a href="https://www.google.com/chrome" target="_blank"><img src="https://expo.namaline.ir/TemplateData/img/browser-chrome.png" alt="Chrome browser" /></a>
                            <a href="https://www.apple.com/safari/" target="_blank"><img src="https://expo.namaline.ir/TemplateData/img/browser-safari.png" alt="Safari browser" /></a>
                        </div>
                    </div>
                </div>
                <div id="warningMobileBlock" style={{ display: styleProgress.warningMobileRef }}>
                    <div className="warningBrowserText">
                        Please note that Unity WebGL is not currently supported on mobiles.
                    </div>
                </div>
                <div id="errorBrowserBlock" style={{ display: styleProgress.errorRef }}>
                    {/* <img className="logo" src="TemplateData/img/Logo.png"></img> */}
                    <span className="subtitle">  <br />
                    </span>

                    <div id="errorContent">
                        <div id="errorBrowserText">
                            مرور گر شما زا وب جی ال پشتیبانی نمیکند.
                        </div>
                        <div className="browserIcons">
                            <a href="https://www.mozilla.org/firefox" target="_blank"><img src="https://expo.namaline.ir/TemplateData/img/browser-firefox.png" alt="Firefox browser" /></a>
                            <a href="https://www.google.com/chrome" target="_blank"><img src="https://expo.namaline.ir/TemplateData/img/browser-chrome.png" alt="Chrome browser" /></a>
                            <a href="https://www.apple.com/safari/" target="_blank"><img src="https://expo.namaline.ir/TemplateData/img/browser-safari.png" alt="Safari browser" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ExpoComponent;
