import React, { useState, useEffect } from "react";
import {
  Grid,
  Container,
  Typography,
  Button,
  Accordion,
  AccordionDetails,
  AccordionSummary,
} from "@material-ui/core";
import Header from "./Header";
import Footer from "../Landing/Footer";
import Header_Menu from "../Landing/Header_Menu";
import imageHeader from "./header.jpg";

import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import MessageLang ,{messageLang} from '../../lang';
import { useSelector } from 'react-redux';

const Faq = () => {
  const [expanded, setExpanded] = useState(false);
  const [data, setData] = useState([]);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  const lang = useSelector((state) => state.lang.lang );
  useEffect(() => {
   if(lang==="en"){
    setData(require("./dataEn.json"));
   } else {
    setData(require("./data.json"));
   }
  }, []);
  return (
    <>
      <Header_Menu />
      <Header
        img={imageHeader}
        title={<MessageLang id="Faq.header.title" />}
        text={{ link: "/", text: <MessageLang id="Faq.header.text" />}}
      >
        <Paper component="form" className="header-index__wrapper__box__root">
          <IconButton type="submit" className="iconButton" aria-label="search">
            <SearchIcon />
          </IconButton>
          <InputBase
            className="input"
            placeholder={messageLang("Faq.header.input")}
            inputProps={{ "aria-label": <MessageLang id="Faq.header.input" /> }}
          />
        </Paper>
      </Header>
      <Grid container className="index__faq">
        <Container maxWidth="xl">
          <Grid
            container
            alignItems="center"
            justify="center"
            direction="column"
            className="index__faq__top"
          >
            <Typography variant="h4" className="index__faq__top__title">
            <MessageLang id="Faq.body.title" />

            </Typography>
            <Typography variant="body2" gutterBottom>
            <MessageLang id="Faq.body.subtitle" />
            </Typography>
          </Grid>
          <Grid>
            {data.map((item, key) => (
              <Accordion
                key={key}
                expanded={expanded === key}
                onChange={handleChange(key)}
                className="index__faq__accordion"
              >
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1bh-content"
                  id="panel1bh-header"
                >
                  <Typography
                    component="h3"
                    color="textPrimary"
                    variant="body1"
                    className="heading"
                  >
                    {item.title}
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography
                    component="p"
                    color="textSecondary"
                    variant="body2"
                  >
                    {item.value}
                  </Typography>
                </AccordionDetails>
              </Accordion>
            ))}
          </Grid>
          {/* <Grid container justify="center" className="index__faq__questions">
            <Button variant="contained" className="index__faq__questions__btn">مشاهده‌ی سوالات بیشتر</Button>
          </Grid> */}
        </Container>
      </Grid>
      <Footer />
    </>
  );
};

export default Faq;
