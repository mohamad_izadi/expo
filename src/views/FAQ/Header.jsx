import React from "react";
import {
  Grid,
  CardMedia,
  Typography,
  Container,
  Link,
  Box,
} from "@material-ui/core";

import PlayCircleFilledIcon from "@material-ui/icons/PlayCircleFilled";

import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";








const Header = ({ img, title, text, children, className }) => {
  const styles = (theme) => ({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });
  const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant="h6">{children}</Typography>
        {onClose ? (
          <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
            <CloseIcon />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  });
  const DialogContent = withStyles((theme) => ({
    root: {
      padding: theme.spacing(2),
    },
  }))(MuiDialogContent);
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
      setOpen(true);
    };
    const handleClose = () => {
      setOpen(false);
      }
  return (
    <Grid container className={`header-index ${className}`}>
      {img && <CardMedia className="header-index__bg" image={img} />}
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        className="header-index__wrapper"
      >
        <Container maxWidth="xl" style={{ zIndex: 1 }}>
          <Grid
            container
            className="header-index__wrapper__box"
            alignItems="center"
            justify="center"
            direction="column"
          >
            <Typography
              variant="h2"
              className="header-index__wrapper__box__title"
            >
              {title}
            </Typography>
            {children}
            {text && text.link !== undefined ? (
              <Link href="#" className="header-index__wrapper__box__link" onClick={handleClickOpen}>
                <Typography>{text.text}</Typography>
                <PlayCircleFilledIcon />


              </Link>
            ) : (
              <Typography>{text.text}</Typography>
            )}
          </Grid>
          <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open} >
                <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                  راهنما‌ی استفاده از محصول
                </DialogTitle>
                <DialogContent>
                <video width="550" height="450" controls >
                  <source src="./Videos/video1.mp4" type="video/mp4"/>
                </video>
                </DialogContent>
          </Dialog>
        </Container>
      </Box>
    </Grid>
  );
};

export default Header;