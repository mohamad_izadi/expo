import React from 'react'
import WorkSheetHeader from './WorkSheetHeader';
import WorkSheetList from './WorkSheetList'

const WorkSheet = () => {
    return (
        <div>
            <WorkSheetHeader />
            <WorkSheetList />

        </div>
    )
}

export default WorkSheet
