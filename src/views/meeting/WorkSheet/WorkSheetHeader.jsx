import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { AppBar, CircularProgress, Grid, Typography, Box, Card, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Carousel from 'react-elastic-carousel'
import { useDispatch } from 'react-redux';
import { loadEvents, setFilters } from '../../../app/store/actions/eventActions'
import { propTypes } from 'react-addons-css-transition-group';
import {
    fetchCalenders
} from '../../../app/store/actions/meetingActions';

const useStyles = makeStyles((theme) => ({
    root: {
        height: 80,
        backgroundColor:'#fff',
        alignItems: 'center',
        '& .bTybGL,.kyJvRZ': {
            backgroundColor: '#fff',
            boxShadow: 'none',
            color: '#9f9f9f'
        },
        '& .bTybGL:hover:enabled': {
            backgroundColor: '#fff',
            boxShadow: 'none',
            color: '#9f9f9f'
        },
        '& .bTybGL:focus:enabled': {
            backgroundColor: '#fff',
            boxShadow: 'none',
            color: '#9f9f9f'
        },
        '& .rec-carousel-item': {
            color: '#9f9f9f'
        }
    },
    carousel: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '294px'
    },
    item: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '50px',
        width: '50px',
        backgroundColor: '#fff',
        border: '1px solid #eee',
        margin: '15px',
        fontSize: '17px',
        cursor: 'pointer'
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
}))

const WorkSheetHeader = () => {
    const classes = useStyles();
    const { dates } = useSelector((state) => state.persist)
    const [selected, setSelected] = useState()
    const [loading, setLoading] = useState(false)
    const dispatch = useDispatch();

    useEffect(() => {
        if (dates.length > 0) {
            setSelected(dates.find(date => date.today)?.id || 1)
        }
    }, [dates]);
    const handelSelect =async(index)=>{
        setLoading(true)
        const selected_date = dates.find(date => date.id === index).date
        setSelected(index)
        await dispatch(fetchCalenders(selected_date))
        setLoading(false)
    }
    if (selected === undefined) return <CircularProgress />
    return (
        <AppBar position="static" color="default" className={classes.root}
        style={{display:'flex' , justifyContent:"center"}}>
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                item lg={4} xs={12}>
                <Carousel
                    onChange={(event) => handelSelect( event.index + 3)}
                    // breakPoints={breakPoints}
                    itemsToShow={5}
                    pagination={false}
                    // itemPadding={[0, 10, 0, 10]}
                    transitionMs={1}
                    initialActiveIndex={selected - 3}
                    className={`${classes.carousel} calendar`}
                >
                    {dates.map(date => (
                        <div key={date.id} date={date.date} 
                        // onClick={()=>handelSelect(date.id)} 
                        className={selected === date.id ? 'events-header-item events-header-item__selected' : 'events-header-item'}>
                            <div className={selected === date.id ? 'events-header-item__id events-header-item__id-selected' : "events-header-item__id"}>
                                {date.format.day}
                            </div>
                            <div className={selected === date.id ? 'events-header-item__title events-header-item__id-selected' : "events-header-item__title"}>
                                {date.format.month}
                            </div>
                        </div>
                    ))}
                </Carousel>
            </Grid>
        </AppBar >
    )
}

export default WorkSheetHeader
