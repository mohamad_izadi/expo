import React from 'react';
import { Avatar } from '@material-ui/core'

/*************change lang ***************/
import MessageLang, { messageLang } from '../../../lang/';
/*************change lang ***************/


const WorkSheetItem = ({ text }) => {
    return (
        <div className="worksheet-item">
            <div className="worksheet-item__calender-data">
                <div className="worksheet-item__calender-data__details">
                    <div className={`worksheet-item__calender-data__details__text ${text && text.event && 'worksheet-item__calender-data__details__text-event'}`}>
                        <Avatar
                            src='/static/images/avatars/user.png'
                        />
                        <p>
                            {
                                text && text.event && `${text.event.subject} - ${text.event.lecturer}`
                            }
                            {
                                text && text.meeting && text.meeting.user && `${messageLang("Meeting.meetingDashboard.workSheet.item")} ${text.meeting.user.full_name}`
                            }
                        </p>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default WorkSheetItem
