import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {
    Table, TableBody, TableCell, TableContainer, TableRow, Paper, Avatar, CardHeader
} from '@material-ui/core';
import WorkSheetItem from './WorkSheetItem';
import { useSelector, useDispatch } from 'react-redux';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import LockIcon from '@material-ui/icons/Lock';
import {lockDate,fetchCalenders} from '../../../app/store/actions/meetingActions';
import CircularProgress from '@material-ui/core/CircularProgress';
import {toastr} from 'react-redux-toastr'
const useStyles = makeStyles((theme) => ({
    
}));
const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const WorkSheetList = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {calenders} = useSelector((state) => state.meeting);
    const [loading, setLoading] = useState(false)
    const [selectedIndex, setSelectedIndex] = useState(-1)

    async function lockedDateTime(datetime, index) {
        setLoading(true)
        setSelectedIndex(index)
        try {
            await dispatch(lockDate(datetime));
            dispatch(fetchCalenders())
        } catch (error) {
            toastr.error(error.data.messages.flat())
        } finally {
            setLoading(false);
            setSelectedIndex(-1)
        }
    }
    return (
        <TableContainer component={Paper} className={'worksheetlist'} >
            <Table  aria-label="simple table ">
                <TableBody>
                    {calenders.map((row, index) => {
                        // console.log(row);
                        return(
                        <StyledTableRow key={index} style={{height:'48px'}}>
                            <TableCell align="center" component="th" scope="row"  className='time'>
                                <CardHeader
                                    title={row.time}
                                />
                            </TableCell >
                            <TableCell align="center" >
                                {
                                    (row.data && (row.data.event || row.data.meeting)) && <WorkSheetItem text={row.data} type={row.type} />
                                }

                            </TableCell>
                            <TableCell align="center" >
                                <div className={`worksheet-item__icon ${row.data && (!row.data.is_blocked ? '' : 'worksheet-item__icon-locked')}`}
                                    onClick={() => {
                                        lockedDateTime(`${row.datetime}`,index)
                                    }}>
                                    {
                                        (loading && selectedIndex === index) ? <CircularProgress size={20} /> : (row.data && row.data.is_blocked)?<LockIcon/>:<LockOpenIcon />
                                    }

                                </div>
                            </TableCell>
                        </StyledTableRow>
                    )})}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default WorkSheetList

