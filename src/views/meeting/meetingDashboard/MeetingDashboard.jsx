import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { AppBar, Tabs, Tab, Box, Typography, Container, Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import MeetingList from './MeetingList';
import WorkSheet from '../WorkSheet/WorkSheet'
import {
    fetchMeetings,
    fetchTodayMeetings,
    fetchCalenders
} from '../../../app/store/actions/meetingActions';
import { loadNotificationCounter } from '../../../app/store/actions/persistActions';
import { useSelector, useDispatch } from 'react-redux';
import { setTourGuide } from '../../../app/store/actions/authActions';
import './Meeting.css';
import MeetingListSkeleton from './MeetingListSkeleton';
import Tour from 'reactour'

/*************change lang ***************/
import MessageLang from '../../../lang/';
/*************change lang ***************/


const useStyles = makeStyles((theme) => ({

    description: {
        fontSize: '17px',
        color: 'secondary'
    },
    image: {
        display: 'inline-block',
        maxWidth: '40%',
    },

}))

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <>
                    {children}
                </>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const MeetingDashboard = () => {
    const classes = useStyles()
    const dispatch = useDispatch();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);
    const [isTourOpen, setTourOpen] = useState(false)
    const { currentUser } = useSelector(state => state.auth);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    useEffect(() => {
        if (!currentUser.tour_guide.meetings) {
            setTourOpen(true)
            dispatch(setTourGuide("meetings"))
        }
        dispatch(fetchMeetings());
        dispatch(fetchTodayMeetings());
        dispatch(fetchCalenders())
        dispatch(loadNotificationCounter());
    }, [dispatch]);

    const {
        meetings,
        loading,
        todayMeetings,
        todayLoading,
    } = useSelector((state) => state.meeting);

    const steps = [
        {
            selector: '.MuiAppBar-root',
            content: <>
                <h4 className={'tour_header'}>{<MessageLang id="TourGuide.meeting.title" />}</h4>
                <p>{<MessageLang id="TourGuide.meeting.desc" />}</p>
                <Button className={'btn_close'} variant="contained" color="primary"
                    onClick={() => setTourOpen(false)}>
                    <MessageLang id="TourGuide.Button.Ok" />
                </Button>
            </>,
            style: {
                backgroundColor: '#fff',
                textAlign: 'center'
            },
        },
        // ...
    ];

    // alert(JSON.stringify(meetings))
    return (
        <div className={`${classes.root} tab_header fftabs`}>
            <Tour
                steps={steps}
                isOpen={isTourOpen}
                onRequestClose={() => setTourOpen(false)}
                showNavigation={false}
                showNavigationNumber={false}
                showNumber={false}
                showButtons={false}
                showNavigationScreenReaders={false}
                disableDotsNavigation={true}
                className={'tourguide'}
            />
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="secondary"
                    textColor="primary"
                    className={classes.tabs}
                >
                    <Tab label={<MessageLang id="Meeting.meetingDashboard.meetingDashboard.tabLable1" />} {...a11yProps(0)} />
                    <Tab label={<MessageLang id="Meeting.meetingDashboard.meetingDashboard.tabLable2" />} {...a11yProps(1)} />
                </Tabs>
            </AppBar>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={value} index={0} dir={theme.direction}>
                    <Box className='meeting-view'>
                        <div className={'separator'}>
                            {<MessageLang id="Meeting.meetingDashboard.meetingDashboard.tabLable1" />}
                        </div>
                        {
                            (!loading && !meetings) && (
                                <Box
                                    display="flex"
                                    flexDirection="column"
                                    height="100%"
                                    justifyContent="start"
                                >
                                    <Container maxWidth="sm">
                                        <Box textAlign="center">
                                            <img
                                                alt="Empty Page"
                                                className={classes.image}
                                                src="/static/images/backgrounds/SVG/meetings.svg"
                                            />
                                        </Box>
                                        <Typography
                                            align="center"
                                            color="textPrimary"
                                            variant="subtitle2"
                                            className={classes.description}
                                        >
                                            {<MessageLang id="Meeting.meetingDashboard.meetingDashboard.noInventation" />}
                                        </Typography>
                                    </Container>
                                </Box>
                            )
                        }
                        {
                            !loading ? (
                                meetings && meetings.length > 0 ?
                                    <MeetingList meetings={meetings} reloadList={() => {
                                        dispatch(fetchMeetings());
                                    }} />
                                    :
                                    <img src={'/static/images/backgrounds/SVG/meetings.svg'} className='meetingicon' />
                            )
                                :
                                <MeetingListSkeleton />
                        }


                        <div className={'separator'} style={{ paddingTop: '20px' }}>
                            {<MessageLang id="Meeting.meetingDashboard.meetingDashboard.Today" />}
                        </div>
                        {
                            (!todayLoading && todayMeetings && todayMeetings.length === 0) && (
                                <div className="meeting-empty">
                                    {/* <VoiceChatIcon /> */}
                                    <img src={'/static/images/backgrounds/SVG/meetings.svg'} className='meetingicon' />
                                    <p> {<MessageLang id="Meeting.meetingDashboard.meetingDashboard.noInventationtoday" />}</p>
                                </div>
                            )
                        }
                        {
                            todayMeetings && todayMeetings.length > 0 && (
                                <MeetingList meetings={todayMeetings}
                                    reloadList={() => {
                                        dispatch(fetchMeetings());
                                    }} />
                            )
                        }
                        {
                            todayLoading && <MeetingListSkeleton />
                        }
                        <div className="set-meeting">
                            <div className="set-meeting__text">{<MessageLang id="Meeting.meetingDashboard.meetingDashboard.setMeetingText" />}</div>
                            <div className="set-meeting__btn">
                                <Link to={{
                                    pathname: 'stand',
                                    state: { value: 1 }
                                }}><button><MessageLang id="Meeting.meetingDashboard.meetingDashboard.tabLable11" /></button></Link>
                            </div>

                        </div>
                    </Box>

                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                    <WorkSheet />
                </TabPanel>
            </SwipeableViews>

        </div >

    )
}

export default MeetingDashboard
