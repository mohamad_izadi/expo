import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import MessageLang, { messageLang } from '../../../lang/';
import { Box, Avatar, Button, CircularProgress, Slide, IconButton } from '@material-ui/core';
import agent from '../../../app/api/agent';
import { toastr } from 'react-redux-toastr';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CloseIcon from '@material-ui/icons/Close';
import GeneralDrawer from '../../../component/Drawer/GeneralDrawer.jsx'
import ConfirmModal from '../../../component/ConfirmModal/ConfirmModal'
import {asyncActionStart} from '../../../app/store/actions/meetingActions'
import { getMinutes } from 'date-fns';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});
const MeetingInvitationCreate = ({ open, handleClose }) => {

    const list_hours = ["00:00", "00:30",
        "01:00", "01:30",
        "02:00", "02:30",
        "03:00", "03:30",
        "04:00", "04:30",
        "05:00", "05:30",
        "06:00", "06:30",
        "07:00", "07:30",
        "08:00", "08:30",
        "09:00", "09:30",
        "10:00", "10:30",
        "11:00", "11:30",
        "12:00", "12:30",
        "13:00", "13:30",
        "14:00", "14:30",
        "15:00", "15:30",
        "16:00", "16:30",
        "17:00", "17:30",
        "18:00", "18:30",
        "19:00", "19:30",
        "20:00", "20:30",
        "21:00", "21:30",
        "22:00", "22:30",
        "23:00", "23:30"]

    const [newDate, setNewDate] = useState('');
    const [newHour, setNewHour] = useState("07:00");
    const [loading, setLoading] = useState(false)
    const [msg, setMsg] = useState('')
    const [error, setError] = useState(null)
    const [confirm, setconfirm] = useState(false)
    const [dateRange, setDateRange] = useState([])
    
    const { dates } = useSelector((state) => state.persist);
    const { details: audience } = useSelector((state) => state.stand);
    const { lang } = useSelector((state) => state.lang);
    const { currentUser } = useSelector((state) => state.auth);

    const moment = require("moment");
    const time_= new Date;
    const mobileSize = window.innerWidth < 600 ? true : false

    var hours= list_hours.filter(time => time> moment(time_).format("HH:mm"))
    const close = () => {
        setconfirm(false)
    }

    useEffect(() => {
        if (dates !== undefined && dates.length > 0) {
            const today = dates.find(date => date.today)?.id || 1
            setDateRange(dates.filter(date => date.id >= today))
            setNewDate(dates.find(date => date.today === true).date)
            setNewHour(hours[0])
        }
    }, [dates])

    async function setMeeting(userId) {
        var _datetime=newDate+' '+newHour
        setError(null)
        setconfirm(false)
        setLoading(true)
        try {
            const response = await agent.Meetings.checkcalender({
                user_id: currentUser.id,
                datetime: _datetime,
            })
            if(response.status){
                toastr.error(messageLang("Meeting.meetingDashboard.meetingInventationCreate.checkcalender.currentUser"))
                setLoading(false)
            }
            else{
                const response = await agent.Meetings.checkcalender({
                    user_id: userId,
                    datetime: _datetime,
                })
                if(response.status){
                    toastr.error(messageLang("Meeting.meetingDashboard.meetingInventationCreate.checkcalender"))
                    setLoading(false)
                }
                else{
                    try {
                        const res = await agent.Meetings.create({
                            datetime: `${newDate} ${newHour}`,
                            description: `${msg}`,
                            audience_id: userId
                        })
                        setLoading(false)
                        handleClose()
                        toastr.success(messageLang("Meeting.meetingDashboard.meetingInventationCreate.send-alert"))
                    }
                    catch (err) {
                        setLoading(false)
                        setError(err)
                    }
                }
            }
        }
        catch (err) {
            setLoading(false)
            setError(err)
        }
    }

    const ContentData =
        <div className="invitation-box">
            <div className="invitation-box__time">
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="column"
                >
                    <div className="invitation-box__time__title"> <MessageLang id="events.LectureList.table.clock" /></div>
                    <select className={lang==='fa' ? 'fontnumber' :null}
                        onChange={(event) => setNewHour(event.target.value)}
                        value={newHour}
                    >
                    {dateRange && dateRange[0] && dateRange[0].date===newDate  ? 
                        hours && hours.map((item, index) => {
                            return (<option key={index} value={item}>{item}</option>)
                        })
                        :
                        list_hours && list_hours.map((item, index) => {
                            return (<option key={index} value={item}>{item}</option>)
                        })
                    }
                    </select>
                </Box>
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="column"
                >
                    <div className="invitation-box__time__title"><MessageLang id="Meeting.meetingDashboard.meetingInventation.selectedDate" /></div>
                    <select className={lang==='fa' ? 'fontnumber' :null}
                        onChange={(event) => setNewDate(event.target.value)}
                        value={newDate}>
                        {dateRange && dateRange.map((item, index) => {
                            return (<option key={index} value={item.date}> {`${item.format.day} ${item.format.month} ${item.format.year}`}  </option>)
                        })}
                    </select>
                </Box>
            </div>
            <div className="meeting-error">
                <p>{error && error.data && error.data.messages.length > 0 && error.data.messages[0]}</p>
            </div>
            <div>
                <p className='createmeeting_simplestyle'><MessageLang id="Meeting.meetingDashboard.meetingInventation.inventationText" /></p>
                <textarea
                    rows="4" cols="45"
                    type="input"
                    value={msg}
                    onChange={(event) => setMsg(event.target.value)}
                    className={'comment-send-input'}
                    placeholder={messageLang("form.label.placeholder.enterMeetingText")} />
            </div>
            <Box
                alignItems="center"
                display="flex"
                flexDirection="row"
            >
                <p className='createmeeting_simplestyle'><MessageLang id="Meeting.meetingDashboard.meetingInventation.boxPlace" /></p>
                <h4 className={'meeting-location'}><MessageLang id="Meeting.meetingDashboard.meetingInventation.boxContent" /></h4>
            </Box>
            <p className="invitation-box__heading createmeeting_simplestyle">
                <MessageLang id="Meeting.meetingDashboard.meetingInventation.boxHeading" />
            </p>
            <div className="invitation-box__items">
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="row"
                >
                    <Avatar src={audience.avatar}
                    />
                    <div className="invitation-box__items__name"> {audience.full_name} <br />
                        <span className={'invitation-box__items__name__status-pending'}>
                            <MessageLang id="Meeting.meetingDashboard.meetingInventation.statusPendingReceive" />
                        </span>
                    </div>
                </Box>
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="row"
                >
                    <p className='createmeeting_simplestyle'><MessageLang id="Meeting.meetingDashboard.meetingInventation.timeNotSet" /></p>
                </Box>
            </div>
            <div className="invitation-box__actions">
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="row"
                >
                    <Button variant="contained" className='bluebtn' onClick={() => {
                        if (msg === "") {
                            toastr.error(messageLang("Meeting.meetingDashboard.meetingInventation.sendInvitation.emptymsg.alert"))
                        }
                        else {
                            setconfirm(true)
                        }
                    }
                    }>
                        {loading ? <CircularProgress color='secondary' size={26} /> :
                            <MessageLang id="Meeting.meetingDashboard.meetingInventation.sendInvitation" />
                        }
                    </Button>
                </Box>
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="row">
                    <Button variant="outlined" color="primary" onClick={() => handleClose()}>
                        <MessageLang id="Meeting.meetingDashboard.meetingInventation.cancelInvitation" />
                    </Button>
                </Box>
            </div>
        </div >

    return (
        <>
            {mobileSize ?
                <GeneralDrawer
                    from="bottom"
                    onClose={() => handleClose()}
                    isOpen={open}
                    title={<MessageLang id="Meeting.meetingDashboard.meetingList.tableTitle" />}
                    subTitle={<MessageLang id="Meeting.meetingDashboard.meetingList.subtitle" />}>
                    {ContentData}
                </GeneralDrawer>
                :
                <Dialog
                    open={open}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={() => handleClose()}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle id="scroll-dialog-title" className="modal__header">
                        <Box
                            alignItems="flex-start"
                            display="flex"
                            flexDirection="column"
                        >
                            <div className="modal__header__title"
                            ><MessageLang id="Meeting.meetingDashboard.meetingList.tableTitle" /></div>
                            <div className="modal__header__sub-title"><MessageLang id="Meeting.meetingDashboard.meetingList.subtitle" /></div>
                        </Box>
                        <IconButton aria-label="close" onClick={() => handleClose()} className="modal__header__close-btn">
                            <CloseIcon />
                        </IconButton>
                    </DialogTitle>
                    <DialogContent>
                        {ContentData}
                    </DialogContent>
                </Dialog>
            }
            {confirm ?
                <ConfirmModal title={messageLang("Meeting.meetingDashboard.meetingInventation.send_confirm.title")}
                    agree={messageLang("Meeting.meetingDashboard.meetingInventation.send_confirm.agree")}
                    disagree={messageLang("Meeting.meetingDashboard.meetingInventation.send_confirm.disagree")}
                    close={close} accept={() => setMeeting(audience.id)} />
                :
                null}
        </>
    )
}

export default MeetingInvitationCreate
