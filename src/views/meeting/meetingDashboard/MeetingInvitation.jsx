import React, { useState } from 'react';
import {
    Avatar,
    Box,
    Button,
    CircularProgress,
    Link
} from '@material-ui/core';

import {
    fetchRanges
} from '../../../app/store/actions/meetingActions';

import { useSelector, useDispatch } from 'react-redux';
import ConfirmModal from '../../../component/ConfirmModal/ConfirmModal'

/*************change lang ***************/
import MessageLang, { messageLang } from '../../../lang/';
/*************change lang ***************/

const MeetingInvitation = ({ onClose, selectedMeeting, changeStaus, isLoading, isLoadingAccept, handleChangeTime }) => {
    const [changeTime, setChangeTime] = useState(false);
    const [loading, setLoading] = useState(false);
    const [loadingAccept, setLoadingAccept] = useState(false);
    const [newDate, setNewDate] = useState('');
    const [newHour, setNewHour] = useState("07:00");
    const [acceptconfirm, setacceptconfirm] = useState(false);
    const [rejectconfirm, setrejectconfirm] = useState(false);
    const [changetimeconfirm, setchangetimeconfirm] = useState(false);
    const {
        dates: ranges
    } = useSelector((state) => state.persist);
    const userId = useSelector((state) => state.auth.currentUser.id);
    const dispatch = useDispatch();
    React.useEffect(() => {
        dispatch(fetchRanges())
    }, []);
    React.useEffect(() => {
        setLoading(isLoading)
    }, [isLoading]);
    React.useEffect(() => {
        setLoadingAccept(isLoadingAccept)
    }, [isLoadingAccept]);

    React.useEffect(() => {
        if (ranges && ranges.length > 0) {
            setNewDate(ranges[0].date)
        }
    }, [ranges]);

    const handleClick = (url) => {
        const win = window.open(url, "_blank");
        win.focus();
    }

    // console.log(selectedMeeting)

    const getMonth = (month) => {
        if (month == 1) {
            return <MessageLang id="Meeting.meetingDashboard.meetingInventation.month1" />;
        }
        else if (month == 2) {
            return <MessageLang id="Meeting.meetingDashboard.meetingInventation.month2" />;
        }
        else if (month == 3) {
            return <MessageLang id="Meeting.meetingDashboard.meetingInventation.month3" />;
        }
        else if (month == 4) {
            return <MessageLang id="Meeting.meetingDashboard.meetingInventation.month4" />;
        }
        else if (month == 5) {
            return <MessageLang id="Meeting.meetingDashboard.meetingInventation.month5" />;
        }
        else if (month == 6) {
            return <MessageLang id="Meeting.meetingDashboard.meetingInventation.month6" />;
        }
        else if (month == 7) {
            return <MessageLang id="Meeting.meetingDashboard.meetingInventation.month7" />;
        }
        else if (month == 8) {
            return <MessageLang id="Meeting.meetingDashboard.meetingInventation.month8" />;
        }
        else if (month == 9) {
            return <MessageLang id="Meeting.meetingDashboard.meetingInventation.month9" />;
        }
        else if (month == 10) {
            return <MessageLang id="Meeting.meetingDashboard.meetingInventation.month10" />;
        } else {
            return <MessageLang id="Meeting.meetingDashboard.meetingInventation.month11" />;
        }

    }

    let mettingdate = "";
    let day = "";
    let month = '';
    let status = <MessageLang id="Meeting.meetingDashboard.meetingInventation.status" />;
    if (selectedMeeting) {
        mettingdate = selectedMeeting.datetime.date;
        month = getMonth(parseInt(mettingdate.slice(5, 7), 10));
        day = mettingdate.slice(8, 10);

        if (selectedMeeting.status === 'pending') {
            status = <MessageLang id="Meeting.meetingDashboard.meetingInventation.selectedPending" />;
        } else if (selectedMeeting.status === 'rejected') {
            status = <MessageLang id="Meeting.meetingDashboard.meetingInventation.selectedRejected" />;
        }
        else if (selectedMeeting.status === 'accepted') {
            status = <MessageLang id="Meeting.meetingDashboard.meetingInventation.selectedAccepted" />;
        }
    }

    const Acceptclose = () => {
        setacceptconfirm(false)
    }
    const Acceptaccept = () => {
        changeStaus('accepted', selectedMeeting.id)
        setacceptconfirm(false)
        setacceptconfirm(false)
    }

    const Rejectclose = () => {
        setrejectconfirm(false)
    }
    const Rejectaccept = () => {
        changeStaus('rejected', selectedMeeting.id)
        setrejectconfirm(false)
    }

    const Changeclose = () => {
        setchangetimeconfirm(false)
    }
    const Changeaccept = () => {
        handleChangeTime(`${newDate} ${newHour}`, selectedMeeting.id)
        setchangetimeconfirm(false)
    }

    const hours = ["00:00", "00:30",
        "01:00", "01:30",
        "02:00", "02:30",
        "03:00", "03:30",
        "04:00", "04:30",
        "05:00", "05:30",
        "06:00", "06:30",
        "07:00", "07:30",
        "08:00", "08:30",
        "09:00", "09:30",
        "10:00", "10:30",
        "11:00", "11:30",
        "12:00", "12:30",
        "13:00", "13:30",
        "14:00", "14:30",
        "15:00", "15:30",
        "16:00", "16:30",
        "17:00", "17:30",
        "18:00", "18:30",
        "19:00", "19:30",
        "20:00", "20:30",
        "21:00", "21:30",
        "22:00", "22:30",
        "23:00", "23:30"]
    return (
        <div className="invitation-box" >
            <div className="invitation-box__time" >
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="column"
                >
                    <div className="invitation-box__time__title">
                        <MessageLang id="events.LectureList.table.clock" /></div>
                    {
                        changeTime ? <select
                            onChange={(event) => setNewHour(event.target.value)}
                            value={newHour}
                        >
                            {
                                hours.map((item, index) => {
                                    return (
                                        <option key={index} value={item}>{item}</option>
                                    )
                                })
                            }
                        </select> : <div className="invitation-box__time__sub-title">{(selectedMeeting && selectedMeeting.datetime) ? selectedMeeting.datetime.time : <MessageLang id="Meeting.meetingDashboard.meetingInventation.selectedMeeting" />}</div>
                    }
                </Box>
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="column"
                >
                    <div className="invitation-box__time__title"><MessageLang id="Meeting.meetingDashboard.meetingInventation.selectedDate" /></div>
                    {
                        changeTime ? (<div>
                            <select
                                onChange={(event) => setNewDate(event.target.value)}
                                value={newDate}>
                                {
                                    ranges.map((item, index) => {
                                        return (
                                            <option key={index} value={item.date}>{item.format.year} {item.format.month} {item.format.day}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>) : <div className="invitation-box__time__sub-title">{`${selectedMeeting.datetime.format.day}`} {selectedMeeting.datetime.format.month}</div>
                    }
                </Box>
            </div>
            {
                !changeTime && (
                    <p className='createmeeting_simplestyle'>
                        <MessageLang id="Meeting.meetingDashboard.meetingInventation.inventationText" />
                    </p>
                )
            }
            {
                !changeTime && (
                    <div className="invitation-box__description">
                        {
                            selectedMeeting && selectedMeeting.description
                        }
                    </div>
                )
            }

            <Box
                alignItems="center"
                display="flex"
                flexDirection="row"
                marginBottom='15px'
            >
                <p className='createmeeting_simplestyle'><MessageLang id="Meeting.meetingDashboard.meetingInventation.boxPlace" /></p>
                <h3><MessageLang id="Meeting.meetingDashboard.meetingInventation.boxContent" /></h3>
            </Box>
            {/* <p className="invitation-box__heading">
                <MessageLang id="Meeting.meetingDashboard.meetingInventation.boxHeading" />
            </p> */}
            {/* <div className="invitation-box__items">
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="row"
                >
                    <Avatar src='/static/images/avatars/user.png' />
                    <div className="invitation-box__items__name"> {selectedMeeting && selectedMeeting.participants.to.full_name} <br />
                        <span className={`invitation-box__items__name${(selectedMeeting && selectedMeeting.status === 'rejected') ? '__status-rejected' : '__status-accepted'}`}> {status}</span>
                    </div>
                </Box>
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="row"
                >
                    <p>{`${day} ${month}`}</p>
                    <p>{selectedMeeting ? selectedMeeting.datetime.time : <MessageLang id="Meeting.meetingDashboard.meetingInventation.selectedMeeting" />}</p>
                </Box>
            </div> */}
            <div className="invitation-box__items">
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="row"
                >
                    <Avatar src={selectedMeeting && selectedMeeting.audience.avatar} />
                    <div className="invitation-box__items__name"> {selectedMeeting && selectedMeeting.audience.full_name} <br />
                        <span className={`invitation-box__items__name${(selectedMeeting && selectedMeeting.status === 'rejected') ? '__status-rejected' : selectedMeeting.status === 'pending' ?'__status-invitor' : '__status-accepted'}`}> {status}</span>
                    </div>
                </Box>
                <Box
                    alignItems="center"
                    display="flex"
                    flexDirection="row"
                >
                    <p className='invitation-box__date createmeeting_simplestyle'>{`${day}`} {month}</p>
                    <p className='createmeeting_simplestyle'>{selectedMeeting ? selectedMeeting.datetime.time : <MessageLang id="Meeting.meetingDashboard.meetingInventation.selectedMeeting" />}</p>
                </Box>
            </div>
            {(selectedMeeting && selectedMeeting.status === 'pending') ?
                <div className="invitation-box__actions">
                    {selectedMeeting.inbox &&
                        <>
                            <Button variant="contained" className={changeTime ? 'bluebtn' : 'accptbtn' } onClick={() => {
                                if (changeTime) {
                                    // handleChangeTime(`${newDate} ${newHour}`, selectedMeeting.id)
                                    setchangetimeconfirm(true)
                                }
                                else {
                                    // changeStaus('accepted', selectedMeeting.id)
                                    setacceptconfirm(true)
                                }

                            }}>
                                {loadingAccept ? <CircularProgress color='secondary' size={26} /> :
                                    !changeTime ? <MessageLang id="Meeting.meetingDashboard.meetingInventation.loadingAccepttwo" /> : <MessageLang id="Meeting.meetingDashboard.meetingInventation.loadingAccept" />
                                }
                            </Button>
                            <Button variant="outlined" color="primary"
                                onClick={() => {
                                    if (changeTime) {
                                        onClose()
                                    }
                                    else {
                                        setChangeTime(true)
                                    }
                                }}>
                                {
                                    changeTime ?
                                        <MessageLang id="Meeting.meetingDashboard.meetingInventation.changeTimetwo" />
                                        :
                                        <MessageLang id="Meeting.meetingDashboard.meetingInventation.changeTime" />
                                }

                            </Button>

                            {
                                !changeTime && (
                                    <Button
                                        className='rejectbtn'
                                        variant="contained"
                                        color="secondary"
                                        onClick={() => {
                                            // changeStaus('rejected', selectedMeeting.id)
                                            setrejectconfirm(true)
                                        }}>
                                        {loading && <CircularProgress color='secondary' size={26} />}
                                        {!loading && <MessageLang id="Meeting.meetingDashboard.meetingInventation.changetimeReject" />}

                                    </Button>
                                )
                            }
                        </>}
                </div> :

                (selectedMeeting.status === 'accepted' ?
                    < div className={'meeting_link'} >
                        <Button
                            className='bluebtn'
                            variant="contained"
                            color="secondary"
                            href="#contained-buttons"
                            onClick={() => handleClick(selectedMeeting.room_url)}>
                            <MessageLang id="Meeting.meetingDashboard.meetingInventation.link" />
                        </Button>
                    </div> : '')
            }

            {acceptconfirm ?
                <ConfirmModal title={messageLang("Meeting.meetingDashboard.meetingInventation.accept_confirm.title")}
                    agree={messageLang("Meeting.meetingDashboard.meetingInventation.accept_confirm.agree")}
                    disagree={messageLang("Meeting.meetingDashboard.meetingInventation.accept_confirm.disagree")}
                    close={Acceptclose}
                    accept={Acceptaccept}
                />
                : null}
            {rejectconfirm ?
                <ConfirmModal title={messageLang("Meeting.meetingDashboard.meetingInventation.reject_confirm.title")}
                    agree={messageLang("Meeting.meetingDashboard.meetingInventation.reject_confirm.agree")}
                    disagree={messageLang("Meeting.meetingDashboard.meetingInventation.reject_confirm.disagree")}
                    close={Rejectclose}
                    accept={Rejectaccept}
                />
                : null}
            {changetimeconfirm ?
                <ConfirmModal title={messageLang("Meeting.meetingDashboard.meetingInventation.changetime_confirm.title")}
                    agree={messageLang("Meeting.meetingDashboard.meetingInventation.changetime_confirm.agree")}
                    disagree={messageLang("Meeting.meetingDashboard.meetingInventation.changetime_confirm.disagree")}
                    close={Changeclose}
                    accept={Changeaccept}
                />
                : null}
        </div >
    )
}

export default MeetingInvitation
