import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Paper, CardHeader } from '@material-ui/core'
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    card: {
        maxWidth:345,
        margin: theme.spacing(2),
    },
    media: {
        height: 190,
    },
}));

const EventListSkeleton = () => {
    const classes = useStyles();

    return (
        <TableContainer component={Paper} >
            <Table className={classes.table} aria-label="simple table">
                <TableBody>
                    <TableRow >
                        <TableCell align="center" component="th" scope="row" width="9%">
                            <CardHeader
                                title={<Skeleton animation="wave" height={15} width="50%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="50%" />}
                            />
                        </TableCell >
                        <TableCell align="left" width="91%" className={classes.avatar_cell} >
                            <CardHeader
                                avatar={<Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />}
                                title={<Skeleton animation="wave" height={15} width="20%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="20%" />}
                            />
                        </TableCell>
                    </TableRow>
                    <TableRow >
                        <TableCell align="center" component="th" scope="row" width="9%">
                            <CardHeader
                                title={<Skeleton animation="wave" height={15} width="50%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="50%" />}
                            />
                        </TableCell >
                        <TableCell align="left" width="91%" className={classes.avatar_cell} >
                            <CardHeader
                                avatar={<Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />}
                                title={<Skeleton animation="wave" height={15} width="20%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="20%" />}
                            />
                        </TableCell>
                    </TableRow>
                    <TableRow >
                        <TableCell align="center" component="th" scope="row" width="9%">
                            <CardHeader
                                title={<Skeleton animation="wave" height={15} width="50%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="50%" />}
                            />
                        </TableCell >
                        <TableCell align="left" width="91%" className={classes.avatar_cell} >
                            <CardHeader
                                avatar={<Skeleton style={{ marginRight: '10px' }} animation="wave" variant="circle" width={40} height={40} />}
                                title={<Skeleton animation="wave" height={15} width="20%" style={{ marginBottom: 6 }} />}
                                subheader={<Skeleton animation="wave" height={15} width="20%" />}
                            />
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default EventListSkeleton
