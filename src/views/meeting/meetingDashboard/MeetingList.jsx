import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Table, TableBody, TableCell, TableContainer, TableRow, Paper, Avatar, CardHeader, Box
} from '@material-ui/core'
import {
    Modal,
    RecieveIcon,
    SendIcon
} from '../../../component';
import MeetingInvitation from './MeetingInvitation';

import {
    fetchMeetingDetail,
    changeStaus,
    changeDate,
} from '../../../app/store/actions/meetingActions';
import { useSelector, useDispatch } from 'react-redux';
import Loading from '../../../component/Loading/Loading';
import Chip from '@material-ui/core/Chip';
import DoneIcon from '@material-ui/icons/Done';
import HistoryIcon from '@material-ui/icons/History'
import CancelIcon from '@material-ui/icons/Cancel'
import GeneralDrawer from '../../../component/Drawer/GeneralDrawer.jsx'

/*************change lang ***************/
import MessageLang, { meesageLang, messageLang } from '../../../lang/';
/*************change lang ***************/

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTypography-h5': {
            fontSize: '12px',
            paddingBottom: '5px'
        }
    },
    table: {
    },
    avatar_cell: {
        backgroundColor: theme.palette.primary.lightGray,
        borderBottom: '1px solid #fff',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    accepted: {
        color: '#fff',
        backgroundColor: '#1976d2',
        marginRight: '25px',
    },
    chip: {
        fontSize: '10px',
    }
}));

const EventList = ({ meetings, reloadList }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [open, setOpen] = React.useState(false);
    const [loading, setLoading] = useState(false);
    const [loadingAccept, setLoadingAccept] = useState(false);

    const mobileSize = window.innerWidth < 600 ? true : false

    const {
        selectedMeeting,
        loadingSelectedMeeting
    } = useSelector((state) => state.meeting);

    async function handleChangeMeeting(status, id) {
        if (status === 'rejected') {
            setLoading(true);
        }
        else {
            setLoadingAccept(true)
        }

        try {
            await dispatch(changeStaus(status, id))
            await dispatch(fetchMeetingDetail(id));
        } catch (error) {
            console.log(error.message);
        } finally {
            setLoading(false);
            setLoadingAccept(false)
        }
    }


    async function handleChangeTime(date, id) {
        setLoadingAccept(true);
        try {
            await dispatch(changeDate(date, id))
            await dispatch(fetchMeetingDetail(id));
            await reloadList()
        } catch (error) {
            console.log(error.message);
        } finally {
            setLoading(false);
            setLoadingAccept(false)
        }
    }

    function renderSwitch(row) {
        switch (row.status) {
            case 'pending':
                return <Chip
                    label={messageLang('Meeting.alert.pending')}
                    clickable
                    className={`${classes.chip} pending`}
                    color="primary"
                    onDelete={() => console.log()}
                    deleteIcon={<HistoryIcon />}
                />;
            case 'rejected':
                return <Chip
                    label={messageLang('Meeting.alert.rejected')}
                    clickable
                    className={`${classes.chip} rejected`}
                    onDelete={() => console.log()}
                    deleteIcon={<CancelIcon />}
                />;
            case 'accepted':
                return <Chip
                    label={messageLang('Meeting.alert.accepted')}
                    clickable
                    className={`${classes.chip} accepted`}
                    onDelete={() => console.log()}
                    deleteIcon={<DoneIcon />}
                />;
            default:
                return <></>;
        }
    }

    return (
        <TableContainer component={Paper} className={classes.root}>
            {mobileSize && open ?
                <GeneralDrawer
                    from="bottom"

                    onClose={() => setOpen(false)}
                    isOpen={open}
                    title={<MessageLang id="Meeting.meetingDashboard.meetingList.tableTitle" />}
                    subTitle={<MessageLang id="Meeting.meetingDashboard.meetingList.subtitle" />}>
                    {
                        loadingSelectedMeeting ? <Loading /> : <MeetingInvitation
                            selectedMeeting={selectedMeeting}
                            isLoading={loading}
                            isLoadingAccept={loadingAccept}
                            changeStaus={(status, id) => {
                                handleChangeMeeting(status, id)

                            }}
                            handleChangeTime={(date, id) => {
                                handleChangeTime(date, id)
                            }}
                            onClose={() => {
                                setOpen(false)
                            }} />
                    }
                </GeneralDrawer>
                :
                <Modal
                    onClose={() => {
                        setOpen(false)
                    }}
                    isOpen={open}
                    title={<MessageLang id="Meeting.meetingDashboard.meetingList.tableTitle" />}
                    subTitle={<MessageLang id="Meeting.meetingDashboard.meetingList.subtitle" />}
                >
                    {
                        loadingSelectedMeeting ? <Loading /> : <MeetingInvitation
                            selectedMeeting={selectedMeeting}
                            isLoading={loading}
                            isLoadingAccept={loadingAccept}
                            changeStaus={(status, id) => {
                                handleChangeMeeting(status, id)
                            }}
                            handleChangeTime={(date, id) => {
                                handleChangeTime(date, id)
                            }}
                            onClose={() => {
                                setOpen(false)
                            }} />
                    }
                </Modal>
            }
            <Table className={classes.table} aria-label="simple table">
                <TableBody>
                    {meetings && meetings.map((row, index) => {
                        return (
                            <TableRow key={index} className='meetingrow'
                                onClick={() => {
                                    dispatch(fetchMeetingDetail(row.id));
                                    setOpen(true)
                                }}
                            >
                                <TableCell align="center" component="th" scope="row" className={'meetingrow__dateside'}>
                                    <CardHeader
                                        title={`${row.datetime.format.day} ${row.datetime.format.month}`}
                                        subheader={row.datetime && row.datetime.time}
                                        className={'meetingrow__date-time'}
                                    />
                                </TableCell >
                                <TableCell align="left" width="auto" className={classes.avatar_cell} >
                                    <CardHeader
                                        className={'meetingrow__data'}
                                        avatar={
                                            <div className="meeting-avatar">
                                                <Avatar alt="" src={row.audience.avatar} width={30} height={30} />
                                                {
                                                    row.inbox ? (
                                                        <div className="meeting-avatar__icon">
                                                            <div className="meeting-avatar__icon__send">
                                                                <RecieveIcon />
                                                            </div>
                                                        </div>
                                                    ) :
                                                        <div className="meeting-avatar__icon">
                                                            <div className="meeting-avatar__icon__recieve">
                                                                <SendIcon />
                                                            </div>
                                                        </div>
                                                }
                                            </div>
                                        }
                                        title={`${row.audience.full_name}`}
                                        subheader={
                                            (row.audience.company_name ? row.audience.company_name : '') + ' - ' +
                                            (row.audience.job_title ? row.audience.job_title : '')
                                        }
                                    />
                                    {renderSwitch(row)}
                                </TableCell>
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default EventList

